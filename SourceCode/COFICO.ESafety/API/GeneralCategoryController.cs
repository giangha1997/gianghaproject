﻿using System.Collections.Generic;
using System.Web.Http;
using COFICO.ESafety.DAL.DBModel;
using COFICO.ESafety.DAL.Services;

namespace COFICO.ESafety
{
    //[Authorize]
    [RoutePrefix("api/GeneralCategory")]
    public class GeneralCategoryController : ApiController
    {
        [HttpGet, Route("GetRiskLevel")]
        public IHttpActionResult GetRiskLevel()
        {
            var items = new List<GeneralCategory>();
            using (var svc = new GeneralCategoryService())
            {
                items = svc.GetList(DAL.Constants.GeneralCateType.RiskLevel);
            }
            return Ok(new { items });
        }

        [HttpGet, Route("GetSafetyStatus")]
        public IHttpActionResult GetSafetyStatus()
        {
            var items = new List<GeneralCategory>();
            using (var svc = new GeneralCategoryService())
            {
                items = svc.GetList(DAL.Constants.GeneralCateType.SafetyStatus);
            }
            return Ok(new { items });
        }
    }
}