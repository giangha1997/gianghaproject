﻿using COFICO.ESafety.AppCode;
using COFICO.ESafety.DAL.DBModel;
using COFICO.ESafety.DAL.Services;
using System.Collections.Generic;
using System.Web.Http;

namespace COFICO.ESafety
{
    //[Authorize]
    [RoutePrefix("api/ProjectTowerFacZone")]
    public class ProjectTowerFacZoneController : ApiController
    {
        [HttpGet, Route("items/{projectID}/{parentID}")]
        public IHttpActionResult Items(int projectID, int? parentID)
        {
            var items = new List<Project_TowerFacZone>();
            using (var svc = new ProjectTowerFacZoneService())
            {
                items = svc.GetList(projectID, parentID);
            }
            return Ok(new { items });
        }

        /// <summary>
        /// Insert/update a TowerFacZone of a project
        /// </summary>
        /// <returns>null: error or empty; -1: exist Name; > 0: success</returns>
        [HttpPut]
        public IHttpActionResult Put(Project_TowerFacZone item)
        {
            var items = new Response<object>
            {
                IsError = true,
            };

            int? rs = null;
            using (var svc = new ProjectTowerFacZoneService())
            {
                rs = svc.Update(item);
                if (rs != null)
                {
                    if (rs == -1)
                    {
                        items.IsDuplicate = true;
                        items.Messages.Add(DAL.Constants.ErrorText.ItemExists);
                    }
                    else if (rs > 0)
                    {
                        items.IsError = false;
                        items.Id = item.ID;
                    }
                }
            }
            return Ok(new { items });
        }

        [HttpDelete, Route("Delete/{projectID}/{id}")]
        public IHttpActionResult Delete(int projectID, int id)
        {
            var items = new Response<object>
            {
                IsError = true,
            };

            int? rs = null;
            using (var svc = new ProjectTowerFacZoneService())
            {
                rs = svc.Delete(projectID, id);
                if (rs != null)
                {
                    if (rs == -1)
                    {
                        items.Result = rs;
                        items.Messages.Add(DAL.Constants.ErrorText.ItemInUse);
                    }
                    else if (rs > 0)
                    {
                        items.IsError = false;
                    }
                }
            }
            return Ok(new { items });
        }
    }
}