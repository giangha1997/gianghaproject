﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace COFICO.ESafety.DAL.DBModel
{
    [Table("Project_Contractor")]
    public class Project_Contractor
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int RowID { get; set; }

        public int ProjectID { get; set; }

        public int ContractorID { get; set; }

        [JsonIgnore]
        public DateTime Created { get; set; }

        [JsonIgnore]
        [StringLength(50)]
        public string CreatedBy { get; set; }

        [JsonIgnore]
        public DateTime? Modified { get; set; }

        [JsonIgnore]
        [StringLength(50)]
        public string ModifiedBy { get; set; }

        [JsonIgnore]
        public bool Deleted { get; set; }
    }
}
