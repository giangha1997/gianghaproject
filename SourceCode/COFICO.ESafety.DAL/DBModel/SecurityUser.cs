﻿using System;
using System.ComponentModel.DataAnnotations;

namespace COFICO.ESafety.DAL.DBModel
{
    public partial class SecurityUser
    {
        [Key]
        public int RowId { get; set; }

        public int? SecurityGroupId { get; set; }

        [Required]
        [StringLength(100)]
        public string SPUser { get; set; }

        [Required]
        [StringLength(50)]
        public string UserName { get; set; }

        [StringLength(255)]
        public string FullName { get; set; }

        [StringLength(255)]
        public string Note { get; set; }

        public bool AllowInPast { get; set; }

        public DateTime? Created { get; set; }

        public DateTime? AllowedDate { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime? Modified { get; set; }

        [StringLength(50)]
        public string ModifiedBy { get; set; }

        public bool? Deleted { get; set; }
    }
}
