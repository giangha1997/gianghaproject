﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace COFICO.ESafety.DAL.DBModel
{
    [Table("ImageCategory")]
    public class ImageCategory
    {
        [Key]
        [Column("ImageCategoryID")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [StringLength(50)]
        public string CategoryCode { get; set; }

        [StringLength(255)]
        public string CategoryName { get; set; }

        public int? ParentID { get; set; }

        public bool? IsParent { get; set; }

        public bool IsActive { get; set; }

        public int? SortOrder { get; set; }

        public DateTime Created { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime? Modified { get; set; }

        [StringLength(50)]
        public string ModifiedBy { get; set; }

        public bool Deleted { get; set; }
    }
}
