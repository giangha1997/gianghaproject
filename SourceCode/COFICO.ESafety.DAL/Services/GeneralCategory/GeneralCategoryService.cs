﻿using System;
using System.Collections.Generic;
using System.Linq;
using COFICO.ESafety.Base;
using COFICO.ESafety.DAL.DBModel;

namespace COFICO.ESafety.DAL.Services
{
    public class GeneralCategoryService : GenericService<GeneralCategory>
    {
        public List<GeneralCategory> GetList()
        {
            try
            {
                return _table.Where(x => !x.Deleted && x.IsActive).OrderBy(x => x.SortOrder).ToList();
            }
            catch (Exception ex)
            {
                Logger.Error("GeneralCategory", ex);
                throw ex;
            }
        }

        public List<GeneralCategory> GetList(Constants.GeneralCateType type)
        {
            try
            {
                return _table.Where(x => !x.Deleted && x.IsActive && x.Type == (byte)type).OrderBy(x => x.SortOrder).ToList();
            }
            catch (Exception ex)
            {
                Logger.Error("GeneralCategory", ex);
                throw ex;
            }
        }
    }
}
