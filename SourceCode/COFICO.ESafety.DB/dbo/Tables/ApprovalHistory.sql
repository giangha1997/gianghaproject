﻿CREATE TABLE [dbo].[ApprovalHistory] (
    [Id]          INT             IDENTITY (1, 1) NOT NULL,
    [RequestId]   INT             NOT NULL,
    [Type]        TINYINT         NOT NULL,
    [Approver]    NVARCHAR (100)  NULL,
    [Username]    NVARCHAR (100)  NULL,
    [Name]        NVARCHAR (100)  NULL,
    [Level]       NVARCHAR (255)  NULL,
    [Action]      NVARCHAR (255)  NULL,
    [ActionValue] NVARCHAR (1000) NULL,
    [Note]        NVARCHAR (1000) NULL,
    [Created]     DATETIME        NULL,
    [CreatedBy]   NVARCHAR (255)  NULL,
    CONSTRAINT [PK_ApprovalHistory] PRIMARY KEY CLUSTERED ([Id] ASC)
);

