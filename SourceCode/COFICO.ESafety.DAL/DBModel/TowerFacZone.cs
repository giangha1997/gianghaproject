﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace COFICO.ESafety.DAL.DBModel
{
    [Table("TowerFacZone")]
    public class TowerFacZone
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [MaxLength(50)]
        [StringLength(50)]
        public string Code { get; set; }

        [MaxLength(255)]
        [StringLength(255)]
        public string Name { get; set; }

        public byte? Type { get; set; }

        public int? ParentID { get; set; }

        public bool IsParent { get; set; }

        [MaxLength(1000)]
        [StringLength(1000)]
        public string Note { get; set; }

        public bool IsActive { get; set; }

        public int? SortOrder { get; set; }

        public DateTime Created { get; set; }

        [MaxLength(50)]
        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime? Modified { get; set; }

        [MaxLength(50)]
        [StringLength(50)]
        public string ModifiedBy { get; set; }

        public bool Deleted { get; set; }

    }
}
