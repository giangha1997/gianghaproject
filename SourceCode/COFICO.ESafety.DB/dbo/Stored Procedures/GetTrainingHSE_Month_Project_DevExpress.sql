﻿-- =============================================
-- Author:		HLinh
-- Create date: 2018/07/20
-- Description:	Report 03 - Thống kê Huấn luyện an toàn trong tháng/quý/năm theo dự án
-- @ShowAllProject = 0: chỉ lấy những dự án có tình trạng = Đang thực hiện & Đang bảo trì
-- @ShowAllProject = 1: lấy tất cả những dự án bao gồm tình trạng = Kết thúc
-- =============================================
CREATE PROCEDURE [dbo].[GetTrainingHSE_Month_Project_DevExpress] 
	@FromDate SMALLDATETIME,
	@ToDate SMALLDATETIME,
	@FromPrevDate SMALLDATETIME,
	@ToPrevDate SMALLDATETIME,
	@ShowAllProject BIT
AS
BEGIN
	SET NOCOUNT ON;

	CREATE TABLE #Tmp
	(
		TrainingCategoryID INT,
		TrainingCode NVARCHAR(50),
		TrainingName NVARCHAR(255),
		ProjectID INT,
		ProjectCode NVARCHAR(50),
		ProjectName NVARCHAR(255),
		NumOfEmployees DECIMAL(9,2),
		IsThisWeek BIT,
		ThisWeek DECIMAL(9,2),
		LastWeek DECIMAL(9,2),
		Total DECIMAL(9,2)
	)

	SELECT TOP 0 * INTO #Remaining FROM #Tmp

	--thống kê Huấn luyện an toàn
	INSERT #Tmp (TrainingCategoryID, ProjectID, ProjectCode, ProjectName, NumOfEmployees, IsThisWeek)
		SELECT d.TrainingCategoryID, t.ProjectID, ProjectCode, ProjectName, SUM(NumOfEmployees) AS NumOfEmployees, CAST(CASE WHEN d.[Date] >= @FromDate AND d.[Date] <= @ToDate THEN 1 ELSE 0 END AS BIT) AS IsThisWeek
		FROM Training t 
			INNER JOIN TrainingDetail d ON (t.TrainingID = d.TrainingID AND t.Deleted = 0 AND d.Deleted = 0)
			INNER JOIN Project p ON (t.ProjectID = p.ProjectID AND p.Deleted = 0 AND ((@ShowAllProject = 0 AND (p.StatusID = 1 OR p.StatusID = 2)) OR @ShowAllProject = 1) )
			INNER JOIN TrainingCategory c ON (c.TrainingCategoryID = d.TrainingCategoryID AND c.Deleted = 0 AND c.IsActive = 1)
		WHERE ( d.[Date] BETWEEN @FromDate AND @ToDate OR d.[Date] BETWEEN @FromPrevDate AND @ToPrevDate )
		GROUP BY d.TrainingCategoryID, t.ProjectID, ProjectCode, ProjectName, CAST(CASE WHEN d.[Date] >= @FromDate AND d.[Date] <= @ToDate THEN 1 ELSE 0 END AS BIT)
		
	----get remainings
	--lấy tất cả các dự án chưa có dữ liệu
	--DECLARE @TrainingCategoryID INT

	--DECLARE cursorA CURSOR FOR
	--	SELECT DISTINCT TrainingCategoryID FROM #Tmp
	--	UNION
	--	SELECT TrainingCategoryID FROM TrainingCategory WHERE Deleted = 0 AND IsActive = 1
	--OPEN cursorA
	--FETCH NEXT FROM cursorA INTO @TrainingCategoryID
	--WHILE @@FETCH_STATUS = 0
	--BEGIN
	--	INSERT INTO #Remaining(TrainingCategoryID, ProjectID, ProjectCode, ProjectName, IsThisWeek)
	--		SELECT	@TrainingCategoryID, ProjectID, ProjectCode, ProjectName, 1
	--		FROM Project c
	--		WHERE c.Deleted = 0 AND (StatusID = 1 OR StatusID = 2)
	--			  AND NOT EXISTS (SELECT TOP 1 1 FROM #Tmp t1 WHERE c.ProjectID = t1.ProjectID AND t1.TrainingCategoryID = @TrainingCategoryID AND t1.IsThisWeek = 1)

	--	FETCH NEXT FROM cursorA INTO @TrainingCategoryID
	--END

	--CLOSE cursorA
	--DEALLOCATE cursorA
	--lấy tất cả các dự án chưa có dữ liệu - end
	

	--bổ sung những dự án còn thiếu và những danh mục còn thiếu
	DECLARE @ProjectID INT
	DECLARE @ProjectCode NVARCHAR(50)
	DECLARE @ProjectName NVARCHAR(255)

	DECLARE cursorA CURSOR FOR
		SELECT DISTINCT ProjectID, ProjectCode, ProjectName FROM #Tmp
	OPEN cursorA
	FETCH NEXT FROM cursorA INTO @ProjectID, @projectCode, @ProjectName
	WHILE @@FETCH_STATUS = 0
	BEGIN
		INSERT INTO #Remaining (TrainingCategoryID, ProjectID, ProjectCode, ProjectName, IsThisWeek)
			SELECT	TrainingCategoryID, @ProjectID, @ProjectCode, @ProjectName, 1
			FROM TrainingCategory c
			WHERE Deleted = 0 AND IsActive = 1
				  AND NOT EXISTS (SELECT TOP 1 1 FROM #Tmp t1 WHERE t1.ProjectID = @ProjectID AND t1.TrainingCategoryID = c.TrainingCategoryID AND t1.IsThisWeek = 1)

		FETCH NEXT FROM cursorA INTO @ProjectID, @projectCode, @ProjectName
	END
	
	CLOSE cursorA
	DEALLOCATE cursorA
	

	--merge data
	INSERT INTO #Tmp(TrainingCategoryID, ProjectID, ProjectCode, ProjectName, NumOfEmployees, IsThisWeek)
		SELECT TrainingCategoryID, ProjectID, ProjectCode, ProjectName, NumOfEmployees, IsThisWeek 
		FROM #Remaining

	--update ThisWeek
	UPDATE #Tmp
	SET ThisWeek = (SELECT SUM(ISNULL(NumOfEmployees, 0)) FROM #Tmp t WHERE t.TrainingCategoryID = #Tmp.TrainingCategoryID AND t.IsThisWeek = 1)

	UPDATE #Tmp SET ThisWeek = NULL WHERE ThisWeek = 0

	--update LastWeek
	UPDATE #Tmp
	SET LastWeek = (SELECT SUM(ISNULL(NumOfEmployees, 0)) FROM #Tmp t WHERE t.TrainingCategoryID = #Tmp.TrainingCategoryID AND t.IsThisWeek = 0)

	UPDATE #Tmp SET LastWeek = NULL WHERE LastWeek = 0

	--update Total
	UPDATE #Tmp
	SET Total = ISNULL(ThisWeek, 0) + ISNULL(LastWeek, 0)

	UPDATE #Tmp SET Total = NULL WHERE Total = 0


	--show data
	SELECT a1.TrainingCategoryID, c.TrainingCode, c.TrainingName, ProjectID, ProjectCode, ProjectName, NumOfEmployees, ThisWeek, LastWeek, Total, SortOrder
	FROM #Tmp a1
		INNER JOIN TrainingCategory c ON (c.TrainingCategoryID = a1.TrainingCategoryID)
	WHERE IsThisWeek = 1
	ORDER BY c.SortOrder, c.TrainingName, ProjectName

	DROP TABLE #Remaining
END