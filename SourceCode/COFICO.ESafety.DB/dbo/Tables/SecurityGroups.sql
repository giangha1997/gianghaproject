﻿CREATE TABLE [dbo].[SecurityGroups] (
    [RowId]          INT            IDENTITY (1, 1) NOT NULL,
    [Code]           NVARCHAR (25)  NULL,
    [Name]           NVARCHAR (255) NOT NULL,
    [ShowAllProject] BIT            CONSTRAINT [DF_SecurityGroups_ShowAllProject] DEFAULT ((0)) NOT NULL,
    [IsSystem]       BIT            CONSTRAINT [DF_SecurityGroups_IsSystem] DEFAULT ((0)) NOT NULL,
    [Created]        DATETIME       NULL,
    [CreatedBy]      VARCHAR (50)   NULL,
    [Deleted]        BIT            NULL,
    CONSTRAINT [PK_SecurityGroups] PRIMARY KEY CLUSTERED ([RowId] ASC)
);

