﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace COFICO.ESafety.DAL.ServiceModels
{
    [Table("ProjectContractorTeamView")]
    public class ProjectContractorTeamView
    {
        [Key]
        public int RowID { get; set; }

        //public int ProjectContractorID { get; set; }

        public int ProjectID { get; set; }

        public int ContractorID { get; set; }

        public int TeamID { get; set; }

        public string TeamCode { get; set; }

        public string TeamName { get; set; }

        public int? SortOrder { get; set; }
    }
}
