﻿-- =============================================
-- Author:		HLinh
-- Create date: 2018/07/20
-- Description:	
-- 1. Báo cáo tháng - HSE dự án: @ProjectID > 0 && ShowInReport04 = 1
-- 2. Báo cáo tháng - HSE công ty: @ProjectID = 0 && ShowInReport03 = 1
-- =============================================
CREATE PROCEDURE [dbo].[GetImageHSE_Month_Project] 
	@ProjectID INT,
	@FromDate SMALLDATETIME,
	@ToDate SMALLDATETIME,
	@ShowAllProject BIT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Date SMALLDATETIME = @FromDate --dbo.GetFirstDateOfWeek(@Month, @Year, 1)

	IF (@Date IS NULL) RETURN

	DECLARE @Month TINYINT = MONTH(@FromDate)
	DECLARE @Year SMALLINT = YEAR(@FromDate)

	CREATE TABLE #Tmp
	(
		ImageID INT NOT NULL,
		ProjectID INT NOT NULL,
		ProjectCode NVARCHAR(50),
		ProjectName NVARCHAR(255),
		[Type] TINYINT,
		ImageCategoryID INT NOT NULL,
		CategoryCode NVARCHAR(50),
		CategoryName NVARCHAR(255),
		[Month] TINYINT,
		[Year] SMALLINT,
		[Week] TINYINT,
		[Date] SMALLDATETIME,
		ImageDetailID INT,
		[Filename] NVARCHAR(255),
		ImageTitle NVARCHAR(255),
		[Description] NVARCHAR(255),
		ShowInReport01 BIT,
		ShowInReport02 BIT,
		ShowInReport03 BIT,
		ShowInReport04 BIT,
		SortOrder INT,
		Id INT,
		Created DATETIME
	)

	--lấy tất cả các dự án chưa có dữ liệu
	--INSERT #Tmp (ImageID, ProjectID, ProjectCode, ProjectName, [Type], ImageCategoryID, [Month], [Year], [Week], [Date], ImageDetailID, [Filename], ImageTitle, [Description], ShowInReport01, ShowInReport02, ShowInReport03, SortOrder, Id,Created)
	--	SELECT ISNULL(ImageID, 0) AS ImageID, p.ProjectID, p.ProjectCode, p.ProjectName, CAST(1 AS TINYINT) AS [Type], ISNULL(ImageCategoryID, 0) AS ImageCategoryID, @Month AS [Month], @Year AS [Year], ISNULL([Week], 0), @Date AS [Date], ISNULL(ImageDetailID, 0) AS ImageDetailID, [Filename], ImageTitle, [Description], ISNULL(ShowInReport01, 0) AS ShowInReport01, ISNULL(ShowInReport02, 0) AS ShowInReport02, ISNULL(ShowInReport03, 0) AS ShowInReport03, p.SortOrder, ROW_NUMBER() OVER (ORDER BY ISNULL(ImageID, 0), p.ProjectID), a1.Created
	--	FROM (
	--		SELECT i.ImageID, ProjectID, d.ImageCategoryID, @Month AS [Month], @Year AS [Year], [Week], @Date AS [Date], ImageDetailID, d.[Filename], d.ImageTitle, d.[Description], d.ShowInReport01, d.ShowInReport02, d.ShowInReport03, d.Created
	--		FROM [Image] i
	--			INNER JOIN ImageDetail d ON (d.ImageID = i.ImageID AND d.Deleted = 0 AND i.Deleted = 0)
	--		WHERE (d.Created BETWEEN @FromDate AND @ToDate) AND ShowInReport03 = 1
	--	) a1
	--		RIGHT JOIN Project p ON (p.ProjectID = a1.ProjectID)
	--	WHERE p.Deleted = 0 AND (p.StatusID = 1 OR p.StatusID = 2)
	--show tất cả các dự án chưa có dữ liệu - end


	--chỉ lấy các dự án có dữ liệu
	INSERT #Tmp (ImageID, ProjectID, ProjectCode, ProjectName, [Type], ImageCategoryID, [Month], [Year], [Week], [Date], ImageDetailID, [Filename], ImageTitle, [Description], ShowInReport01, ShowInReport02, ShowInReport03, ShowInReport04, SortOrder, Id,Created)
		SELECT ISNULL(ImageID, 0) AS ImageID, p.ProjectID, p.ProjectCode, p.ProjectName, CAST(1 AS TINYINT) AS [Type], ISNULL(ImageCategoryID, 0) AS ImageCategoryID, @Month AS [Month], @Year AS [Year], ISNULL([Week], 0), @Date AS [Date], ISNULL(ImageDetailID, 0) AS ImageDetailID, [Filename], ImageTitle, [Description], ISNULL(ShowInReport01, 0) AS ShowInReport01, ISNULL(ShowInReport02, 0) AS ShowInReport02, ISNULL(ShowInReport03, 0) AS ShowInReport03, ISNULL(ShowInReport04, 0) AS ShowInReport04, p.SortOrder, ROW_NUMBER() OVER (ORDER BY ISNULL(ImageID, 0), p.ProjectID), a1.Created
		FROM (
			SELECT i.ImageID, ProjectID, d.ImageCategoryID, @Month AS [Month], @Year AS [Year], [Week], @Date AS [Date], ImageDetailID, d.[Filename], d.ImageTitle, d.[Description], d.ShowInReport01, d.ShowInReport02, d.ShowInReport03, d.ShowInReport04, d.Created
			FROM [Image] i
				INNER JOIN ImageDetail d ON (d.ImageID = i.ImageID AND d.Deleted = 0 AND i.Deleted = 0)
				INNER JOIN ImageCategory c ON (c.ImageCategoryID = d.ImageCategoryID AND c.Deleted = 0 AND c.IsActive = 1)
			WHERE (d.Created BETWEEN @FromDate AND @ToDate) 
				  AND ( (@ProjectID = 0 AND ShowInReport03 = 1) OR (@ProjectID > 0 AND ShowInReport04 = 1) )
		) a1
			INNER JOIN Project p ON (p.ProjectID = a1.ProjectID)
		WHERE p.Deleted = 0 AND ((@ShowAllProject = 0 AND (p.StatusID = 1 OR p.StatusID = 2)) OR @ShowAllProject = 1)
			  AND (p.ProjectID = @ProjectID OR @ProjectID = 0)
	--chỉ lấy các dự án có dữ liệu - end

	--only show 4 images of each project
	SELECT ImageID, ProjectID, ProjectCode, ProjectName, [Type], ImageCategoryID, CategoryCode, CategoryName, [Month], [Year], [Week], [Date], ImageDetailID, [Filename], ImageTitle, [Description], ShowInReport01, ShowInReport02, ShowInReport03, ShowInReport04, SortOrder, Created
	FROM #Tmp t
	WHERE Id IN (SELECT TOP 4 Id FROM #Tmp t1 WHERE t1.ProjectID = t.ProjectID)
	ORDER BY SortOrder, CategoryName, ProjectCode

	DROP TABLE #Tmp
END