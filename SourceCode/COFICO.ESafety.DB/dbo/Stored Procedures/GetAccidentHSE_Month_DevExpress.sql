﻿-- =============================================
-- Author:		HLinh
-- Create date: 2018/09/12
-- Description:	Báo cáo tháng - Thống kê Sự cố, an toàn trong tháng của 1 dự án
-- =============================================
CREATE PROCEDURE [dbo].[GetAccidentHSE_Month_DevExpress] 
	@ProjectID INT,
	@Month TINYINT,
	@Year SMALLINT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @PreYear SMALLINT = @Year
	DECLARE @PreMonth TINYINT = @Month - 1

	IF @PreMonth <= 0
	BEGIN
		SET @PreMonth = 12
		SET @PreYear = @PreYear - 1
	END

	CREATE TABLE #Tmp
	(
		AccidentCategoryID INT,
		AccidentCode NVARCHAR(50),
		AccidentName NVARCHAR(255),
		ProjectID INT,
		ProjectCode NVARCHAR(50),
		ProjectName NVARCHAR(255),
		NumOfAccidents DECIMAL(9,2),
		IsFooter BIT,
		IsThisWeek BIT,
		ThisWeek DECIMAL(9,2),
		LastWeek DECIMAL(9,2),
		Total DECIMAL(9,2)
	)

	SELECT TOP 0 * INTO #Remaining FROM #Tmp
	
	--get ProjectCode, ProjectName
	DECLARE @ProjectCode NVARCHAR(50)
	DECLARE @ProjectName NVARCHAR(255)

	SELECT @ProjectCode = ProjectCode, @ProjectName = ProjectName
	FROM Project
	WHERE ProjectID = @ProjectID


	--thống kê Sự cố, an toàn
	INSERT #Tmp (AccidentCategoryID, ProjectID, ProjectCode, ProjectName, NumOfAccidents, IsFooter, IsThisWeek)
		SELECT d.AccidentCategoryID, t.ProjectID, @ProjectCode, @ProjectName, SUM(NumOfAccidents) AS NumOfAccidents, 0, CAST(CASE WHEN t.[Month] = @Month THEN 1 ELSE 0 END AS BIT) AS IsThisWeek
		FROM Accident t 
			INNER JOIN AccidentDetail d ON (t.AccidentID =  d.AccidentID AND t.Deleted = 0 AND d.Deleted = 0)
			INNER JOIN AccidentCategory c ON (c.AccidentCategoryID = d.AccidentCategoryID AND c.Deleted = 0 AND c.IsActive = 1)
		WHERE t.ProjectID = @ProjectID AND (([Month] = @Month AND [Year] = @Year) OR ([Month] = @PreMonth AND [Year] = @PreYear))
		GROUP BY d.AccidentCategoryID, t.ProjectID, CAST(CASE WHEN t.[Month] = @Month THEN 1 ELSE 0 END AS BIT)


	--bổ sung những danh mục còn thiếu
	INSERT INTO #Remaining (AccidentCategoryID, ProjectID, ProjectCode, ProjectName, IsFooter, IsThisWeek)
		SELECT	AccidentCategoryID, @ProjectID, @ProjectCode, @ProjectName, IsFooter, 1
		FROM AccidentCategory c
		WHERE Deleted = 0 AND IsActive = 1
				AND NOT EXISTS (SELECT TOP 1 1 FROM #Tmp t1 WHERE t1.ProjectID = @ProjectID AND t1.AccidentCategoryID = c.AccidentCategoryID AND t1.IsThisWeek = 1)
	

	--merge data
	INSERT INTO #Tmp(AccidentCategoryID, ProjectID, ProjectCode, ProjectName, NumOfAccidents, IsFooter, IsThisWeek)
		SELECT AccidentCategoryID, ProjectID, ProjectCode, ProjectName, NumOfAccidents, IsFooter, IsThisWeek 
		FROM #Remaining

	--update ThisWeek
	UPDATE #Tmp
	SET ThisWeek = (SELECT SUM(ISNULL(NumOfAccidents, 0)) FROM #Tmp t WHERE t.AccidentCategoryID = #Tmp.AccidentCategoryID AND t.IsThisWeek = 1)

	UPDATE #Tmp SET ThisWeek = NULL WHERE ThisWeek = 0

	--update LastWeek
	UPDATE #Tmp
	SET LastWeek = (SELECT SUM(ISNULL(NumOfAccidents, 0)) FROM #Tmp t WHERE t.AccidentCategoryID = #Tmp.AccidentCategoryID AND t.IsThisWeek = 0)

	UPDATE #Tmp SET LastWeek = NULL WHERE LastWeek = 0

	--update Total
	UPDATE #Tmp
	SET Total = ISNULL(ThisWeek, 0) + ISNULL(LastWeek, 0)

	UPDATE #Tmp SET Total = NULL WHERE Total = 0


	--show data
	SELECT a1.AccidentCategoryID, c.AccidentCode, c.AccidentName, ProjectID, ProjectCode, ProjectName, NumOfAccidents, ThisWeek, LastWeek, Total, a1.IsFooter
	FROM #Tmp a1
		INNER JOIN AccidentCategory c ON (c.AccidentCategoryID = a1.AccidentCategoryID)
	WHERE IsThisWeek = 1
	ORDER BY a1.IsFooter, c.SortOrder, c.AccidentName, ProjectName

	DROP TABLE #Remaining
END