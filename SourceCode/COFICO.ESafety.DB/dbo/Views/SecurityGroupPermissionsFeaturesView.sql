﻿
CREATE VIEW [dbo].[SecurityGroupPermissionsFeaturesView] AS
	Select p.RowId, p.[SecurityGroupId], p.[View], p.[Add], p.[Edit], p.[Delete], p.[Approval], p.[Note], p.[Created], p.[CreatedBy], p.[Modified], p.[ModifiedBy], 
			p.[SecurityFeatureId], f.[FeatureCode], f.[FeatureName]
	From [dbo].[SecurityGroupPermissions] p 
		Left Join [dbo].[SecurityFeatures] f on f.RowId = p.[SecurityFeatureId] 
	Where (f.Deleted Is Null Or f.Deleted = 0) And (p.Deleted Is Null Or p.Deleted = 0)