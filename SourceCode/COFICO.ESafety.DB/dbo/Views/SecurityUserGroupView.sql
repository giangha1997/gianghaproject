﻿
CREATE VIEW [dbo].[SecurityUserGroupView] AS
	Select ug.RowId, ug.SecurityUserId, ug.SecurityGroupId, ug.Note, ug.Created, ug.CreatedBy, ug.Modified, ug.ModifiedBy, ug.Deleted, 
	u.SPUser, u.UserName, u.FullName, GroupName = g.Name 
	From SecurityUserGroups ug
		Inner Join SecurityUsers u on u.RowId = ug.SecurityUserId
		Inner Join SecurityGroups g on g.RowId = ug.SecurityGroupId
	Where (ug.Deleted Is Null Or ug.Deleted = 0) And (u.Deleted Is Null Or u.Deleted = 0) And (g.Deleted Is Null Or g.Deleted = 0)