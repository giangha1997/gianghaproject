﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COFICO.ESafety.DAL.Constants
{
    public struct Configurations
    {
        public const string IMAGE_FOLDER = "IMAGE_FOLDER";
    }

    public struct StoreProcedure_Permission
    {
        public const string LoadSecurityUserPermissionsFeaturesByUserFeatureCode = "usp_SecurityUserPermissionsFeaturesByUserFeatureCode";
        public const string LoadSecurityUserPermissionsFeaturesByUserId = "usp_SecurityUserPermissionsFeaturesByUserId";
        public const string LoadSecurityGroupPermissionsFeaturesByGroupId = "usp_SecurityGroupPermissionsFeaturesByGroupId";
    }

    public struct StoreProcedures
    {
        public const string GetProjectByUser = "usp_Project_GetUser";
        public const string ExistTowerFacZone = "usp_Project_TowerFacZone_Exist";

        public const string ExistContractor = "usp_Contractor_Exist";
        public const string ExistProject = "usp_Project_Exist";
        public const string ExistTeam = "usp_Team_Exist";

        public const string GetUserOfSafetyDept = "usp_SecurityUser_GetSafetyDept";

        public const string GetRequest = "usp_Request_Get";
        public const string GetMyRequest = "usp_Request_GetMyRequest";
        public const string SubmitRequest = "usp_Request_Submit";
        public const string FinishProcessRequest = "usp_Request_FinishProcess";
        public const string ApproveRequest = "usp_Request_Approve";
        public const string RejectRequest = "usp_Request_Reject";
    }

    public struct FolderPath
    {
        public const string IMAGE_FOLDER = "Upload/Images";
    }

    public enum ProjectUserType
    {
        User = 1,
        Engineer = 2,
        SiteCommander = 3,
        PersonWhoProcesses = 4
    }

    public enum GeneralCateType
    {
        RiskLevel = 1,
        SafetyStatus = 2
    }

    //public enum User_UsedFor
    //{
    //    Project = 1,
    //    Project_Contractor_Team = 2
    //}

    public enum RequestType
    {
        Hazard = 1,
        Violation = 2
    }

    public struct ApprovalLevel
    {
        public const string Engineer = "Kỹ sư phụ trách";
        public const string PersonWhoProcesses = "Người xử lý";
        public const string SiteCommander = "Chỉ huy công trường";
        public const string End = "END";
    }

    public enum StatusProcess
    {
        Draft = 1,
        Submitted = 2,
        Processed = 3,
        Approved = 4,
        Rejected = 5,
        Processing = 6
    }

    public struct ApprovalHistory_Actions
    {
        public const string Submit = "Submitted";
        public const string Processing = "Processing";
        public const string Process = "Processed";
        public const string Approve = "Approved";
        public const string Reject = "Rejected";
    }

    //public class ApprovalProcess
    //{
    //    public static List<string> Hazard = new List<string> { ApprovalLevel.SiteCommander, ApprovalLevel.End };
    //    public static List<string> Violation = new List<string> { ApprovalLevel.PersonWhoProcesses, ApprovalLevel.Engineer, ApprovalLevel.SiteCommander, ApprovalLevel.End };
    //}

    public struct ErrorText
    {
        public const string ItemNotFound = "Could not find this item. It seems that you do not have enough permission or this item has been deleted.";
        public const string RowVersionChanged = "Row version was changed.";
        public const string ItemExists = "This item exists.";
        public const string ItemInUse = "Could not delete this item. It has been used.";
    }
}
