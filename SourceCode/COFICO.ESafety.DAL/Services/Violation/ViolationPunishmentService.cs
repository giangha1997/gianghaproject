﻿using System;
using System.Collections.Generic;
using System.Linq;
using COFICO.ESafety.Base;
using COFICO.ESafety.DAL.DBModel;
using COFICO.ESafety.DAL.Services;

namespace COFICO.ESafety.DAL.Services
{
    public class ViolationPunishmentService : GenericService<ViolationPunishment>
    {
        /// <summary>
        /// isAll = true: get all (except Deleted = true)
        /// isAll = false: get by isActive (except Deleted = true)
        /// </summary>
        public List<ViolationPunishment> GetPunishments(bool isActive, bool isAll)
        {
            List<ViolationPunishment> lst = null;
            try
            {
                var data = _table.Where(s => !s.Deleted && (isAll || (!isAll && s.IsActive == isActive))).ToList();
                if (data == null || data.Count == 0) return lst;

                var parents = data.Where(s => s.ParentID == null).OrderBy(s => s.SortOrder).ToList();
                if (parents == null || parents.Count == 0) return lst;

                lst = new List<ViolationPunishment>();
                foreach (var v in parents)
                {
                    lst.Add(v);
                    SortViolation(data, lst, v);
                }

                return lst;
            }
            catch (Exception ex)
            {
                Logger.Error("GetPunishments", ex);
                throw ex;
            }
        }


        public List<ViolationPunishment> GetParentPunishments()
        {
            try
            {
                return _table.Where(s => s.IsActive && !s.Deleted && s.ParentID == null).OrderBy(s => s.SortOrder).ToList();
            }
            catch (Exception ex)
            {
                Logger.Error("GetParentPunishments", ex);
                throw ex;
            }
        }

        public List<ViolationPunishment> GetParentPunishments(int catID)
        {
            try
            {
                return _table.Where(s => ((catID > 0 && s.ID != catID) || catID <= 0) && (s.ParentID == null || s.IsParent) && s.IsActive && !s.Deleted).OrderBy(s => s.SortOrder).ToList();
            }
            catch (Exception ex)
            {
                Logger.Error(catID, ex);
                throw ex;
            }
        }

        private void SortViolation(List<ViolationPunishment> orig, List<ViolationPunishment> lst, ViolationPunishment v)
        {
            if (v == null) return;

            var children = orig.Where(s => s.ParentID == v.ID).OrderBy(s => s.SortOrder).ToList();
            if (children == null) return;

            foreach (var c in children)
            {
                lst.Add(c);
                SortViolation(orig, lst, c);
            }
        }

        public int? Update(ViolationPunishment p)
        {
            try
            {
                if (p == null) return null;

                //check existance
                if (ExistCode(p)) return -1;

                int? parentID = null;
                var obj = ParseToPunishment(p, ref parentID);
                SaveChanges();

                //check parent to update IsParent
                if (obj.ParentID > 0 && _table.Any(s => s.ParentID == obj.ParentID && !s.Deleted))
                {
                    var entityParent = _table.FirstOrDefault(s => s.ID == obj.ParentID && !s.Deleted);
                    if (entityParent != null && !entityParent.IsParent)
                    {
                        entityParent.IsParent = true;
                        SaveChanges();
                    }
                }

                //check if this category changes parent.
                if (parentID > 0 && !_table.Any(s => s.ParentID == parentID && !s.Deleted))
                {
                    var entityParent = _table.FirstOrDefault(s => s.ID == parentID && !s.Deleted);
                    if (entityParent != null && entityParent.IsParent)
                    {
                        entityParent.IsParent = false;
                        SaveChanges();
                    }
                }

                return obj.ID;

            }
            catch (Exception ex)
            {
                Logger.Error(p, ex);
                throw ex;
            }
        }

        public bool Delete(int punishmentID)
        {
            try
            {   
                //// kiem tra detail
                //if (_db.ViolationDetails.Any(s => s.PunishmentID == punishmentID && !s.Deleted))
                //    return false;

                // lay thong tin master
                var entity = _table.FirstOrDefault(s => s.ID == punishmentID && !s.Deleted);
                if (entity == null) return false;

                DeleteMark(entity.ID);
                SaveChanges();

                // update lai IsParent of master
                if (entity.ParentID > 0 && !_table.Any(s => s.ParentID == entity.ParentID && !s.Deleted))
                {
                    var entityParent = _table.FirstOrDefault(s => s.ID == entity.ParentID && !s.Deleted);
                    if (entityParent != null && entityParent.IsParent)
                    {
                        entityParent.IsParent = false;
                        SaveChanges();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Logger.Error(punishmentID, ex);
                throw ex;
            }
        }

        private bool ExistCode(ViolationPunishment p)
        {
            try
            {
                return _table.Any(s => ((p.ID == 0 && s.PunishmentCode == p.PunishmentCode)
                                        || (p.ID > 0 && s.PunishmentCode == p.PunishmentCode && s.ID != p.ID)) && !s.Deleted);
            }
            catch (Exception ex)
            {
                Logger.Error(p, ex);
                throw ex;
            }
        }

        private ViolationPunishment ParseToPunishment(ViolationPunishment dto, ref int? parentID)
        {
            if (dto == null) return null;
            ViolationPunishment entity = null;
            if (dto.ID == 0)
            {
                InsertObj(ref dto);

                entity = dto;
            }
            else
            {
                entity = _table.SingleOrDefault(x => x.ID == dto.ID && !x.Deleted);
                if (entity == null) return null;

                parentID = null;
                if (entity.ParentID != dto.ParentID) parentID = entity.ParentID;

                entity.PunishmentCode = dto.PunishmentCode;
                entity.PunishmentName = dto.PunishmentName;
                entity.ParentID = dto.ParentID;
                entity.IsParent = dto.IsParent;
                entity.PenaltyFee = dto.PenaltyFee;
                entity.SortOrder = dto.SortOrder;
                entity.IsActive = dto.IsActive;
                entity.Note = dto.Note;

                UpdateObj(entity);
            }

            return entity;
        }
    }
}
