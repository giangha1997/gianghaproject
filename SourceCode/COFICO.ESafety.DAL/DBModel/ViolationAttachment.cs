﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace COFICO.ESafety.DAL.DBModel
{
    [Table("ViolationAttachment")]
    public class ViolationAttachment
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ViolationAttachmentID { get; set; }

        public int ViolationDetailID { get; set; }

        [StringLength(255)]
        public string Filename { get; set; }

        [StringLength(255)]
        public string FilenameOrig { get; set; }

        [StringLength(500)]
        public string Note { get; set; }

        public DateTime? Created { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime? Modified { get; set; }

        [StringLength(50)]
        public string ModifiedBy { get; set; }

        public bool Deleted { get; set; }
    }
}
