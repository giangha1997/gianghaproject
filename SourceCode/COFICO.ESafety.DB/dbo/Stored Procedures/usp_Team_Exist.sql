﻿-- =============================================
-- Author:		HLinh
-- Create date: 2019/03/22
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_Team_Exist] 
	@teamID INT
AS
BEGIN
	SET NOCOUNT ON;

	IF EXISTS (SELECT TOP 1 1
			   FROM Project_Contractor_Team t
				   INNER JOIN Project_Contractor pc ON pc.RowID = t.ProjectContractorID AND pc.Deleted = 0
				   INNER JOIN Contractor c ON c.ContractorID = pc.ContractorID AND c.Deleted = 0
				   INNER JOIN Project p ON p.ProjectID = pc.ProjectID AND p.Deleted = 0
			   WHERE t.TeamID = @teamID AND t.Deleted = 0)
	BEGIN
		SELECT CAST(1 AS BIT)
		RETURN 1
	END

	SELECT CAST(0 AS BIT)
END