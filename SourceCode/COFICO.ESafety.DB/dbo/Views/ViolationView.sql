﻿

















CREATE View [dbo].[ViolationView] As

SELECT t.ViolationID, t.ProjectID, p.ProjectCode, p.ProjectName, t.[Month], t.[Year], t.[Week], t.[Date], ViolationDetailID, PublicationDate, PublishedBy, d.Username, d.ContractorID, o.ContractorCode, o.ContractorName, Location, d.ViolationCategoryID, c.ViolationCode, c.ViolationName, d.ViolationCategoryDetailID, c1.ViolationCode AS ViolationCodeDetail, c1.ViolationName AS ViolationNameDetail,  IsClosed, RequirementDate, d.CompletionDate, PunishmentID, PunishmentCode, PunishmentName, d.PenaltyFee, [Description], d.Note, Image1, Image2, o.SortOrder AS SortOrder_Cont
FROM Violation t 
	INNER JOIN ViolationDetail d ON (t.ViolationID =  d.ViolationID AND t.Deleted = 0 AND d.Deleted = 0)
	INNER JOIN Project p ON (t.ProjectID = p.ProjectID AND p.Deleted = 0 AND (p.StatusID = 1 OR p.StatusID = 2))
	INNER JOIN Contractor o ON (o.ContractorID = d.ContractorID AND o.Deleted = 0 AND o.IsActive = 1)
	INNER JOIN ViolationCategory c ON (c.ViolationCategoryID = d.ViolationCategoryID AND c.Deleted = 0 AND c.IsActive = 1)
	INNER JOIN ViolationCategory c1 ON (c1.ViolationCategoryID = d.ViolationCategoryDetailID AND c1.Deleted = 0 AND c1.IsActive = 1)
	LEFT JOIN ViolationPunishment h ON (h.ViolationPunishmentID = d.PunishmentID AND h.Deleted = 0 AND h.IsActive = 1)