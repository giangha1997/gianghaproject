﻿-- =============================================
-- Author:		HLinh
-- Create date: 2018/07/23
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetTraining_DataOfWeeks] 
	@ProjectID INT,
	@Month TINYINT,
	@Year SMALLINT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT [Week]
	FROM Training t
		INNER JOIN TrainingDetail c ON (c.TrainingID = t.TrainingID AND c.Deleted = 0 AND t.Deleted = 0 AND ISNULL(c.NumOfEmployees, 0) > 0)
	WHERE t.ProjectID = @ProjectID AND t.[Month] = @Month AND t.[Year] = @Year
END