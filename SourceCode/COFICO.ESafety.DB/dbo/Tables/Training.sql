﻿CREATE TABLE [dbo].[Training] (
    [TrainingID] INT           IDENTITY (1, 1) NOT NULL,
    [ProjectID]  INT           NOT NULL,
    [Month]      TINYINT       NOT NULL,
    [Year]       SMALLINT      NOT NULL,
    [Week]       TINYINT       NOT NULL,
    [Date]       SMALLDATETIME NULL,
    [Created]    DATETIME      NOT NULL,
    [CreatedBy]  NVARCHAR (50) NULL,
    [Modified]   DATETIME      NULL,
    [ModifiedBy] NVARCHAR (50) NULL,
    [Deleted]    BIT           CONSTRAINT [DF_Training_Deleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Training] PRIMARY KEY CLUSTERED ([TrainingID] ASC)
);

