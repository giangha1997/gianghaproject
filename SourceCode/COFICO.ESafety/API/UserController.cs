﻿using COFICO.ESafety.DAL.ServiceModels;
using COFICO.ESafety.DAL.Services;
using System.Collections.Generic;
using System.Web.Http;

namespace COFICO.ESafety
{
    //[Authorize]
    [RoutePrefix("api/User")]
    public class UserController : ApiController
    {
        [HttpGet, Route("Items")]
        public IHttpActionResult Items()
        {
            var items = new List<UserView>();
            using (var svc = new UserService())
            {
                items = svc.GetUsers();
            }
            return Ok(new { items });
        }

        [HttpGet, Route("GetActiveUsers")]
        public IHttpActionResult GetActiveUsers()
        {
            var items = new List<UserView>();
            using (var svc = new UserService())
            {
                items = svc.GetActiveUsers();
            }
            return Ok(new { items });
        }
    }
}