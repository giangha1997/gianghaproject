﻿using System.Linq;
using System.Collections.Generic;
using System.Web.Http;
using COFICO.ESafety.AppCode;
using COFICO.ESafety.DAL.DBModel;
using COFICO.ESafety.DAL.ServiceModels;
using COFICO.ESafety.DAL.Services;

namespace COFICO.ESafety
{
    //[Authorize]
    [RoutePrefix("api/Project")]
    public class ProjectController : ApiController
    {
        [HttpGet, Route("items")]
        public IHttpActionResult Items()
        {
            var items = new List<ProjectView>();
            using (var svc = new ProjectService())
            {
                items = svc.GetProjects();
            }

            if (items != null && items.Count > 0)
            {
                using (var svc = new ProjectUserService())
                {
                    foreach (var obj in items)
                    {
                        var users = svc.GetProjectUsers(obj.ID);

                        if (users != null && users.Count > 0)
                        {
                            obj.Users = users.Where(s => s.Type == (byte)DAL.Constants.ProjectUserType.User).ToList();
                            obj.Engineers = users.Where(s => s.Type == (byte)DAL.Constants.ProjectUserType.Engineer).ToList();
                            obj.SiteCommanders = users.Where(s => s.Type == (byte)DAL.Constants.ProjectUserType.SiteCommander).ToList();
                        }
                    }
                }

                using (var svc = new ProjectContractorService())
                {
                    foreach (var obj in items)
                        obj.Contractors = svc.GetContractors(obj.ID);
                }

                //using (var svc = new ProjectTowerFacZoneService())
                //{
                //    foreach (var obj in items)
                //        obj.TowerFacs = svc.GetList(obj.ID, null);
                //}
            }

            return Ok(new { items });
        }

        [HttpGet, Route("Get/{projectID}")]
        public IHttpActionResult Get(int projectID)
        {
            var items = new ProjectView();
            using (var svc = new ProjectService())
            {
                items = svc.GetProject(projectID);
            }

            using (var svc = new ProjectUserService())
            {
                var users = svc.GetProjectUsers(projectID);

                if (users != null && users.Count > 0)
                {
                    items.Users = users.Where(s => s.Type == (byte)DAL.Constants.ProjectUserType.User).ToList();
                    items.Engineers = users.Where(s => s.Type == (byte)DAL.Constants.ProjectUserType.Engineer).ToList();
                    items.SiteCommanders = users.Where(s => s.Type == (byte)DAL.Constants.ProjectUserType.SiteCommander).ToList();
                }
            }

            using (var svc = new ProjectContractorService())
            {
                items.Contractors = svc.GetContractors(projectID);
            }

            using (var svc = new ProjectTowerFacZoneService())
            {
                items.TowerFacs = svc.GetList(projectID, null);
            }

            return Ok(new { items });
        }

        [HttpGet, Route("GetProjectByUser")]
        public IHttpActionResult GetProjectByUser()
        {
            var items = new List<ProjectView>();
            using (var svc = new ProjectService())
            {
                string user = Base.Helper.GetCurrentUsername();
                items = svc.GetProjectByUser();
            }
            return Ok(new { items });
        }

        [HttpGet, Route("ProjectStatus")]
        public IHttpActionResult ProjectStatus()
        {
            var items = new List<ProjectStatus>();
            using (var svc = new ProjectStatusService())
            {
                items = svc.GetProjectStatus();
            }
            return Ok(new { items });
        }

        /// <summary>
        /// Insert/update a project
        /// </summary>
        /// <returns>null: error or empty; -1: exist ProjectCode; > 0: success</returns>
        [HttpPut, Route("Put")]
        public IHttpActionResult Put(ProjectView item)
        {
            var items = new Response<object>
            {
                IsError = true,
            };

            int? rs = null;
            using (var svc = new ProjectService())
            {
                rs = svc.Update(item);
                if (rs != null)
                {
                    if (rs == -1)
                        items.IsDuplicate = true;
                    else
                    {
                        items.IsError = false;
                        items.Id = rs;
                    }
                }
            }
            return Ok(new { items });
        }

        ///// <summary>
        ///// Insert/update a project includes Project_Contractor, Project_User
        ///// </summary>
        ///// <returns>null: error or empty; -1: exist ProjectCode; > 0: success</returns>
        //[HttpPut]
        //public IHttpActionResult PutPCU(ProjectContractorUser item)
        //{
        //    var items = new Response<object>
        //    {
        //        IsError = true,
        //        Result = -1,// update error
        //        Id = item.ProjectID
        //    };

        //    using (var svc = new ProjectService())
        //    {
        //        items.Id = svc.UpdatePCU(item);
        //        if (items.Id != null)
        //        {
        //            if (items.Id == -1)
        //                items.IsDuplicate = true;
        //            else
        //            {
        //                items.IsError = false;
        //                items.Result = 1;// update success
        //            }
        //        }
        //    }
        //    return Ok(new { items });
        //}

        [HttpDelete, Route("Delete")]
        public IHttpActionResult Delete(int id)
        {
            var items = new Response<object>
            {
                IsError = true,
            };

            bool? rs = false;
            using (var svc = new ProjectService())
            {
                rs = svc.Delete(id);
                if (rs == true)
                {
                    items.IsError = false;
                }
            }
            return Ok(new { items });
        }

        [HttpGet, Route("GetUserByProject/{projectID}")]
        public IHttpActionResult GetUserByProject(int projectID)
        {
            var items = new List<Project_User>();
            using (var svc = new ProjectUserService())
            {
                items = svc.GetProjectUsers(projectID);
            }
            return Ok(new { items });
        }

        [HttpGet, Route("GetSPUserByProject/{projectID}")]
        public IHttpActionResult GetSPUserByProject(int projectID)
        {
            var lstUsers = new List<Users>();
            var items = new List<Project_User>();

            using (var svc = new ProjectUserService())
            {
                using (var service = new ProjectUserService())
                {
                    items = service.GetProjectUsers(projectID);
                }

                if (items != null)
                {
                    lstUsers = new List<Users>();
                    foreach (Project_User item in items)
                    {
                        lstUsers.Add(new Users { UserName = item.Username, LoginName = item.Username, LoginNameFull = item.Username });
                    }
                }
            }
            return Ok(new { lstUsers });
        }

        //[HttpPut]
        //public IHttpActionResult AddUserToProject(List<Project_User> lst)
        //{
        //    bool res = false;

        //    using (var service = new ProjectUserService())
        //    {
        //        res = service.InsertDelete_ProjectUsers(lst);
        //    }

        //    return Ok(new { res });
        //}

        [HttpGet, Route("GetProjectFullInfo/{projectID}")]
        public object GetProjectFullInfo(int projectID)
        {
            var items = new ProjectFullInfo();
            using (var svc = new ProjectService())
            {
                items = svc.GetProjectFullInfo(projectID);
            }
            return Ok(new { items });
        }

        #region Contractor By Project
        [HttpGet, Route("GetContractorByProject/{projectID}")]
        public IHttpActionResult GetContractorByProject(int projectID)
        {
            var items = new List<ProjectContractorView>();
            using (var svc = new ProjectContractorService())
            {
                items = svc.GetContractors(projectID);
            }
            return Ok(new { items });
        }

        [HttpPut, Route("UpdateContractorToProject")]
        public IHttpActionResult UpdateContractorToProject(Project_Contractor dto)
        {
            var items = new Response<object>
            {
                IsError = true,
            };

            int? rs = null;
            using (var service = new ProjectContractorService())
            {
                rs = service.Update(dto);

                if (rs != null)
                {
                    if (rs == -1)
                    {
                        items.IsDuplicate = true;
                        items.Messages.Add(DAL.Constants.ErrorText.ItemExists);
                    }
                    else if (rs > 0)
                    {
                        items.IsError = false;
                        items.Id = rs;
                    }
                }
            }

            return Ok(new { items });
        }

        //[HttpPut]
        //public IHttpActionResult AddContractorToProject(List<Project_Contractor> lst)
        //{
        //    bool res = false;

        //    using (var service = new ProjectContractorService())
        //    {
        //        res = service.InsertDelete(lst);
        //    }

        //    return Ok(new { res });
        //}
        #endregion

        #region Team By Contractor
        [HttpGet, Route("GetTeamByContractor/{projectID}/{contractorID}")]
        public IHttpActionResult GetTeamByContractor(int projectID, int contractorID)
        {
            var items = new List<ProjectContractorTeamView>();
            using (var svc = new ProjectContractorTeamService())
            {
                items = svc.GetTeams(projectID, contractorID);
            }
            return Ok(new { items });
        }

        [HttpPut, Route("UpdateTeamToContractor")]
        public IHttpActionResult UpdateTeamToContractor(Project_Contractor_Team dto)
        {
            var items = new Response<object>
            {
                IsError = true,
            };

            int? rs = null;
            using (var service = new ProjectContractorTeamService())
            {
                rs = service.Update(dto);

                if (rs != null)
                {
                    if (rs == -1)
                    {
                        items.IsDuplicate = true;
                        items.Messages.Add(DAL.Constants.ErrorText.ItemExists);
                    }
                    else if (rs > 0)
                    {
                        items.IsError = false;
                        items.Id = rs;
                    }
                }
            }

            return Ok(new { items });
        }

        //[HttpPut]
        //public IHttpActionResult AddTeamToContractor(List<ProjectContractorTeamView> lst)
        //{
        //    bool res = false;

        //    using (var service = new ProjectContractorTeamService())
        //    {
        //        res = service.InsertDelete(lst);
        //    }

        //    return Ok(new { res });
        //}
        #endregion

        #region User By Team
        [HttpGet, Route("GetUserByTeam/{teamID}")]
        public IHttpActionResult GetUserByTeam(int teamID)
        {
            var items = new List<Project_User>();
            using (var svc = new ProjectUserService())
            {
                items = svc.GetProjectUserByType(teamID, DAL.Constants.ProjectUserType.PersonWhoProcesses);
            }
            return Ok(new { items });
        }

        [HttpPut, Route("UpdateUserToTeam")]
        public IHttpActionResult UpdateUserToTeam(Project_User dto)
        {
            var items = new Response<object>
            {
                IsError = true,
            };

            int? rs = null;
            using (var service = new ProjectUserService())
            {
                rs = service.Update_ProjectUsers(dto);

                if (rs != null)
                {
                    if (rs == -1)
                    {
                        items.IsDuplicate = true;
                        items.Messages.Add(DAL.Constants.ErrorText.ItemExists);
                    }
                    else if (rs > 0)
                    {
                        items.IsError = false;
                        items.Id = rs;
                    }
                }
            }

            return Ok(new { items });
        }
        #endregion
    }
}