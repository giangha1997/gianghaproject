﻿-- =============================================
-- Author:		HLinh
-- Create date: 2018/06/13
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[usp_Contractor_Exist] 
	@ContractorID INT
AS
BEGIN
	SET NOCOUNT ON;

	--IF EXISTS(SELECT TOP 1 1 FROM Project_Contractor WHERE ContractorID = @ContractorID AND Deleted = 0)
	--BEGIN
	--	SELECT CAST(1 AS BIT)
	--	RETURN 1
	--END

	IF EXISTS(SELECT TOP 1 1 FROM TBM WHERE ContractorID = @ContractorID AND Deleted = 0)
	BEGIN
		SELECT CAST(1 AS BIT)
		RETURN 1
	END

	SELECT CAST(0 AS BIT)
END