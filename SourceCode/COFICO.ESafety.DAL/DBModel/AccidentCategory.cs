﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace COFICO.ESafety.DAL.DBModel
{
    [Table("AccidentCategory")]
    public class AccidentCategory
    {
        [Key]
        [Column("AccidentCategoryID")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [StringLength(50)]
        public string AccidentCode { get; set; }

        [StringLength(255)]
        public string AccidentName { get; set; }

        public int? ParentID { get; set; }

        public bool IsParent { get; set; }

        public bool IsFooter { get; set; }

        public bool IsActive { get; set; }

        public int? SortOrder { get; set; }

        [JsonIgnore]
        public DateTime Created { get; set; }

        [JsonIgnore]
        [StringLength(50)]
        public string CreatedBy { get; set; }

        [JsonIgnore]
        public DateTime? Modified { get; set; }

        [JsonIgnore]
        [StringLength(50)]
        public string ModifiedBy { get; set; }

        public bool Deleted { get; set; }

        public string Note { get; set; }
    }
}