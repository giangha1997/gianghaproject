﻿CREATE TABLE [dbo].[Project_User] (
    [RowID]      INT            IDENTITY (1, 1) NOT NULL,
    [Username]   NVARCHAR (100) NOT NULL,
    [ObjectID]   INT            NULL,
    [Type]       TINYINT        NULL,
    [Created]    DATETIME       NOT NULL,
    [CreatedBy]  NVARCHAR (100) NULL,
    [Modified]   DATETIME       NULL,
    [ModifiedBy] NVARCHAR (100) NULL,
    [Deleted]    BIT            CONSTRAINT [DF_Project_User_Deleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Project_User] PRIMARY KEY CLUSTERED ([RowID] ASC)
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'1: User; 2: Engineer; 3: SiteCommander; 4: ng xử lý', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Project_User', @level2type = N'COLUMN', @level2name = N'Type';



