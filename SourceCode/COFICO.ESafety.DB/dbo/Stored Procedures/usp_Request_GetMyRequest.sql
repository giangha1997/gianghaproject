﻿-- =============================================
-- Author:		HLinh
-- Create date: 2019/03/21
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_Request_GetMyRequest]
	@username NVARCHAR(100),
	@requestType TINYINT,
	@fromDate DATETIME,
	@toDate DATETIME,
	@projectID INT,
	@contractorID INT,
	@teamID INT
AS
BEGIN
	SET NOCOUNT ON;

	SET @todate = DATEADD(SECOND, -1, DATEADD(DAY, 1, @todate));

	SELECT TOP 0 * INTO #projects FROM ProjectView;
	ALTER TABLE #projects ADD ID INT;
	INSERT INTO #projects
		EXEC usp_Project_GetUser @username

	IF NOT EXISTS (SELECT TOP 1 1 FROM #projects WHERE ProjectID = @projectID)
	BEGIN
		IF @requestType = 1
			SELECT * FROM HazardView WHERE HazardID = -1;
		RETURN;
	END

	IF @requestType = 1
		SELECT *
		FROM HazardView
		WHERE (@fromDate IS NULL OR @toDate IS NULL OR IssuedDate BETWEEN @fromDate AND @toDate)
			  AND (@projectID IS NULL OR ProjectID = @projectID)
			  AND (@contractorID IS NULL OR ContractorID = @contractorID)
			  AND (@teamID IS NULL OR TeamID = @teamID)
	--ELSE IF @requestType = 2
	--	SELECT *
	--	FROM ViolationForm
	--	WHERE Username = @username
	--		  AND (@fromDate IS NULL OR @toDate IS NULL OR IssuedDate BETWEEN @fromDate AND @toDate)
	--		  AND (@projectID IS NULL OR ProjectID = @projectID)
	--		  AND (@contractorID IS NULL OR ContractorID = @contractorID)
	--		  AND (@teamID IS NULL OR TeamID = @teamID)

	DROP TABLE #projects;
END