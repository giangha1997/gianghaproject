﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace COFICO.ESafety.DAL.DBModel
{
    [Table("ViolationCategory")]
    public class ViolationCategory
    {
        [Key]
        [Column("ViolationCategoryID")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ViolationCategoryID { get; set; } // int, not null

        [MaxLength(50)]
        [StringLength(50)]
        public string ViolationCode { get; set; } // nvarchar(50), null

        [MaxLength(500)]
        [StringLength(500)]
        public string ViolationName { get; set; } // nvarchar(500), null

        public int? ParentID { get; set; } // int, null

        public bool IsParent { get; set; } // bit, not null

        public bool IsFooter { get; set; } // bit, not null

        public byte FooterType { get; set; } // tinyint, not null

        [MaxLength(4000)]
        [StringLength(4000)]
        public string Note { get; set; } // nvarchar(4000), null

        public bool IsActive { get; set; } // bit, not null

        public int? SortOrder { get; set; } // int, null

        [JsonIgnore]
        public DateTime Created { get; set; } // datetime, not null

        [MaxLength(50)]
        [StringLength(50)]
        [JsonIgnore]
        public string CreatedBy { get; set; } // nvarchar(50), null

        [JsonIgnore]
        public DateTime? Modified { get; set; } // datetime, null

        [MaxLength(50)]
        [StringLength(50)]
        [JsonIgnore]
        public string ModifiedBy { get; set; } // nvarchar(50), null

        public bool Deleted { get; set; } // bit, not null
    }

    [Table("ViolationCategory")]
    public class ViolationCategoryDTO
    {
        //Khi khai báo tên class ViolationCateGoryDTO ở EsafetyModel.cs thì báo lỗi khóa liên kết bảng giống nhau, quan hệ 0-1
        [Key]
        [Column("ViolationCategoryID")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ViolationCategoryID { get; set; } // int, not null

        [MaxLength(50)]
        [StringLength(50)]
        public string ViolationCode { get; set; } // nvarchar(50), null

        [MaxLength(500)]
        [StringLength(500)]
        public string ViolationName { get; set; } // nvarchar(500), null

        public int? ParentID { get; set; } // int, null

        public bool IsParent { get; set; } // bit, not null

        public bool IsFooter { get; set; } // bit, not null

        public byte FooterType { get; set; } // tinyint, not null

        [MaxLength(4000)]
        [StringLength(4000)]
        public string Note { get; set; } // nvarchar(4000), null

        public bool IsActive { get; set; } // bit, not null

        public int? SortOrder { get; set; } // int, null

        [JsonIgnore]
        public DateTime Created { get; set; } // datetime, not null

        [MaxLength(50)]
        [StringLength(50)]
        [JsonIgnore]
        public string CreatedBy { get; set; } // nvarchar(50), null

        [JsonIgnore]
        public DateTime? Modified { get; set; } // datetime, null

        [MaxLength(50)]
        [StringLength(50)]
        [JsonIgnore]
        public string ModifiedBy { get; set; } // nvarchar(50), null

        public bool Deleted { get; set; } // bit, not null

        [Required]
        public virtual ICollection<ViolationCategory> ChildViolationCategory { get; set; }
    }

}