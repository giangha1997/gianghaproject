﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using COFICO.ESafety.Base;
using COFICO.ESafety.DAL.Constants;
using COFICO.ESafety.DAL.DBModel;

namespace COFICO.ESafety.DAL.Services
{
    public class ProjectTowerFacZoneService : GenericService<Project_TowerFacZone>
    {
        //get list by projectID & parentID
        public List<Project_TowerFacZone> GetList(int projectID, int? parentID)
        {
            try
            {
                return _table.Where(s => s.ProjectID == projectID && s.ParentID == parentID && !s.Deleted).OrderBy(s => s.SortOrder).ToList();
            }
            catch (Exception ex)
            {
                Logger.Error(new { projectID, parentID }, ex);
                throw ex;
            }
        }

        //get list by projectID
        public List<Project_TowerFacZone> GetList(int projectID)
        {
            try
            {
                return _table.Where(s => s.ProjectID == projectID && !s.Deleted).ToList();
            }
            catch (Exception ex)
            {
                Logger.Error(projectID, ex);
                throw ex;
            }
        }

        public int? Update(Project_TowerFacZone item)
        {
            if (item == null) return null;

            if (Exists(item.ID, item.Name, item.ProjectID)) return -1;

            int? id = null;
            try
            {
                Project_TowerFacZone entity = null;
                if ((entity = ParseToObj(item)) != null)
                {
                    SaveChanges();
                    id = entity.ID;
                }
                return id;
            }
            catch (Exception ex)
            {
                Logger.Error(item, ex);
                throw ex;
            }
        }

        public int? Delete(int projectID, int towerFacZoneID)
        {
            if (projectID <= 0 || towerFacZoneID <= 0) return null;

            try
            {
                //check if exists
                if (ExistsInHazard(projectID, towerFacZoneID)) return -1;

                if (DeleteMark(towerFacZoneID))
                {
                    SaveChanges();
                    return 1;
                }
                return null;
            }
            catch (Exception ex)
            {
                Logger.Error(projectID, ex);
                throw ex;
            }
        }

        private bool Exists(int id, string name, int projectID)
        {
            try
            {
                return _table.Any(s => ((id == 0 && s.Name == name) ||
                                            (id != 0 && s.ID != id && s.Name == name)) &&
                                        s.ProjectID == projectID && !s.Deleted);
            }
            catch (Exception ex)
            {
                Logger.Error(new { id, name, projectID }, ex);
                throw ex;
            }
        }

        private bool ExistsInHazard(int projectID, int towerFacZoneID)
        {
            try
            {
                string strSql = "EXEC " + StoreProcedures.ExistTowerFacZone + " @spProjectID, @spTowerFacZoneID";
                var prProjectID = new SqlParameter("@spProjectID", projectID);
                var prTowerFacZoneID = new SqlParameter("@spTowerFacZoneID", towerFacZoneID);
                return _db.Database.SqlQuery<bool>(strSql, prProjectID, prTowerFacZoneID).Single();
            }
            catch (Exception ex)
            {
                Logger.Error(projectID, ex);
                throw ex;
            }
        }

        private Project_TowerFacZone ParseToObj(Project_TowerFacZone dto)
        {
            if (dto == null) return null;

            Project_TowerFacZone entity = null;
            if (dto.ID == 0)
            {
                InsertObj(ref dto);
                entity = dto;
            }
            else
            {
                entity = _table.SingleOrDefault(x => x.ID == dto.ID && !x.Deleted);
                if (entity == null) return null;

                entity.ProjectID = dto.ProjectID;
                entity.Code = dto.Code;
                entity.Name = dto.Name;
                entity.Type = dto.Type;
                entity.Note = dto.Note;
                entity.SortOrder = dto.SortOrder;

                UpdateObj(entity);
            }

            return entity;
        }
    }
}
