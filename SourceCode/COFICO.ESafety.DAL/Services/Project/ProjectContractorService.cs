﻿using System;
using System.Collections.Generic;
using System.Linq;
using COFICO.ESafety.Base;
using COFICO.ESafety.DAL.DBModel;
using COFICO.ESafety.DAL.ServiceModels;

namespace COFICO.ESafety.DAL.Services
{
    public class ProjectContractorService : GenericService<Project_Contractor>
    {
        public List<ProjectContractorView> GetContractors(int projectID)
        {
            try
            {
                return _db.ProjectContractorViews.Where(s => s.ProjectID == projectID).OrderBy(s => s.SortOrder).ThenBy(s => s.ContractorCode).ToList();
            }
            catch (Exception ex)
            {
                Logger.Error(projectID, ex);
                throw ex;
            }
        }

        public int? Update(Project_Contractor dto)
        {
            if (dto == null) return null;

            int res = 0;
            try
            {
                if (dto.RowID == 0)
                {
                    if (_table.Any(s => s.ProjectID == dto.ProjectID && s.ContractorID == dto.ContractorID && !s.Deleted))
                        return -1;

                    InsertObj(ref dto);
                    SaveChanges();
                    res = dto.RowID;
                }
                else if (dto.RowID < 0)
                {
                    DeleteMark(Math.Abs(dto.RowID));
                    SaveChanges();
                    res = Math.Abs(dto.RowID);
                }

                return res;
            }
            catch (Exception ex)
            {
                Logger.Error(new { dto.ProjectID, dto.ContractorID }, ex);
                throw ex;
            }
        }

        //public bool InsertDelete(List<Project_Contractor> lst)
        //{
        //    if (lst == null || lst.Count == 0) return false;

        //    try
        //    {
        //        var pID = lst[0].ProjectID;
        //        var entities = _table.Where(s => s.ProjectID == pID && !s.Deleted).ToList();

        //        //remove
        //        foreach (var p in entities)
        //        {
        //            if (!lst.Any(s => s.ProjectID == p.ProjectID && s.ContractorID == p.ContractorID))
        //                DeleteMark(p.RowID);
        //        }

        //        //insert
        //        for (int i = 0; i < lst.Count; i++)
        //        {
        //            var p = lst[i];
        //            if (!entities.Any(s => s.ProjectID == p.ProjectID && s.ContractorID == p.ContractorID))
        //                InsertObj(ref p);
        //        }

        //        SaveChanges();
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error(lst, ex);
        //        throw ex;
        //    }
        //}

        public bool DeleteByProject(int projectID)
        {
            if (projectID <= 0) return false;

            try
            {
                var entities = _table.Where(s => s.ProjectID == projectID && !s.Deleted).ToList();
                if (entities == null || entities.Count == 0) return false;

                foreach (var e in entities)
                    DeleteMark(e.RowID);

                SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                Logger.Error(projectID, ex);
                throw ex;
            }
        }

        public bool DeleteByContractor(int contractorID)
        {
            if (contractorID <= 0) return false;

            try
            {
                var entities = _table.Where(s => s.ContractorID == contractorID && !s.Deleted).ToList();
                if (entities == null || entities.Count == 0) return false;

                foreach (var e in entities)
                    DeleteMark(e.RowID);

                SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                Logger.Error(contractorID, ex);
                throw ex;
            }
        }
    }
}
