﻿CREATE TABLE [dbo].[Project] (
    [ProjectID]   INT             IDENTITY (1, 1) NOT NULL,
    [ProjectCode] NVARCHAR (50)   NULL,
    [ProjectName] NVARCHAR (255)  NULL,
    [Note]        NVARCHAR (1000) NULL,
    [StatusID]    INT             CONSTRAINT [DF_Project_StatusID] DEFAULT ((0)) NOT NULL,
    [SortOrder]   INT             NULL,
    [Created]     DATETIME        NOT NULL,
    [CreatedBy]   NVARCHAR (50)   NULL,
    [Modified]    DATETIME        NULL,
    [ModifiedBy]  NVARCHAR (50)   NULL,
    [Deleted]     BIT             CONSTRAINT [DF_Project_Deleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Project] PRIMARY KEY CLUSTERED ([ProjectID] ASC)
);

