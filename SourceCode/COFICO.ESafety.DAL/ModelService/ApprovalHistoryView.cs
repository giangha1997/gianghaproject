﻿using COFICO.ESafety.DAL.DBModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace COFICO.ESafety.DAL.ServiceModels
{
    [Table("ApprovalHistoryView")]
    public class ApprovalHistoryView : ApprovalHistoryBase
    {
        public string FullName { get; set; }
    }
}
