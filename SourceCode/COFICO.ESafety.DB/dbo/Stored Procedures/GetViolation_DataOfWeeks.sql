﻿-- =============================================
-- Author:		HLinh
-- Create date: 2018/07/23
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetViolation_DataOfWeeks] 
	@ProjectID INT,
	@Month TINYINT,
	@Year SMALLINT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT [Week]
	FROM Violation t
		INNER JOIN ViolationDetail c ON (c.ViolationID = t.ViolationID AND c.Deleted = 0 AND t.Deleted = 0)
	WHERE t.ProjectID = @ProjectID AND t.[Month] = @Month AND t.[Year] = @Year
END