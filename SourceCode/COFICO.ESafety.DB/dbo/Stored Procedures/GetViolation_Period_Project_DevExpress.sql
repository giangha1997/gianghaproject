﻿-- =============================================
-- Author:		HLinh
-- Create date: 2018/09/11
-- Description:	Báo cáo tổng hợp - Thống kê vi phạm từ ngày tới ngày theo dự án
-- @ShowAllProject = 0: chỉ lấy những dự án có tình trạng = Đang thực hiện & Đang bảo trì
-- @ShowAllProject = 1: lấy tất cả những dự án bao gồm tình trạng = Kết thúc
-- bỏ 2 dòng lũy kế, lũy kế cộng dồn
-- =============================================
CREATE PROCEDURE [dbo].[GetViolation_Period_Project_DevExpress] 
	@FromDate SMALLDATETIME,
	@ToDate SMALLDATETIME,
	@ShowAllProject BIT
AS
BEGIN
	SET NOCOUNT ON;

	CREATE TABLE #Tmp
	(
		ViolationCategoryID INT,
		ViolationCode NVARCHAR(50),
		ViolationName NVARCHAR(500),
		ParentID INT,
		IsParent BIT,
		ProjectID INT,
		ProjectCode NVARCHAR(50),
		ProjectName NVARCHAR(255),
		Times INT,
		IsFooter BIT,
		IsThisWeek BIT,
		SortOrder INT,
		SortOrder_Proj INT
	)

	SELECT TOP 0 * INTO #Remaining FROM #Tmp

	--thống kê những dự án có nhà thầu vi phạm
	INSERT #Tmp (ViolationCategoryID, ParentID, IsParent, ProjectID, ProjectCode, ProjectName, Times, IsFooter, IsThisWeek)
		SELECT ViolationCategoryDetailID AS ViolationCategoryID, ViolationCategoryID, 0, ProjectID, ProjectCode, ProjectName, COUNT(ViolationCategoryDetailID) AS Times, 0, CAST(CASE WHEN PublicationDate >= @FromDate AND PublicationDate <= @ToDate THEN 1 ELSE 0 END AS BIT) AS IsThisWeek
		FROM ViolationAllProjectView
		WHERE PublicationDate BETWEEN @FromDate AND @ToDate
			  AND ((@ShowAllProject = 0 AND (StatusID = 1 OR StatusID = 2)) OR @ShowAllProject = 1)
		GROUP BY ViolationCategoryDetailID, ViolationCategoryID, ProjectID, ProjectCode, ProjectName, CAST(CASE WHEN PublicationDate >= @FromDate AND PublicationDate <= @ToDate THEN 1 ELSE 0 END AS BIT)
		
	--get remainings
	--lấy tất cả các dự án chưa có dữ liệu
	--DECLARE @ViolationCategoryID INT
	--DECLARE @ParentID INT
	--DECLARE @IsParent BIT
	--DECLARE @IsFooter BIT

	--DECLARE cursorA CURSOR FOR
	--	SELECT DISTINCT ViolationCategoryID, ParentID, IsParent, IsFooter FROM #Tmp
	--	UNION
	--	SELECT ViolationCategoryID, ParentID, IsParent, IsFooter FROM ViolationCategory WHERE Deleted = 0 AND IsActive = 1 AND (IsFooter = 0 OR (IsFooter = 1 AND FooterType = @FooterType))
	--OPEN cursorA
	--FETCH NEXT FROM cursorA INTO @ViolationCategoryID, @ParentID, @IsParent, @IsFooter
	--WHILE @@FETCH_STATUS = 0
	--BEGIN
	--	INSERT INTO #Remaining (ViolationCategoryID, ParentID, IsParent, ProjectID, ProjectCode, ProjectName, IsFooter, IsThisWeek)
	--		SELECT	@ViolationCategoryID, @ParentID, @IsParent, ProjectID, ProjectCode, ProjectName, @IsFooter, 1
	--		FROM Project c
	--		WHERE Deleted = 0 AND (StatusID = 1 OR StatusID = 2)
	--			  AND NOT EXISTS (SELECT TOP 1 1 FROM #Tmp t1 WHERE c.ProjectID = t1.ProjectID AND t1.ViolationCategoryID = @ViolationCategoryID AND t1.IsThisWeek = 1)

	--	FETCH NEXT FROM cursorA INTO @ViolationCategoryID, @ParentID, @IsParent, @IsFooter
	--END
	
	--CLOSE cursorA
	--DEALLOCATE cursorA
	--lấy tất cả các dự án chưa có dữ liệu - end


	--bổ sung những dự án còn thiếu và những mục vi phạm còn thiếu
	DECLARE @ProjectID INT
	DECLARE @ProjectCode NVARCHAR(50)
	DECLARE @ProjectName NVARCHAR(255)

	DECLARE cursorA CURSOR FOR
		SELECT DISTINCT ProjectID, ProjectCode, ProjectName FROM #Tmp
	OPEN cursorA
	FETCH NEXT FROM cursorA INTO @ProjectID, @projectCode, @ProjectName
	WHILE @@FETCH_STATUS = 0
	BEGIN
		INSERT INTO #Remaining (ViolationCategoryID, ParentID, IsParent, ProjectID, ProjectCode, ProjectName, IsFooter, IsThisWeek)
			SELECT	ViolationCategoryID, ParentID, IsParent, @ProjectID, @ProjectCode, @ProjectName, IsFooter, 1
			FROM ViolationCategory c
			WHERE Deleted = 0 AND IsActive = 1 AND (IsFooter = 0 OR (IsFooter = 1 AND FooterType = 2 AND ViolationCode NOT IN ('_COFICO_CATEGORY_F2', '_COFICO_CATEGORY_F3')))
				  AND NOT EXISTS (SELECT TOP 1 1 FROM #Tmp t1 WHERE t1.ProjectID = @ProjectID AND t1.ViolationCategoryID = c.ViolationCategoryID AND t1.IsThisWeek = 1)

		FETCH NEXT FROM cursorA INTO @ProjectID, @projectCode, @ProjectName
	END
	
	CLOSE cursorA
	DEALLOCATE cursorA
	

	--merge data
	INSERT INTO #Tmp(ViolationCategoryID, ViolationCode, ParentID, IsParent, ProjectID, ProjectCode, ProjectName, Times, IsFooter, IsThisWeek)
		SELECT ViolationCategoryID, ViolationCode, ParentID, IsParent, ProjectID, ProjectCode, ProjectName, Times, IsFooter, IsThisWeek 
		FROM #Remaining
		
	--update parent's data
	UPDATE #Tmp
	SET Times = (SELECT SUM(ISNULL(Times, 0)) FROM #Tmp t WHERE t.ParentID = #Tmp.ViolationCategoryID AND t.ProjectID = #Tmp.ProjectID AND t.IsThisWeek = 1)
	WHERE IsParent = 1
	
	--add ViolationCode & ViolationName
	DELETE FROM #Remaining

	INSERT #Remaining (ViolationCategoryID, ViolationCode, ViolationName, ProjectID, ProjectCode, ProjectName, Times, IsFooter, ParentID, IsParent, SortOrder, IsThisWeek, SortOrder_Proj)
		SELECT t.ViolationCategoryID, c.ViolationCode, c.ViolationName, ProjectID, ProjectCode, ProjectName, Times, t.IsFooter, t.ParentID, t.IsParent, c.SortOrder, IsThisWeek, ROW_NUMBER() OVER (PARTITION BY t.ViolationCategoryID ORDER BY t.ViolationCategoryID, ProjectName)
		FROM #Tmp t
			INNER JOIN ViolationCategory c ON (c.ViolationCategoryID = t.ViolationCategoryID)


	--update footer
	----update this week
	UPDATE #Remaining
	SET Times = (SELECT SUM(ISNULL(Times, 0)) FROM #Remaining t WHERE t.ProjectID = #Remaining.ProjectID AND IsParent = 1 AND IsFooter = 0 AND t.IsThisWeek = 1)
	WHERE IsFooter = 1 AND ViolationCode = '_COFICO_CATEGORY_F1'
	
	------update last week
	--UPDATE #Remaining
	--SET Times = (SELECT SUM(ISNULL(Times, 0)) FROM #Remaining t WHERE t.ProjectID = #Remaining.ProjectID AND IsFooter = 0 AND t.IsThisWeek = 0)
	--WHERE IsFooter = 1 AND ViolationCode = '_COFICO_CATEGORY_F2'
	
	----update Total
	--UPDATE #Remaining
	--SET Times = (SELECT SUM(ISNULL(Times, 0)) FROM #Remaining t WHERE t.ProjectID = #Remaining.ProjectID AND IsFooter = 1 AND ViolationCode <> '_COFICO_CATEGORY_F3')
	--WHERE IsFooter = 1 AND ViolationCode = '_COFICO_CATEGORY_F3'

	UPDATE #Remaining SET Times = NULL WHERE Times = 0
		
	--Set SortOrder của Footer luôn lớn hơn Danh mục
	DECLARE @Max INT
	SELECT @Max = MAX(SortOrder) FROM #Remaining WHERE IsFooter = 0

	UPDATE #Remaining SET SortOrder = SortOrder + @Max WHERE IsFooter = 1

	--show data
	SELECT * FROM #Remaining WHERE ParentID IS NULL ORDER BY IsFooter, SortOrder, ViolationName, ProjectName

	DROP TABLE #Tmp
	DROP TABLE #Remaining
END