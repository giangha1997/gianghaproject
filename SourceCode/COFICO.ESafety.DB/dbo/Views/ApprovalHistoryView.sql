﻿


CREATE View [dbo].[ApprovalHistoryView] As

SELECT ah.*, su.FullName
FROM ApprovalHistory ah
	INNER JOIN SecurityUsers su ON su.UserName = ah.UserName