﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace COFICO.ESafety.DAL.DBModel
{
    [Table("Configuration")]
    public class Configuration
    {
        [Key]
        [StringLength(50)]
        public string CfgKey { get; set; }
        
        [StringLength(500)]
        public string Description { get; set; }
        
        [StringLength(500)]
        public string CfgValue { get; set; }

        public DateTime? Created { get; set; }
        
        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime? Modified { get; set; }
        
        [StringLength(50)]
        public string ModifiedBy { get; set; }

        public bool? Deleted { get; set; }
    }
}
