﻿using System;
using System.Collections.Generic;
using System.Linq;
using COFICO.ESafety.Base;
using COFICO.ESafety.DAL.DBModel;
using COFICO.ESafety.DAL.ServiceModels;

namespace COFICO.ESafety.DAL.Services
{
    public class ProjectContractorTeamService : GenericService<Project_Contractor_Team>
    {
        public List<ProjectContractorTeamView> GetTeams(int projectID, int contractorID)
        {
            try
            {
                return _db.ProjectContractorTeamViews.Where(s => s.ProjectID == projectID && s.ContractorID == contractorID).OrderBy(s => s.SortOrder).ThenBy(s => s.TeamName).ToList();
            }
            catch (Exception ex)
            {
                Logger.Error(contractorID, ex);
                throw ex;
            }
        }

        public List<ProjectContractorTeamView> GetTeamByProject(int projectID)
        {
            try
            {
                return _db.ProjectContractorTeamViews.Where(s => s.ProjectID == projectID).ToList();
            }
            catch (Exception ex)
            {
                Logger.Error(projectID, ex);
                throw ex;
            }
        }

        public int? Update(Project_Contractor_Team dto)
        {
            if (dto == null) return null;

            int? res = null;
            try
            {
                if (dto.RowID == 0)
                {
                    if (_table.Any(s => s.ProjectContractorID == dto.ProjectContractorID && s.TeamID == dto.TeamID && !s.Deleted))
                        return -1;

                    InsertObj(ref dto);
                    SaveChanges();
                    res = dto.RowID;
                }
                else if (dto.RowID < 0)
                {
                    DeleteMark(Math.Abs(dto.RowID));
                    SaveChanges();
                    res = Math.Abs(dto.RowID);
                }

                return res;
            }
            catch (Exception ex)
            {
                Logger.Error(new { dto.ProjectContractorID, dto.TeamID }, ex);
                throw ex;
            }
        }

        //public bool InsertDelete(List<ProjectContractorTeamView> lst)
        //{
        //    if (lst == null || lst.Count == 0) return false;

        //    try
        //    {
        //        var pID = lst[0].ProjectID;
        //        var cID = lst[0].ContractorID;

        //        var pc = _db.ProjectContractorViews.FirstOrDefault(s => s.ProjectID == pID && s.ContractorID == cID);
        //        if (pc == null) return false;

        //        var entities = _table.Where(s => s.ProjectContractorID == pc.RowID && !s.Deleted).ToList();

        //        //remove
        //        foreach (var p in entities)
        //        {
        //            if (!lst.Any(s => s.TeamID == p.TeamID))
        //                DeleteMark(p.RowID);
        //        }

        //        //insert
        //        for (int i = 0; i < lst.Count; i++)
        //        {
        //            var p = lst[i];
        //            if (!entities.Any(s => s.TeamID == p.TeamID))
        //            {
        //                var obj = new Project_Contractor_Team
        //                {
        //                    ProjectContractorID = pc.RowID,
        //                    TeamID = p.TeamID,
        //                    Deleted = false
        //                };
        //                InsertObj(ref obj);
        //            }
        //        }

        //        SaveChanges();
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error(lst, ex);
        //        throw ex;
        //    }
        //}

        public bool DeleteByProject(int projectID)
        {
            if (projectID <= 0) return false;

            try
            {
                var pc = _db.ProjectContractorViews.FirstOrDefault(s => s.ProjectID == projectID);
                if (pc == null) return false;

                var entities = _table.Where(s => s.ProjectContractorID == pc.RowID && !s.Deleted).ToList();
                if (entities == null || entities.Count == 0) return false;

                foreach (var e in entities)
                    DeleteMark(e.RowID);

                SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                Logger.Error(projectID, ex);
                throw ex;
            }
        }

        public bool DeleteByContractor(int contractorID)
        {
            if (contractorID <= 0) return false;

            try
            {
                var pc = _db.ProjectContractorViews.FirstOrDefault(s => s.ContractorID == contractorID);
                if (pc == null) return false;

                var entities = _table.Where(s => s.ProjectContractorID == pc.RowID && !s.Deleted).ToList();
                if (entities == null || entities.Count == 0) return false;

                foreach (var e in entities)
                    DeleteMark(e.RowID);

                SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                Logger.Error(contractorID, ex);
                throw ex;
            }
        }
    }
}
