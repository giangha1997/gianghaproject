﻿CREATE TABLE [dbo].[Contractor_Team] (
    [RowID]        INT           IDENTITY (1, 1) NOT NULL,
    [ContractorID] INT           NOT NULL,
    [TeamID]       INT           NOT NULL,
    [Created]      DATETIME      NULL,
    [CreatedBy]    NVARCHAR (50) NULL,
    [Modified]     DATETIME      NULL,
    [ModifiedBy]   NVARCHAR (50) NULL,
    [Deleted]      BIT           CONSTRAINT [DF_Contractor_Team_Deleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Contractor_Team] PRIMARY KEY CLUSTERED ([RowID] ASC)
);

