﻿-- =============================================
-- Author:		HLinh
-- Create date: 2018/09/12
-- Description:	Báo cáo tổng hợp - Thống kê TBM từ ngày tới ngày của 1 dự án theo nhà thầu
-- bao gồm các nhà thầu hiện tại có trong dự án & các nhà thầu đã bị remove khỏi dự án
-- bỏ 2 dòng lũy kế, lũy kế cộng dồn
-- =============================================
CREATE PROCEDURE [dbo].[GetTBM_HSE_Period_Contractor] 
	@ProjectID INT,
	@FromDate SMALLDATETIME,
	@ToDate SMALLDATETIME
AS
BEGIN
	SET NOCOUNT ON;

	CREATE TABLE #TBMs
	(
		ProjectID INT,
		ProjectCode NVARCHAR(50),
		ProjectName NVARCHAR(255),
		ContractorID INT,
		ContractorCode NVARCHAR(50),
		ContractorName NVARCHAR(255),
		CatGroupCode NVARCHAR(50),
		DataType VARCHAR(10),
		TBMCategoryID INT,
		TBMCode NVARCHAR(50),
		TBMName NVARCHAR(255),
		Total DECIMAL(9,2),	
		SortOrder_Cont INT,
		SortOrder_TBMCat INT,
		ThisWeek_C1 DECIMAL(9,2), --use in report
		ThisWeek_C2 DECIMAL(9,2),
		ThisWeek_C3 DECIMAL(9,2)
	)

	SELECT TOP 0 * INTO #TBMs1 FROM #TBMs


	-- nhập liệu HSE, chỉ lấy nhà thầu có trong dự án
	INSERT INTO #TBMs(ProjectID, ProjectCode, ProjectName, ContractorID, ContractorCode, ContractorName, CatGroupCode, DataType, TBMCategoryID, TBMCode, TBMName, SortOrder_Cont, SortOrder_TBMCat, Total)
		SELECT	ProjectID, ProjectCode, ProjectName, ContractorID, ContractorCode, ContractorName, CatGroupCode, a.DataType, a.TBMCategoryID, TBMCode, TBMName, SortOrder_Cont, SortOrder_TBMCat, SUM(ISNULL(NumOfEmployees, 0))
		FROM TBMAllContractorUnpivotView a
			LEFT JOIN TBMCategory t ON (t.TBMCategoryID = a.TBMCategoryID AND t.IsTotalColumn = 0)
		WHERE ProjectID = @ProjectID AND ([Date] BETWEEN @FromDate AND @ToDate)
		GROUP BY ProjectID, ProjectCode, ProjectName, ContractorID, ContractorCode, ContractorName, CatGroupCode, a.DataType, a.TBMCategoryID, TBMCode, TBMName, SortOrder_Cont, SortOrder_TBMCat

	
	--bổ sung những dự án còn thiếu và những danh mục còn thiếu
	INSERT INTO #TBMs1(ProjectID, ProjectCode, ProjectName, ContractorID, ContractorCode, ContractorName, CatGroupCode, DataType, TBMCategoryID, TBMCode, TBMName, SortOrder_Cont, SortOrder_TBMCat)
		SELECT DISTINCT @ProjectID, p.ProjectCode, p.ProjectName, c.ContractorID, c.ContractorCode, c.ContractorName, GroupCode, t.DataType, t.TBMCategoryID, TBMCode, t.TBMName, c.SortOrder, t.SortOrder
		FROM Contractor c
			INNER JOIN (
				SELECT DISTINCT ContractorID, ProjectID FROM #TBMs1

				UNION

				--lấy những nhà thầu có trong dự án
				SELECT DISTINCT c.ContractorID, @ProjectID AS ProjectID
				FROM Contractor c
					INNER JOIN Project_Contractor pc ON (pc.ContractorID = c.ContractorID AND pc.ProjectID = @ProjectID AND c.Deleted = 0 AND pc.Deleted = 0 AND c.IsActive = 1)

				UNION 

				--lấy những nhà thầu đã từng có trong dự án và có dữ liệu
				SELECT DISTINCT c.ContractorID, @ProjectID AS ProjectID
				FROM Contractor c
					INNER JOIN Project_Contractor pc ON (pc.ContractorID = c.ContractorID AND pc.ProjectID = @ProjectID AND c.Deleted = 0 AND c.IsActive = 1)
					INNER JOIN TBM t ON (t.ContractorID = c.ContractorID AND t.ProjectID = @ProjectID AND t.Deleted = 0)
					INNER JOIN TBMDetail d ON (d.TBMID = t.TBMID AND d.Deleted = 0 AND ISNULL(d.NumOfEmployees, 0) > 0 AND (d.[Date] BETWEEN @FromDate AND @ToDate))
			) pc ON (pc.ContractorID = c.ContractorID)
			RIGHT JOIN TBMCategory t ON (t.Deleted = 0 AND t.IsActive = 1 AND t.IsTotalColumn = 0)
			INNER JOIN Project p ON (p.ProjectID = pc.ProjectID AND p.Deleted = 0 AND (p.StatusID = 1 OR p.StatusID = 2))
		WHERE NOT EXISTS (SELECT TOP 1 1 FROM #TBMs k WHERE c.ContractorID = k.ContractorID AND t.TBMCategoryID = k.TBMCategoryID)
	
	-- merge data
	INSERT INTO #TBMs(ProjectID, ProjectCode, ProjectName, ContractorID, ContractorCode, ContractorName, CatGroupCode, DataType, TBMCategoryID, TBMCode, TBMName, Total, SortOrder_Cont, SortOrder_TBMCat)
		SELECT ProjectID, ProjectCode, ProjectName, ContractorID, ContractorCode, ContractorName, CatGroupCode, DataType, TBMCategoryID, TBMCode, TBMName, Total, SortOrder_Cont, SortOrder_TBMCat
		FROM #TBMs1
	
	DELETE FROM #TBMs1

	DECLARE @MaxSortOrder INT
	SELECT @MaxSortOrder = MAX(SortOrder_Cont) FROM #TBMs
	SET @MaxSortOrder = ISNULL(@MaxSortOrder, 0) + 1

	-- add footer rows
	INSERT INTO #TBMs1(ProjectID, ProjectCode, ProjectName, ContractorID, ContractorCode, ContractorName, CatGroupCode, DataType, TBMCategoryID, TBMCode, TBMName, SortOrder_Cont, SortOrder_TBMCat, Total)
		SELECT @ProjectID, NULL, NULL, -1, N'Tổng cộng', N'Tổng cộng', GroupCode, DataType, TBMCategoryID, TBMCode, TBMName, @MaxSortOrder, SortOrder,
				(SELECT SUM(ISNULL(Total, 0)) FROM #TBMs m WHERE m.TBMCategoryID = t.TBMCategoryID)
		FROM TBMCategory t
		WHERE Deleted = 0 AND IsActive = 1 AND IsTotalColumn = 0

	UPDATE #TBMs1
	SET Total = CASE WHEN Total = 0 THEN NULL ELSE Total END
	WHERE ContractorID = -1

	--merge data
	INSERT INTO #TBMs1(ProjectID, ProjectCode, ProjectName, ContractorID, ContractorCode, ContractorName, CatGroupCode, DataType, TBMCategoryID, TBMCode, TBMName, SortOrder_Cont, SortOrder_TBMCat, Total)
		SELECT ProjectID, ProjectCode, ProjectName, ContractorID, ContractorCode, ContractorName, CatGroupCode, DataType, TBMCategoryID, TBMCode, TBMName, SortOrder_Cont, SortOrder_TBMCat, Total
		FROM #TBMs

	--update rate
	UPDATE #TBMs1 SET Total = NULL WHERE Total = 0
	
	UPDATE #TBMs1
	SET Total = (SELECT TOP 1 ISNULL(Total, 0) FROM #TBMs1 t WHERE t.ContractorID = #TBMs1.ContractorID AND t.TBMCategoryID = 2) / (SELECT TOP 1 ISNULL(Total, 1) FROM #TBMs1 t WHERE t.ContractorID = #TBMs1.ContractorID AND t.TBMCategoryID = 1) * 100
	WHERE TBMCategoryID = 3

	UPDATE #TBMs1
	SET Total = (SELECT TOP 1 ISNULL(Total, 0) FROM #TBMs1 t WHERE t.ContractorID = #TBMs1.ContractorID AND t.TBMCategoryID = 5) / (SELECT TOP 1 ISNULL(Total, 1) FROM #TBMs1 t WHERE t.ContractorID = #TBMs1.ContractorID AND t.TBMCategoryID = 4) * 100
	WHERE TBMCategoryID = 6

	--update rate if TBMCategoryID = 3 or 6 has total = null or 0
	UPDATE #TBMs1 SET Total = 0 WHERE TBMCategoryID = 3 AND EXISTS (SELECT TOP 1 1 FROM #TBMs1 r WHERE r.ContractorID = #TBMs1.ContractorID AND r.TBMCategoryID = #TBMs1.TBMCategoryID AND r.TBMCategoryID = 1 AND ISNULL(r.Total, 0) = 0)

	UPDATE #TBMs1 SET Total = 0 WHERE TBMCategoryID = 6 AND EXISTS (SELECT TOP 1 1 FROM #TBMs1 r WHERE r.ContractorID = #TBMs1.ContractorID AND r.TBMCategoryID = #TBMs1.TBMCategoryID AND r.TBMCategoryID = 4 AND ISNULL(r.Total, 0) = 0)
	
	--update for report
	UPDATE #TBMs1 SET ThisWeek_C1 = ( SELECT SUM(ISNULL(Total, 0)) FROM #TBMs1 t WHERE (t.TBMCategoryID = 1 OR t.TBMCategoryID = 4) AND t.ContractorID = #TBMs1.ContractorID)
	UPDATE #TBMs1 SET ThisWeek_C2 = ( SELECT SUM(ISNULL(Total, 0)) FROM #TBMs1 t WHERE (t.TBMCategoryID = 2 OR t.TBMCategoryID = 5) AND t.ContractorID = #TBMs1.ContractorID)
	UPDATE #TBMs1 SET ThisWeek_C3 = CASE WHEN ISNULL(ThisWeek_C1, 0) <> 0 THEN ( ISNULL(ThisWeek_C2, 0) / ISNULL(ThisWeek_C1, 0) * 100 ) ELSE 0 END
	--update for report - end

	UPDATE #TBMs1 
	SET Total = CASE WHEN ISNULL(Total, 0) = 0 THEN NULL ELSE Total END,
		ThisWeek_C1 = CASE WHEN ISNULL(ThisWeek_C1, 0) = 0 THEN NULL ELSE ThisWeek_C1 END,
		ThisWeek_C2 = CASE WHEN ISNULL(ThisWeek_C2, 0) = 0 THEN NULL ELSE ThisWeek_C2 END,
		ThisWeek_C3 = CASE WHEN ISNULL(ThisWeek_C3, 0) = 0 THEN NULL ELSE ThisWeek_C3 END

	--show data
	SELECT * FROM #TBMs1 ORDER BY SortOrder_Cont, ContractorName, SortOrder_TBMCat, TBMName

	DROP TABLE #TBMs1
	DROP TABLE #TBMs
END