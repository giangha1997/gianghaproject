﻿using System;
using System.Collections.Generic;
using System.Linq;
using COFICO.ESafety.DAL.ServiceModels;
using COFICO.ESafety.Base;

namespace COFICO.ESafety.DAL.Services
{
    public class UserService : GenericService<UserView>
    {
        public List<UserView> GetUsers()
        {
            List<UserView> lst = null;
            try
            {
                lst = _db.UserViews.ToList();
                return lst;
            }
            catch (Exception ex)
            {
                Logger.Error("GetUsers", ex);
                throw ex;
            }
        }

        public List<UserView> GetActiveUsers()
        {
            List<UserView> lst = null;
            try
            {
                lst = _db.UserViews.Where(s => !s.LockoutEnabled).ToList();
                return lst;
            }
            catch (Exception ex)
            {
                Logger.Error("GetActiveUsers", ex);
                throw ex;
            }
        }

        public UserView GetUser(string username)
        {
            if (string.IsNullOrWhiteSpace(username)) return null;

            try
            {
                return _db.UserViews.FirstOrDefault(s => s.UserName == username);
            }
            catch (Exception ex)
            {
                Logger.Error("GetUsers", ex);
                throw ex;
            }
        }
    }
}
