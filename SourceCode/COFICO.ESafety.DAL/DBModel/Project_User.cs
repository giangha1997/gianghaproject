﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace COFICO.ESafety.DAL.DBModel
{
    [Table("Project_User")]
    public class Project_User
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int RowID { get; set; }

        public string Username { get; set; }

        //ProjectID or ProjContTeamID
        public int? ObjectID { get; set; }

        //1: nhan vien; 2: ki su; 3: approver
        public byte? Type { get; set; }

        ////1: Project; 2: Project_Contractor_Team
        //public byte? UsedFor { get; set; }

        [JsonIgnore]
        public DateTime Created { get; set; }

        [JsonIgnore]
        [StringLength(100)]
        public string CreatedBy { get; set; }

        [JsonIgnore]
        public DateTime? Modified { get; set; }

        [JsonIgnore]
        [StringLength(100)]
        public string ModifiedBy { get; set; }

        public bool Deleted { get; set; }
    }

}
