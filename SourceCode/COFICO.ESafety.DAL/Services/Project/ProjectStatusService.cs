﻿using System;
using System.Collections.Generic;
using System.Linq;
using COFICO.ESafety.Base;
using COFICO.ESafety.DAL.DBModel;

namespace COFICO.ESafety.DAL.Services
{
    public class ProjectStatusService : GenericService<ProjectStatus>
    {
        public List<ProjectStatus> GetProjectStatus()
        {
            try
            {
                return _table.ToList();
            }
            catch (Exception ex)
            {
                Logger.Error("GetProjectStatus", ex);
                throw ex;
            }
        }
    }
}
