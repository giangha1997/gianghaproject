﻿-- =============================================
-- Author:		HLinh
-- Create date: 2018/07/13
-- Description:	Report 02 - Thống kê Huấn luyện an toàn trong tuần theo dự án
-- bao gồm những dự án có tình trạng = Đang thực hiện & Đang bảo trì
-- =============================================
CREATE PROCEDURE [dbo].[GetTrainingHSE_Week_Project_DevExpress] 
	@Month TINYINT,
	@Year SMALLINT,
	@Week TINYINT,
	@PrevWeek TINYINT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Date SMALLDATETIME = dbo.GetFirstDateOfWeek(@Month, @Year, @Week)
	DECLARE @PreYear SMALLINT = @Year
	DECLARE @PreMonth TINYINT = @Month

	IF @PrevWeek > @Week
		SET @PreMonth = @Month - 1

	IF @PreMonth <= 0
	BEGIN
		SET @PreMonth = 12
		SET @PreYear = @PreYear - 1
	END

	CREATE TABLE #Tmp
	(
		TrainingCategoryID INT,
		TrainingCode NVARCHAR(50),
		TrainingName NVARCHAR(255),
		ProjectID INT,
		ProjectCode NVARCHAR(50),
		ProjectName NVARCHAR(255),
		NumOfEmployees DECIMAL(9,2),
		IsThisWeek BIT,
		ThisWeek DECIMAL(9,2),
		LastWeek DECIMAL(9,2),
		Total DECIMAL(9,2)
	)

	SELECT TOP 0 * INTO #Remaining FROM #Tmp

	--thống kê Huấn luyện an toàn
	INSERT #Tmp (TrainingCategoryID, ProjectID, ProjectCode, ProjectName, NumOfEmployees, IsThisWeek)
		SELECT d.TrainingCategoryID, t.ProjectID, ProjectCode, ProjectName, SUM(NumOfEmployees) AS NumOfEmployees, CAST(CASE WHEN [Week] = @Week THEN 1 ELSE 0 END AS BIT) AS IsThisWeek
		FROM Training t 
			INNER JOIN TrainingDetail d ON (t.TrainingID =  d.TrainingID AND t.Deleted = 0 AND d.Deleted = 0)
			INNER JOIN Project p ON (t.ProjectID = p.ProjectID AND p.Deleted = 0 AND (p.StatusID = 1 OR p.StatusID = 2))
			INNER JOIN TrainingCategory c ON (c.TrainingCategoryID = d.TrainingCategoryID AND c.Deleted = 0 AND c.IsActive = 1)
		WHERE ( ([Month] = @Month AND [Year] = @Year AND [Week] = @Week) OR ([Month] = @PreMonth AND [Year] = @PreYear AND [Week] = @PrevWeek) )
		GROUP BY d.TrainingCategoryID, t.ProjectID, ProjectCode, ProjectName, CAST(CASE WHEN [Week] = @Week THEN 1 ELSE 0 END AS BIT)

	--bổ sung những dự án còn thiếu và những danh mục còn thiếu
	DECLARE @TrainingCategoryID INT

	DECLARE cursorA CURSOR FOR
		SELECT DISTINCT TrainingCategoryID FROM #Tmp
		UNION
		SELECT TrainingCategoryID FROM TrainingCategory WHERE Deleted = 0 AND IsActive = 1
	OPEN cursorA
	FETCH NEXT FROM cursorA INTO @TrainingCategoryID
	WHILE @@FETCH_STATUS = 0
	BEGIN
		INSERT INTO #Remaining(TrainingCategoryID, ProjectID, ProjectCode, ProjectName, IsThisWeek)
			SELECT	@TrainingCategoryID, ProjectID, ProjectCode, ProjectName, 1
			FROM Project c
			WHERE Deleted = 0 AND (StatusID = 1 OR StatusID = 2)
				  AND NOT EXISTS (SELECT TOP 1 1 FROM #Tmp t1 WHERE c.ProjectID = t1.ProjectID AND t1.TrainingCategoryID = @TrainingCategoryID AND t1.IsThisWeek = 1)

		FETCH NEXT FROM cursorA INTO @TrainingCategoryID
	END

	CLOSE cursorA
	DEALLOCATE cursorA

	--merge data
	INSERT INTO #Tmp(TrainingCategoryID, ProjectID, ProjectCode, ProjectName, NumOfEmployees, IsThisWeek)
		SELECT TrainingCategoryID, ProjectID, ProjectCode, ProjectName, NumOfEmployees, IsThisWeek 
		FROM #Remaining

	--update ThisWeek
	UPDATE #Tmp
	SET ThisWeek = (SELECT SUM(ISNULL(NumOfEmployees, 0)) FROM #Tmp t WHERE t.TrainingCategoryID = #Tmp.TrainingCategoryID AND t.IsThisWeek = 1)

	UPDATE #Tmp SET ThisWeek = NULL WHERE ThisWeek = 0

	--update LastWeek
	UPDATE #Tmp
	SET LastWeek = (SELECT SUM(ISNULL(NumOfEmployees, 0)) FROM #Tmp t WHERE t.TrainingCategoryID = #Tmp.TrainingCategoryID AND t.IsThisWeek = 0)

	UPDATE #Tmp SET LastWeek = NULL WHERE LastWeek = 0

	--update Total
	UPDATE #Tmp
	SET Total = ISNULL(ThisWeek, 0) + ISNULL(LastWeek, 0)

	UPDATE #Tmp SET Total = NULL WHERE Total = 0


	--show data
	SELECT a1.TrainingCategoryID, c.TrainingCode, c.TrainingName, ProjectID, ProjectCode, ProjectName, NumOfEmployees, ThisWeek, LastWeek, Total, c.SortOrder
	FROM #Tmp a1
		INNER JOIN TrainingCategory c ON (c.TrainingCategoryID = a1.TrainingCategoryID)
	WHERE IsThisWeek = 1
	ORDER BY c.SortOrder, c.TrainingName, ProjectName

	DROP TABLE #Remaining
END