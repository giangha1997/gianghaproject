﻿


CREATE VIEW [dbo].[SecurityUserPermissionsFeaturesView] AS
	Select p.[RowId], p.[Username], p.[View], p.[Add], p.[Edit], p.[Delete], p.[Approval], p.[Note], p.[Created], p.[CreatedBy], p.[Modified], p.[ModifiedBy], 
			p.[SecurityFeatureId], f.[FeatureCode], f.[FeatureName]
	From [dbo].[SecurityUserPermissions] p 
		Left Join [dbo].[SecurityFeatures] f on f.RowId = p.[SecurityFeatureId] 
	Where (f.Deleted Is Null Or f.Deleted = 0) And (p.Deleted Is Null Or p.Deleted = 0)