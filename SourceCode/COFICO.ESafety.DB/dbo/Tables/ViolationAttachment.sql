﻿CREATE TABLE [dbo].[ViolationAttachment] (
    [ViolationAttachmentID] INT            IDENTITY (1, 1) NOT NULL,
    [ViolationDetailID]     INT            NOT NULL,
    [Filename]              NVARCHAR (255) NULL,
    [FilenameOrig]          NVARCHAR (255) NULL,
    [Note]                  NVARCHAR (500) NULL,
    [Created]               DATETIME       NULL,
    [CreatedBy]             NVARCHAR (50)  NULL,
    [Modified]              DATETIME       NULL,
    [ModifiedBy]            NVARCHAR (50)  NULL,
    [Deleted]               BIT            CONSTRAINT [DF_ViolationAttachment_Deleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_ViolationAttachment] PRIMARY KEY CLUSTERED ([ViolationAttachmentID] ASC)
);

