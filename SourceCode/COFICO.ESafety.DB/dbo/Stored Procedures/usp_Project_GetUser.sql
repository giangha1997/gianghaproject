﻿-- =============================================
-- Author:		HLinh
-- Create date: 2018/06/06
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_Project_GetUser] 
	@UserName NVARCHAR(100)
AS
BEGIN
	SET NOCOUNT ON;

	declare @i int

	--while (1 = 1)
	--begin
	--	set @i = CHARINDEX('|', @UserName)
	--	if @i > 0
	--		set @UserName  = STUFF(@UserName, 1, @i, '')
	--	else
	--		break
	--end

	--IF EXISTS(SELECT TOP 1 RowID FROM UserAdminGroup WHERE UserName = @UserName)
	IF EXISTS(	SELECT TOP 1 1 
				FROM SecurityUserGroups ug
					INNER JOIN SecurityGroups g ON (g.RowId = ug.SecurityGroupId AND g.ShowAllProject = 1 AND ISNULL(g.Deleted, 0) = 0 AND ISNULL(ug.Deleted, 0) = 0)
					INNER JOIN SecurityUsers su ON su.RowId = ug.SecurityUserId
					INNER JOIN AspNetUsers u ON (u.UserName = su.UserName AND u.LockoutEnabled = 0)
				WHERE u.UserName = @UserName)
		SELECT *, ProjectID AS ID FROM ProjectView WHERE StatusID IN (1, 2)
		ORDER BY SortOrder, ProjectCode
	ELSE
		SELECT DISTINCT p.*, p.ProjectID AS ID
		FROM ProjectView p
			INNER JOIN Project_User u ON (u.ObjectID = p.ProjectID /*AND u.UsedFor = 1*/ AND u.Deleted = 0 AND p.StatusID IN (1, 2))
		WHERE u.UserName = @UserName
		ORDER BY SortOrder, ProjectCode
END