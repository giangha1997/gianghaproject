﻿using COFICO.ESafety.AppCode;
using COFICO.ESafety.DAL.DBModel;
using COFICO.ESafety.DAL.Services;
using System.Collections.Generic;
using System.Web.Http;

namespace COFICO.ESafety
{
    //[Authorize]
    [RoutePrefix("api/Team")]
    public class TeamController : ApiController
    {
        [HttpGet, Route("Items")]
        public IHttpActionResult Items()
        {
            var items = new List<Team>();
            using (var svc = new TeamService())
            {
                items = svc.GetTeams();
            }
            return Ok(new { items });
        }

        [HttpPut]
        public IHttpActionResult Put(Team item)
        {
            var items = new Response<object>
            {
                IsError = true,
            };

            int? rs = null;
            using (var svc = new TeamService())
            {
                rs = svc.Update(item);
                if (rs != null)
                {
                    if (rs == -1)
                        items.IsDuplicate = true;
                    else if (rs > 0)
                    {
                        items.IsError = false;
                        items.Id = rs;
                    }
                }
            }
            return Ok(new { items });
        }

        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            var items = new Response<object>
            {
                IsError = true,
            };

            bool? rs = false;
            using (var svc = new TeamService())
            {
                rs = svc.Delete(id);
                if (rs == true)
                {
                    items.IsError = false;
                }
            }
            return Ok(new { items });
        }
    }
}