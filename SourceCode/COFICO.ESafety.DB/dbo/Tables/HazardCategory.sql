﻿CREATE TABLE [dbo].[HazardCategory] (
    [HazardCategoryID] INT             IDENTITY (1, 1) NOT NULL,
    [HazardCode]       NVARCHAR (50)   NULL,
    [HazardName]       NVARCHAR (255)  NULL,
    [ParentID]         INT             NULL,
    [IsParent]         BIT             CONSTRAINT [DF_HazardCategory_IsParent] DEFAULT ((0)) NOT NULL,
    [IsActive]         BIT             CONSTRAINT [DF_HazardCategory_IsActive] DEFAULT ((0)) NOT NULL,
    [SortOrder]        INT             NULL,
    [Note]             NVARCHAR (1000) NULL,
    [Created]          DATETIME        NULL,
    [CreatedBy]        NVARCHAR (50)   NULL,
    [Modified]         DATETIME        NULL,
    [ModifiedBy]       NVARCHAR (50)   NULL,
    [Deleted]          BIT             CONSTRAINT [DF_HazardCategory_Deleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_HazardCategory] PRIMARY KEY CLUSTERED ([HazardCategoryID] ASC)
);

