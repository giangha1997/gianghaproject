﻿-- =============================================
-- Author:		HLinh
-- Create date: 2018/09/11
-- Description:	Báo cáo tổng hợp - Thống kê vi phạm từ ngày tới ngày của 1 dự án theo nhà thầu
-- bao gồm các nhà thầu hiện tại có trong dự án & các nhà thầu có vi phạm đã bị remove khỏi dự án
-- bỏ 2 dòng lũy kế, lũy kế cộng dồn
-- =============================================
CREATE PROCEDURE [dbo].[GetViolation_Period_Contractor_DevExpress] 
	@ProjectID INT,
	@FromDate SMALLDATETIME,
	@ToDate SMALLDATETIME
AS
BEGIN
	SET NOCOUNT ON;

	CREATE TABLE #Tmp
	(
		ViolationCategoryID INT,
		ViolationCode NVARCHAR(50),
		ViolationName NVARCHAR(500),
		ParentID INT,
		IsParent BIT,
		ContractorID INT,
		ContractorCode NVARCHAR(50),
		ContractorName NVARCHAR(255),
		Times INT,
		IsFooter BIT,
		IsThisWeek BIT,
		SortOrder INT,
		SortOrder_Cont INT,
		Id INT
	)

	SELECT TOP 0 * INTO #Remaining FROM #Tmp

	--thống kê các nhà thầu có vi phạm trong tháng này và tháng trước
	INSERT #Tmp (ViolationCategoryID, ParentID, IsParent, ContractorID, ContractorCode, ContractorName, Times, IsFooter, IsThisWeek, SortOrder_Cont)
		SELECT ViolationCategoryDetailID AS ViolationCategoryID, ViolationCategoryID, 0, ContractorID, ContractorCode, ContractorName, COUNT(ViolationCategoryDetailID) AS Times, 0, CAST(CASE WHEN PublicationDate >= @FromDate AND PublicationDate <= @ToDate THEN 1 ELSE 0 END AS BIT) AS IsThisWeek, SortOrder_Cont
		FROM ViolationView
		WHERE ProjectID = @ProjectID AND (PublicationDate BETWEEN @FromDate AND @ToDate)
		GROUP BY ViolationCategoryDetailID, ViolationCategoryID, ContractorID, ContractorCode, ContractorName, CAST(CASE WHEN PublicationDate >= @FromDate AND PublicationDate <= @ToDate THEN 1 ELSE 0 END AS BIT), SortOrder_Cont
		
	--bổ sung những nhà thầu có vi phạm (đã bị remove khỏi dự án) còn thiếu và những mục vi phạm còn thiếu
	DECLARE @ViolationCategoryID INT
	DECLARE @ParentID INT
	DECLARE @IsParent BIT
	DECLARE @IsFooter BIT

	DECLARE cursorA CURSOR FOR
		SELECT DISTINCT ViolationCategoryID, ParentID, IsParent, IsFooter FROM #Tmp
		UNION
		SELECT ViolationCategoryID, ParentID, IsParent, IsFooter FROM ViolationCategory WHERE Deleted = 0 AND IsActive = 1 AND (IsFooter = 0 OR (IsFooter = 1 AND FooterType = 2 AND ViolationCode NOT IN ('_COFICO_CATEGORY_F2', '_COFICO_CATEGORY_F3')))
	OPEN cursorA
	FETCH NEXT FROM cursorA INTO @ViolationCategoryID, @ParentID, @IsParent, @IsFooter
	WHILE @@FETCH_STATUS = 0
	BEGIN
		INSERT INTO #Remaining (ViolationCategoryID, ParentID, IsParent, ContractorID, ContractorCode, ContractorName, IsFooter, IsThisWeek, SortOrder_Cont)
			SELECT DISTINCT	@ViolationCategoryID, @ParentID, @IsParent, c.ContractorID, ContractorCode, ContractorName, @IsFooter, 1, c.SortOrder
			FROM Contractor c
				INNER JOIN Project_Contractor p ON (p.ContractorID = c.ContractorID AND p.ProjectID = @ProjectID AND c.Deleted = 0 AND c.IsActive = 1)
				INNER JOIN Project j ON (j.ProjectID = @ProjectID AND j.Deleted = 0 AND (j.StatusID = 1 OR j.StatusID = 2))
			WHERE NOT EXISTS (SELECT TOP 1 1 FROM #Tmp t1 WHERE c.ContractorID = t1.ContractorID AND t1.ViolationCategoryID = @ViolationCategoryID AND t1.IsThisWeek = 1)
				  AND (p.Deleted = 0 OR ( p.Deleted = 1 AND EXISTS (
					--chỉ lấy những nhà thầu có vi phạm (bao gồm nhà thầu đã bị remove khỏi dự án)
					SELECT TOP 1 1 FROM Violation v
						INNER JOIN ViolationDetail d ON (d.ViolationID = v.ViolationID AND v.ProjectID = @ProjectID AND d.ContractorID = c.ContractorID AND v.Deleted = 0 AND d.Deleted = 0)
					WHERE (PublicationDate BETWEEN @FromDate AND @ToDate)
				  )) )

		FETCH NEXT FROM cursorA INTO @ViolationCategoryID, @ParentID, @IsParent, @IsFooter
	END
	
	CLOSE cursorA
	DEALLOCATE cursorA

	--merge data
	INSERT INTO #Tmp(ViolationCategoryID, ViolationCode, ParentID, IsParent, ContractorID, ContractorCode, ContractorName, Times, IsFooter, IsThisWeek, SortOrder_Cont)
		SELECT ViolationCategoryID, ViolationCode, ParentID, IsParent, ContractorID, ContractorCode, ContractorName, Times, IsFooter, IsThisWeek, SortOrder_Cont
		FROM #Remaining
	
	--
	DELETE FROM #Remaining

	--add ViolationCode & ViolationName
	INSERT #Remaining (ViolationCategoryID, ViolationCode, ViolationName, ContractorID, ContractorCode, ContractorName, Times, IsFooter, IsThisWeek, ParentID, IsParent, SortOrder, SortOrder_Cont, Id)
		SELECT t.ViolationCategoryID, c.ViolationCode, c.ViolationName, ContractorID, ContractorCode, ContractorName, Times, t.IsFooter, IsThisWeek, t.ParentID, t.IsParent, c.SortOrder, t.SortOrder_Cont, ROW_NUMBER() OVER (PARTITION BY t.ViolationCategoryID ORDER BY t.ViolationCategoryID, t.SortOrder_Cont, ContractorName)
		FROM #Tmp t
			INNER JOIN ViolationCategory c ON (c.ViolationCategoryID = t.ViolationCategoryID)
	
	--update parent
	UPDATE #Remaining
	SET Times = (SELECT SUM(ISNULL(Times, 0)) FROM #Remaining t WHERE t.ParentID = #Remaining.ViolationCategoryID AND t.ContractorID = #Remaining.ContractorID AND t.IsThisWeek = 1)
	WHERE IsParent = 1
	
	--update footer
	----update this week
	UPDATE #Remaining
	SET Times = (SELECT SUM(ISNULL(Times, 0)) FROM #Remaining t WHERE t.ContractorID = #Remaining.ContractorID AND IsParent = 1 AND IsFooter = 0 AND t.IsThisWeek = 1)
	WHERE IsFooter = 1 AND ViolationCode = '_COFICO_CATEGORY_F1'

	------update last week
	--UPDATE #Remaining
	--SET Times = (SELECT SUM(ISNULL(Times, 0)) FROM #Remaining t WHERE t.ContractorID = #Remaining.ContractorID AND IsFooter = 0 AND t.IsThisWeek = 0)
	--WHERE IsFooter = 1 AND ViolationCode = '_COFICO_CATEGORY_F2'

	----update Total
	--UPDATE #Remaining
	--SET Times = (SELECT SUM(ISNULL(Times, 0)) FROM #Remaining t WHERE t.ContractorID = #Remaining.ContractorID AND IsFooter = 1 AND ViolationCode <> '_COFICO_CATEGORY_F3')
	--WHERE IsFooter = 1 AND ViolationCode = '_COFICO_CATEGORY_F3'

	UPDATE #Remaining SET Times = NULL WHERE Times = 0

	--Set SortOrder của Footer luôn lớn hơn Danh mục
	DECLARE @Max INT
	SELECT @Max = MAX(SortOrder) FROM #Remaining WHERE IsFooter = 0

	UPDATE #Remaining SET SortOrder = SortOrder + @Max WHERE IsFooter = 1

	UPDATE #Remaining SET SortOrder_Cont = Id

	--show data
	SELECT * FROM #Remaining 
	WHERE IsThisWeek = 1 AND (ParentID IS NULL OR IsFooter = 1)
	ORDER BY IsFooter, SortOrder, ViolationName, SortOrder_Cont

	DROP TABLE #Tmp
	DROP TABLE #Remaining
END