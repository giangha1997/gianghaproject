﻿using COFICO.ESafety.AppCode;
using COFICO.ESafety.DAL.DBModel;
using COFICO.ESafety.DAL.Services;
using System.Collections.Generic;
using System.Web.Http;

namespace COFICO.ESafety
{
    //[Authorize]
    [RoutePrefix("api/ViolationPunishment")]
    public class ViolationPunishmentController : ApiController
    {
        [HttpGet, Route("items")]
        public IHttpActionResult Items()
        {
            var items = new List<ViolationPunishment>();
            using (var svc = new ViolationPunishmentService())
            {
                items = svc.GetPunishments(true,true);
            }
            return Ok(new { items });
        }

        [HttpGet, Route("ParentItems/{cateID}")]
        public IHttpActionResult ParentItems(int cateID)
        {
            var items = new List<ViolationPunishment>();
            using (var svc = new ViolationPunishmentService())
            {
                items = svc.GetParentPunishments(cateID);
            }
            return Ok(new { items });
        }

        [HttpPut]
        public IHttpActionResult Put(ViolationPunishment item)
        {
            var items = new Response<object>
            {
                IsError = true,
            };

            if (item != null && item.ID >= 0)
            {
                int? rs = null;
                using (var svc = new ViolationPunishmentService())
                {
                    rs = svc.Update(item);
                    if (rs != null)
                    {
                        if (rs == -1)
                        {
                            items.IsDuplicate = true;
                            items.Messages.Add(DAL.Constants.ErrorText.ItemExists);
                        }
                        else
                        {
                            items.IsError = false;
                            items.Id = rs;
                        }
                    }
                }
            }

            return Ok(new { items });
        }

        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            var items = new Response<object>
            {
                IsError = true,
            };

            bool? rs = false;
            using (var svc = new ViolationPunishmentService())
            {
                rs = svc.Delete(id);
                if (rs == true)
                {
                    items.IsError = false;
                }
            }
            return Ok(new { items });
        }
    }
}