﻿-- =============================================
-- Author:		HLinh
-- Create date: 2018/07/23
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetImage_DataOfWeeks] 
	@ProjectID INT,
	@Month TINYINT,
	@Year SMALLINT,
	@Type TINYINT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT [Week]
	FROM [Image] t
		INNER JOIN ImageDetail c ON (c.ImageID = t.ImageID AND c.Deleted = 0 AND t.Deleted = 0 AND c.[Filename] IS NOT NULL)
	WHERE t.ProjectID = @ProjectID AND t.[Month] = @Month AND t.[Year] = @Year AND ((@Type = 0 AND c.[Type] = @Type) OR (@Type = 1))
END