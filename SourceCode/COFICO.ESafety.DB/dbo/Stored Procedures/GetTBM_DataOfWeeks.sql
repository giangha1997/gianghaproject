﻿-- =============================================
-- Author:		HLinh
-- Create date: 2018/07/18
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetTBM_DataOfWeeks] 
	@ProjectID INT,
	@Month TINYINT,
	@Year SMALLINT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT [Week]
	FROM TBM t
		INNER JOIN TBMDetail d ON (d.TBMID = t.TBMID AND d.Deleted = 0 AND t.Deleted = 0 AND ISNULL(d.NumOfEmployees, 0) > 0)
		INNER JOIN Contractor c ON (c.ContractorID = t.ContractorID AND c.IsActive = 1 AND c.Deleted = 0)
		INNER JOIN Project_Contractor pc ON (pc.ProjectID = t.ProjectID AND pc.ContractorID = t.ContractorID AND pc.Deleted = 0)
	WHERE t.ProjectID = @ProjectID AND t.[Month] = @Month AND t.[Year] = @Year
END