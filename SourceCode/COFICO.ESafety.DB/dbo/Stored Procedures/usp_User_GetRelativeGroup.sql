﻿-- =============================================
-- Author:		HLinh
-- Create date: 2019/03/25
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_User_GetRelativeGroup]
	@projectID INT,
	@contractorID INT,
	@teamID INT,
	@username NVARCHAR(100)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @projContTeamID INT;

	SELECT @projContTeamID = pct.RowID
	FROM Project_Contractor pc
		INNER JOIN Project_Contractor_Team pct ON pct.ProjectContractorID = pc.RowID AND pct.Deleted = 0 AND pc.Deleted = 0
	WHERE pc.ProjectID = @projectID
		  AND pc.ContractorID = @contractorID
		  AND pct.TeamID = @teamID;

	
	--engineer (requester)
	SELECT u.UserName, su.FullName, u.Email, pu.[Type]
	FROM AspNetUsers u
		INNER JOIN Project_User pu ON pu.Username = u.UserName AND pu.ObjectID = @projectID AND pu.Deleted = 0
		INNER JOIN SecurityUsers su ON su.UserName = pu.UserName
	WHERE u.UserName = @username AND u.LockoutEnabled = 0

	UNION

	--site commander, nguoi xu ly
	SELECT u.UserName, su.FullName, u.Email, pu.[Type]
	FROM AspNetUsers u
		INNER JOIN Project_User pu ON pu.ObjectID = @projectID AND pu.[Type] IN (3, 4) AND pu.Deleted = 0
		INNER JOIN SecurityUsers su ON su.UserName = pu.UserName
	WHERE u.UserName = @username AND u.LockoutEnabled = 0
END