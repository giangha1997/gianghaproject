﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace COFICO.ESafety.DAL.DBModel
{
    [Table("ViolationPunishment")]
    public class ViolationPunishment
    {
        [Key]
        [Column("ViolationPunishmentID")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [StringLength(50)]
        public string PunishmentCode { get; set; }

        [StringLength(1000)]
        public string PunishmentName { get; set; }

        public int? ParentID { get; set; }

        public bool IsParent { get; set; }

        public decimal PenaltyFee { get; set; }

        [StringLength(4000)]
        public string Note { get; set; }

        public bool IsActive { get; set; }

        public int? SortOrder { get; set; }

        [JsonIgnore]
        public DateTime Created { get; set; }

        [JsonIgnore]
        [StringLength(50)]
        public string CreatedBy { get; set; }

        [JsonIgnore]
        public DateTime? Modified { get; set; }

        [JsonIgnore]
        [StringLength(50)]
        public string ModifiedBy { get; set; }

        public bool Deleted { get; set; }
    }
}
