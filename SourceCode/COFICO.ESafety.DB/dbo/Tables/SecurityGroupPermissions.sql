﻿CREATE TABLE [dbo].[SecurityGroupPermissions] (
    [RowId]             INT            IDENTITY (1, 1) NOT NULL,
    [SecurityGroupId]   INT            NOT NULL,
    [SecurityFeatureId] INT            NOT NULL,
    [View]              BIT            NULL,
    [Add]               BIT            NULL,
    [Edit]              BIT            NULL,
    [Delete]            BIT            NULL,
    [Note]              NVARCHAR (255) NULL,
    [Created]           DATETIME       NULL,
    [CreatedBy]         VARCHAR (50)   NULL,
    [Modified]          DATETIME       NULL,
    [ModifiedBy]        VARCHAR (50)   NULL,
    [Deleted]           BIT            NULL,
    [Approval]          BIT            NULL,
    CONSTRAINT [PK_SecurityGroupPermissions] PRIMARY KEY CLUSTERED ([RowId] ASC)
);

