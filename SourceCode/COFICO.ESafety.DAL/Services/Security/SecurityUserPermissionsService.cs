﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using COFICO.ESafety.DAL.DBModel;
using COFICO.ESafety.DAL.ServiceModels;
using COFICO.ESafety.DAL.Constants;

namespace COFICO.ESafety.DAL.Services
{
    public class SecurityUserPermissionsService : GenericService<SecurityUserPermission>
    {
        public List<SecurityUserPermissionView> GetSecurityUserPermissionsByUserFeatureCode(string spUser, string featureCode)
        {
            List<SecurityUserPermissionView> lst = null;
            try
            {
                spUser = Base.Helper.RemoveDomainName(spUser);

                string strSql = "EXEC " + StoreProcedure_Permission.LoadSecurityUserPermissionsFeaturesByUserFeatureCode + " @spUser, @featureCode";
                var prUser = new SqlParameter("@spUser", spUser);
                var prFeatureCode = new SqlParameter("@featureCode", featureCode);
                lst = _db.Database.SqlQuery<SecurityUserPermissionView>(strSql, prUser, prFeatureCode).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lst;
        }

        public List<SecurityUserPermissionsFeaturesView> GetSecurityUserPermissionsFeatures(int userId)
        {
            List<SecurityUserPermissionsFeaturesView> lst = null;
            try
            {
                string strSql = "EXEC " + StoreProcedure_Permission.LoadSecurityUserPermissionsFeaturesByUserId + " @userId";
                var prUserId = new SqlParameter("@userId", userId);
                lst = _db.Database.SqlQuery<SecurityUserPermissionsFeaturesView>(strSql, prUserId).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lst;
        }

        public bool Submit(SecurityUserPermissionsFeaturesView item, string requester)
        {
            try
            {
                var obj = ToEntity(item, requester);

                if (obj != null)
                {
                    _db.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Submit(List<SecurityUserPermissionsFeaturesView> lst, string requester)
        {
            try
            {
                foreach (var item in lst)
                {
                    var obj = ToEntity(item, requester);
                }
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal SecurityUserPermission ToEntity(SecurityUserPermissionsFeaturesView dto, string requester)
        {
            if (dto == null) return null;

            SecurityUserPermission entity;
            if (dto.RowId == 0)
            {
                entity = new SecurityUserPermission();
                entity.Created = DateTime.Now;
                entity.CreatedBy = requester;
                _db.SecurityUserPermissions.Add(entity);
            }
            else
            {
                entity = _db.SecurityUserPermissions.Where(p => p.RowId == dto.RowId).SingleOrDefault();
                if (entity == null)
                    return null;
                entity.Modified = DateTime.Now;
                entity.ModifiedBy = requester;
            }

            entity.SecurityUserId = dto.SecurityUserId;
            entity.SecurityFeatureId = dto.SecurityFeatureId;
            entity.View = dto.View;
            entity.Add = dto.Add;
            entity.Edit = dto.Edit;
            entity.Delete = dto.Delete;
            entity.Approval = dto.Approval;
            entity.Note = dto.Note;

            return entity;
        }
    }
}
