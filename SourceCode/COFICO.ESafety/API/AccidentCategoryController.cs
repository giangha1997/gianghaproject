﻿using COFICO.ESafety.AppCode;
using COFICO.ESafety.DAL.DBModel;
using COFICO.ESafety.DAL.Services;
using System.Collections.Generic;
using System.Web.Http;

namespace COFICO.ESafety
{
    //[Authorize]
    [RoutePrefix("api/AccidentCategory")]
    public class AccidentCategoryController : ApiController
    {
        [HttpGet, Route("items")]
        public IHttpActionResult Items()
        {
            var items = new List<AccidentCategory>();
            using (var svc = new AccidentCategoryService())
            {
                items = svc.GetAccidents();
            }
            return Ok(new { items });
        }

        //[HttpGet]
        //public IHttpActionResult Get(string id)
        //{
        //    var items = new AccidentCategory();
        //    using (var svc = new AccidentCategoryService())
        //    {
        //        items = svc.GetDetailById(CastUtilities.GetIntValue(id));
        //    }
        //    return Ok(new { items });
        //}

        [HttpPut]
        public IHttpActionResult Put(AccidentCategory item)
        {
            var items = new Response<object>
            {
                IsError = true,
                Result = -1,// update error
                Id = item.ID
            };

            int? rs = null;
            using (var svc = new AccidentCategoryService())
            {
                rs = svc.Update(item);
                if (rs != null)
                {
                    if (rs == -1)
                        items.IsDuplicate = true;
                    else
                    {
                        items.IsError = false;
                        items.Result = 1;// update success
                    }
                }
            }
            return Ok(new { items });
        }

        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            var items = new Response<object>
            {
                IsError = true,
                Result = -1,
                Id = id
            };

            bool rs = false;
            using (var svc = new AccidentCategoryService())
            {
                rs = svc.Delete(id);
                if (rs)
                {
                    items.IsError = false;
                    items.Result = 1;
                }
            }
            return Ok(new { items });
        }
    }
}