﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using COFICO.ESafety.DAL.Constants;
using COFICO.ESafety.DAL.DBModel;
using COFICO.ESafety.DAL.ServiceModels;

namespace COFICO.ESafety.DAL.Services
{
    public class SecurityGroupPermissionsService : GenericService<SecurityGroupPermission>
    {
        public List<SecurityGroupPermissionsFeaturesView> GetSecurityGroupPermissionsFeatures(int groupId)
        {
            List<SecurityGroupPermissionsFeaturesView> lst = null;
            try
            {
                string strSql = "EXEC " + StoreProcedure_Permission.LoadSecurityGroupPermissionsFeaturesByGroupId + " @groupId";
                var prGroupId = new SqlParameter("@groupId", groupId);
                lst = _db.Database.SqlQuery<SecurityGroupPermissionsFeaturesView>(strSql, prGroupId).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lst;
        }

        public bool Submit(SecurityGroupPermissionsFeaturesView item, string requester)
        {
            try
            {
                var obj = ToEntity(item, requester);

                if (obj != null)
                {
                    _db.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Submit(List<SecurityGroupPermissionsFeaturesView> lst, string requester)
        {
            try
            {
                foreach (var item in lst)
                {
                    var obj = ToEntity(item, requester);
                }
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal SecurityGroupPermission ToEntity(SecurityGroupPermissionsFeaturesView dto, string requester)
        {
            if (dto == null) return null;

            SecurityGroupPermission entity;
            if (dto.RowId == 0)
            {
                entity = new SecurityGroupPermission();
                entity.Created = DateTime.Now;
                entity.CreatedBy = requester;
                _db.SecurityGroupPermissions.Add(entity);
            }
            else
            {
                entity = _db.SecurityGroupPermissions.Where(p => p.RowId == dto.RowId).SingleOrDefault();
                if (entity == null)
                    return null;
                entity.Modified = DateTime.Now;
                entity.ModifiedBy = requester;
            }

            entity.SecurityGroupId = dto.SecurityGroupId;
            entity.SecurityFeatureId = dto.SecurityFeatureId;
            entity.View = dto.View;
            entity.Add = dto.Add;
            entity.Edit = dto.Edit;
            entity.Delete = dto.Delete;
            entity.Approval = dto.Approval;
            entity.Note = dto.Note;

            return entity;
        }
    }
}
