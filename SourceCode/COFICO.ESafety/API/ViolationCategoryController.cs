﻿using COFICO.ESafety.AppCode;
using COFICO.ESafety.DAL;
using COFICO.ESafety.DAL.DBModel;
using COFICO.ESafety.DAL.Services;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Linq;


namespace COFICO.ESafety
{
    //[Authorize]
    [RoutePrefix("api/ViolationCategory")]
    public class ViolationCategoryController : ApiController
    {
        [HttpGet, Route("items")]
        public IHttpActionResult Items()
        {
            var items = new List<ViolationCategoryDTO>();
            using (var svc = new ViolationCategoryService())
            {
                  items = svc.GetViolations();  
            }
            
            return Ok( items );
        }

        //2019-04-05
        //đặt tên route dễ hiểu nhầm
        [HttpPost, Route("items")]
        public IHttpActionResult InsertItem(ViolationCategory data)
        {
            var datareturn = new ViolationCategory();
            if (data == null)
            {
                return BadRequest("ViolationCategory is null.");
            }
            else
            using (var svc = new ViolationCategoryService())
            {
                datareturn = svc.InsertViolationCategory(data);
            }
            return Ok(datareturn);
        }

        [HttpGet, Route("ParentItems")]
        public IHttpActionResult ParentItems()
        {
            var items = new List<ViolationCategory>();
            using (var svc = new ViolationCategoryService())
            {
                //do something here
                items = svc.GetParentViolations();
            }
            return Ok(new { items });
        }

        [HttpGet, Route("ParentItems/{cateID}")]
        public IHttpActionResult ParentItems(int cateID)
        {
            var items = new List<ViolationCategory>();
            using (var svc = new ViolationCategoryService())
            {
                //do something here
                items = svc.GetParentViolations(cateID);
            }
            return Ok(new { items });
        }

        [HttpPut]
        public IHttpActionResult Put(ViolationCategory item)
        {
            var items = new Response<object>
            {
                IsError = true,
            };

            if (item != null && item.ViolationCategoryID >= 0)
            {
                int? rs = null;
                using (var svc = new ViolationCategoryService())
                {
                    rs = svc.Update(item);
                    if (rs != null)
                    {
                        if (rs == -1)
                        {
                            items.IsDuplicate = true;
                            items.Messages.Add(DAL.Constants.ErrorText.ItemExists);
                        }
                        else
                        {
                            items.IsError = false;
                            items.Id = rs;
                        }
                    }
                }
            }
            return Ok(new { items });
        }

        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            var items = new Response<object>
            {
                IsError = true,
            };

            bool? rs = false;
            using (var svc = new ViolationCategoryService())
            {
                rs = svc.Delete(id);
                if (rs == true)
                {
                    items.IsError = false;
                }
            }

            return Ok(new { items });
        }
    }
}