﻿CREATE TABLE [dbo].[RequestStatus] (
    [Id]   INT           IDENTITY (1, 1) NOT NULL,
    [Name] VARCHAR (100) NULL,
    CONSTRAINT [PK_RequestStatus] PRIMARY KEY CLUSTERED ([Id] ASC)
);

