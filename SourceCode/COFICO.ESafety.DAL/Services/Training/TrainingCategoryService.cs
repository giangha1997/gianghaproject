﻿using System;
using System.Collections.Generic;
using System.Linq;
using COFICO.ESafety.Base;
using COFICO.ESafety.DAL.DBModel;
using COFICO.ESafety.DAL.Services;

namespace COFICO.ESafety.DAL.Services
{
    public class TrainingCategoryService : GenericService<TrainingCategory>
    {
        public List<TrainingCategory> GetTrainings()
        {
            try
            {
                return _table.Where(x => !x.Deleted).OrderBy(s => s.SortOrder).ToList();
            }
            catch (Exception ex)
            {
                Logger.Error("GetTrainings", ex);
                throw ex;
            }
        }

        //public int? Insert(TrainingCategory p)
        //{
        //    if (p == null) return null;

        //    try
        //    {
        //        InsertObj(ref p);
        //        SaveChanges();
        //        return p.ID;
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error(p, ex);
        //        throw ex;
        //    }
        //}

        public int? Update(TrainingCategory p)
        {
            if (p == null) return null;
            //check existance
            if (ExistCode(p)) return -1;
            try
            {

                TrainingCategory entity = null;
                if ((entity = ParseToCategory(p)) != null)
                {
                    SaveChanges();
                    return entity.ID;
                }
                return null;
            }
            catch (Exception ex)
            {
                Logger.Error(p, ex);
                throw ex;
            }
        }

        private bool ExistCode(TrainingCategory p)
        {
            try
            {
                return _table.Any(s => ((p.ID == 0 && s.TrainingCode == p.TrainingCode)
                                        || (p.ID > 0 && s.TrainingCode == p.TrainingCode && s.ID != p.ID)) && !s.Deleted);
            }
            catch (Exception ex)
            {
                Logger.Error(p, ex);
                throw ex;
            }
        }

        public bool? Delete(int categoryID)
        {
            try
            {
                ////check existance in table TrainingDetail
                //if (_db.TrainingDetails.Any(s => s.TrainingCategoryID == categoryID && !s.Deleted))
                //    return false;

                if (DeleteMark(categoryID))
                {
                    SaveChanges();
                    return true;
                }
                return null;
            }
            catch (Exception ex)
            {
                Logger.Error(categoryID, ex);
                throw ex;
            }
        }

        private TrainingCategory ParseToCategory(TrainingCategory dto)
        {
            if (dto == null) return null;

            TrainingCategory entity = null;
            if (dto.ID == 0)
            {
                InsertObj(ref dto);
                entity = dto;
            }
            else
            {
                entity = _table.SingleOrDefault(x => x.ID == dto.ID && !x.Deleted);
                if (entity == null) return null;

                entity.TrainingCode = dto.TrainingCode;
                entity.TrainingName = dto.TrainingName;
                entity.ParentID = dto.ParentID;
                entity.IsParent = dto.IsParent;
                entity.IsActive = dto.IsActive;
                entity.SortOrder = dto.SortOrder;

                UpdateObj(entity);
            }

            return entity;
        }


    }
}
