﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Transactions;
using COFICO.ESafety.Base;
using COFICO.ESafety.DAL.Constants;
using COFICO.ESafety.DAL.DBModel;
using COFICO.ESafety.DAL.ServiceModels;

namespace COFICO.ESafety.DAL.Services
{
    public class HazardService : GenericService<Hazard>
    {
        public List<HazardView> GetMyRequest(DateTime? fromDate, DateTime? toDate, int? projectID, int? contractorID, int? teamID)
        {
            try
            {
                using (var svc = new RequestService())
                {
                    return svc.GetMyRequest<HazardView>(RequestType.Hazard, fromDate, toDate, projectID, contractorID, teamID);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(new { Text = "Hazard-GetMyRequest", fromDate, toDate, projectID, contractorID, teamID}, ex);
                throw ex;
            }
        }

        #region GetRequest
        public HazardView GetRequest(int id)
        {
            try
            {
                string username = Helper.GetCurrentUsername();

                string strSql = "EXEC " + StoreProcedures.GetRequest + " @requestId, @requestType, @username";
                var prRequestId = new SqlParameter("@requestId", id);
                var prRequestType = new SqlParameter("@requestType", RequestType.Hazard);
                var prUsername = new SqlParameter("@username", System.Data.SqlDbType.NVarChar, 100)
                {
                    Value = string.IsNullOrWhiteSpace(username) ? (object)DBNull.Value : username
                };

                var hazard = _db.Database.SqlQuery<HazardView>(strSql, prRequestId, prRequestType, prUsername).FirstOrDefault();

                return hazard;
            }
            catch (Exception ex)
            {
                Logger.Error(id, ex);
                throw ex;
            }
        }

        public HazardView GetRequest(Guid guid)
        {
            try
            {
                int? id = GetRequestId(guid);
                if (id > 0)
                    return GetRequest(id.Value);
                return null;
            }
            catch (Exception ex)
            {
                Logger.Error(guid, ex);
                throw ex;
            }
        }
        #endregion

        public int? Update(Hazard dto)
        {
            if (dto == null) return null;

            if (!Helper.CompareUpper(dto.Username, Helper.GetCurrentUsername()))
                return 0;

            try
            {
                Hazard entity = null;
                if ((entity = ParseToObj(dto)) != null)
                {
                    SaveChanges();
                    return entity.HazardID;
                }
                return 0;
            }
            catch (Exception ex)
            {
                Logger.Error("Hazard-Update", ex);
                throw ex;
            }
        }

        #region Delete
        public bool? Delete(int id)
        {
            try
            {
                if (!CanDelete(id)) return false;

                if (DeleteMark(id))
                {
                    SaveChanges();
                    return true;
                }
                return null;
            }
            catch (Exception ex)
            {
                Logger.Error(id, ex);
                throw ex;
            }
        }

        public bool? Delete(Guid guid)
        {
            try
            {
                int? id = GetRequestId(guid);
                if (id > 0)
                    return Delete(id.Value);
                return null;
            }
            catch (Exception ex)
            {
                Logger.Error(guid, ex);
                throw ex;
            }
        }
        #endregion

        #region Submit
        public int? Submit(int id)
        {
            using (var scope = new TransactionScope())
            {
                int? rs = null;
                try
                {
                    using (var svc = new RequestService())
                    {
                        if ((rs = svc.Submit(id, (byte)RequestType.Hazard, null, null)) == 0)
                        {
                            //approval history
                            var ah = new ApprovalHistory()
                            {
                                RequestId = id,
                                Type = (byte)RequestType.Hazard,
                                Action = ApprovalHistory_Actions.Submit,
                                Username = Helper.GetCurrentUsername(),
                                Level = ApprovalLevel.Engineer
                            };

                            using (var ahSvc = new ApprovalHistoryService())
                            {
                                if (ahSvc.Insert(ah) > 0)
                                    rs = 0;
                            }
                        }
                    }

                    if (rs == 0) scope.Complete();

                    return rs;
                }
                catch (Exception ex)
                {
                    Logger.Error(id, ex);
                    throw ex;
                }
            }
        }

        public int? Submit(Guid guid)
        {
            try
            {
                int? id = GetRequestId(guid);
                if (id > 0)
                    return Submit(id.Value);
                return null;
            }
            catch (Exception ex)
            {
                Logger.Error(guid, ex);
                throw ex;
            }
        }
        #endregion

        #region UpdateProcess
        public int? UpdateProcessByID(Hazard dto)
        {
            if (dto == null) return null;

            using (var scope = new TransactionScope())
            {
                int? rs = null;
                try
                {
                    var entity = _table.FirstOrDefault(s => s.HazardID == dto.HazardID && s.Status == (byte)StatusProcess.Submitted && !s.Deleted);
                    if (entity == null) return 1;

                    if (!Helper.CompareUpper(entity.Username, Helper.GetCurrentUsername()))
                        return 1;
                    
                    if (Convert.ToBase64String(entity.RowVersion) != Convert.ToBase64String(dto.RowVersion))
                        return 2;

                    //update process in the hazard
                    entity.FinishedPercent = dto.FinishedPercent;
                    entity.SafetyStatusID = dto.SafetyStatusID;
                    entity.SafetyStatusPercent = dto.SafetyStatusPercent;
                    entity.Modified = DateTime.Now;
                    entity.ModifiedBy = Helper.GetCurrentUsername();
                    SaveChanges();

                    var actionValue = new { dto.FinishedPercent, dto.SafetyStatusID, dto.SafetyStatusPercent };

                    //approval history
                    var ah = new ApprovalHistory()
                    {
                        RequestId = dto.HazardID,
                        Type = (byte)RequestType.Hazard,
                        Action = ApprovalHistory_Actions.Processing,
                        Username = Helper.GetCurrentUsername(),
                        Level = ApprovalLevel.Engineer,
                        Note = dto.Note,
                        ActionValue = Newtonsoft.Json.JsonConvert.SerializeObject(actionValue)
                    };

                    using (var ahSvc = new ApprovalHistoryService())
                    {
                        if (ahSvc.Insert(ah) > 0)
                            rs = 0;
                    }

                    if (rs == 0) scope.Complete();

                    return rs;
                }
                catch (Exception ex)
                {
                    Logger.Error(dto.HazardID, ex);
                    throw ex;
                }
            }
        }

        public int? UpdateProcess(Hazard dto)
        {
            try
            {
                int? id = GetRequestId(dto.GUID);
                if (id > 0)
                {
                    dto.HazardID = id.Value;
                    return UpdateProcessByID(dto);
                }
                return null;
            }
            catch (Exception ex)
            {
                Logger.Error(dto.GUID, ex);
                throw ex;
            }
        }
        #endregion

        #region FinishProcess
        public int? FinishProcessById(Hazard dto)
        {
            using (var scope = new TransactionScope())
            {
                int? rs = null;
                try
                {
                    using (var svc = new RequestService())
                    {
                        if ((rs = svc.FinishProcess(dto.HazardID, dto.RowVersion, (byte)RequestType.Hazard, null, null)) == 0)
                        {
                            //approval history
                            var ah = new ApprovalHistory()
                            {
                                RequestId = dto.HazardID,
                                Type = (byte)RequestType.Hazard,
                                Action = ApprovalHistory_Actions.Process,
                                Username = Helper.GetCurrentUsername(),
                                Level = ApprovalLevel.Engineer
                            };

                            using (var ahSvc = new ApprovalHistoryService())
                            {
                                if (ahSvc.Insert(ah) > 0)
                                    rs = 0;
                            }
                        }
                    }

                    if (rs == 0) scope.Complete();

                    return rs;
                }
                catch (Exception ex)
                {
                    Logger.Error(dto.HazardID, ex);
                    throw ex;
                }
            }
        }

        public int? FinishProcess(Hazard dto)
        {
            try
            {
                int? id = GetRequestId(dto.GUID);
                if (id > 0)
                {
                    dto.HazardID = id.Value;
                    return FinishProcessById(dto);
                }
                return null;
            }
            catch (Exception ex)
            {
                Logger.Error(dto.GUID, ex);
                throw ex;
            }
        }
        #endregion

        #region Approve
        public int? ApproveById(Hazard dto)
        {
            using (var scope = new TransactionScope())
            {
                int? rs = null;
                try
                {
                    using (var svc = new RequestService())
                    {
                        if ((rs = svc.Approve(dto.HazardID, dto.RowVersion, (byte)RequestType.Hazard, null, null)) == 0)
                        {
                            //approval history
                            var ah = new ApprovalHistory()
                            {
                                RequestId = dto.HazardID,
                                Type = (byte)RequestType.Hazard,
                                Action = ApprovalHistory_Actions.Approve,
                                Username = Helper.GetCurrentUsername(),
                                Level = ApprovalLevel.SiteCommander,
                                Note = dto.Note
                            };

                            using (var ahSvc = new ApprovalHistoryService())
                            {
                                if (ahSvc.Insert(ah) > 0)
                                    rs = 0;
                            }
                        }
                    }

                    if (rs == 0) scope.Complete();

                    return rs;
                }
                catch (Exception ex)
                {
                    Logger.Error(dto.HazardID, ex);
                    throw ex;
                }
            }
        }

        public int? Approve(Hazard dto)
        {
            try
            {
                int? id = GetRequestId(dto.GUID);
                if (id > 0)
                {
                    dto.HazardID = id.Value;
                    return ApproveById(dto);
                }
                return null;
            }
            catch (Exception ex)
            {
                Logger.Error(dto.GUID, ex);
                throw ex;
            }
        }
        #endregion

        #region Reject
        public int? RejectById(Hazard dto)
        {
            using (var scope = new TransactionScope())
            {
                int? rs = null;
                try
                {
                    using (var svc = new RequestService())
                    {
                        if ((rs = svc.Reject(dto.HazardID, dto.RowVersion, (byte)RequestType.Hazard, null, null)) == 0)
                        {
                            //approval history
                            var ah = new ApprovalHistory()
                            {
                                RequestId = dto.HazardID,
                                Type = (byte)RequestType.Hazard,
                                Action = ApprovalHistory_Actions.Reject,
                                Username = Helper.GetCurrentUsername(),
                                Level = ApprovalLevel.SiteCommander,
                                Note = dto.Note
                            };

                            using (var ahSvc = new ApprovalHistoryService())
                            {
                                if (ahSvc.Insert(ah) > 0)
                                    rs = 0;
                            }
                        }
                    }

                    if (rs == 0) scope.Complete();

                    return rs;
                }
                catch (Exception ex)
                {
                    Logger.Error(dto.HazardID, ex);
                    throw ex;
                }
            }
        }

        public int? Reject(Hazard dto)
        {
            try
            {
                int? id = GetRequestId(dto.GUID);
                if (id > 0)
                {
                    dto.HazardID = id.Value;
                    return RejectById(dto);
                }
                return null;
            }
            catch (Exception ex)
            {
                Logger.Error(dto.GUID, ex);
                throw ex;
            }
        }
        #endregion

        private int? GetRequestId(Guid guid)
        {
            return _table.Where(s => s.GUID == guid).FirstOrDefault()?.HazardID;
        }

        private bool CanDelete(int id, StatusProcess status = StatusProcess.Draft)
        {
            try
            {
                string username = Helper.GetCurrentUsername();
                return _table.Any(s => s.HazardID == id && s.Status == (byte)status && s.Username == username && !s.Deleted);
            }
            catch (Exception ex)
            {
                Logger.Error(id, ex);
                throw ex;
            }
        }

        private Hazard ParseToObj(Hazard dto)
        {
            if (dto == null) return null;

            Hazard entity = null;
            //if (dto.HazardID == 0)
            if (dto.GUID == null || dto.GUID == Guid.Empty)
            {
                dto.Type = (byte)RequestType.Hazard;
                dto.Status = (byte)StatusProcess.Draft;

                using (var svc = new UserService())
                {
                    var user = svc.GetUser(dto.Username);
                    dto.Fullname = user?.FullName;
                }

                InsertObj(ref dto);
                entity = dto;
            }
            else
            {
                string username = Helper.GetCurrentUsername();
                entity = _table.SingleOrDefault(x => x.GUID == dto.GUID && x.Username == username && !x.Deleted);
                if (entity == null) return null;

                using (var svc = new UserService())
                {
                    var user = svc.GetUser(dto.Username);
                    entity.Fullname = user?.FullName;
                }

                entity.Username = dto.Username;
                entity.ProjectID = dto.ProjectID;
                entity.IssuedDate = dto.IssuedDate;
                entity.ContractorID = dto.ContractorID;
                entity.TeamID = dto.TeamID;
                entity.NumOfEmployees = dto.NumOfEmployees;
                entity.TowerFacID = dto.TowerFacID;
                entity.ZoneID = dto.ZoneID;
                entity.LevelID = dto.LevelID;
                entity.SOW = dto.SOW;
                entity.HazardCategoryID = dto.HazardCategoryID;
                entity.HazardDetail = dto.HazardDetail;
                entity.RiskLevelID = dto.RiskLevelID;
                entity.RiskControl = dto.RiskControl;
                entity.FinishedPercent = dto.FinishedPercent;
                entity.SafetyStatusID = dto.SafetyStatusID;
                entity.SafetyStatusPercent = dto.SafetyStatusPercent;
                entity.Note = dto.Note;

                UpdateObj(entity);
            }

            return entity;
        }
    }
}