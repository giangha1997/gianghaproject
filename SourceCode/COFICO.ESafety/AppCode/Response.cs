﻿using System.Collections.Generic;

namespace COFICO.ESafety.AppCode
{
    public class Response<T>
    {
        public int? Id { get; set; }
        public bool IsError { get; set; }
        public bool IsInvalidItemsInside { get; set; }
        public bool IsDuplicate { get; set; }
        public List<string> Messages { get; set; }
        public int? Result { get; set; }
        public List<T> List { get; set; }

        public Response()
        {
            List = new List<T>();
            Messages = new List<string>();
        }
    }
}