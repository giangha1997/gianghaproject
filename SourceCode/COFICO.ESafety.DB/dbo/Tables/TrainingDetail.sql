﻿CREATE TABLE [dbo].[TrainingDetail] (
    [TrainingDetailID]   INT           IDENTITY (1, 1) NOT NULL,
    [TrainingID]         INT           NOT NULL,
    [TrainingCategoryID] INT           NOT NULL,
    [DayOfWeek]          TINYINT       CONSTRAINT [DF_TrainingDetail_DayOfWeek] DEFAULT ((0)) NOT NULL,
    [NumOfEmployees]     INT           NULL,
    [Date]               SMALLDATETIME NOT NULL,
    [Deleted]            BIT           CONSTRAINT [DF_TrainingDetail_Deleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_TrainingDetail] PRIMARY KEY CLUSTERED ([TrainingDetailID] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'2: Mon; 3: Tue; ....; 8: Sun', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TrainingDetail', @level2type = N'COLUMN', @level2name = N'DayOfWeek';

