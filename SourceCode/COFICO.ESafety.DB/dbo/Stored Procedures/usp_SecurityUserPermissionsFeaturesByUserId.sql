﻿
CREATE PROCEDURE [dbo].[usp_SecurityUserPermissionsFeaturesByUserId]
	@userId int
AS
BEGIN
	SET NOCOUNT ON;
	Select IsNull(p.[RowId],0) [RowId], p.[View], p.[Add], p.[Edit], p.[Delete], p.[Approval], p.[Note], p.[Created], p.[CreatedBy], p.[Modified], p.[ModifiedBy], 
			[SecurityFeatureId] = f.RowId, f.[FeatureCode], f.[FeatureName], @userId As [SecurityUserId]
	From [dbo].[SecurityFeatures] f	
		Left Join [dbo].[SecurityUserPermissions] p on f.RowId = p.[SecurityFeatureId] And p.[SecurityUserId]= @userId
	Where (f.Deleted Is Null Or f.Deleted = 0) And (p.Deleted Is Null Or p.Deleted = 0) And @userId Is Not Null
	ORDER BY f.SortOrder
END