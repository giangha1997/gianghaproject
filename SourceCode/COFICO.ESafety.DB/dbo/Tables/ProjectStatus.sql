﻿CREATE TABLE [dbo].[ProjectStatus] (
    [RowID] INT            IDENTITY (1, 1) NOT NULL,
    [Code]  NVARCHAR (50)  NOT NULL,
    [Name]  NVARCHAR (128) NOT NULL,
    CONSTRAINT [PK_ProjectStatus] PRIMARY KEY CLUSTERED ([RowID] ASC)
);

