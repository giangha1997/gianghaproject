﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COFICO.ESafety.Base
{
    public class CastUtilities
    {
        public static string GetStringValue(object obj)
        {
            string str = null;
            if (obj != null && obj != DBNull.Value)
            {
                str = obj.ToString();
            }
            return str;
        }

        public static short GetShortValue(object obj)
        {
            short num = 0;
            if (obj != null && string.IsNullOrEmpty(obj.ToString()) == false)
            {
                short.TryParse(obj.ToString(), out num);
            }
            return num;
        }

        public static short? GetNullableShortValue(object obj)
        {
            short num;
            if (obj != null && string.IsNullOrEmpty(obj.ToString()) == false && short.TryParse(obj.ToString(), out num))
            {
                return num;
            }
            return null;
        }

        public static int GetIntValue(object obj)
        {
            int num = 0;
            if (obj != null && string.IsNullOrEmpty(obj.ToString()) == false)
            {
                int.TryParse(obj.ToString(), out num);
            }
            return num;
        }

        public static double GetDoubleValue(object obj)
        {
            double num = 0;
            if (obj != null && string.IsNullOrEmpty(obj.ToString()) == false)
            {
                double.TryParse(obj.ToString(), out num);
            }
            return num;
        }

        public static int? GetNullableIntValue(object obj)
        {
            int num;
            if (obj != null && string.IsNullOrEmpty(obj.ToString()) == false && int.TryParse(obj.ToString(), out num))
            {
                return num;
            }
            return null;
        }

        public static bool GetBoolValue(object obj)
        {
            bool rs = false;
            if (obj != null && string.IsNullOrEmpty(obj.ToString()) == false)
            {
                bool.TryParse(obj.ToString(), out rs);
            }
            return rs;
        }

        public static bool? GetNullableBoolValue(object obj)
        {
            bool rs;
            if (obj != null && string.IsNullOrEmpty(obj.ToString()) == false && bool.TryParse(obj.ToString(), out rs))
            {
                return rs;
            }
            return null;
        }

        public static double? GetNullableDoubleValue(object obj)
        {
            double num;
            if (obj != null && string.IsNullOrEmpty(obj.ToString()) == false && double.TryParse(obj.ToString(), out num))
            {
                return num;
            }
            return null;
        }

        public static decimal? GetNullableDecimalValue(object obj)
        {
            decimal num;
            if (obj != null && string.IsNullOrEmpty(obj.ToString()) == false && decimal.TryParse(obj.ToString(), out num))
            {
                return num;
            }
            return null;
        }

        public static DateTime? GetNullableDateTime(object obj, bool getTime = false)
        {
            DateTime? dtReturn = null;
            if (obj != null && string.IsNullOrEmpty(obj.ToString()) == false)
            {
                var sValue = obj.ToString().Trim();
                bool res = false;
                if (res = DateTime.TryParseExact(sValue, "d-M-yyyy", new CultureInfo("vi-VN"), DateTimeStyles.None, out DateTime dt))
                { }
                else if (res = DateTime.TryParseExact(sValue, "d/M/yyyy", new CultureInfo("vi-VN"), DateTimeStyles.None, out dt))
                { }
                else if (res = DateTime.TryParseExact(sValue, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt))
                { }
                else if (res = DateTime.TryParse(sValue, out dt))
                { }

                if (res)
                    dtReturn = getTime ? dt : dt.Date;
            }

            return dtReturn;
        }

        public static DateTime ParseDate(string date)
        {
            DateTime result;
            if (DateTime.TryParseExact(date, "MM-dd-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out result))
                return result;
            return result;
        }

        //public static DateTime? GetSystemDateTimeFromISOString(object obj, bool getTime)
        //{
        //    DateTime? dtReturn = null;
        //    DateTime dt;
        //    if (obj != null && string.IsNullOrEmpty(obj.ToString()) == false)
        //    {
        //        try
        //        {
        //            dt = SPUtility.CreateDateTimeFromISO8601DateTimeString(obj.ToString());
        //            if (getTime)
        //            {
        //                dtReturn = dt;
        //            }
        //            else
        //            {
        //                dtReturn = dt.Date;
        //            }
        //        }
        //        catch { }
        //    }

        //    return dtReturn;
        //}

        public static string GetDateTimeString(object obj, bool getTime)
        {
            string str = null;
            DateTime? dt = GetNullableDateTime(obj, true);
            if (dt.HasValue)
            {
                if (getTime)
                {
                    str = dt.Value.ToString("MM/dd/yyyy hh:mm");
                }
                else
                {
                    str = dt.Value.ToString("MM/dd/yyyy");
                }
            }
            return str;
        }

        public static string GetLongDateString(object obj)
        {
            string str = null;
            DateTime? dt = GetNullableDateTime(obj, true);
            if (dt.HasValue)
            {
                str = dt.Value.ToString("MMMM dd, yyyy");
            }
            return str;
        }
    }
}

