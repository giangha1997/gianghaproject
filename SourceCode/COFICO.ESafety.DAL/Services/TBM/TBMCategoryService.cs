﻿using System;
using System.Collections.Generic;
using System.Linq;
using COFICO.ESafety.DAL.DBModel;

namespace COFICO.ESafety.DAL.Services
{
    public class TBMCategoryService : GenericService<TBMCategory>
    {
        public List<TBMCategory> GetTBMCategoryByTotalColum()
        {
            try
            {
                return _table.Where(x => x.IsTotalColumn && x.IsActive && !x.Deleted).OrderBy(s => s.SortOrder).ThenBy(s => s.TBMCode).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
