﻿using System.Collections.Generic;

namespace COFICO.ESafety.DAL.ServiceModels
{
    public class ProjectFullInfo
    {
        public int ID { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        public List<Item> Contractors { get; set; }

        public List<Item> TowerFacs { get; set; }

        public ProjectFullInfo()
        {
            Contractors = new List<Item>();
            TowerFacs = new List<Item>();
        }
    }
}
