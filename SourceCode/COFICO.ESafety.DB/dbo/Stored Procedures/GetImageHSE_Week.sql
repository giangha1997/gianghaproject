﻿-- =============================================
-- Author:		HLinh
-- Create date: 2018/07/06
-- Description:	Báo cáo tuần - HSE dự án
-- =============================================
CREATE PROCEDURE [dbo].[GetImageHSE_Week] 
	@ProjectID INT,
	@Month TINYINT,
	@Year SMALLINT,
	@Week TINYINT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Date SMALLDATETIME = dbo.GetFirstDateOfWeek(@Month, @Year, @Week)

	IF (@Date IS NULL) RETURN

	CREATE TABLE #Tmp  
	(
		ImageID INT,
		ProjectID INT,
		ProjectCode NVARCHAR(50),
		ProjectName NVARCHAR(255),
		[Type] TINYINT,
		ImageCategoryID INT, 
		CategoryCode NVARCHAR(50), 
		CategoryName NVARCHAR(255),
		[Month] TINYINT,
		[Year] SMALLINT,
		[Week] TINYINT,
		[Date] SMALLDATETIME,
		ImageDetailID INT,
		[Filename] NVARCHAR(255), 
		ImageTitle NVARCHAR(255), 
		[Description] NVARCHAR(255), 
		ShowInReport01 BIT, 
		ShowInReport02 BIT, 
		ShowInReport03 BIT,
		ShowInReport04 BIT,
		SortOrder INT,
		Id INT,
		Created DATETIME
	)

	--
	INSERT #Tmp (ImageID, ProjectID, ProjectCode, ProjectName, [Type], ImageCategoryID, CategoryCode, CategoryName, [Month], [Year], [Week], [Date], ImageDetailID, [Filename], ImageTitle, [Description], ShowInReport01, ShowInReport02, ShowInReport03, ShowInReport04, SortOrder, Id, Created)
		SELECT ImageID, ProjectID, ProjectCode, ProjectName, [Type], ImageCategoryID, CategoryCode, CategoryName, [Month], [Year], [Week], [Date], ImageDetailID, [Filename], ImageTitle, [Description], ShowInReport01, ShowInReport02, ShowInReport03, ShowInReport04, SortOrder, ROW_NUMBER() over (order by ImageID, ProjectID), Created
		FROM dbo.func_ImageHSE(@ProjectID, @Month, @Year, @Week, @Date, 0, 0, 1)
		--WHERE ShowInReport01 = 1 OR ImageID = 0

	UPDATE #Tmp SET Filename = NULL, ImageID = 0 WHERE ShowInReport01 = 0

	--only show 4 images of each ImageCategory
	SELECT DISTINCT ImageID, ProjectID, ProjectCode, ProjectName, [Type], ImageCategoryID, CategoryCode, CategoryName, [Month], [Year], [Week], [Date], ImageDetailID, [Filename], ImageTitle, [Description], ShowInReport01, ShowInReport02, ShowInReport03, ShowInReport04, SortOrder, Created
	FROM #Tmp t
	WHERE Id IN (SELECT TOP 4 Id FROM #Tmp t1 WHERE t1.ImageID = t.ImageID AND t1.ImageCategoryID = t.ImageCategoryID)
	ORDER BY SortOrder, CategoryName, ProjectCode
END