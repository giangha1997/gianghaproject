﻿using System;
using System.ComponentModel.DataAnnotations;

namespace COFICO.ESafety.DAL.DBModel
{
    public partial class SecurityFeature
    {
        [Key]
        public int RowId { get; set; }

        [Required]
        [StringLength(50)]
        public string FeatureCode { get; set; }

        [StringLength(255)]
        public string FeatureName { get; set; }

        public int? SortOrder { get; set; }

        public DateTime? Created { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        public bool? Deleted { get; set; }
    }
}
