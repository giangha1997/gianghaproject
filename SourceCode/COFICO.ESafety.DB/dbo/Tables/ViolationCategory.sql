﻿CREATE TABLE [dbo].[ViolationCategory] (
    [ViolationCategoryID] INT             IDENTITY (1, 1) NOT NULL,
    [ViolationCode]       NVARCHAR (50)   NULL,
    [ViolationName]       NVARCHAR (500)  NULL,
    [ParentID]            INT             NULL,
    [IsParent]            BIT             CONSTRAINT [DF_ViolationCategory_IsParent] DEFAULT ((0)) NOT NULL,
    [IsFooter]            BIT             CONSTRAINT [DF_ViolationCategory_IsFooter] DEFAULT ((0)) NOT NULL,
    [FooterType]          TINYINT         CONSTRAINT [DF_ViolationCategory_FooterType] DEFAULT ((0)) NOT NULL,
    [Note]                NVARCHAR (4000) NULL,
    [IsActive]            BIT             CONSTRAINT [DF_ViolationCategory_IsActive] DEFAULT ((0)) NOT NULL,
    [SortOrder]           INT             NULL,
    [Created]             DATETIME        NOT NULL,
    [CreatedBy]           NVARCHAR (50)   NULL,
    [Modified]            DATETIME        NULL,
    [ModifiedBy]          NVARCHAR (50)   NULL,
    [Deleted]             BIT             CONSTRAINT [DF_ViolationCategory_Deleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_ViolationCategory] PRIMARY KEY CLUSTERED ([ViolationCategoryID] ASC)
);

