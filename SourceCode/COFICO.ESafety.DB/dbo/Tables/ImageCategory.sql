﻿CREATE TABLE [dbo].[ImageCategory] (
    [ImageCategoryID] INT            IDENTITY (1, 1) NOT NULL,
    [CategoryCode]    NVARCHAR (50)  NULL,
    [CategoryName]    NVARCHAR (255) NULL,
    [ParentID]        INT            NULL,
    [IsParent]        BIT            NULL,
    [IsActive]        BIT            CONSTRAINT [DF_ImageCategory_IsActive] DEFAULT ((0)) NOT NULL,
    [SortOrder]       INT            NULL,
    [Created]         DATETIME       NOT NULL,
    [CreatedBy]       NVARCHAR (50)  NULL,
    [Modified]        DATETIME       NULL,
    [ModifiedBy]      NVARCHAR (50)  NULL,
    [Deleted]         BIT            CONSTRAINT [DF_ImageCategory_Deleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_ImageCategory] PRIMARY KEY CLUSTERED ([ImageCategoryID] ASC)
);

