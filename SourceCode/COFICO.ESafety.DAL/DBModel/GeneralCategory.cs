﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace COFICO.ESafety.DAL.DBModel
{
    [Table("GeneralCategory")]
    public class GeneralCategory
    {
        [Key]
        [Column("GeneralCategoryID")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [MaxLength(50)]
        [StringLength(50)]
        public string GeneralCategoryCode { get; set; }

        [MaxLength(255)]
        [StringLength(255)]
        public string GeneralCategoryName { get; set; }

        public int? ParentID { get; set; }

        public bool IsParent { get; set; }

        public byte? Type { get; set; }

        public bool IsActive { get; set; }

        public int? SortOrder { get; set; }

        [JsonIgnore]
        public DateTime? Created { get; set; }

        [JsonIgnore]
        [MaxLength(50)]
        [StringLength(50)]
        public string CreatedBy { get; set; }

        [JsonIgnore]
        public DateTime? Modified { get; set; }

        [JsonIgnore]
        [MaxLength(50)]
        [StringLength(50)]
        public string ModifiedBy { get; set; }

        public bool Deleted { get; set; }
    }
}
