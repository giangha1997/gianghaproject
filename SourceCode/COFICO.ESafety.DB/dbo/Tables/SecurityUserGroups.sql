﻿CREATE TABLE [dbo].[SecurityUserGroups] (
    [RowId]           INT            IDENTITY (1, 1) NOT NULL,
    [SecurityUserId]  INT            NOT NULL,
    [SecurityGroupId] INT            NOT NULL,
    [Note]            NVARCHAR (255) NULL,
    [Created]         DATETIME       NULL,
    [CreatedBy]       VARCHAR (50)   NULL,
    [Modified]        DATETIME       NULL,
    [ModifiedBy]      VARCHAR (50)   NULL,
    [Deleted]         BIT            NULL,
    CONSTRAINT [PK_SecurityUserGroups] PRIMARY KEY CLUSTERED ([RowId] ASC)
);

