﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using COFICO.ESafety.DAL.DBModel;
using COFICO.ESafety.DAL.Constants;
using COFICO.ESafety.Base;

namespace COFICO.ESafety.DAL.Services
{
    public class TeamService : GenericService<Team>
    {
        public List<Team> GetTeams()
        {
            List<Team> lst = null;
            try
            {
                lst = _db.Teams.Where(x => !x.Deleted).OrderBy(x => x.SortOrder).ThenBy(x => x.TeamName).ToList();
                return lst;
            }
            catch (Exception ex)
            {
                Logger.Error("GetTeams", ex);
                throw ex;
            }
        }

        public List<Team> GetActiveTeams()
        {
            List<Team> lst = null;
            try
            {
                lst = _db.Teams.Where(x => !x.Deleted && x.IsActive).OrderBy(x => x.SortOrder).ThenBy(x => x.TeamName).ToList();
                return lst;
            }
            catch (Exception ex)
            {
                Logger.Error("GetActiveTeams", ex);
                throw ex;
            }
        }

        private bool ExistCode(int teamID, string teamName)
        {
            try
            {
                return _table.Any(s => ((teamID == 0 && s.TeamName == teamName) ||
                                            (teamID != 0 && s.ID != teamID && s.TeamName == teamName)) &&
                                        !s.Deleted);
            }
            catch (Exception ex)
            {
                var obj = new { teamID, teamName };
                Logger.Error(obj, ex);
                throw ex;
            }
        }

        public int? Update(Team p)
        {
            if (p == null) return null;

            if (ExistCode(p.ID, p.TeamName)) return -1;

            try
            {
                Team entity = null;
                if ((entity = ParseToObj(p)) != null)
                {
                    SaveChanges();
                    return entity.ID;
                }
                return null;
            }
            catch (Exception ex)
            {
                Logger.Error(p, ex);
                throw ex;
            }
        }

        public bool? Delete(int teamID)
        {
            try
            {
                //check existance
                string strSql = "EXEC " + StoreProcedures.ExistTeam + " @spTeamID";
                var prTeamID = new SqlParameter("@spTeamID", teamID);
                if (_db.Database.SqlQuery<bool>(strSql, prTeamID).Single())
                    return false;

                if (DeleteMark(teamID))
                {
                    SaveChanges();
                    return true;
                }
                return null;
            }
            catch (Exception ex)
            {
                Logger.Error(teamID, ex);
                throw ex;
            }
        }

        private Team ParseToObj(Team dto)
        {
            if (dto == null) return null;

            Team entity = null;
            if (dto.ID == 0)
            {
                dto.SortOrder = 1;
                InsertObj(ref dto);
                entity = dto;
            }
            else
            {
                entity = _table.SingleOrDefault(x => x.ID == dto.ID && !x.Deleted);
                if (entity == null) return null;

                entity.TeamCode = dto.TeamCode;
                entity.TeamName = dto.TeamName;
                entity.IsActive = dto.IsActive;
                entity.Note = dto.Note;
                entity.SortOrder = dto.SortOrder;

                UpdateObj(entity);
            }

            return entity;
        }
    }
}
