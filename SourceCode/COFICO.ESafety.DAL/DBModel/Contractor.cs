﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace COFICO.ESafety.DAL.DBModel
{
    [Table("Contractor")]
    public class Contractor
    {
        [Key]
        [Column("ContractorID")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [StringLength(50)]
        public string ContractorCode { get; set; }

        [StringLength(255)]
        public string ContractorName { get; set; }

        [StringLength(1000)]
        public string Note { get; set; }

        public bool IsActive { get; set; }

        public int? SortOrder { get; set; }

        [JsonIgnore]
        public DateTime Created { get; set; }

        [JsonIgnore]
        [StringLength(50)]
        public string CreatedBy { get; set; }

        [JsonIgnore]
        public DateTime? Modified { get; set; }

        [JsonIgnore]
        [StringLength(50)]
        public string ModifiedBy { get; set; }

        public bool Deleted { get; set; }
    }

    public class ContractorSimple
    {
        public int ContractorID { get; set; }
        
        public string ContractorCode { get; set; }
        
        public string ContractorName { get; set; }

        public bool IsActive { get; set; }

        public int? SortOrder { get; set; }
    }
}
