﻿CREATE TABLE [dbo].[ImageDetail] (
    [ImageDetailID]   INT            IDENTITY (1, 1) NOT NULL,
    [ImageID]         INT            NOT NULL,
    [ImageCategoryID] INT            NOT NULL,
    [Type]            TINYINT        CONSTRAINT [DF_ImageDetail_Type] DEFAULT ((0)) NOT NULL,
    [ImageTitle]      NVARCHAR (255) NULL,
    [Description]     NVARCHAR (255) NULL,
    [Filename]        NVARCHAR (255) NULL,
    [FilenameOrig]    NVARCHAR (255) NULL,
    [ShowInReport01]  BIT            CONSTRAINT [DF_ImageDetail_ShowInReport01] DEFAULT ((0)) NOT NULL,
    [ShowInReport02]  BIT            CONSTRAINT [DF_ImageDetail_ShowInReport02] DEFAULT ((0)) NOT NULL,
    [ShowInReport03]  BIT            CONSTRAINT [DF_ImageDetail_ShowInReport03] DEFAULT ((0)) NOT NULL,
    [ShowInReport04]  BIT            CONSTRAINT [DF_ImageDetail_ShowInReport04] DEFAULT ((0)) NOT NULL,
    [SortOrder]       INT            NULL,
    [Created]         DATETIME       NULL,
    [CreatedBy]       NVARCHAR (50)  NULL,
    [Modified]        DATETIME       NULL,
    [ModifiedBy]      NVARCHAR (50)  NULL,
    [Deleted]         BIT            CONSTRAINT [DF_ImageDetail_Deleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_ImageDetail] PRIMARY KEY CLUSTERED ([ImageDetailID] ASC) WITH (FILLFACTOR = 80)
);

