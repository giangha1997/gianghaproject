﻿using System;
using System.IO;
using System.Security.AccessControl;
using System.Security.Principal;

namespace COFICO.ESafety.Base
{
    public class Helper
    {
        public const int NumberDigit = 2;

        public static void GrantAccess(string fullPath)
        {
            DirectoryInfo dInfo = new DirectoryInfo(fullPath);
            DirectorySecurity dSecurity = dInfo.GetAccessControl();
            dSecurity.AddAccessRule(new FileSystemAccessRule(new SecurityIdentifier(WellKnownSidType.WorldSid, null), FileSystemRights.FullControl, InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit, PropagationFlags.NoPropagateInherit, AccessControlType.Allow));
            dInfo.SetAccessControl(dSecurity);
        }

        public static void CopyStream(Stream stream, string destPath)
        {
            using (var fileStream = new FileStream(destPath, FileMode.Create, FileAccess.Write))
            {
                stream.CopyTo(fileStream);
            }
        }

        public static bool CompareUpper(string s1, string s2)
        {
            if (s1?.ToUpper() == s2?.ToUpper()) return true;
            return false;
        }

        public static decimal RoundRate(decimal d)
        {
            return decimal.Round(d, NumberDigit);
        }

        public static string RemoveDomainName(string loginName)
        {
            if (!string.IsNullOrWhiteSpace(loginName))
            {
                var txts = loginName.Split('\\');
                if (txts != null && txts.Length > 0)
                    loginName = txts[txts.Length - 1];

                txts = loginName.Split('|');
                if (txts != null && txts.Length > 0)
                    loginName = txts[txts.Length - 1];
            }

            return loginName;
        }

        public static string GetCurrentUsername()
        {
            var requester = "test"; System.Web.HttpContext.Current.User.Identity.Name.ToString();

            return RemoveDomainName(requester);
        }


        #region Convert to days
        //convert to first day & last day in a month
        public static void ConverToDayInMonth(byte month, short year, out DateTime fromDate, out DateTime toDate, out DateTime fromPrevDate, out DateTime toPrevDate)
        {
            fromDate = new DateTime(year, month, 1);
            toDate = fromDate.AddMonths(1).AddDays(-1);

            toPrevDate = fromDate.AddDays(-1);
            fromPrevDate = new DateTime(toPrevDate.Year, toPrevDate.Month, 1);
        }

        //convert to first day & last day in a quarter
        public static void ConverToDayInQuarter(byte quarter, short year, out DateTime fromDate, out DateTime toDate, out DateTime fromPrevDate, out DateTime toPrevDate)
        {
            byte month = 1;
            if (quarter == 2) month = 4;
            else if (quarter == 3) month = 7;
            else if (quarter == 4) month = 10;

            fromDate = new DateTime(year, month, 1);
            toDate = fromDate.AddMonths(3).AddDays(-1);

            fromPrevDate = fromDate.AddMonths(-3);
            toPrevDate = fromDate.AddDays(-1);
        }


        //convert to first day & last day in a year
        public static void ConverToDayInYear(short year, out DateTime fromDate, out DateTime toDate, out DateTime fromPrevDate, out DateTime toPrevDate)
        {
            fromDate = new DateTime(year, 1, 1);
            toDate = new DateTime(year, 12, 31);

            toPrevDate = new DateTime(year - 1, 1, 1);
            fromPrevDate = new DateTime(year - 1, 12, 31);
        }
        #endregion

        ////
        //public static string ServerRelativeUrl()
        //{
        //    string sru = SPContext.Current.Web.ServerRelativeUrl;

        //    if (sru != null && sru.Length == 1 && sru == "/") sru = "";

        //    return sru;
        //}

        //public static string WebUrl()
        //{
        //    string url = SPContext.Current.Web.Url;

        //    if (url[url.Length - 1] != '/') url += "/";

        //    return url;
        //}
    }

    public static class DateTimeExtensions
    {
        public static DateTime StartOfWeek(this DateTime dt, DayOfWeek startOfWeek = DayOfWeek.Monday)
        {
            int diff = (7 + (dt.DayOfWeek - startOfWeek)) % 7;
            return dt.AddDays(-1 * diff).Date;
        }

        public static int NumberOfWeeks(this DateTime dt, DayOfWeek startOfWeek = DayOfWeek.Monday)
        {
            var monday = dt.StartOfWeek(startOfWeek);

            if (monday.Day > 7) monday = monday.AddDays(7);

            int month = monday.Month;

            int i = 0;
            while (month == monday.Month)
            {
                i++;
                monday = monday.AddDays(7);
            }

            return i;
        }

        public static DateTime? StartOfWeek(byte month, short year, byte week, DayOfWeek startOfWeek = DayOfWeek.Monday)
        {
            var dt = new DateTime(year, month, 1);

            var monday = dt.StartOfWeek(startOfWeek);
            if (monday.Day > 7) monday = monday.AddDays(7);

            int m = monday.Month;
            byte i = 1;

            while (m == monday.Month)
            {
                i++;
                if (i > week) break;
                monday = monday.AddDays(7);
            }

            if (m != monday.Month) return null;
            return monday;
        }
    }
}
