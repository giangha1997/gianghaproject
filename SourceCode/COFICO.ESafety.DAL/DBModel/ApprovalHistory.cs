﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace COFICO.ESafety.DAL.DBModel
{
    [Table("ApprovalHistory")]
    public class ApprovalHistory : ApprovalHistoryBase
    {
        
    }

    public abstract class ApprovalHistoryBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [JsonIgnore]
        public int RequestId { get; set; }

        public byte Type { get; set; }

        [JsonIgnore]
        [MaxLength(100)]
        [StringLength(100)]
        public string Approver { get; set; }

        [JsonIgnore]
        [MaxLength(100)]
        [StringLength(100)]
        public string Username { get; set; }

        [JsonIgnore]
        [MaxLength(100)]
        [StringLength(100)]
        public string Name { get; set; }

        [MaxLength(255)]
        [StringLength(255)]
        public string Level { get; set; }

        [MaxLength(255)]
        [StringLength(255)]
        public string Action { get; set; }

        [JsonIgnore]
        [MaxLength(1000)]
        [StringLength(1000)]
        public string ActionValue { get; set; }

        [MaxLength(1000)]
        [StringLength(1000)]
        public string Note { get; set; }

        public DateTime? Created { get; set; }

        [JsonIgnore]
        [MaxLength(255)]
        [StringLength(255)]
        public string CreatedBy { get; set; }

        [NotMapped]
        public object ActionItem { get; set; }
    }
}
