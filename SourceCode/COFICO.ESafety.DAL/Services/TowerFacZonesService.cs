﻿using System;
using System.Collections.Generic;
using System.Linq;
using COFICO.ESafety.Base;
using COFICO.ESafety.DAL.DBModel;
using COFICO.ESafety.DAL.Services;

namespace COFICO.ESafety.DAL.Services
{
    public class TowerFacZonesService : GenericService<TowerFacZone>
    {

        public List<TowerFacZone> GetList()
        {
            try
            {
                return _table.Where(s => !s.Deleted).ToList();
            }
            catch (Exception ex)
            {
                Logger.Error("GetTowerFacZone", ex);
                throw ex;
            }
        }

        public int? Update(TowerFacZone p)
        {
            try
            {
                if (p == null) return null;
                TowerFacZone entity = null;
                if (p.Id == 0)
                {
                    InsertObj(ref p);
                    entity = p;
                }
                else
                {
                    entity = _table.SingleOrDefault(x => x.Id == p.Id && !x.Deleted);
                    if (entity == null) return null;
                    entity.Code = p.Code;
                    entity.Name = p.Name;
                    entity.IsParent = p.IsParent;
                    entity.Note = p.Note;
                    entity.IsActive = p.IsActive;
                    entity.Created = p.Created;
                    entity.Deleted = p.Deleted;
                    UpdateObj(entity);
                }

                SaveChanges();



                return p.Id;
            }
            catch (Exception ex)
            {
                Logger.Error(p, ex);
                throw ex;
            }
        }
        public bool Delete(int TowerFacZonesID)
        {

            try
            {

                if (DeleteMark(TowerFacZonesID))
                {
                    SaveChanges();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                Logger.Error(TowerFacZonesID, ex);
                throw ex;
            }
            
        }
    }
}
