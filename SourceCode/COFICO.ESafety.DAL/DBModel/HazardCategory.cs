﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace COFICO.ESafety.DAL.DBModel
{
    [Table("HazardCategory")]
    public class HazardCategory
    {
        [Key]
        [Column("HazardCategoryID")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [MaxLength(50)]
        [StringLength(50)]
        public string HazardCode { get; set; }

        [MaxLength(255)]
        [StringLength(255)]
        public string HazardName { get; set; }

        public int? ParentID { get; set; }

        public bool IsParent { get; set; }

        public bool IsActive { get; set; }

        public int? SortOrder { get; set; }

        [MaxLength(1000)]
        [StringLength(1000)]
        public string Note { get; set; }

        [JsonIgnore]
        public DateTime? Created { get; set; }

        [JsonIgnore]
        [MaxLength(50)]
        [StringLength(50)]
        public string CreatedBy { get; set; }

        [JsonIgnore]
        public DateTime? Modified { get; set; }

        [JsonIgnore]
        [MaxLength(50)]
        [StringLength(50)]
        public string ModifiedBy { get; set; }

        public bool Deleted { get; set; }
    }
}
