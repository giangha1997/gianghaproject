﻿using Microsoft.AspNet.Identity.EntityFramework;
using COFICO.ESafety.Entities;
using COFICO.ESafety.Models;
using System;
using System.Data.Entity;

namespace COFICO.ESafety.AppCode
{
    //public class AuthContext : IdentityDbContext<IdentityUser>
    public class AuthContext : IdentityDbContext<CoficoUser>
    {

        public AuthContext() : base("COFICO_Entities")
        {

        }
        public DbSet<Client> Clients { get; set; }
        public DbSet<RefreshToken> RefreshTokens { get; set; }
    }
}