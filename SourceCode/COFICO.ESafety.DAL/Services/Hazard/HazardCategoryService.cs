﻿using System;
using System.Collections.Generic;
using System.Linq;
using COFICO.ESafety.Base;
using COFICO.ESafety.DAL.DBModel;

namespace COFICO.ESafety.DAL.Services
{
    public class HazardCategoryService : GenericService<HazardCategory>
    {
        public List<HazardCategory> GetList()
        {
            try
            {
                return _table.Where(x => !x.Deleted).OrderBy(x => x.SortOrder).ToList();
            }
            catch (Exception ex)
            {
                Logger.Error("GetHazardCate", ex);
                throw ex;
            }
        }

        public List<HazardCategory> GetActiveList()
        {
            try
            {
                return _table.Where(x => !x.Deleted && x.IsActive).OrderBy(x => x.SortOrder).ToList();
            }
            catch (Exception ex)
            {
                Logger.Error("GetHazardCate", ex);
                throw ex;
            }
        }

        public int? Insert(HazardCategory p)
        {
            if (p == null) return null;

            try
            {
                InsertObj(ref p);
                SaveChanges();
                return p.ID;
            }
            catch (Exception ex)
            {
                Logger.Error(p, ex);
                throw ex;
            }
        }

        public int? Update(HazardCategory p)
        {
            if (p == null) return null;
            if (Exists(p)) return -1;// ton tai trong he thong

            try
            {
                HazardCategory entity = null;
                if ((entity = ParseToObj(p)) != null)
                {
                    SaveChanges();
                    return entity.ID;
                }
                return null;
            }
            catch (Exception ex)
            {
                Logger.Error(p, ex);
                throw ex;
            }
        }

        public bool Exists(HazardCategory p)
        {
            try
            {
                return _table.Any(s => ((p.ID == 0 && s.HazardName == p.HazardName)
                                        || (p.ID > 0 && s.HazardName == p.HazardName && s.ID != p.ID)) &&
                                            !s.Deleted);
            }
            catch (Exception ex)
            {
                Logger.Error(p, ex);
                throw ex;
            }
        }

        public bool Delete(int categoryID)
        {
            try
            {
                ////check existance in table AccidentDetail
                //if (_db.AccidentDetails.Any(s => s.AccidentCategoryID == categoryID && !s.Deleted))
                //    return false;

                if (DeleteMark(categoryID))
                {
                    SaveChanges();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                Logger.Error(categoryID, ex);
                throw ex;
            }
        }

        private HazardCategory ParseToObj(HazardCategory dto)
        {
            if (dto == null) return null;

            HazardCategory entity = null;
            if (dto.ID == 0)
            {
                InsertObj(ref dto);
                entity = dto;
            }
            else
            {
                entity = _table.SingleOrDefault(x => x.ID == dto.ID && !x.Deleted);
                if (entity == null) return null;

                entity.HazardCode = dto.HazardCode;
                entity.HazardName = dto.HazardName;
                entity.ParentID = dto.ParentID;
                entity.IsParent = dto.IsParent;
                entity.IsActive = dto.IsActive;
                entity.SortOrder = dto.SortOrder;
                entity.Note = dto.Note;
                UpdateObj(entity);
            }
            return entity;
        }

    }
}
