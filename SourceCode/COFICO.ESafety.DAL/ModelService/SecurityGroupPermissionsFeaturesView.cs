﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace COFICO.ESafety.DAL.ServiceModels
{
    [Table("SecurityGroupPermissionsFeaturesView")]
    public partial class SecurityGroupPermissionsFeaturesView
    {
        [Key]
        public int RowId { get; set; }

        public int SecurityGroupId { get; set; }

        public bool? View { get; set; }

        public bool? Add { get; set; }

        public bool? Edit { get; set; }

        public bool? Delete { get; set; }

        public bool? Approval { get; set; }

        [StringLength(255)]
        public string Note { get; set; }

        public DateTime? Created { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime? Modified { get; set; }

        [StringLength(50)]
        public string ModifiedBy { get; set; }

        public int SecurityFeatureId { get; set; }

        [StringLength(50)]
        public string FeatureCode { get; set; }

        [StringLength(255)]
        public string FeatureName { get; set; }
    }
}
