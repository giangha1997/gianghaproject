﻿CREATE TABLE [dbo].[AccidentCategory] (
    [AccidentCategoryID] INT             IDENTITY (1, 1) NOT NULL,
    [AccidentCode]       NVARCHAR (50)   NULL,
    [AccidentName]       NVARCHAR (255)  NULL,
    [ParentID]           INT             NULL,
    [IsParent]           BIT             CONSTRAINT [DF_AccidentCategory_IsParent] DEFAULT ((0)) NOT NULL,
    [IsFooter]           BIT             CONSTRAINT [DF_AccidentCategory_IsFooter] DEFAULT ((0)) NOT NULL,
    [Note]               NVARCHAR (4000) NULL,
    [IsActive]           BIT             CONSTRAINT [DF_AccidentCategory_IsActive] DEFAULT ((0)) NOT NULL,
    [SortOrder]          INT             NULL,
    [Created]            DATETIME        NOT NULL,
    [CreatedBy]          NVARCHAR (50)   NULL,
    [Modified]           DATETIME        NULL,
    [ModifiedBy]         NVARCHAR (50)   NULL,
    [Deleted]            BIT             CONSTRAINT [DF_AccidentCategory_Deleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_AccidentCategory] PRIMARY KEY CLUSTERED ([AccidentCategoryID] ASC)
);

