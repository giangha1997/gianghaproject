﻿-- =============================================
-- Author:		HLinh
-- Create date: 2018/06/14
-- Description:	get monday of week
-- =============================================
CREATE FUNCTION [dbo].[GetFirstDateOfWeek]
(
	@Month TINYINT,
	@Year SMALLINT,
	@Week TINYINT
)
RETURNS SMALLDATETIME
AS
BEGIN
	DECLARE @d SMALLDATETIME = CAST(CAST(@Year AS VARCHAR(4)) + '/' + cast(@Month AS VARCHAR(2)) + '/' +'1' AS SMALLDATETIME)

	DECLARE @t INT = DATEPART(dw, @d) - 1

	SET @d = DATEADD(DAY, -1 * @t + 1, @d)

	if(day(@d) > 7) SET @d = DATEADD(DAY, 7, @d)

	DECLARE @m1 INT = MONTH(@d)
	DECLARE @i INT = 1

	WHILE (@m1 = MONTH(@d))
	BEGIN
		SET @i = @i + 1
		IF @i > @Week BREAK
		SET @d = DATEADD(DAY, 7, @d)
	END

	IF @m1 <> MONTH(@d) RETURN NULL

	RETURN @d
END