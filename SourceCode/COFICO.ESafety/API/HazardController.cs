﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.Http;
using COFICO.ESafety.AppCode;
using COFICO.ESafety.DAL.DBModel;
using COFICO.ESafety.DAL.ServiceModels;
using COFICO.ESafety.DAL.Services;
using Newtonsoft.Json;
using COFICO.ESafety.Base;

namespace COFICO.ESafety
{
    //[Authorize]
    [RoutePrefix("api/Hazard")]
    public class HazardController : ApiController
    {
        [HttpGet, Route("GetMyRequest/{fromDate}/{toDate}/{projectID}/{contractorID}/{teamID}")]
        public IHttpActionResult GetMyRequest(string fromDate, string toDate, int? projectID, int? contractorID, int? teamID)
        {
            var fDate = CastUtilities.GetNullableDateTime(fromDate);
            var tDate = CastUtilities.GetNullableDateTime(toDate);

            fDate = fDate?.Date;
            tDate = tDate?.Date;

            var items = new List<HazardView>();
            using (var svc = new HazardService())
            {
                items = svc.GetMyRequest(fDate, tDate, projectID, contractorID, teamID);
            }
            return Ok(new { items });
        }

        [HttpGet, Route("Get/{guid}")]
        public IHttpActionResult Get(Guid guid)
        {

            HazardView items = null;
            using (var svc = new HazardService())
            {
                items = svc.GetRequest(guid);
            }

            if (items != null)
            {
                using (var svc = new ApprovalHistoryService())
                {
                    items.ApprovalHistories = svc.GetList(items.HazardID, items.Type);
                }

                List<GeneralCategory> safetyStatus = null;
                using (var svc = new GeneralCategoryService())
                {
                    safetyStatus = svc.GetList(DAL.Constants.GeneralCateType.SafetyStatus);
                }


                if (items.ApprovalHistories != null)
                    foreach (var ah in items.ApprovalHistories)
                    {
                        if (!string.IsNullOrWhiteSpace(ah.ActionValue))
                        {
                            try
                            {
                                var obj = JsonConvert.DeserializeObject<HazardView>(ah.ActionValue);
                                string safetyStatusName = safetyStatus?.FirstOrDefault(s => s.ID == obj.SafetyStatusID)?.GeneralCategoryName;
                                ah.ActionItem = new { obj.FinishedPercent, obj.SafetyStatusID, SafetyStatus = safetyStatusName, obj.SafetyStatusPercent };
                            }
                            catch (Exception ex)
                            {
                                Logger.Error(new { guid, ah.ActionValue }, ex);
                            }
                        }
                    }

                //CanEdit
                if (Base.Helper.CompareUpper(items.Username, Base.Helper.GetCurrentUsername()))
                {
                    if (items.Status == (byte)DAL.Constants.StatusProcess.Draft)
                        items.CanEdit = true;
                    else if (items.Status == (byte)DAL.Constants.StatusProcess.Processing)
                        items.CanUpdateProcess = items.CanFinishProcess = true;
                }
            }

            return Ok(new { items });
        }

        [HttpGet, Route("GetCategories")]
        public IHttpActionResult GetCategories()
        {
            var items = new HazardCateObj();

            //using (var svc = new ProjectService())
            //{
            //    var projects = svc.GetProjectByUser();
            //    if (projects != null && projects.Count > 0)
            //    {
            //        items.Projects = new List<object>();
            //        foreach (var p in projects)
            //            items.Projects.Add(new { p.ID, p.ProjectCode, p.ProjectName });
            //    }
            //}

            using (var svc = new HazardCategoryService())
            {
                var cates = svc.GetActiveList();
                if (cates != null && cates.Count > 0)
                {
                    items.HazardCategories = new List<object>();
                    foreach (var c in cates)
                        items.HazardCategories.Add(new { c.ID, c.HazardName });
                }
            }

            using (var svc = new GeneralCategoryService())
            {
                var cates = svc.GetList();
                if (cates != null && cates.Count > 0)
                {
                    items.RiskLevels = new List<object>();
                    items.SafetyStatus = new List<object>();

                    foreach (var c in cates.Where(s => s.Type == (byte)DAL.Constants.GeneralCateType.RiskLevel).ToList())
                        items.RiskLevels.Add(new { c.ID, Name = c.GeneralCategoryName });

                    foreach (var c in cates.Where(s => s.Type == (byte)DAL.Constants.GeneralCateType.SafetyStatus).ToList())
                        items.SafetyStatus.Add(new { c.ID, Name = c.GeneralCategoryName });
                }
            }

            return Ok(new { items });
        }

        [HttpGet, Route("GetStatus")]
        public IHttpActionResult GetStatus()
        {
            var items = new List<RequestStatus>();
            using (var svc = new RequestStatusService())
            {
                items = svc.GetList();
            }
            return Ok(new { items });
        }

        [HttpPut, Route("Put")]
        public IHttpActionResult Put(Hazard item)
        {
            var items = new Response<object>
            {
                IsError = true,
            };

            int? rs = null;
            using (var svc = new HazardService())
            {
                rs = svc.Update(item);
                if (rs != null && rs.Value > 0)
                {
                    items.IsError = false;
                    items.Id = rs;
                }
            }
            return Ok(new { items });
        }

        [HttpDelete, Route("Delete/{guid}")]
        public IHttpActionResult Delete(Guid guid)
        {
            var items = new Response<object>
            {
                IsError = true,
            };

            bool? rs = false;
            using (var svc = new HazardService())
            {
                rs = svc.Delete(guid);
                if (rs == true)
                {
                    items.IsError = false;
                }
            }
            return Ok(new { items });
        }

        [HttpGet, Route("Submit/{guid}")]
        public IHttpActionResult Submit(Guid guid)
        {
            var items = new Response<object>
            {
                IsError = true,
            };

            int? rs = null;
            using (var svc = new HazardService())
            {
                rs = svc.Submit(guid);
                SetApprovalMessage(items, rs);
            }
            return Ok(new { items });
        }

        [HttpPut, Route("UpdateProcess")]
        public IHttpActionResult UpdateProcess(Hazard dto)
        {
            var items = new Response<object>
            {
                IsError = true,
            };

            int? rs = null;
            using (var svc = new HazardService())
            {
                rs = svc.UpdateProcess(dto);
                SetApprovalMessage(items, rs);
            }
            return Ok(new { items });
        }

        [HttpPut, Route("FinishProcess")]
        public IHttpActionResult FinishProcess(Hazard dto)
        {
            var items = new Response<object>
            {
                IsError = true,
            };

            int? rs = null;
            using (var svc = new HazardService())
            {
                rs = svc.FinishProcess(dto);
                SetApprovalMessage(items, rs);
            }
            return Ok(new { items });
        }

        [HttpPut, Route("Approve")]
        public IHttpActionResult Approve(Hazard dto)
        {
            var items = new Response<object>
            {
                IsError = true,
            };

            int? rs = null;
            using (var svc = new HazardService())
            {
                rs = svc.Approve(dto);
                SetApprovalMessage(items, rs);
            }
            return Ok(new { items });
        }

        [HttpPut, Route("Reject")]
        public IHttpActionResult Reject(Hazard dto)
        {
            var items = new Response<object>
            {
                IsError = true,
            };

            int? rs = null;
            using (var svc = new HazardService())
            {
                rs = svc.Reject(dto);
                SetApprovalMessage(items, rs);
            }
            return Ok(new { items });
        }

        private void SetApprovalMessage(Response<object> resp, int? result)
        {
            if (result == 0)
            {
                resp.IsError = false;
            }
            else if (result == 1 || result == null)
            {
                resp.Result = result;
                resp.Messages.Add(DAL.Constants.ErrorText.ItemNotFound);
            }
            else if (result == 2)
            {
                resp.Result = result;
                resp.Messages.Add(DAL.Constants.ErrorText.RowVersionChanged);
            }
            else
            {
                resp.Result = result;
            }
        }
    }
}