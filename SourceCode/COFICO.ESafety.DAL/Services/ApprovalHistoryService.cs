﻿using System;
using System.Collections.Generic;
using System.Linq;
using COFICO.ESafety.Base;
using COFICO.ESafety.DAL.DBModel;
using COFICO.ESafety.DAL.ServiceModels;

namespace COFICO.ESafety.DAL.Services
{
    public class ApprovalHistoryService : GenericService<ApprovalHistory>
    {
        public List<ApprovalHistoryView> GetList(int requestId, byte requestType)
        {
            List<ApprovalHistoryView> listView = null;
            try
            {
                //listView = _db.ApprovalHistories
                //              .Where(x => x.RequestId == requestId && x.Type == requestType)
                //              .OrderByDescending(x => x.Created).ToList();

                listView = _db.ApprovalHistoryViews.Where(x => x.RequestId == requestId && x.Type == requestType)
                                                   .OrderByDescending(x => x.Created).ToList();
            }
            catch (Exception ex)
            {
                Logger.Error(requestId, ex);
                throw ex;
            }
            return listView;
        }

        public int? Insert(ApprovalHistory item)
        {
            int? id = null;
            try
            {
                InsertObj(ref item);
                SaveChanges();
                id = item.Id;
            }
            catch (Exception ex)
            {
                Logger.Error(item, ex);
                throw ex;
            }
            return id;
        }
    }
}
