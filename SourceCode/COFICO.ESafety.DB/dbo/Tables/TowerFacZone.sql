﻿CREATE TABLE [dbo].[TowerFacZone] (
    [Id]         INT             IDENTITY (1, 1) NOT NULL,
    [Code]       NVARCHAR (50)   NULL,
    [Name]       NVARCHAR (255)  NULL,
    [Type]       TINYINT         NULL,
    [ParentID]   INT             NULL,
    [IsParent]   BIT             CONSTRAINT [DF_TowerFacZone_IsParent] DEFAULT ((0)) NOT NULL,
    [Note]       NVARCHAR (1000) NULL,
    [IsActive]   BIT             CONSTRAINT [DF_TowerFacZone_IsActive] DEFAULT ((0)) NOT NULL,
    [SortOrder]  INT             NULL,
    [Created]    DATETIME        NOT NULL,
    [CreatedBy]  NVARCHAR (50)   NULL,
    [Modified]   DATETIME        NULL,
    [ModifiedBy] NVARCHAR (50)   NULL,
    [Deleted]    BIT             CONSTRAINT [DF_TowerFacZone_Deleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_TowerFacZone] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'1: TowerFac; 2: Zone', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TowerFacZone', @level2type = N'COLUMN', @level2name = N'Type';

