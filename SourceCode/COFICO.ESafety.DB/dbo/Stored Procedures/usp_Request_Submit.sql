﻿
/*==============================================================================================
   Author			Date			Description
   ---------------------------------------------
   Linh Ngo			21.03.2019		Logic for Submitting request
   
============================================================================================= 

Draft = 1;
Submitted = 2;
Processed = 3;
Approved = 4;
Rejected = 5;
*/

CREATE PROCEDURE [dbo].[usp_Request_Submit] 
	@requestId INT,
	@requestType TINYINT,
	@approver NVARCHAR(100),
	@approverLevel NVARCHAR(100),
	@username NVARCHAR(100),
	@result TINYINT OUTPUT
AS
BEGIN
	DECLARE @type TINYINT, @requestStatus TINYINT;

	IF @requestType = 1
	BEGIN
		SELECT @type = [Type], @requestStatus = [Status] 
		FROM Hazard 
		WHERE HazardID = @requestId AND [Status] = 1 AND Username = @username

		IF @type != @requestType OR @requestStatus IS NULL
			SET @result = 1

		IF (@result IS NULL)
		BEGIN
			UPDATE Hazard
			SET [Status] = 2, Approver = @approver, 
				ApproverLevel = @approverLevel, Modified = GETDATE(), ModifiedBy = @username
			WHERE HazardID  = @requestId

			SET @result = 0
		END
	END
	ELSE IF @requestType = 2
	BEGIN
		SELECT @type = [Type], @requestStatus = [Status] 
		FROM ViolationForm 
		WHERE ViolationID = @requestId AND [Status] = 1 AND Username = @username

		IF @type != @requestType OR @requestStatus IS NULL
			SET @result = 1

		IF (@result IS NULL)
		BEGIN
			UPDATE ViolationForm
			SET [Status] = 2, Approver = @approver, 
				ApproverLevel = @approverLevel, Modified = GETDATE(), ModifiedBy = @username
			WHERE ViolationID  = @requestId

			SET @result = 0
		END
	END
END