﻿using System;
using System.Collections.Generic;
using System.Linq;
using COFICO.ESafety.DAL.Services;
using COFICO.ESafety.DAL.DBModel;
using COFICO.ESafety.Base;

namespace COFICO.ESafety.DAL.Services
{
    public class ProjectUserService : GenericService<Project_User>
    {
        #region Project
        public List<Project_User> GetProjectUsers(int projectID)
        {
            try
            {
                return _table.Where(s => s.ObjectID == projectID && !s.Deleted).OrderBy(s => s.Username).ToList();
            }
            catch (Exception ex)
            {
                Logger.Error(projectID, ex);
                throw ex;
            }
        }

        public List<Project_User> GetProjectUserByType(int objectID, Constants.ProjectUserType type)
        {
            try
            {
                return _table.Where(s => s.ObjectID == objectID && s.Type == (byte)type && !s.Deleted).OrderBy(s => s.Username).ToList();
            }
            catch (Exception ex)
            {
                Logger.Error(objectID, ex);
                throw ex;
            }
        }

        public bool Update_ProjectUsers(List<Project_User> lst)
        {
            if (lst == null || lst.Count == 0) return false;

            try
            {
                var pID = lst[0].ObjectID;
                var entities = _table.Where(s => s.ObjectID == pID && !s.Deleted).ToList();

                //remove
                foreach (var p in entities)
                {
                    if (!lst.Any(s => s.ObjectID == p.ObjectID && Helper.CompareUpper(s.Username, p.Username) && s.Type == p.Type))
                        DeleteMark(p.RowID);
                }

                //insert
                for (int i = 0; i < lst.Count; i++)
                {
                    var p = lst[i];
                    if (!entities.Any(s => s.ObjectID == p.ObjectID && Helper.CompareUpper(s.Username, p.Username) && s.Type == p.Type))
                        InsertObj(ref p);
                }

                SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                Logger.Error("Update_ProjectUsers", ex);
                throw ex;
            }
        }

        public int? Update_ProjectUsers(Project_User dto)
        {
            if (dto == null) return null;

            int? res = null;
            try
            {
                dto.Type = (byte)Constants.ProjectUserType.PersonWhoProcesses;
                if (dto.RowID == 0)
                {
                    if (_table.Any(s => s.ObjectID == dto.ObjectID && s.Username == dto.Username && s.Type == dto.Type && !s.Deleted))
                        return -1;

                    InsertObj(ref dto);
                    SaveChanges();
                    res = dto.RowID;
                }
                else if (dto.RowID < 0)
                {
                    DeleteMark(Math.Abs(dto.RowID));
                    SaveChanges();
                    res = Math.Abs(dto.RowID);
                }

                return res;
            }
            catch (Exception ex)
            {
                Logger.Error("Update_ProjectUser", ex);
                throw ex;
            }
        }

        public bool DeleteByProject(int projectID)
        {
            if (projectID <= 0) return false;

            try
            {
                var entities = _table.Where(s => s.ObjectID == projectID && !s.Deleted).ToList();
                if (entities == null || entities.Count == 0) return false;

                foreach (var e in entities)
                    DeleteMark(e.RowID);

                SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                Logger.Error(projectID, ex);
                throw ex;
            }
        }
        #endregion


        #region Project_Contractor_Team
        public List<Project_User> GetProjConTeamUsers(int projContTeamID)
        {
            try
            {
                return _table.Where(s => s.ObjectID == projContTeamID && !s.Deleted).OrderBy(s => s.Username).ToList();
            }
            catch (Exception ex)
            {
                Logger.Error(projContTeamID, ex);
                throw ex;
            }
        }

        public bool InsertDelete_ProjConTeamUsers(List<Project_User> lst)
        {
            if (lst == null || lst.Count == 0) return false;

            try
            {
                var pID = lst[0].ObjectID;
                var entities = _table.Where(s => s.ObjectID == pID && !s.Deleted).ToList();

                //remove
                foreach (var p in entities)
                {
                    if (!lst.Any(s => s.ObjectID == p.ObjectID && Helper.CompareUpper(s.Username, p.Username)))
                        DeleteMark(p.RowID);
                }

                //insert
                for (int i = 0; i < lst.Count; i++)
                {
                    var p = lst[i];
                    if (!entities.Any(s => s.ObjectID == p.ObjectID && Helper.CompareUpper(s.Username, p.Username)))
                        InsertObj(ref p);
                }

                SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                Logger.Error(lst, ex);
                throw ex;
            }
        }
        #endregion
    }
}
