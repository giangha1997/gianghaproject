﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using COFICO.ESafety.DAL.DBModel;

namespace COFICO.ESafety.DAL.ModelService
{
    public class ProjectContractorUser : ProjectBase
    {
        public int ProjectID { get; set; }

        [NotMapped]
        public List<Project_Contractor> ProjectContractors { get; set; }

        [NotMapped]
        public List<Project_User> ProjectUsers { get; set; }
    }
}
