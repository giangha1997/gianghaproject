﻿





CREATE View [dbo].[ProjectContractorView] As

SELECT RowID, pc.ProjectID, pc.ContractorID, c.ContractorCode, c.ContractorName, c.SortOrder
FROM Project_Contractor pc 
	INNER JOIN Contractor c ON (c.ContractorID =  pc.ContractorID AND c.Deleted = 0 AND c.IsActive = 1 AND pc.Deleted = 0)