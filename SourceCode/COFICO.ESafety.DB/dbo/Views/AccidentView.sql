﻿








CREATE View [dbo].[AccidentView] As

SELECT AccidentID, ProjectID, ProjectCode, ProjectName, AccidentCategoryID, AccidentCode, AccidentName, [Month], [Year], [Week], [Date], [2] AS D2, [3] AS D3, [4] AS D4, [5] AS D5, [6] AS D6, [7] AS D7, [8] AS D8, SortOrder
FROM (
	SELECT t.AccidentID, t.ProjectID, p.ProjectCode, p.ProjectName, d.AccidentCategoryID, c.AccidentCode, c.AccidentName, t.[Month], t.[Year], t.[Week], t.[Date], d.[DayOfWeek], ISNULL(d.NumOfAccidents, 0) AS NumOfAccidents, c.SortOrder
	FROM Accident t 
		INNER JOIN AccidentDetail d ON (t.AccidentID =  d.AccidentID AND t.Deleted = 0 AND d.Deleted = 0)
		LEFT JOIN Project p ON (t.ProjectID = p.ProjectID)
		INNER JOIN AccidentCategory c ON (c.AccidentCategoryID = d.AccidentCategoryID AND c.Deleted = 0 AND c.IsActive = 1)
	WHERE p.Deleted = 0
) c
PIVOT
(
	SUM(NumOfAccidents)
	FOR [DayOfWeek] in ([2],[3],[4],[5],[6],[7],[8])
) p