﻿CREATE TABLE [dbo].[SecurityFeatures] (
    [RowId]       INT            IDENTITY (1, 1) NOT NULL,
    [FeatureCode] NVARCHAR (50)  NOT NULL,
    [FeatureName] NVARCHAR (255) NULL,
    [SortOrder]   INT            NULL,
    [Created]     DATETIME       NULL,
    [CreatedBy]   VARCHAR (50)   NULL,
    [Deleted]     BIT            NULL,
    CONSTRAINT [PK_SecurityFeatures] PRIMARY KEY CLUSTERED ([RowId] ASC)
);

