﻿using System;
using System.ComponentModel.DataAnnotations;

namespace COFICO.ESafety.DAL.DBModel
{
    public partial class SecurityUserGroup
    {
        [Key]
        public int RowId { get; set; }

        public int SecurityUserId { get; set; }

        public int SecurityGroupId { get; set; }

        [StringLength(255)]
        public string Note { get; set; }

        public DateTime? Created { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime? Modified { get; set; }

        [StringLength(50)]
        public string ModifiedBy { get; set; }

        public bool? Deleted { get; set; }
    }
}
