﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using COFICO.ESafety.Base;
using COFICO.ESafety.DAL.DBModel;
using COFICO.ESafety.DAL.Services;

namespace COFICO.ESafety.DAL.Services
{
    public class ViolationCategoryService : GenericService<ViolationCategory>
    {
        //get all (except Deleted = true)
        protected DbSet<ViolationCategory> DbViolationCategoriesa;

        public List<ViolationCategory> GetViolations(bool isActive, bool isAll)
        {
            List<ViolationCategory> lst = null;
            try
            {
                //do something here
                var data = _table.Where(s => !s.Deleted && (isAll || (!isAll && s.IsActive == isActive))).ToList();
                if (data == null || data.Count == 0) return lst;

                var parents = data.Where(s => s.ParentID == null).OrderBy(s => s.SortOrder).ToList();
                if (parents == null || parents.Count == 0) return lst;

                lst = new List<ViolationCategory>();
                foreach (var v in parents)
                {
                    lst.Add(v);

                }


                return lst;
            }
            catch (Exception ex)
            {
                Logger.Error("GetViolations", ex);
                throw ex;
            }
        }

        public List<ViolationCategoryDTO> GetViolations()
        {
            List<ViolationCategoryDTO> lst = null;
            try
            {
                var data = _table.Where(s => !s.Deleted && s.IsActive).ToList();
                if (data == null || data.Count == 0) return lst;

                var parents = data.Where(s => s.IsParent == true).OrderBy(s => s.SortOrder).ToList();
                if (parents == null || parents.Count == 0) return lst;


                lst = new List<ViolationCategoryDTO>();
                foreach (var v in parents)
                {
                    lst.Add(
                    new ViolationCategoryDTO
                    {
                        ViolationCategoryID = v.ViolationCategoryID,
                        ViolationCode = v.ViolationCode,
                        ViolationName = v.ViolationName,
                        ParentID = v.ParentID,
                        IsParent = v.IsParent,
                        ChildViolationCategory = new List<ViolationCategory>(SortViolation(data, v))
                    }
                    );
                }
                return lst;
            }
            catch (Exception ex)
            {
                Logger.Error("GetViolations", ex);
                throw ex;
            }
        }

        //just get IsActive = true
        public List<ViolationCategory> GetViolations(bool showFooter)
        {
            List<ViolationCategory> lst = null;
            try
            {

                //do something here
                var data = _table.Where(s => !s.Deleted && (showFooter || (!showFooter && s.IsActive == true))).ToList();
                if (data == null || data.Count == 0) return lst;

                var parents = data.Where(s => s.ParentID == null).OrderBy(s => s.SortOrder).ToList();
                if (parents == null || parents.Count == 0) return lst;

                lst = new List<ViolationCategory>();
                foreach (var v in parents)
                {
                    lst.Add(v);

                }


                return lst;
            }
            catch (Exception ex)
            {
                Logger.Error(showFooter, ex);
                throw ex;
            }
        }

        private List<ViolationCategory> SortViolation(List<ViolationCategory> orig, ViolationCategory v)
        {
            List<ViolationCategory> storeChild = null;

            if (v == null) return null;

            var children = orig.Where(s => !s.ParentID.Equals(null) && s.ParentID == v.ViolationCategoryID).OrderBy(s => s.SortOrder).ToList();
            if (children == null) return null;
            storeChild = new List<ViolationCategory>();
            foreach (var c in children)
            {
                storeChild.Add(c);
            }
            return storeChild;
        }

        public List<ViolationCategory> GetParentViolations(int catID)
        {

            try
            {


                return _table.Where(s => s.IsActive && !s.Deleted && s.IsParent && s.ViolationCategoryID == catID).OrderBy(s => s.SortOrder).ToList();
            }
            catch (Exception ex)
            {
                Logger.Error("GetParentViolations", ex);
                throw ex;
            }
        }

        public List<ViolationCategory> GetParentViolations()
        {
            List<ViolationCategory> lst = null;
            try
            {
                var data = _table.Where(s => s.IsActive && !s.Deleted && s.IsParent).OrderBy(s => s.SortOrder).ToList();
                if (data == null || data.Count == 0) return null;

                lst = new List<ViolationCategory>();
                foreach (var v in data)
                {
                    lst.Add(v);
                }
                return _table.Where(s => s.IsActive && !s.Deleted && s.IsParent).OrderBy(s => s.SortOrder).ToList();
            }
            catch (Exception ex)
            {
                Logger.Error("GetParentViolations", ex);
                throw ex;
            }
        }

        //2019-04-05
        //hàm này dư thừa
        public string NameWho(string userName = null)
        {
            if (string.IsNullOrWhiteSpace(userName))
                userName = Base.Helper.GetCurrentUsername();
            return userName;
        }

        //2019-04-05
        //Sử dụng hàm InsertObj: kế thừa từ class GenericService<T>
        //khi đó, không cần set CreatedBy, Created
        //khi insert thì ModifiedBy, Modified là null.
        //cột luôn luôn set [Deleted] = false, không được set [Deleted] theo client data đẩy lên. Nếu muốn xóa thì phải gọi Delete.
        public ViolationCategory InsertViolationCategory(ViolationCategory p)
        {
            
            try
            {
                //do something here

                
                if (p == null) return null;
                if (ExistCode(p)) return null;
                ViolationCategory entity = null;
                _table.Add(new ViolationCategory
                {
                    ViolationCode = p.ViolationCode,
                    ViolationName = p.ViolationName,
                    ParentID = p.ParentID,
                    IsParent = p.IsParent,
                    IsFooter = p.IsFooter,
                    FooterType = p.FooterType,
                    Note = p.Note,
                    IsActive = p.IsActive,
                    SortOrder = p.SortOrder,
                    CreatedBy = NameWho(),
                    ModifiedBy = NameWho(),
                    Created = DateTime.Now,
                    Modified = DateTime.Now,
                    Deleted = p.Deleted
                }
                );
                entity = p;
                SaveChanges();
                return entity;
            }
            catch (Exception ex)
            {
                Logger.Error(p, ex);
                throw ex;
            }
        }

        public int? Insert(ViolationCategory p)
        {
            int? id = null;
            try
            {
                //do something here

                // update a field IsParent of parent
                //...
            }
            catch (Exception ex)
            {
                Logger.Error(p, ex);
                throw ex;
            }
            return id;
        }

        //2019-04-05
        //IsFooter & FooterType: chỉ sử dụng cho báo cáo, trên UI không được phép edit/delete => không cần phải set entity.IsFooter = p.IsFooter; entity.FooterType = p.FooterType;
        //trường hợp update thay đổi ParentID thì phải kiểm tra column [IsParent] để set lại true/false theo ParentID trước và sau khi update.
        public new int? Update(ViolationCategory p)
        {
            try
            {
                if (p == null) return -1;

                //do something here
                //check if exists, return -1
                if (ExistCode(p)) return -1;
                //check if it's parent to update IsParent
                //...
                var entity = _table.SingleOrDefault(x => x.ViolationCategoryID == p.ViolationCategoryID && !x.Deleted);
                if (entity == null) return -1;
                if (entity.IsParent)
                {
                    entity.ViolationCode = p.ViolationCode;
                    entity.ViolationName = p.ViolationName;
                    entity.FooterType = p.FooterType;
                    entity.IsFooter = p.IsFooter;
                    entity.SortOrder = p.SortOrder;
                    entity.Note = p.Note;

                }
                else
                {
                    entity.ViolationCode = p.ViolationCode;
                    entity.ViolationName = p.ViolationName;
                    entity.FooterType = p.FooterType;
                    entity.IsFooter = p.IsFooter;
                    entity.SortOrder = p.SortOrder;
                    entity.Note = p.Note;
                    entity.ParentID = p.ParentID;
                }
                SaveChanges();
                return entity.ViolationCategoryID;
                
                //check if this category has changed parent.
                //...

                //return ID;
                 //remove this after we got the actual ID

            }
            catch (Exception ex)
            {
                Logger.Error(p, ex);
                throw ex;
            }
        }

        //2019-04-05
        //table có column Deleted, em chỉ set Deleted = 1, mình không xóa luôn dữ liệu khỏi db
        //sử dụng hàm DeleteMark: kế thừa từ class GenericService<T>
        //nên xóa item con trước, rồi mới cho phép xóa item cha.
        //Nếu xóa 1 item con (A) xong, cần kiểm tra item cha của (A) còn item con nào không? Nếu không thì set IsParent = 0.
        public bool Delete(int violationCatID)
        {
            try
            {
                var entity = _table.FirstOrDefault(s => s.ViolationCategoryID == violationCatID && !s.Deleted);
                if (entity == null) return false;
                if (entity.IsParent && !entity.Deleted)
                {
                    var entityParent = _table.Where(s => s.ParentID == entity.ViolationCategoryID && !s.Deleted);
                    if (entityParent != null)
                    {
                        foreach(var v in entityParent)
                        {
                            DeleteFunction(v.ViolationCategoryID);    
                        }
                    }
                }
                DeleteFunction(entity.ViolationCategoryID);
                SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                Logger.Error(violationCatID, ex);
                throw ex;
            }
        }

        public bool DeleteFunction(object id, string userName = null)
        {
            ViolationCategory obj = _table.Find(id);
            if (obj == null) return false;
            System.Reflection.PropertyInfo info = obj.GetType().GetProperty("Modified");
            if (info != null)
                info.SetValue(obj, DateTime.Now);

            info = obj.GetType().GetProperty("ModifiedBy");
            if (info != null)
            {
                if (string.IsNullOrWhiteSpace(userName))
                    userName = Base.Helper.GetCurrentUsername();
                info.SetValue(obj, userName);
            }

            info = obj.GetType().GetProperty("Deleted");
            if (info != null)
                info.SetValue(obj, true);

            _table.Attach(obj);
            _db.Entry(obj).State = EntityState.Modified;
            return true;
        }
        private bool ExistCode(ViolationCategory p)
        {
            try
            {
                return _table.Any(s => ((p.ViolationCategoryID == 0 && s.ViolationCode == p.ViolationCode)
                                        || (p.ViolationCategoryID > 0 && s.ViolationCode == p.ViolationCode)) && !s.Deleted);
            }
            catch (Exception ex)
            {
                Logger.Error(p, ex);
                throw ex;
            }
        }

        private ViolationCategory ParseToViolation(ViolationCategory dto, ref int? parentID)
        {
            if (dto == null) return null;
            ViolationCategory entity = null;

            //do something here

            return entity;
        }
    }
}
