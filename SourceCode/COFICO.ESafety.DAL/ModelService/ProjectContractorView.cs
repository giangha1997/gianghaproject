﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace COFICO.ESafety.DAL.ServiceModels
{
    [Table("ProjectContractorView")]
    public class ProjectContractorView
    {
        [Key]
        public int RowID { get; set; }

        public int ProjectID { get; set; }

        public int ContractorID { get; set; }

        public string ContractorCode { get; set; }

        public string ContractorName { get; set; }

        public int? SortOrder { get; set; }
    }
}
