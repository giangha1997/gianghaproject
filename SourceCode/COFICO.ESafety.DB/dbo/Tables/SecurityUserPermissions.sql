﻿CREATE TABLE [dbo].[SecurityUserPermissions] (
    [RowId]             INT            IDENTITY (1, 1) NOT NULL,
    [SecurityUserId]    INT            NOT NULL,
    [SecurityFeatureId] INT            NOT NULL,
    [View]              BIT            NULL,
    [Add]               BIT            NULL,
    [Edit]              BIT            NULL,
    [Delete]            BIT            NULL,
    [Approval]          BIT            NULL,
    [Note]              NVARCHAR (255) NULL,
    [Created]           DATETIME       NULL,
    [CreatedBy]         VARCHAR (50)   NULL,
    [Modified]          DATETIME       NULL,
    [ModifiedBy]        VARCHAR (50)   NULL,
    [Deleted]           BIT            NULL,
    CONSTRAINT [PK_SecurityUserPermissions] PRIMARY KEY CLUSTERED ([RowId] ASC)
);

