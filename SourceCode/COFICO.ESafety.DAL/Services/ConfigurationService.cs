﻿using System;
using System.Linq;
using COFICO.ESafety.DAL.DBModel;
using COFICO.ESafety.DAL.Constants;
using COFICO.ESafety.Base;

namespace COFICO.ESafety.DAL.Services
{
    public class ConfigurationService : GenericService<Configuration>
    {
        public string GetImageFolder()
        {
            try
            {
                var config = _table.FirstOrDefault(s => s.CfgKey == Configurations.IMAGE_FOLDER);
                return config?.CfgValue;
            }
            catch (Exception ex)
            {
                Logger.Error("GetImageFolder", ex);
                throw ex;
            }
        }
    }
}
