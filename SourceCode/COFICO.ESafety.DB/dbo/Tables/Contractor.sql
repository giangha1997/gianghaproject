﻿CREATE TABLE [dbo].[Contractor] (
    [ContractorID]   INT             IDENTITY (1, 1) NOT NULL,
    [ContractorCode] NVARCHAR (50)   NULL,
    [ContractorName] NVARCHAR (255)  NULL,
    [Note]           NVARCHAR (1000) NULL,
    [IsActive]       BIT             CONSTRAINT [DF_Contractor_IsActive] DEFAULT ((0)) NOT NULL,
    [SortOrder]      INT             NULL,
    [Created]        DATETIME        NOT NULL,
    [CreatedBy]      NVARCHAR (50)   NULL,
    [Modified]       DATETIME        NULL,
    [ModifiedBy]     NVARCHAR (50)   NULL,
    [Deleted]        BIT             CONSTRAINT [DF_Contractor_Deleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Contractor] PRIMARY KEY CLUSTERED ([ContractorID] ASC)
);

