﻿





CREATE View [dbo].[UserView] As

SELECT u.Id, u.Email, u.UserName, su.FullName, LockoutEnabled
FROM AspNetUsers u
	INNER JOIN SecurityUsers su ON su.UserName = u.UserName