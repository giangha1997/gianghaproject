﻿-- =============================================
-- Author:		HLinh
-- Create date: 2018/06/05
-- Description:	HSE/Report 01 - Thống kê TBM trong tuần theo ngày của 1 dự án theo nhà thầu
-- @IsReport01 = 0: chỉ lấy nhà thầu có trong dự án (nhập liệu HSE)
-- @IsReport01 = 1: lấy tất cả những nhà thầu có trong dự án và đã từng có trong dự án
-- =============================================
CREATE PROCEDURE [dbo].[GetTBM_HSE] 
	@ProjectID INT,
	@Month TINYINT,
	@Year SMALLINT,
	@Week TINYINT,
	@PrevWeek TINYINT,
	@IsReport01 BIT
AS
BEGIN
	SET NOCOUNT ON;

	CREATE TABLE #TBMs
	(
		TBMID INT,
		ProjectID INT,
		ProjectCode NVARCHAR(50),
		ProjectName NVARCHAR(255),
		ContractorID INT,
		ContractorCode NVARCHAR(50),
		ContractorName NVARCHAR(255),
		CatGroupCode NVARCHAR(50),
		DataType VARCHAR(10),
		TBMCategoryID INT,
		TBMCode NVARCHAR(50),
		TBMName NVARCHAR(255),
		[Month] TINYINT,
		[Year] SMALLINT,
		[Week] TINYINT,
		[Date] SMALLDATETIME,
		D2 DECIMAL(9,2),
		D3 DECIMAL(9,2),
		D4 DECIMAL(9,2),
		D5 DECIMAL(9,2),
		D6 DECIMAL(9,2),
		D7 DECIMAL(9,2),
		D8 DECIMAL(9,2),
		Total DECIMAL(9,2),	
		PrevTotal DECIMAL(9,2),
		Total1 DECIMAL(9,2),
		SortOrder_Cont INT,
		SortOrder_TBMCat INT,
		ThisWeek_C1 DECIMAL(9,2), --use in report
		ThisWeek_C2 DECIMAL(9,2),
		ThisWeek_C3 DECIMAL(9,2),
		LastWeek_C1 DECIMAL(9,2),
		LastWeek_C2 DECIMAL(9,2),
		LastWeek_C3 DECIMAL(9,2),
		Total_C1 DECIMAL(9,2),
		Total_C2 DECIMAL(9,2),
		Total_C3 DECIMAL(9,2)
	)

	SELECT TOP 0 * INTO #TBMs_Prev FROM #TBMs
	SELECT TOP 0 * INTO #TBMs1 FROM #TBMs

	--
	DECLARE @Date SMALLDATETIME = dbo.GetFirstDateOfWeek(@Month, @Year, @Week)
	DECLARE @PreYear SMALLINT = @Year
	DECLARE @PreMonth TINYINT = @Month

	IF @PrevWeek > @Week
		SET @PreMonth = @Month - 1

	IF @PreMonth <= 0
	BEGIN
		SET @PreMonth = 12
		SET @PreYear = @PreYear - 1
	END

	IF (@Date IS NULL) RETURN

	--get data a week of a month
	IF @IsReport01 = 0 -- nhập liệu HSE, chỉ lấy nhà thầu có trong dự án
	BEGIN
		INSERT INTO #TBMs(TBMID, ProjectID, ProjectCode, ProjectName, ContractorID, ContractorCode, ContractorName, CatGroupCode, DataType, TBMCategoryID, [Month], [Year], [Week], [Date], D2, D3, D4, D5, D6, D7, D8, SortOrder_Cont, SortOrder_TBMCat, Total, PrevTotal)
			SELECT	*, 
					ISNULL(D2, 0) + ISNULL(D3, 0) + ISNULL(D4, 0) + ISNULL(D5, 0) + ISNULL(D6, 0) + ISNULL(D7, 0) + ISNULL(D8, 0) AS Total,
					(SELECT ISNULL(D2, 0) + ISNULL(D3, 0) + ISNULL(D4, 0) + ISNULL(D5, 0) + ISNULL(D6, 0) + ISNULL(D7, 0) + ISNULL(D8, 0)
						FROM TBMView a1
						WHERE ProjectID = @ProjectID AND [Month] = @PreMonth AND [Year] = @PreYear AND [Week] = @PrevWeek AND a.TBMCategoryID = a1.TBMCategoryID AND a.ContractorID = a1.ContractorID
					) AS PrevTotal
			FROM TBMView a
			WHERE ProjectID = @ProjectID AND [Month] = @Month AND [Year] = @Year AND [Week] = @Week
	
		UPDATE #TBMs SET Total1 = ISNULL(Total, 0) + ISNULL(PrevTotal, 0)
	
		--get data a previous week of a (previous) month
		INSERT INTO #TBMs_Prev(TBMID, ProjectID, ProjectCode, ProjectName, ContractorID, ContractorCode, ContractorName, CatGroupCode, DataType, TBMCategoryID, [Month], [Year], [Week], [Date], D2, D3, D4, D5, D6, D7, D8, SortOrder_Cont, SortOrder_TBMCat, Total, PrevTotal)
			SELECT	0, ProjectID, ProjectCode, ProjectName, ContractorID, ContractorCode, ContractorName, CatGroupCode, DataType, TBMCategoryID, [Month], [Year], [Week], @Date, NULL, NULL, NULL, NULL, NULL, NULL, NULL, SortOrder_Cont, SortOrder_TBMCat,
					NULL, --total
					ISNULL(D2, 0) + ISNULL(D3, 0) + ISNULL(D4, 0) + ISNULL(D5, 0) + ISNULL(D6, 0) + ISNULL(D7, 0) + ISNULL(D8, 0)
			FROM TBMView a
			WHERE ProjectID = @ProjectID AND [Month] = @PreMonth AND [Year] = @PreYear AND [Week] = @PrevWeek
					AND NOT EXISTS (SELECT TOP 1 1 FROM #TBMs a1 WHERE a1.ProjectID = a.ProjectID AND a1.TBMCategoryID = a.TBMCategoryID)
	
		UPDATE #TBMs_Prev SET Total1 = ISNULL(Total, 0) + ISNULL(PrevTotal, 0)
	END
	ELSE  -- báo cáo 01, tất cả những nhà thầu có trong dự án và đã từng có trong dự án
	BEGIN
		INSERT INTO #TBMs(TBMID, ProjectID, ProjectCode, ProjectName, ContractorID, ContractorCode, ContractorName, CatGroupCode, DataType, TBMCategoryID, [Month], [Year], [Week], [Date], D2, D3, D4, D5, D6, D7, D8, SortOrder_Cont, SortOrder_TBMCat, Total, PrevTotal)
			SELECT	*, 
					ISNULL(D2, 0) + ISNULL(D3, 0) + ISNULL(D4, 0) + ISNULL(D5, 0) + ISNULL(D6, 0) + ISNULL(D7, 0) + ISNULL(D8, 0) AS Total,
					(SELECT ISNULL(D2, 0) + ISNULL(D3, 0) + ISNULL(D4, 0) + ISNULL(D5, 0) + ISNULL(D6, 0) + ISNULL(D7, 0) + ISNULL(D8, 0)
						FROM TBMAllContractorView a1
						WHERE ProjectID = @ProjectID AND [Month] = @PreMonth AND [Year] = @PreYear AND [Week] = @PrevWeek AND a.TBMCategoryID = a1.TBMCategoryID AND a.ContractorID = a1.ContractorID
					) AS PrevTotal
			FROM TBMAllContractorView a
			WHERE ProjectID = @ProjectID AND [Month] = @Month AND [Year] = @Year AND [Week] = @Week
	
		UPDATE #TBMs SET Total1 = ISNULL(Total, 0) + ISNULL(PrevTotal, 0)
	
		--get data a previous week of a (previous) month
		INSERT INTO #TBMs_Prev(TBMID, ProjectID, ProjectCode, ProjectName, ContractorID, ContractorCode, ContractorName, CatGroupCode, DataType, TBMCategoryID, [Month], [Year], [Week], [Date], D2, D3, D4, D5, D6, D7, D8, SortOrder_Cont, SortOrder_TBMCat, Total, PrevTotal)
			SELECT	0, ProjectID, ProjectCode, ProjectName, ContractorID, ContractorCode, ContractorName, CatGroupCode, DataType, TBMCategoryID, [Month], [Year], [Week], @Date, NULL, NULL, NULL, NULL, NULL, NULL, NULL, SortOrder_Cont, SortOrder_TBMCat,
					NULL, --total
					ISNULL(D2, 0) + ISNULL(D3, 0) + ISNULL(D4, 0) + ISNULL(D5, 0) + ISNULL(D6, 0) + ISNULL(D7, 0) + ISNULL(D8, 0)
			FROM TBMAllContractorView a
			WHERE ProjectID = @ProjectID AND [Month] = @PreMonth AND [Year] = @PreYear AND [Week] = @PrevWeek
					AND NOT EXISTS (SELECT TOP 1 1 FROM #TBMs a1 WHERE a1.ProjectID = a.ProjectID AND a1.TBMCategoryID = a.TBMCategoryID)
	
		UPDATE #TBMs_Prev SET Total1 = ISNULL(Total, 0) + ISNULL(PrevTotal, 0)
	END

	--merge data
	INSERT INTO #TBMs1(TBMID, ProjectID, ProjectCode, ProjectName, ContractorID, ContractorCode, ContractorName, CatGroupCode, DataType, TBMCategoryID, TBMCode, TBMName, [Month], [Year], [Week], [Date], D2, D3, D4, D5, D6, D7, D8, Total, PrevTotal, SortOrder_Cont, SortOrder_TBMCat)
		SELECT TBMID, ProjectID, ProjectCode, ProjectName, ContractorID, ContractorCode, ContractorName, CatGroupCode, b1.DataType, b1.TBMCategoryID, TBMCode, TBMName, [Month], [Year], [Week], [Date], D2, D3, D4, D5, D6, D7, D8, Total, PrevTotal, SortOrder_Cont, SortOrder_TBMCat
		FROM (
			SELECT CASE WHEN TBMID IS NULL THEN 0 ELSE TBMID END AS TBMID, ProjectID, ProjectCode, ProjectName, ContractorID, ContractorCode, ContractorName, CatGroupCode, DataType, TBMCategoryID, @Month AS [Month], @Year AS [Year], @Week AS [Week], @Date AS [Date], D2, D3, D4, D5, D6, D7, D8, Total, PrevTotal, SortOrder_Cont, SortOrder_TBMCat
			FROM (
				SELECT * FROM #TBMs
				UNION
				SELECT * FROM #TBMs_Prev
			) AS a1
		) AS b1
			LEFT JOIN TBMCategory t ON (t.TBMCategoryID = b1.TBMCategoryID AND t.IsTotalColumn = 0)
			
	DELETE FROM #TBMs
	
	--bổ sung những dự án còn thiếu và những danh mục còn thiếu
	IF @IsReport01 = 0
	BEGIN
		INSERT INTO #TBMs(TBMID, ProjectID, ProjectCode, ProjectName, ContractorID, ContractorCode, ContractorName, CatGroupCode, DataType, TBMCategoryID, TBMCode, TBMName, [Month], [Year], [Week], [Date], SortOrder_Cont, SortOrder_TBMCat)
			SELECT DISTINCT 0,  @ProjectID, p.ProjectCode, p.ProjectName, c.ContractorID, c.ContractorCode, c.ContractorName, GroupCode, t.DataType, t.TBMCategoryID, TBMCode, t.TBMName, @Month, @Year, @Week, @Date, c.SortOrder, t.SortOrder
			FROM Contractor c
				INNER JOIN Project_Contractor pc ON (pc.ContractorID = c.ContractorID AND pc.ProjectID = @ProjectID AND pc.Deleted = 0 AND c.Deleted = 0 AND c.IsActive = 1)
				RIGHT JOIN TBMCategory t ON (t.Deleted = 0 AND t.IsActive = 1 AND t.IsTotalColumn = 0)
				INNER JOIN Project p ON (p.ProjectID = pc.ProjectID AND p.Deleted = 0 AND (p.StatusID = 1 OR p.StatusID = 2))
			WHERE NOT EXISTS (SELECT TOP 1 1 FROM #TBMs1 k WHERE c.ContractorID = k.ContractorID AND t.TBMCategoryID = k.TBMCategoryID)
	END
	ELSE
	BEGIN
		INSERT INTO #TBMs(TBMID, ProjectID, ProjectCode, ProjectName, ContractorID, ContractorCode, ContractorName, CatGroupCode, DataType, TBMCategoryID, TBMCode, TBMName, [Month], [Year], [Week], [Date], SortOrder_Cont, SortOrder_TBMCat)
			SELECT DISTINCT 0,  @ProjectID, p.ProjectCode, p.ProjectName, c.ContractorID, c.ContractorCode, c.ContractorName, GroupCode, t.DataType, t.TBMCategoryID, TBMCode, t.TBMName, @Month, @Year, @Week, @Date, c.SortOrder, t.SortOrder
			FROM Contractor c
				--INNER JOIN Project_Contractor pc ON (pc.ContractorID = c.ContractorID AND pc.ProjectID = @ProjectID AND c.Deleted = 0 AND c.IsActive = 1)
				--INNER JOIN TBM tb ON (tb.ContractorID = c.ContractorID AND tb.ProjectID = 1022 AND tb.Deleted = 0 AND tb.[Month] = @Month and tb.[Year] = @Year AND tb.[Week] = @Week)
				--INNER JOIN TBMDetail d ON (d.TBMID = tb.TBMID AND d.Deleted = 0 AND ISNULL(d.NumOfEmployees, 0) > 0)
				INNER JOIN (
					SELECT DISTINCT ContractorID, ProjectID FROM #TBMs1

					UNION

					--lấy những nhà thầu có trong dự án
					SELECT DISTINCT c.ContractorID, @ProjectID AS ProjectID
					FROM Contractor c
						INNER JOIN Project_Contractor pc ON (pc.ContractorID = c.ContractorID AND pc.ProjectID = @ProjectID AND c.Deleted = 0 AND pc.Deleted = 0 AND c.IsActive = 1)

					UNION 

					--lấy những nhà thầu đã từng có trong dự án và có dữ liệu
					SELECT DISTINCT c.ContractorID, @ProjectID AS ProjectID
					FROM Contractor c
						INNER JOIN Project_Contractor pc ON (pc.ContractorID = c.ContractorID AND pc.ProjectID = @ProjectID AND c.Deleted = 0 AND c.IsActive = 1)
						INNER JOIN TBM t ON (t.ContractorID = c.ContractorID AND t.ProjectID = @ProjectID AND t.Deleted = 0 AND t.[Month] = @Month and t.[Year] = @Year AND t.[Week] = @Week)
						INNER JOIN TBMDetail d ON (d.TBMID = t.TBMID AND d.Deleted = 0 AND ISNULL(d.NumOfEmployees, 0) > 0)
				) pc ON (pc.ContractorID = c.ContractorID)
				RIGHT JOIN TBMCategory t ON (t.Deleted = 0 AND t.IsActive = 1 AND t.IsTotalColumn = 0)
				INNER JOIN Project p ON (p.ProjectID = pc.ProjectID AND p.Deleted = 0 AND (p.StatusID = 1 OR p.StatusID = 2))
			WHERE NOT EXISTS (SELECT TOP 1 1 FROM #TBMs1 k WHERE c.ContractorID = k.ContractorID AND t.TBMCategoryID = k.TBMCategoryID)
	END
	
	-- merge data
	INSERT INTO #TBMs(TBMID, ProjectID, ProjectCode, ProjectName, ContractorID, ContractorCode, ContractorName, CatGroupCode, DataType, TBMCategoryID, TBMCode, TBMName, [Month], [Year], [Week], [Date], D2, D3, D4, D5, D6, D7, D8, Total, PrevTotal, Total1, SortOrder_Cont, SortOrder_TBMCat)
		SELECT TBMID, ProjectID, ProjectCode, ProjectName, ContractorID, ContractorCode, ContractorName, CatGroupCode, DataType, TBMCategoryID, TBMCode, TBMName, [Month], [Year], [Week], [Date], D2, D3, D4, D5, D6, D7, D8, Total, PrevTotal, Total1, SortOrder_Cont, SortOrder_TBMCat
		FROM #TBMs1
	
	DELETE FROM #TBMs1

	DECLARE @MaxSortOrder INT
	SELECT @MaxSortOrder = MAX(SortOrder_Cont) FROM #TBMs
	SET @MaxSortOrder = ISNULL(@MaxSortOrder, 0) + 1

	-- add footer rows
	INSERT INTO #TBMs1(TBMID, ProjectID, ProjectCode, ProjectName, ContractorID, ContractorCode, ContractorName, CatGroupCode, DataType, TBMCategoryID, TBMCode, TBMName, [Month], [Year], [Week], [Date], SortOrder_Cont, SortOrder_TBMCat, D2, D3, D4, D5, D6, D7, D8, Total, PrevTotal, Total1)
		SELECT -1, @ProjectID, NULL, NULL, -1, N'Tổng cộng', N'Tổng cộng', GroupCode, DataType, TBMCategoryID, TBMCode, TBMName, @Month, @Year, @Week, @Date, @MaxSortOrder, SortOrder,
				(SELECT SUM(ISNULL(D2, 0)) FROM #TBMs m WHERE m.TBMCategoryID = t.TBMCategoryID),
				(SELECT SUM(ISNULL(D3, 0)) FROM #TBMs m WHERE m.TBMCategoryID = t.TBMCategoryID),
				(SELECT SUM(ISNULL(D4, 0)) FROM #TBMs m WHERE m.TBMCategoryID = t.TBMCategoryID),
				(SELECT SUM(ISNULL(D5, 0)) FROM #TBMs m WHERE m.TBMCategoryID = t.TBMCategoryID),
				(SELECT SUM(ISNULL(D6, 0)) FROM #TBMs m WHERE m.TBMCategoryID = t.TBMCategoryID),
				(SELECT SUM(ISNULL(D7, 0)) FROM #TBMs m WHERE m.TBMCategoryID = t.TBMCategoryID),
				(SELECT SUM(ISNULL(D8, 0)) FROM #TBMs m WHERE m.TBMCategoryID = t.TBMCategoryID),
				(SELECT SUM(ISNULL(Total, 0)) FROM #TBMs m WHERE m.TBMCategoryID = t.TBMCategoryID),
				(SELECT SUM(ISNULL(PrevTotal, 0)) FROM #TBMs m WHERE m.TBMCategoryID = t.TBMCategoryID),
				(SELECT SUM(ISNULL(Total1, 0)) FROM #TBMs m WHERE m.TBMCategoryID = t.TBMCategoryID)
		FROM TBMCategory t
		WHERE Deleted = 0 AND IsActive = 1 AND IsTotalColumn = 0

	UPDATE #TBMs1
	SET D2 = CASE WHEN D2 = 0 THEN NULL ELSE D2 END,
		D3 = CASE WHEN D3 = 0 THEN NULL ELSE D3 END,
		D4 = CASE WHEN D4 = 0 THEN NULL ELSE D4 END,
		D5 = CASE WHEN D5 = 0 THEN NULL ELSE D5 END,
		D6 = CASE WHEN D6 = 0 THEN NULL ELSE D6 END,
		D7 = CASE WHEN D7 = 0 THEN NULL ELSE D7 END,
		D8 = CASE WHEN D8 = 0 THEN NULL ELSE D8 END,
		Total = CASE WHEN Total = 0 THEN NULL ELSE Total END
	WHERE TBMID = -1

	--merge data
	INSERT INTO #TBMs1(TBMID, ProjectID, ProjectCode, ProjectName, ContractorID, ContractorCode, ContractorName, CatGroupCode, DataType, TBMCategoryID, TBMCode, TBMName, [Month], [Year], [Week], [Date], SortOrder_Cont, SortOrder_TBMCat, D2, D3, D4, D5, D6, D7, D8, Total, PrevTotal, Total1)
		SELECT TBMID, ProjectID, ProjectCode, ProjectName, ContractorID, ContractorCode, ContractorName, CatGroupCode, DataType, TBMCategoryID, TBMCode, TBMName, [Month], [Year], [Week], [Date], SortOrder_Cont, SortOrder_TBMCat, D2, D3, D4, D5, D6, D7, D8, Total, PrevTotal, Total1
		FROM #TBMs

	--update rate
	UPDATE #TBMs1 SET D2 = NULL WHERE D2 = 0
	UPDATE #TBMs1 SET D3 = NULL WHERE D3 = 0
	UPDATE #TBMs1 SET D4 = NULL WHERE D4 = 0
	UPDATE #TBMs1 SET D5 = NULL WHERE D5 = 0
	UPDATE #TBMs1 SET D6 = NULL WHERE D6 = 0
	UPDATE #TBMs1 SET D7 = NULL WHERE D7 = 0
	UPDATE #TBMs1 SET D8 = NULL WHERE D8 = 0
	UPDATE #TBMs1 SET PrevTotal = NULL WHERE PrevTotal = 0
	UPDATE #TBMs1 SET Total = NULL WHERE Total = 0
	
	UPDATE #TBMs1
	SET D2 = (SELECT TOP 1 ISNULL(D2, 0) FROM #TBMs1 t WHERE t.ContractorID = #TBMs1.ContractorID AND t.TBMCategoryID = 2) / (SELECT TOP 1 ISNULL(D2, 1) FROM #TBMs1 t WHERE t.ContractorID = #TBMs1.ContractorID AND t.TBMCategoryID = 1) * 100,
		D3 = (SELECT TOP 1 ISNULL(D3, 0) FROM #TBMs1 t WHERE t.ContractorID = #TBMs1.ContractorID AND t.TBMCategoryID = 2) / (SELECT TOP 1 ISNULL(D3, 1) FROM #TBMs1 t WHERE t.ContractorID = #TBMs1.ContractorID AND t.TBMCategoryID = 1) * 100,
		D4 = (SELECT TOP 1 ISNULL(D4, 0) FROM #TBMs1 t WHERE t.ContractorID = #TBMs1.ContractorID AND t.TBMCategoryID = 2) / (SELECT TOP 1 ISNULL(D4, 1) FROM #TBMs1 t WHERE t.ContractorID = #TBMs1.ContractorID AND t.TBMCategoryID = 1) * 100,
		D5 = (SELECT TOP 1 ISNULL(D5, 0) FROM #TBMs1 t WHERE t.ContractorID = #TBMs1.ContractorID AND t.TBMCategoryID = 2) / (SELECT TOP 1 ISNULL(D5, 1) FROM #TBMs1 t WHERE t.ContractorID = #TBMs1.ContractorID AND t.TBMCategoryID = 1) * 100,
		D6 = (SELECT TOP 1 ISNULL(D6, 0) FROM #TBMs1 t WHERE t.ContractorID = #TBMs1.ContractorID AND t.TBMCategoryID = 2) / (SELECT TOP 1 ISNULL(D6, 1) FROM #TBMs1 t WHERE t.ContractorID = #TBMs1.ContractorID AND t.TBMCategoryID = 1) * 100,
		D7 = (SELECT TOP 1 ISNULL(D7, 0) FROM #TBMs1 t WHERE t.ContractorID = #TBMs1.ContractorID AND t.TBMCategoryID = 2) / (SELECT TOP 1 ISNULL(D7, 1) FROM #TBMs1 t WHERE t.ContractorID = #TBMs1.ContractorID AND t.TBMCategoryID = 1) * 100,
		D8 = (SELECT TOP 1 ISNULL(D8, 0) FROM #TBMs1 t WHERE t.ContractorID = #TBMs1.ContractorID AND t.TBMCategoryID = 2) / (SELECT TOP 1 ISNULL(D8, 1) FROM #TBMs1 t WHERE t.ContractorID = #TBMs1.ContractorID AND t.TBMCategoryID = 1) * 100,
		Total = (SELECT TOP 1 ISNULL(Total, 0) FROM #TBMs1 t WHERE t.ContractorID = #TBMs1.ContractorID AND t.TBMCategoryID = 2) / (SELECT TOP 1 ISNULL(Total, 1) FROM #TBMs1 t WHERE t.ContractorID = #TBMs1.ContractorID AND t.TBMCategoryID = 1) * 100
	WHERE TBMCategoryID = 3

	UPDATE #TBMs1
	SET D2 = (SELECT TOP 1 ISNULL(D2, 0) FROM #TBMs1 t WHERE t.ContractorID = #TBMs1.ContractorID AND t.TBMCategoryID = 5) / (SELECT TOP 1 ISNULL(D2, 1) FROM #TBMs1 t WHERE t.ContractorID = #TBMs1.ContractorID AND t.TBMCategoryID = 4) * 100,
		D3 = (SELECT TOP 1 ISNULL(D3, 0) FROM #TBMs1 t WHERE t.ContractorID = #TBMs1.ContractorID AND t.TBMCategoryID = 5) / (SELECT TOP 1 ISNULL(D3, 1) FROM #TBMs1 t WHERE t.ContractorID = #TBMs1.ContractorID AND t.TBMCategoryID = 4) * 100,
		D4 = (SELECT TOP 1 ISNULL(D4, 0) FROM #TBMs1 t WHERE t.ContractorID = #TBMs1.ContractorID AND t.TBMCategoryID = 5) / (SELECT TOP 1 ISNULL(D4, 1) FROM #TBMs1 t WHERE t.ContractorID = #TBMs1.ContractorID AND t.TBMCategoryID = 4) * 100,
		D5 = (SELECT TOP 1 ISNULL(D5, 0) FROM #TBMs1 t WHERE t.ContractorID = #TBMs1.ContractorID AND t.TBMCategoryID = 5) / (SELECT TOP 1 ISNULL(D5, 1) FROM #TBMs1 t WHERE t.ContractorID = #TBMs1.ContractorID AND t.TBMCategoryID = 4) * 100,
		D6 = (SELECT TOP 1 ISNULL(D6, 0) FROM #TBMs1 t WHERE t.ContractorID = #TBMs1.ContractorID AND t.TBMCategoryID = 5) / (SELECT TOP 1 ISNULL(D6, 1) FROM #TBMs1 t WHERE t.ContractorID = #TBMs1.ContractorID AND t.TBMCategoryID = 4) * 100,
		D7 = (SELECT TOP 1 ISNULL(D7, 0) FROM #TBMs1 t WHERE t.ContractorID = #TBMs1.ContractorID AND t.TBMCategoryID = 5) / (SELECT TOP 1 ISNULL(D7, 1) FROM #TBMs1 t WHERE t.ContractorID = #TBMs1.ContractorID AND t.TBMCategoryID = 4) * 100,
		D8 = (SELECT TOP 1 ISNULL(D8, 0) FROM #TBMs1 t WHERE t.ContractorID = #TBMs1.ContractorID AND t.TBMCategoryID = 5) / (SELECT TOP 1 ISNULL(D8, 1) FROM #TBMs1 t WHERE t.ContractorID = #TBMs1.ContractorID AND t.TBMCategoryID = 4) * 100,
		Total = (SELECT TOP 1 ISNULL(Total, 0) FROM #TBMs1 t WHERE t.ContractorID = #TBMs1.ContractorID AND t.TBMCategoryID = 5) / (SELECT TOP 1 ISNULL(Total, 1) FROM #TBMs1 t WHERE t.ContractorID = #TBMs1.ContractorID AND t.TBMCategoryID = 4) * 100
	WHERE TBMCategoryID = 6

	--update rate if TBMCategoryID = 3 or 6 has total = null or 0
	UPDATE #TBMs1 SET Total = 0 WHERE TBMCategoryID = 3 AND EXISTS (SELECT TOP 1 1 FROM #TBMs1 r WHERE r.ProjectID = #TBMs1.ProjectID AND r.TBMCategoryID = #TBMs1.TBMCategoryID AND r.TBMCategoryID = 1 AND ISNULL(r.Total, 0) = 0)
	--UPDATE #TBMs1 SET PrevTotal = 0 WHERE TBMCategoryID = 3 AND EXISTS (SELECT TOP 1 1 FROM #TBMs1 r WHERE r.ProjectID = #TBMs1.ProjectID AND r.TBMCategoryID = 1 AND ISNULL(r.PrevTotal, 0) = 0)

	UPDATE #TBMs1 SET Total = 0 WHERE TBMCategoryID = 6 AND EXISTS (SELECT TOP 1 1 FROM #TBMs1 r WHERE r.ProjectID = #TBMs1.ProjectID AND r.TBMCategoryID = #TBMs1.TBMCategoryID AND r.TBMCategoryID = 4 AND ISNULL(r.Total, 0) = 0)
	--UPDATE #TBMs1 SET PrevTotal = 0 WHERE TBMCategoryID = 6 AND EXISTS (SELECT TOP 1 1 FROM #TBMs1 r WHERE r.ProjectID = #TBMs1.ProjectID AND r.TBMCategoryID = 4 AND ISNULL(r.PrevTotal, 0) = 0)

	--update for report
	UPDATE #TBMs1 SET ThisWeek_C1 = ( SELECT SUM(ISNULL(Total, 0)) FROM #TBMs1 t WHERE (t.TBMCategoryID = 1 OR t.TBMCategoryID = 4) AND t.ContractorID = #TBMs1.ContractorID)
	UPDATE #TBMs1 SET ThisWeek_C2 = ( SELECT SUM(ISNULL(Total, 0)) FROM #TBMs1 t WHERE (t.TBMCategoryID = 2 OR t.TBMCategoryID = 5) AND t.ContractorID = #TBMs1.ContractorID)
	UPDATE #TBMs1 SET ThisWeek_C3 = CASE WHEN ISNULL(ThisWeek_C1, 0) <> 0 THEN ( ISNULL(ThisWeek_C2, 0) / ISNULL(ThisWeek_C1, 0) * 100 ) ELSE 0 END

	UPDATE #TBMs1 SET LastWeek_C1 = ( SELECT SUM(ISNULL(PrevTotal, 0)) FROM #TBMs1 t WHERE (t.TBMCategoryID = 1 OR t.TBMCategoryID = 4) AND t.ContractorID = #TBMs1.ContractorID)
	UPDATE #TBMs1 SET LastWeek_C2 = ( SELECT SUM(ISNULL(PrevTotal, 0)) FROM #TBMs1 t WHERE (t.TBMCategoryID = 2 OR t.TBMCategoryID = 5) AND t.ContractorID = #TBMs1.ContractorID)
	UPDATE #TBMs1 SET LastWeek_C3 = CASE WHEN ISNULL(LastWeek_C1, 0) <> 0 THEN ( ISNULL(LastWeek_C2, 0) / ISNULL(LastWeek_C1, 0) * 100 ) ELSE 0 END

	UPDATE #TBMs1 SET Total_C1 = ISNULL(ThisWeek_C1, 0) + ISNULL(LastWeek_C1, 0)
	UPDATE #TBMs1 SET Total_C2 = ISNULL(ThisWeek_C2, 0) + ISNULL(LastWeek_C2, 0)
	UPDATE #TBMs1 SET Total_C3 = CASE WHEN ISNULL(Total_C1, 0) <> 0 THEN ( ISNULL(Total_C2, 0) / ISNULL(Total_C1, 0) * 100 ) ELSE 0 END
	--update for report - end

	UPDATE #TBMs1 
	SET D2 = CASE WHEN ISNULL(D2, 0) = 0 THEN NULL ELSE D2 END,
		D3 = CASE WHEN ISNULL(D3, 0) = 0 THEN NULL ELSE D3 END,
		D4 = CASE WHEN ISNULL(D4, 0) = 0 THEN NULL ELSE D4 END,
		D5 = CASE WHEN ISNULL(D5, 0) = 0 THEN NULL ELSE D5 END,
		D6 = CASE WHEN ISNULL(D6, 0) = 0 THEN NULL ELSE D6 END,
		D7 = CASE WHEN ISNULL(D7, 0) = 0 THEN NULL ELSE D7 END,
		D8 = CASE WHEN ISNULL(D8, 0) = 0 THEN NULL ELSE D8 END,
		Total = CASE WHEN ISNULL(Total, 0) = 0 THEN NULL ELSE Total END,
		PrevTotal = CASE WHEN ISNULL(PrevTotal, 0) = 0 THEN NULL ELSE PrevTotal END,
		Total1 = CASE WHEN ISNULL(Total1, 0) = 0 THEN NULL ELSE Total1 END,
		ThisWeek_C1 = CASE WHEN ISNULL(ThisWeek_C1, 0) = 0 THEN NULL ELSE ThisWeek_C1 END,
		ThisWeek_C2 = CASE WHEN ISNULL(ThisWeek_C2, 0) = 0 THEN NULL ELSE ThisWeek_C2 END,
		ThisWeek_C3 = CASE WHEN ISNULL(ThisWeek_C3, 0) = 0 THEN NULL ELSE ThisWeek_C3 END,
		LastWeek_C1 = CASE WHEN ISNULL(LastWeek_C1, 0) = 0 THEN NULL ELSE LastWeek_C1 END,
		LastWeek_C2 = CASE WHEN ISNULL(LastWeek_C2, 0) = 0 THEN NULL ELSE LastWeek_C2 END,
		LastWeek_C3 = CASE WHEN ISNULL(LastWeek_C3, 0) = 0 THEN NULL ELSE LastWeek_C3 END,
		Total_C1 = CASE WHEN ISNULL(Total_C1, 0) = 0 THEN NULL ELSE Total_C1 END,
		Total_C2 = CASE WHEN ISNULL(Total_C2, 0) = 0 THEN NULL ELSE Total_C2 END,
		Total_C3 = CASE WHEN ISNULL(Total_C3, 0) = 0 THEN NULL ELSE Total_C3 END

	--show data
	SELECT * FROM #TBMs1 ORDER BY SortOrder_Cont, ContractorName, SortOrder_TBMCat, TBMName

	DROP TABLE #TBMs_Prev
	DROP TABLE #TBMs1
	DROP TABLE #TBMs
END