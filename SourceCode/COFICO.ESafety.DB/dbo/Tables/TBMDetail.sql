﻿CREATE TABLE [dbo].[TBMDetail] (
    [TBMDetailID]    INT           IDENTITY (1, 1) NOT NULL,
    [TBMID]          INT           NOT NULL,
    [TBMCategoryID]  INT           NULL,
    [DayOfWeek]      TINYINT       CONSTRAINT [DF_TBMDetail_DayOfWeek] DEFAULT ((0)) NOT NULL,
    [NumOfEmployees] INT           NULL,
    [Date]           SMALLDATETIME NOT NULL,
    [Deleted]        BIT           CONSTRAINT [DF_TBMDetail_Deleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_TBMDetail] PRIMARY KEY CLUSTERED ([TBMDetailID] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'2: Mon; 3: Tue; ....; 8: Sun', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TBMDetail', @level2type = N'COLUMN', @level2name = N'DayOfWeek';

