﻿using COFICO.ESafety.DAL;
using COFICO.ESafety.DAL.Services;
using COFICO.ESafety.helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Security.Claims;
using System.Web;
using System.Web.Http;

namespace COFICO.ESafety.API
{
    [RoutePrefix("api/category")]
    public class CategoryController : ApiController
    {
        [HttpGet]
        [Route("{name}/items")]

        public IHttpActionResult GetList(string name)
        {
            List<RestaurantBase> items = null;
            var ofs = Common.GetOFSKey(Request.GetRequestContext().Principal as ClaimsPrincipal);
            switch(name.ToLower())
            {
                case "table":
                    items = new TableService().GetList(ofs).ToList<RestaurantBase>();
                    break;
                case "status":
                    items = new StatusService().GetList(ofs).ToList<RestaurantBase>();
                    break;
                case "kind":
                    items = new KindService().GetList(ofs).ToList<RestaurantBase>(); 
                    break;
                case "utility":
                    items = new UtilityService().GetList(ofs).ToList<RestaurantBase>();
                    break;
                case "except":
                    items = new ExceptService().GetList(ofs).ToList<RestaurantBase>();
                    break;
                default:
                    items = null;
                    break;
            }
            return Ok(new { items });
        }
        [Authorize]
        [HttpPost]
        [Route("{name}/item")]
        public IHttpActionResult Post([FromUri]string name, [FromBody]RestaurantBase item)
        {
            int? id = null;
            var ofs = Common.GetOFSKey(Request.GetRequestContext().Principal as ClaimsPrincipal);
            item.OfsKey = ofs;
            switch (name.ToLower())
            {
                case "table":
                    Table table = ToDerived<RestaurantBase, Table>(item);
                    using (TableService svc = new TableService())
                    {
                        id = svc.Insert(table, HttpContext.Current.User.Identity.Name);
                    }
                    break;
                case "status":
                    Status status = ToDerived<RestaurantBase, Status>(item);
                    using (StatusService svc = new StatusService())
                    {
                        id = svc.Insert(status, HttpContext.Current.User.Identity.Name);
                    }
                    break;
                case "kind":
                    Kind kind = ToDerived<RestaurantBase, Kind>(item);
                    using (KindService svc = new KindService())
                    {
                        id = svc.Insert(kind, HttpContext.Current.User.Identity.Name);
                    }
                    break;
                case "utility":
                    Utility utility = ToDerived<RestaurantBase, Utility>(item);
                    using (UtilityService svc = new UtilityService())
                    {
                        id = svc.Insert(utility, HttpContext.Current.User.Identity.Name);
                    }
                    break;
                case "except":
                    Except except = ToDerived<RestaurantBase, Except>(item);
                    using (ExceptService svc = new ExceptService())
                    {
                        id = svc.Insert(except, HttpContext.Current.User.Identity.Name);
                    }
                    break;
                default:
                    id = null;
                    break;
            }
            
            return Ok(new { id });
        }
        [Authorize]
        [HttpPut]
        public IHttpActionResult Put(Except item)
        {
            using (ExceptService svc = new ExceptService())
            {
                svc.Update(item, HttpContext.Current.User.Identity.Name);
            }
            return Ok(item);

        }
        [Authorize]
        [HttpDelete]
        public IHttpActionResult Delete(Except item)
        {
            using (ExceptService svc = new ExceptService())
            {
                svc.Delete(item, HttpContext.Current.User.Identity.Name);
            }
            return Ok(item);
        }
        //https://www.codeproject.com/Articles/34105/Convert-Base-Type-to-Derived-Type
        public TDerived ToDerived<TBase, TDerived>(TBase tBase)
            where TDerived : TBase, new()
        {
            TDerived tDerived = new TDerived();
            foreach (PropertyInfo propBase in typeof(TBase).GetProperties())
            {
                PropertyInfo propDerived = typeof(TDerived).GetProperty(propBase.Name);
                propDerived.SetValue(tDerived, propBase.GetValue(tBase, null), null);
            }
            return tDerived;
        }
    }
}
