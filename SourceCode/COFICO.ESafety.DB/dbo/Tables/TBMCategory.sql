﻿CREATE TABLE [dbo].[TBMCategory] (
    [TBMCategoryID] INT            IDENTITY (1, 1) NOT NULL,
    [TBMCode]       NVARCHAR (50)  NULL,
    [TBMName]       NVARCHAR (255) NULL,
    [ParentID]      INT            NULL,
    [IsParent]      BIT            CONSTRAINT [DF_TBMCategory_IsParent] DEFAULT ((0)) NOT NULL,
    [GroupCode]     NVARCHAR (50)  NULL,
    [DataType]      VARCHAR (10)   NULL,
    [IsTotalColumn] BIT            CONSTRAINT [DF_TBMCategory_IsTotalColumn] DEFAULT ((0)) NOT NULL,
    [IsActive]      BIT            CONSTRAINT [DF_TBMCategory_IsActive] DEFAULT ((0)) NOT NULL,
    [SortOrder]     INT            NULL,
    [Created]       DATETIME       NOT NULL,
    [CreatedBy]     NVARCHAR (50)  NULL,
    [Modified]      DATETIME       NULL,
    [ModifiedBy]    NVARCHAR (50)  NULL,
    [Deleted]       BIT            CONSTRAINT [DF_TBMCategory_Deleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_TBMCategory] PRIMARY KEY CLUSTERED ([TBMCategoryID] ASC)
);

