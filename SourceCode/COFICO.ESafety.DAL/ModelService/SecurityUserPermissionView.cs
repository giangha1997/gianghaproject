﻿using System.ComponentModel.DataAnnotations;

namespace COFICO.ESafety.DAL.ServiceModels
{
    public partial class SecurityUserPermissionView
    {
        [Key]
        [StringLength(50)]
        public string FeatureCode { get; set; }

        public bool View { get; set; }

        public bool Add { get; set; }

        public bool Edit { get; set; }

        public bool Delete { get; set; }

        public bool Approval { get; set; }
    }
}
