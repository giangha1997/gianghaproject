﻿using COFICO.ESafety.AppCode;
using COFICO.ESafety.DAL.DBModel;
using COFICO.ESafety.DAL.Services;
using System.Collections.Generic;
using System.Web.Http;

namespace COFICO.ESafety
{
    //[Authorize]
    [RoutePrefix("api/ImageCategory")]
    public class ImageCategoryController : ApiController
    {
        [HttpGet, Route("items")]
        public IHttpActionResult Items()
        {
            var items = new List<ImageCategory>();
            using (var svc = new ImageCategoryService())
            {
                items = svc.GetImageCategories();
            }
            return Ok(new { items });
        }

        [HttpPut]
        public IHttpActionResult Put(ImageCategory item)
        {
            var items = new Response<object>();
            items.IsError = true;
            items.Result = -1;// update error

            using (var svc = new ImageCategoryService())
            {
                items.Id = svc.Update(item);
                if (items.Id != null)
                {
                    items.IsError = false;
                    items.Result = 1;// update success
                }
            }
            return Ok(new { items });
        }

        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            var items = new Response<object>();
            items.IsError = true;
            items.Result = -1;
            items.Id = id;

            bool? rs = false;
            using (var svc = new ImageCategoryService())
            {
                rs = svc.Delete(id);
                if (rs == true)
                {
                    items.IsError = false;
                    items.Result = 1;
                }
            }
            return Ok(new { items });
        }
    }
}