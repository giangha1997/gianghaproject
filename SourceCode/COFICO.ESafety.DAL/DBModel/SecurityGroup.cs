﻿using System;
using System.ComponentModel.DataAnnotations;

namespace COFICO.ESafety.DAL.DBModel
{
    public partial class SecurityGroup
    {
        [Key]
        public int RowId { get; set; }

        [StringLength(20)]
        public string Code { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        public bool IsSystem { get; set; }

        public DateTime? Created { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        public bool? Deleted { get; set; }
    }
}
