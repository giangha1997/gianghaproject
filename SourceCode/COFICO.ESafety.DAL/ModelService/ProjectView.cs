﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using COFICO.ESafety.DAL.DBModel;

namespace COFICO.ESafety.DAL.ServiceModels
{
    [Table("ProjectView")]
    public class ProjectView : ProjectBase
    {
        public string StatusName { get; set; }

        [NotMapped]
        public List<Project_User> Users { get; set; }

        [NotMapped]
        public List<Project_User> Engineers { get; set; }

        [NotMapped]
        public List<Project_User> SiteCommanders { get; set; }

        [NotMapped]
        public List<ProjectContractorView> Contractors { get; set; }

        [NotMapped]
        public List<Project_TowerFacZone> TowerFacs { get; set; }
    }

}
