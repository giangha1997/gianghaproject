﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using COFICO.ESafety.DAL.DBModel;
using COFICO.ESafety.DAL.Constants;
using COFICO.ESafety.Base;

namespace COFICO.ESafety.DAL.Services
{
    public class ContractorService : GenericService<Contractor>
    {
        public List<Contractor> GetContractors()
        {
            List<Contractor> lst = null;
            try
            {
                lst = _db.Contractors.Where(x => !x.Deleted).OrderBy(x => x.SortOrder).ThenBy(x => x.ContractorCode).ToList();
                return lst;
            }
            catch (Exception ex)
            {
                Logger.Error("GetContractors", ex);
                throw ex;
            }
        }

        public List<Contractor> GetActiveContractors()
        {
            List<Contractor> lst = null;
            try
            {
                lst = _db.Contractors.Where(x => !x.Deleted && x.IsActive).OrderBy(x => x.SortOrder).ThenBy(x => x.ContractorCode).ToList();
                return lst;
            }
            catch (Exception ex)
            {
                Logger.Error("GetActiveContractors", ex);
                throw ex;
            }
        }

        public List<Contractor> GetContractorsByProjectID(int projectID)
        {
            List<Contractor> lst = null;
            try
            {
                var lst1 = _db.Contractors.Join(_db.Project_Contractors,
                                                post => post.ID,
                                                meta => meta.ContractorID,
                                                (post, meta) => new { P = post, M = meta })
                                          .Where(x => !x.P.Deleted && x.P.IsActive && x.M.ProjectID == projectID)
                                          .Select(s => new
                                          {
                                              s.P.ID,
                                              s.P.ContractorCode,
                                              s.P.ContractorName,
                                              s.P.SortOrder
                                          });

                if (lst1.Count() > 0)
                {
                    lst = new List<Contractor>();

                    foreach (var c in lst1)
                        lst.Add(new Contractor
                        {
                            ID = c.ID,
                            ContractorCode = c.ContractorCode,
                            ContractorName = c.ContractorName,
                            IsActive = true,
                            SortOrder = c.SortOrder
                        });
                }

                return lst;
            }
            catch (Exception ex)
            {
                Logger.Error(projectID, ex);
                throw ex;
            }
        }

        private bool ExistCode(int contractorID, string contractorCode)
        {
            try
            {
                return _table.Any(s => ((contractorID == 0 && s.ContractorCode == contractorCode) ||
                                            (contractorID != 0 && s.ID != contractorID && s.ContractorCode == contractorCode)) &&
                                        !s.Deleted);
            }
            catch (Exception ex)
            {
                Logger.Error("ID = " + contractorID + "; Code = " + contractorCode, ex);
                throw ex;
            }
        }

        public int? Update(Contractor p)
        {
            if (p == null) return null;

            if (ExistCode(p.ID, p.ContractorCode)) return -1;

            try
            {
                Contractor entity = null;
                if ((entity = ParseToContractor(p)) != null)
                {
                    SaveChanges();
                    return entity.ID;
                }
                return null;
            }
            catch (Exception ex)
            {
                Logger.Error(p, ex);
                throw ex;
            }
        }

        public bool? Delete(int contractorID)
        {
            try
            {
                //check existance
                string strSql = "EXEC " + StoreProcedures.ExistContractor + " @spContractorID";
                var prContractorID = new SqlParameter("@spContractorID", contractorID);
                if (_db.Database.SqlQuery<bool>(strSql, prContractorID).Single())
                    return false;

                if (DeleteMark(contractorID))
                {
                    SaveChanges();

                    //delete in Project_Contractor if exists
                    using (var service = new ProjectContractorService())
                    {
                        service.DeleteByContractor(contractorID);
                    }

                    return true;
                }
                return null;
            }
            catch (Exception ex)
            {
                Logger.Error(contractorID, ex);
                throw ex;
            }
        }

        private Contractor ParseToContractor(Contractor dto)
        {
            if (dto == null) return null;

            Contractor entity = null;
            if (dto.ID == 0)
            {
                dto.SortOrder = 1;
                InsertObj(ref dto);
                entity = dto;
            }
            else
            {
                entity = _table.SingleOrDefault(x => x.ID == dto.ID && !x.Deleted);
                if (entity == null) return null;

                entity.ContractorCode = dto.ContractorCode;
                entity.ContractorName = dto.ContractorName;
                entity.IsActive = dto.IsActive;
                entity.Note = dto.Note;
                entity.SortOrder = dto.SortOrder;

                UpdateObj(entity);
            }

            return entity;
        }
    }
}
