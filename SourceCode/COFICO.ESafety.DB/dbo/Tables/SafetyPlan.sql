﻿CREATE TABLE [dbo].[SafetyPlan] (
    [SafetyPlanID]    INT            IDENTITY (1, 1) NOT NULL,
    [ProjectID]       INT            NOT NULL,
    [Month]           TINYINT        NOT NULL,
    [Year]            SMALLINT       NOT NULL,
    [Week]            TINYINT        NOT NULL,
    [PlanForWeek]     NVARCHAR (MAX) NULL,
    [PlanForNextWeek] NVARCHAR (MAX) NULL,
    [Created]         DATETIME       NOT NULL,
    [CreatedBy]       NVARCHAR (50)  NULL,
    [Modified]        DATETIME       NULL,
    [ModifiedBy]      NVARCHAR (50)  NULL,
    [Deleted]         BIT            CONSTRAINT [DF_SafetyPlan_Deleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_SafetyPlan] PRIMARY KEY CLUSTERED ([SafetyPlanID] ASC)
);

