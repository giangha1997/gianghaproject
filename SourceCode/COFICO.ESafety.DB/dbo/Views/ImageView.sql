﻿










CREATE View [dbo].[ImageView] As

SELECT t.ImageID, t.ProjectID, p.ProjectCode, p.ProjectName, d.[Type], d.ImageCategoryID, c.CategoryCode, c.CategoryName, t.[Month], t.[Year], t.[Week]
FROM [Image] t 
	INNER JOIN ImageDetail d ON (t.ImageID =  d.ImageID AND t.Deleted = 0 AND d.Deleted = 0)
	LEFT JOIN Project p ON (t.ProjectID = p.ProjectID)
	LEFT JOIN ImageCategory c ON (c.ImageCategoryID = d.ImageCategoryID)
WHERE p.Deleted = 0 AND c.Deleted = 0 AND c.IsActive = 1