﻿using System;
using System.Linq;
using System.Collections.Generic;
using COFICO.ESafety.DAL.DBModel;
using COFICO.ESafety.Base;

namespace COFICO.ESafety.DAL.Services
{
    public class RequestStatusService : GenericService<RequestStatus>
    {
        public List<RequestStatus> GetList()
        {
            try
            {
                return _table.ToList();
            }
            catch (Exception ex)
            {
                Logger.Error("GetList", ex);
                throw ex;
            }
        }
    }
}
