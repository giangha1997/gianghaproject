﻿CREATE TABLE [dbo].[Team] (
    [TeamID]     INT             IDENTITY (1, 1) NOT NULL,
    [TeamCode]   NVARCHAR (50)   NULL,
    [TeamName]   NVARCHAR (255)  NULL,
    [ParentID]   INT             NULL,
    [IsParent]   BIT             CONSTRAINT [DF_Team_IsParent] DEFAULT ((0)) NOT NULL,
    [Note]       NVARCHAR (1000) NULL,
    [IsActive]   BIT             CONSTRAINT [DF_Team_IsActive] DEFAULT ((0)) NOT NULL,
    [SortOrder]  INT             NULL,
    [Created]    DATETIME        NOT NULL,
    [CreatedBy]  NVARCHAR (50)   NULL,
    [Modified]   DATETIME        NULL,
    [ModifiedBy] NVARCHAR (50)   NULL,
    [Deleted]    BIT             CONSTRAINT [DF_Team_Deleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Team] PRIMARY KEY CLUSTERED ([TeamID] ASC)
);

