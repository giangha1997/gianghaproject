﻿CREATE TABLE [dbo].[AccidentDetail] (
    [AccidentDetailID]   INT           IDENTITY (1, 1) NOT NULL,
    [AccidentID]         INT           NOT NULL,
    [AccidentCategoryID] INT           NOT NULL,
    [DayOfWeek]          TINYINT       CONSTRAINT [DF_AccidentDetail_DayOfWeek] DEFAULT ((0)) NOT NULL,
    [NumOfAccidents]     INT           NULL,
    [Date]               SMALLDATETIME NOT NULL,
    [Deleted]            BIT           CONSTRAINT [DF_AccidentDetail_Deleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_AccidentDetail] PRIMARY KEY CLUSTERED ([AccidentDetailID] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'2: Mon; 3: Tue; ....; 8: Sun', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'AccidentDetail', @level2type = N'COLUMN', @level2name = N'DayOfWeek';

