namespace COFICO.ESafety.DAL
{
    using COFICO.ESafety.DAL.DBModel;
    using COFICO.ESafety.DAL.ServiceModels;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Linq;

    public partial class ESafetyModel : DbContext
    {
        public DbSet<Status> Status { get; set; }

        public DbSet<SecurityGroup> SecurityGroups { get; set; }
        public DbSet<SecurityUserGroupView> SecurityUserGroupViews { get; set; }
        public DbSet<SecurityUser> SecurityUsers { get; set; }
        public DbSet<SecurityUserGroup> SecurityUserGroups { get; set; }
        public DbSet<SecurityUserPermission> SecurityUserPermissions { get; set; }
        public DbSet<SecurityGroupPermission> SecurityGroupPermissions { get; set; }

        public DbSet<RequestStatus> RequestStatus { get; set; }

        public DbSet<Project> Projects { get; set; }
        public DbSet<ProjectStatus> ProjectStatuses { get; set; }
        public DbSet<Project_User> Project_Users { get; set; }
        public DbSet<Contractor> Contractors { get; set; }
        public DbSet<Project_Contractor> Project_Contractors { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<ProjectView> ProjectViews { get; set; }
        public DbSet<ProjectContractorView> ProjectContractorViews { get; set; }
        public DbSet<Project_Contractor_Team> Project_Contractor_Teams { get; set; }
        public DbSet<ProjectContractorTeamView> ProjectContractorTeamViews { get; set; }
        public DbSet<Project_TowerFacZone> Project_TowerFacZones { get; set; }
        public DbSet<ImageCategory> ImageCategories { get; set; }
        public DbSet<AccidentCategory> AccidentCategories { get; set; }
        public DbSet<ViolationCategory> ViolationCategories { get; set; }
        
        public DbSet<ViolationPunishment> ViolationPunishments { get; set; }
        public DbSet<TrainingCategory> TrainingCategories { get; set; }
        public DbSet<Hazard> Hazards { get; set; }
        public DbSet<TowerFacZone> TowerFacZones { get; set; }
        public DbSet<HazardView> HazardViews { get; set; }
        public DbSet<HazardCategory> HazardCategories { get; set; }
        public DbSet<GeneralCategory> GeneralCategories { get; set; }
        public DbSet<ApprovalHistory> ApprovalHistories { get; set; }
        public DbSet<ApprovalHistoryView> ApprovalHistoryViews { get; set; }
        public DbSet<UserView> UserViews { get; set; }

        public ESafetyModel()
            : base("name=COFICO_Entities")
        {

        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        public override int SaveChanges()
        {
            HandleChangeTracking();
            return base.SaveChanges();
        }
        private void HandleChangeTracking()
        {
            foreach (var entity in ChangeTracker.Entries()
               .Where(e => e.State == EntityState.Added
                   || e.State == EntityState.Modified))
            {
                UpdateTrackedEntity(entity);
            }
        }
        /// <summary>
        /// Looks at everything that has changed and
        /// applies any further action if required.
        /// </summary>
        /// <param id="entityEntry""></param>
        /// <returns></returns>
        private static void UpdateTrackedEntity(DbEntityEntry entityEntry)
        {
            var trackUpdateClass = entityEntry.Entity as IModifiedEntity;
            if (trackUpdateClass == null) return;
            if (entityEntry.State == EntityState.Added)
            {
                //trackUpdateClass.rowguid = Guid.NewGuid();
                trackUpdateClass.Created = DateTime.UtcNow;
            }
            else
            {
                trackUpdateClass.Modified = DateTime.UtcNow;
            }
        }
    }
}
