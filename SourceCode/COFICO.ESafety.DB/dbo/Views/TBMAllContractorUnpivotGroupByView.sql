﻿




CREATE View [dbo].[TBMAllContractorUnpivotGroupByView] As

SELECT DISTINCT t.ProjectID, p.ProjectName, p.ProjectCode, t.ContractorID, c.ContractorCode, c.ContractorName, tc.GroupCode AS CatGroupCode, tc.DataType, d.TBMCategoryID, t.[Month], t.[Year], SUM(ISNULL(d.NumOfEmployees, 0)) AS NumOfEmployees, c.SortOrder AS SortOrder_Cont, tc.SortOrder AS SortOrder_TBMCat
FROM TBM t 
	INNER JOIN TBMDetail d ON (t.TBMID =  d.TBMID AND t.Deleted = 0 AND d.Deleted = 0)
	INNER JOIN TBMCategory tc ON (tc.TBMCategoryID =  d.TBMCategoryID AND tc.Deleted = 0 AND tc.IsActive = 1 AND tc.IsTotalColumn = 0)
	--INNER JOIN Project_Contractor pc ON (pc.ProjectID = t.ProjectID AND pc.ContractorID = t.ContractorID)
	INNER JOIN Contractor c ON (c.ContractorID = t.ContractorID AND c.Deleted = 0 AND c.IsActive = 1)
	LEFT JOIN Project p ON (t.ProjectID = p.ProjectID)
WHERE p.Deleted = 0
GROUP BY t.ProjectID, p.ProjectName, p.ProjectCode, t.ContractorID, c.ContractorCode, c.ContractorName, tc.GroupCode, tc.DataType, d.TBMCategoryID, t.[Month], t.[Year], c.SortOrder, tc.SortOrder