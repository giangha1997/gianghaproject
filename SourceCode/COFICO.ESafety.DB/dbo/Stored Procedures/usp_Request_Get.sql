﻿
/*==============================================================================================
   Author			Date			Description
   ---------------------------------------------
   Linh Ngo			25.03.2019		get a request
   
============================================================================================= 

Draft = 1;
Submitted = 2;
Processed = 3;
Approved = 4;
Rejected = 5;
*/

CREATE PROCEDURE [dbo].[usp_Request_Get] 
	@requestId INT,
	@requestType TINYINT,
	@username NVARCHAR(100)
AS
BEGIN
	DECLARE @type TINYINT, @requestStatus TINYINT, @result TINYINT;

	IF @requestType = 1
	BEGIN
		--IF NOT EXISTS (SELECT TOP 1 1 FROM Hazard WHERE HazardID = @requestId AND Username = @username AND Deleted = 0) AND
		--	NOT EXISTS (SELECT TOP 1 1 FROM Project_User WHERE ObjectID = @requestId AND Username = @username AND [Type] IN (2, 3) AND Deleted = 0)
		--	SET @result = 1

		--IF (@result IS NULL)
		--BEGIN
		--	SELECT * FROM HazardView WHERE HazardID = @requestId

		--	SET @result = 0
		--END

		DECLARE @projectID INT;
		SELECT @projectID = ProjectID FROM Hazard WHERE HazardID = @requestId AND Deleted = 0;

		SELECT TOP 0 * INTO #projects FROM ProjectView;
		ALTER TABLE #projects ADD ID INT;
		INSERT INTO #projects
			EXEC usp_Project_GetUser @username;

		IF NOT EXISTS (SELECT TOP 1 1 FROM #projects WHERE ProjectID = @projectID)
			SET @result = 1;

		IF (@result IS NULL)
		BEGIN
			SELECT * FROM HazardView WHERE HazardID = @requestId;

			SET @result = 0;
		END

		DROP TABLE #projects;
	END
	ELSE IF @requestType = 2
	BEGIN
		SELECT @type = [Type], @requestStatus = [Status] 
		FROM ViolationForm 
		WHERE ViolationID = @requestId AND [Status] = 1

		IF @type != @requestType OR @requestStatus IS NULL
			SET @result = 1

		--IF (@result IS NULL)
		--BEGIN
		--	UPDATE ViolationForm
		--	SET [Status] = 2, Approver = @approver, 
		--		ApproverLevel = @approverLevel, Modified = GETDATE(), ModifiedBy = @username
		--	WHERE ViolationID  = @requestId

		--	SET @result = 0
		--END
	END

	IF @result IS NULL OR @result = 1
	BEGIN
		IF @requestType = 1
			SELECT * FROM HazardView WHERE HazardID = -1
	END
END