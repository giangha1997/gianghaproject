﻿CREATE TABLE [dbo].[Configuration] (
    [CfgKey]      VARCHAR (50)   NOT NULL,
    [Description] NVARCHAR (500) NULL,
    [CfgValue]    VARCHAR (500)  NOT NULL,
    [Created]     DATETIME       NULL,
    [CreatedBy]   VARCHAR (50)   NULL,
    [Modified]    DATETIME       NULL,
    [ModifiedBy]  VARCHAR (50)   NULL,
    [Deleted]     BIT            NULL,
    CONSTRAINT [PK_Configuration] PRIMARY KEY CLUSTERED ([CfgKey] ASC)
);

