﻿
CREATE PROCEDURE [dbo].[usp_SecurityGroupPermissionsFeaturesByGroupId]
	@groupId int
AS
BEGIN
	SET NOCOUNT ON;
	Select IsNull(p.[RowId],0) [RowId], p.[View], p.[Add], p.[Edit], p.[Delete], p.[Approval], p.[Note], p.[Created], p.[CreatedBy], p.[Modified], p.[ModifiedBy], 
			[SecurityFeatureId] = f.RowId, f.[FeatureCode], f.[FeatureName], @groupId As [SecurityGroupId]
	From [dbo].[SecurityFeatures] f	
		Left Join [dbo].[SecurityGroupPermissions] p on f.RowId = p.[SecurityFeatureId] And p.[SecurityGroupId]= @groupId
	Where (f.Deleted Is Null Or f.Deleted = 0) And (p.Deleted Is Null Or p.Deleted = 0) And @groupId Is Not Null
	Order by f.SortOrder
END