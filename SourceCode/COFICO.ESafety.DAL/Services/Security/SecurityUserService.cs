﻿using System;
using System.Collections.Generic;
using System.Linq;
using COFICO.ESafety.DAL.Constants;
using COFICO.ESafety.DAL.DBModel;
using COFICO.ESafety.DAL.ServiceModels;

namespace COFICO.ESafety.DAL.Services
{
    public class SecurityUserService : GenericService<SecurityUser>
    {
        public List<SecurityGroup> GetSecurityGroups()
        {
            List<SecurityGroup> lst = null;
            try
            {
                lst = _db.SecurityGroups.Where(x => !x.Deleted.HasValue || !x.Deleted.Value).ToList();
                return lst;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SecurityUserGroupView> GetSecurityUserGroupViews(int groupId)
        {
            List<SecurityUserGroupView> lst = null;
            try
            {
                lst = _db.SecurityUserGroupViews.Where(x => x.SecurityGroupId == groupId).ToList();
                return lst;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public SecurityUser GetSecurityUserBySPUser(string spUser)
        //{
        //    SecurityUser result = null;
        //    try
        //    {
        //        string username = Base.Helper.RemoveDomainName(spUser);
        //        result = _db.SecurityUsers.FirstOrDefault(x => x.UserName == username && (!x.Deleted.HasValue || !x.Deleted.Value));
        //        return result;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public bool ExistsSecurityUserByUserGroupId(int userId, int groupId)
        {
            bool result = false;
            try
            {
                result = _db.SecurityUserGroupViews.Any(x => x.SecurityUserId == userId && x.SecurityGroupId == groupId && (!x.Deleted.HasValue || !x.Deleted.Value));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public SecurityUser SecurityUserCreate(string spUser, string userName, string fullName, string requester)
        {
            SecurityUser entity = null;
            entity = new SecurityUser();
            entity.SPUser = spUser;
            entity.UserName = userName;
            entity.FullName = fullName;
            entity.Created = DateTime.Now;
            entity.CreatedBy = requester;
            _db.SecurityUsers.Add(entity);
            _db.SaveChanges();

            return entity;
        }

        //lấy nhân viên của nhóm Ban an toàn
        public List<SecurityUser> GetUserOfSafetyDept()
        {
            try
            {
                string strSql = "EXEC " + StoreProcedures.GetUserOfSafetyDept;
                return _db.Database.SqlQuery<SecurityUser>(strSql).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //cho phép sửa data ngày quá khứ
        //public bool Update_DateInPast(List<SecurityUser> user)
        //{
        //    if (user == null || user.Count == 0) return false;

        //    try
        //    {
        //        //remove if AllowInPast = false
        //        user.RemoveAll(s => !s.AllowInPast);
        //        if (user.Count == 0) return false;

        //        //set all AllowInPast = false
        //        _db.Database.ExecuteSqlCommand("UPDATE SecurityUsers SET AllowInPast = 0 WHERE ISNULL(Deleted, 0) = 0");

        //        var ids = user.Select(s => s.RowId).ToList();

        //        var entities = _db.SecurityUsers.Where(x => ids.Contains(x.RowId) && !x.Deleted.HasValue).ToList();
        //        if (entities == null || entities.Count == 0) return false;

        //        entities.ForEach(s =>
        //        {
        //            var t = user.FirstOrDefault(x => x.RowId == s.RowId);
        //            if (t != null)
        //            {
        //                s.AllowInPast = t.AllowInPast;
        //                s.AllowedDate = t.AllowedDate;
        //                UpdateObj(s);
        //            }
        //        });

        //        SaveChanges();

        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public bool? Update_DateInPast(SecurityUser user)
        {
            if (user == null) return null;

            try
            {
                string username = Base.Helper.RemoveDomainName(user.SPUser);
                var entity = _db.SecurityUsers.FirstOrDefault(x => x.UserName == username && (!x.Deleted.HasValue || !x.Deleted.Value));
                if (entity == null) return false;

                entity.AllowInPast = user.AllowInPast;
                entity.AllowedDate = user.AllowedDate;

                UpdateObj(entity);

                SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SecurityUser> GetUsersCanAllowEditingInPast()
        {
            try
            {
                return _db.SecurityUsers.Where(x => x.AllowInPast && (!x.Deleted.HasValue || !x.Deleted.Value)).OrderBy(s => s.UserName).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool CanAllowEditingInPast()
        {
            try
            {
                string user = Base.Helper.GetCurrentUsername();
                var entity = _db.SecurityUsers.FirstOrDefault(x => x.UserName == user && (!x.Deleted.HasValue || !x.Deleted.Value));
                if (entity == null) return false;

                return entity.AllowInPast;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

    public class SecurityUserGroupService : GenericService<SecurityUserGroup>
    {
        public bool DeleteSecurityUserGroup(int rowId, string requester)
        {
            try
            {
                var obj = _db.SecurityUserGroups.Where(x => x.RowId == rowId).FirstOrDefault();
                if (obj != null)
                {
                    DeleteMark(obj.RowId, requester);

                    _db.SaveChanges();

                    //remove this user in SecurityUserPermissions
                    _db.Database.ExecuteSqlCommand("UPDATE SecurityUserPermissions SET [View] = NULL, [Add] = NULL, [Edit] = NULL, [Delete] = NULL, [Approval] = NULL WHERE SecurityUserId = " + obj.SecurityUserId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return true;
        }

        public int Submit(SecurityUserGroupView item, string userName)
        {
            try
            {
                if (item == null) return 0;
                var obj = ToEntity(item, userName);

                //update FullName
                if(item.RowId > 0)
                {
                    var user = _db.SecurityUsers.FirstOrDefault(s => s.RowId == item.SecurityUserId && (s.Deleted == null || !s.Deleted.Value));
                    if (user != null && user.FullName != item.FullName)
                        user.FullName = item.FullName?.Trim();
                }

                _db.SaveChanges();

                return obj.RowId;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal SecurityUserGroup ToEntity(SecurityUserGroupView dto, string userName)
        {
            if (dto == null) return null;
            SecurityUserGroup entity;
            if (dto.RowId == 0)
            {
                entity = new SecurityUserGroup();
                entity.Created = DateTime.Now;
                entity.CreatedBy = userName;
                entity.SecurityUserId = dto.SecurityUserId;
                _db.SecurityUserGroups.Add(entity);
            }
            else
            {
                entity = _db.SecurityUserGroups.FirstOrDefault(x => x.RowId == dto.RowId);
                if (entity == null)
                    return null;
                entity.Modified = DateTime.Now;
                entity.ModifiedBy = userName;
            }
            entity.SecurityGroupId = dto.SecurityGroupId;
            entity.SecurityUserId = dto.SecurityUserId;
            entity.Note = dto.Note;

            return entity;
        }
    }
}
