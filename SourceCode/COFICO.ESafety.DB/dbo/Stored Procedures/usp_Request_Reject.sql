﻿
/*==============================================================================================
   Author			Date			Description
   ---------------------------------------------
   Linh Ngo			22.03.2019		Logic for reject request
   
============================================================================================= 

Draft = 1;
Submitted = 2;
Processed = 3;
Approved = 4;
Rejected = 5;
*/

CREATE PROCEDURE [dbo].[usp_Request_Reject] 
	@requestId INT,
	@requestType TINYINT,
	@approver NVARCHAR(100),
	@approverLevel NVARCHAR(100),
	@username NVARCHAR(100),
	@rowVersion TIMESTAMP,
	@result TINYINT OUTPUT
AS
BEGIN
	DECLARE @type TINYINT, @requestStatus TINYINT, @projectID INT, @currentRowVersion TIMESTAMP;

	IF @requestType = 1
	BEGIN
		--check status of request
		SELECT @type = [Type], @requestStatus = [Status], @projectID = ProjectID, @currentRowVersion = [RowVersion]
		FROM Hazard 
		WHERE HazardID = @requestId AND [Status] = 3

		IF @type != @requestType OR @requestStatus IS NULL
			SET @result = 1

		IF @rowVersion <> @currentRowVersion
			SET @result = 2

		--check this user who can reject this request
		IF NOT EXISTS (SELECT TOP 1 1 FROM Project_User WHERE ObjectID = @projectID AND [Type] = 3 AND Username = @username)
			SET @result = 1
		
		IF (@result IS NULL)
		BEGIN
			UPDATE Hazard
			SET [Status] = 2, Approver = @approver, 
				ApproverLevel = @approverLevel, Modified = GETDATE(), ModifiedBy = @username
			WHERE HazardID  = @requestId

			SET @result = 0
		END
	END
	ELSE IF @requestType = 2
	BEGIN
		SELECT @type = [Type], @requestStatus = [Status] 
		FROM ViolationForm 
		WHERE ViolationID = @requestId AND [Status] = 1

		IF @type != @requestType OR @requestStatus IS NULL
			SET @result = 1

		--IF (@result IS NULL)
		--BEGIN
		--	UPDATE ViolationForm
		--	SET [Status] = 2, Approver = @approver, 
		--		ApproverLevel = @approverLevel, Modified = GETDATE(), ModifiedBy = @username
		--	WHERE ViolationID  = @requestId

		--	SET @result = 0
		--END
	END
END