﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace COFICO.ESafety.DAL.ServiceModels
{
    [Table("SecurityUserGroupView")]
    public partial class SecurityUserGroupView
    {
        [Key]
        public int RowId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int SecurityUserId { get; set; }

        public int SecurityGroupId { get; set; }

        [StringLength(255)]
        public string Note { get; set; }

        public DateTime? Created { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime? Modified { get; set; }

        [StringLength(50)]
        public string ModifiedBy { get; set; }

        public bool? Deleted { get; set; }

        [StringLength(100)]
        public string SPUser { get; set; }

        [StringLength(50)]
        public string UserName { get; set; }

        [StringLength(255)]
        public string FullName { get; set; }

        [StringLength(255)]
        public string GroupName { get; set; }
    }
}
