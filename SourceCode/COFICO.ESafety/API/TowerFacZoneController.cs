﻿using COFICO.ESafety.AppCode;
using COFICO.ESafety.DAL.DBModel;
using COFICO.ESafety.DAL.Services;
using System.Collections.Generic;
using System.Security.Claims;
using System.Web.Http;

namespace COFICO.ESafety.API
{
    [RoutePrefix("api/TowerFacZone")]
    public class TowerFacZoneController : ApiController
    {
        [HttpGet, Route("items")]
        public IHttpActionResult Items()
        {
            var items = new List<TowerFacZone>();
            using (var svc = new TowerFacZonesService())
            {
                items = svc.GetList();
            }
            return Ok(new { items });

        }
        [Authorize(Roles ="admin")]
        [HttpGet,Route("name")]
        public IHttpActionResult get()
        {
            var identity = (ClaimsIdentity)User.Identity;
            return Ok("hello "+identity.Name);
        }
        [HttpPut]
        public IHttpActionResult Put(TowerFacZone p)
        {
            var items = new Response<object> { IsError = true };
            if (p != null && p.Id >= 0)
            {
                int? rs = null;
                using (var svc = new TowerFacZonesService())
                {
                    rs = svc.Update(p);
                    if (rs != null)
                    {
                        if (rs == -1)
                        {
                            items.IsDuplicate = true;
                            items.Messages.Add(DAL.Constants.ErrorText.ItemExists);
                        }
                        else
                        {
                            items.IsError = false;
                            items.Id = rs;
                        }
                    }
                }
            }
            return Ok(new { items });
        }
    }
}