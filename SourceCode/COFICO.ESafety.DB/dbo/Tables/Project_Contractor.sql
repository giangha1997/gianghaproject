﻿CREATE TABLE [dbo].[Project_Contractor] (
    [RowID]        INT           IDENTITY (1, 1) NOT NULL,
    [ProjectID]    INT           NOT NULL,
    [ContractorID] INT           NOT NULL,
    [Created]      DATETIME      NOT NULL,
    [CreatedBy]    NVARCHAR (50) NULL,
    [Modified]     DATETIME      NULL,
    [ModifiedBy]   NVARCHAR (50) NULL,
    [Deleted]      BIT           CONSTRAINT [DF_Project_Contractor_Deleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Project_Contractor_1] PRIMARY KEY CLUSTERED ([RowID] ASC)
);

