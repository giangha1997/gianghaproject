﻿-- =============================================
-- Author:		HLinh
-- Create date: 2018/07/23
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetAccident_DataOfWeeks] 
	@ProjectID INT,
	@Month TINYINT,
	@Year SMALLINT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT [Week]
	FROM Accident t
		INNER JOIN AccidentDetail c ON (c.AccidentID = t.AccidentID AND c.Deleted = 0 AND t.Deleted = 0 AND ISNULL(c.NumOfAccidents, 0) > 0)
	WHERE t.ProjectID = @ProjectID AND t.[Month] = @Month AND t.[Year] = @Year
END