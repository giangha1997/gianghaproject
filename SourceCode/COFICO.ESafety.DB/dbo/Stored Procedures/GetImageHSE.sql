﻿-- =============================================
-- Author:		HLinh
-- Create date: 2018/06/20
-- Description:	HSE
-- =============================================
CREATE PROCEDURE [dbo].[GetImageHSE] 
	@ProjectID INT,
	@Month TINYINT,
	@Year SMALLINT,
	@Week TINYINT,
	@Type TINYINT,
	@ImageCategoryID INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Date SMALLDATETIME = dbo.GetFirstDateOfWeek(@Month, @Year, @Week)

	IF (@Date IS NULL) RETURN

	DECLARE @All BIT = 0
	IF @Type = 1 --văn phòng lấy hết
		SET @All = 1

	SELECT * 
	FROM dbo.func_ImageHSE(@ProjectID, @Month, @Year, @Week, @Date, @Type, @ImageCategoryID, @All)
	ORDER BY SortOrder, CategoryName
END