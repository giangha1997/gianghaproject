﻿-- =============================================
-- Author:		HLinh
-- Create date: 2018/07/13
-- Description: Report 02 - Thống kê vi phạm trong tuần theo dự án
-- bao gồm những dự án có tình trạng = Đang thực hiện & Đang bảo trì
-- =============================================
CREATE PROCEDURE [dbo].[GetViolation_Week_Project_DevExpress] 
	@Month TINYINT,
	@Year SMALLINT,
	@Week TINYINT,
	@PrevWeek TINYINT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @PreYear SMALLINT = @Year
	DECLARE @PreMonth TINYINT = @Month

	IF @PrevWeek > @Week
		SET @PreMonth = @Month - 1

	IF @PreMonth <= 0
	BEGIN
		SET @PreMonth = 12
		SET @PreYear = @PreYear - 1
	END

	CREATE TABLE #Tmp
	(
		ViolationCategoryID INT,
		ViolationCode NVARCHAR(50),
		ViolationName NVARCHAR(500),
		ParentID INT,
		IsParent BIT,
		ProjectID INT,
		ProjectCode NVARCHAR(50),
		ProjectName NVARCHAR(255),
		Times INT,
		IsFooter BIT,
		IsThisWeek BIT,
		SortOrder INT,
		SortOrder_Proj INT
	)

	SELECT TOP 0 * INTO #Remaining FROM #Tmp

	--thống kê những dự án có nhà thầu vi phạm trong tuần này và tuần trước
	INSERT #Tmp (ViolationCategoryID, ParentID, IsParent, ProjectID, ProjectCode, ProjectName, Times, IsFooter, IsThisWeek)
		SELECT ViolationCategoryDetailID AS ViolationCategoryID, ViolationCategoryID, 0, ProjectID, ProjectCode, ProjectName, COUNT(ViolationCategoryDetailID) AS Times, 0, CAST(CASE WHEN [Week] = @Week THEN 1 ELSE 0 END AS BIT) AS IsThisWeek
		FROM ViolationView
		WHERE ([Month] = @Month AND [Year] = @Year AND [Week] = @Week) OR ([Month] = @PreMonth AND [Year] = @PreYear AND [Week] = @PrevWeek)
		GROUP BY ViolationCategoryDetailID, ViolationCategoryID, ProjectID, ProjectCode, ProjectName, CAST(CASE WHEN [Week] = @Week THEN 1 ELSE 0 END AS BIT)
		
	--bổ sung những dự án còn thiếu và những mục vi phạm còn thiếu
	DECLARE @ViolationCategoryID INT
	DECLARE @ParentID INT
	DECLARE @IsParent BIT
	DECLARE @IsFooter BIT

	DECLARE cursorA CURSOR FOR
		SELECT DISTINCT ViolationCategoryID, ParentID, IsParent, IsFooter FROM #Tmp
		UNION
		SELECT ViolationCategoryID, ParentID, IsParent, IsFooter FROM ViolationCategory WHERE Deleted = 0 AND IsActive = 1 AND (IsFooter = 0 OR (IsFooter = 1 AND FooterType = 1))
	OPEN cursorA
	FETCH NEXT FROM cursorA INTO @ViolationCategoryID, @ParentID, @IsParent, @IsFooter
	WHILE @@FETCH_STATUS = 0
	BEGIN
		INSERT INTO #Remaining (ViolationCategoryID, ParentID, IsParent, ProjectID, ProjectCode, ProjectName, IsFooter, IsThisWeek)
			SELECT	@ViolationCategoryID, @ParentID, @IsParent, ProjectID, ProjectCode, ProjectName, @IsFooter, 1
			FROM Project c
			WHERE Deleted = 0 AND (StatusID = 1 OR StatusID = 2)
				  AND NOT EXISTS (SELECT TOP 1 1 FROM #Tmp t1 WHERE c.ProjectID = t1.ProjectID AND t1.ViolationCategoryID = @ViolationCategoryID AND t1.IsThisWeek = 1)

		FETCH NEXT FROM cursorA INTO @ViolationCategoryID, @ParentID, @IsParent, @IsFooter
	END
	
	CLOSE cursorA
	DEALLOCATE cursorA

	--merge data
	INSERT INTO #Tmp(ViolationCategoryID, ViolationCode, ParentID, IsParent, ProjectID, ProjectCode, ProjectName, Times, IsFooter, IsThisWeek)
		SELECT ViolationCategoryID, ViolationCode, ParentID, IsParent, ProjectID, ProjectCode, ProjectName, Times, IsFooter, IsThisWeek 
		FROM #Remaining
		
	--update parent's data
	UPDATE #Tmp
	SET Times = (SELECT SUM(ISNULL(Times, 0)) FROM #Tmp t WHERE t.ParentID = #Tmp.ViolationCategoryID AND t.ProjectID = #Tmp.ProjectID AND t.IsThisWeek = 1)
	WHERE IsParent = 1
	
	--add ViolationCode & ViolationName
	DELETE FROM #Remaining

	INSERT #Remaining (ViolationCategoryID, ViolationCode, ViolationName, ProjectID, ProjectCode, ProjectName, Times, IsFooter, ParentID, IsParent, SortOrder, IsThisWeek, SortOrder_Proj)
		SELECT t.ViolationCategoryID, c.ViolationCode, c.ViolationName, ProjectID, ProjectCode, ProjectName, Times, t.IsFooter, t.ParentID, t.IsParent, c.SortOrder, IsThisWeek, ROW_NUMBER() OVER (PARTITION BY t.ViolationCategoryID ORDER BY t.ViolationCategoryID, ProjectCode)
		FROM #Tmp t
			INNER JOIN ViolationCategory c ON (c.ViolationCategoryID = t.ViolationCategoryID)

	
	--update footer
	----update this week
	UPDATE #Remaining
	SET Times = (SELECT SUM(ISNULL(Times, 0)) FROM #Remaining t WHERE t.ProjectID = #Remaining.ProjectID AND IsParent = 1 AND IsFooter = 0 AND t.IsThisWeek = 1)
	WHERE IsFooter = 1 AND ViolationCode = '_COFICO_CATEGORY_F1'

	----update last week
	UPDATE #Remaining
	SET Times = (SELECT SUM(ISNULL(Times, 0)) FROM #Remaining t WHERE t.ProjectID = #Remaining.ProjectID AND IsFooter = 0 AND t.IsThisWeek = 0)
	WHERE IsFooter = 1 AND ViolationCode = '_COFICO_CATEGORY_F2'

	--update Total
	UPDATE #Remaining
	SET Times = (SELECT SUM(ISNULL(Times, 0)) FROM #Remaining t WHERE t.ProjectID = #Remaining.ProjectID AND IsFooter = 1 AND ViolationCode <> '_COFICO_CATEGORY_F3')
	WHERE IsFooter = 1 AND ViolationCode = '_COFICO_CATEGORY_F3'

	UPDATE #Remaining SET Times = NULL WHERE Times = 0
		
	--Set SortOrder của Footer luôn lớn hơn Danh mục
	DECLARE @Max INT
	SELECT @Max = MAX(SortOrder) FROM #Remaining WHERE IsFooter = 0

	UPDATE #Remaining SET SortOrder = SortOrder + @Max WHERE IsFooter = 1

	--show data
	SELECT * FROM #Remaining WHERE ParentID IS NULL ORDER BY IsFooter, SortOrder, ViolationName, ProjectCode

	DROP TABLE #Tmp
	DROP TABLE #Remaining
END