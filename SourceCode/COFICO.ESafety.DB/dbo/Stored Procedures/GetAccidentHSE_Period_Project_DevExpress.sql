﻿-- =============================================
-- Author:		HLinh
-- Create date: 2018/07/20
-- Description:	Báo cáo tổng hợp - Thống kê Sự cố, an toàn từ ngày tới ngày theo dự án
-- @ProjectID = 0: lấy tất cả các dự án
-- @ShowAllProject = 0: chỉ lấy những dự án có tình trạng = Đang thực hiện & Đang bảo trì
-- @ShowAllProject = 1: lấy tất cả những dự án bao gồm tình trạng = Kết thúc
-- =============================================
CREATE PROCEDURE [dbo].[GetAccidentHSE_Period_Project_DevExpress] 
	@ProjectID INT,
	@FromDate SMALLDATETIME,
	@ToDate SMALLDATETIME,
	@ShowAllProject BIT
AS
BEGIN
	SET NOCOUNT ON;

	CREATE TABLE #Tmp
	(
		AccidentCategoryID INT,
		AccidentCode NVARCHAR(50),
		AccidentName NVARCHAR(255),
		ProjectID INT,
		ProjectCode NVARCHAR(50),
		ProjectName NVARCHAR(255),
		NumOfAccidents DECIMAL(9,2),
		IsFooter BIT,
		IsThisWeek BIT,
		ThisWeek DECIMAL(9,2),
		LastWeek DECIMAL(9,2),
		Total DECIMAL(9,2)
	)

	SELECT TOP 0 * INTO #Remaining FROM #Tmp

	--thống kê Sự cố, an toàn
	INSERT #Tmp (AccidentCategoryID, ProjectID, ProjectCode, ProjectName, NumOfAccidents, IsFooter, IsThisWeek)
		SELECT d.AccidentCategoryID, t.ProjectID, ProjectCode, ProjectName, SUM(NumOfAccidents) AS NumOfAccidents, 0, CAST(CASE WHEN d.[Date] >= @FromDate AND d.[Date] <= @ToDate THEN 1 ELSE 0 END AS BIT) AS IsThisWeek
		FROM Accident t 
			INNER JOIN AccidentDetail d ON (t.AccidentID =  d.AccidentID AND t.Deleted = 0 AND d.Deleted = 0)
			INNER JOIN Project p ON (t.ProjectID = p.ProjectID AND p.Deleted = 0  AND (t.ProjectID = @ProjectID OR @ProjectID = 0) AND ((@ShowAllProject = 0 AND (p.StatusID = 1 OR p.StatusID = 2)) OR @ShowAllProject = 1) )
			INNER JOIN AccidentCategory c ON (c.AccidentCategoryID = d.AccidentCategoryID AND c.Deleted = 0 AND c.IsActive = 1)
		WHERE ( d.[Date] BETWEEN @FromDate AND @ToDate )
		GROUP BY d.AccidentCategoryID, t.ProjectID, ProjectCode, ProjectName, CAST(CASE WHEN d.[Date] >= @FromDate AND d.[Date] <= @ToDate THEN 1 ELSE 0 END AS BIT)


	--bổ sung những dự án còn thiếu và những danh mục còn thiếu
	DECLARE @ProjID INT
	DECLARE @ProjectCode NVARCHAR(50)
	DECLARE @ProjectName NVARCHAR(255)

	DECLARE cursorA CURSOR FOR
		SELECT DISTINCT ProjectID, ProjectCode, ProjectName FROM #Tmp
		UNION
		--show dự án @ProjectID nếu là báo cáo HSE dự án và #Tmp = null
		SELECT ProjectID, ProjectCode, ProjectName FROM Project WHERE ProjectID = @ProjectID AND @ProjectID > 0
	OPEN cursorA
	FETCH NEXT FROM cursorA INTO @ProjID, @projectCode, @ProjectName
	WHILE @@FETCH_STATUS = 0
	BEGIN
		INSERT INTO #Remaining (AccidentCategoryID, ProjectID, ProjectCode, ProjectName, IsFooter, IsThisWeek)
			SELECT	AccidentCategoryID, @ProjID, @ProjectCode, @ProjectName, IsFooter, 1
			FROM AccidentCategory c
			WHERE Deleted = 0 AND IsActive = 1
					AND NOT EXISTS (SELECT TOP 1 1 FROM #Tmp t1 WHERE t1.ProjectID = @ProjID AND t1.AccidentCategoryID = c.AccidentCategoryID AND t1.IsThisWeek = 1)

		FETCH NEXT FROM cursorA INTO @ProjID, @projectCode, @ProjectName
	END
	
	CLOSE cursorA
	DEALLOCATE cursorA
	
	--update ThisWeek
	UPDATE #Tmp
	SET ThisWeek = (SELECT SUM(ISNULL(NumOfAccidents, 0)) FROM #Tmp t WHERE t.AccidentCategoryID = #Tmp.AccidentCategoryID AND t.IsThisWeek = 1)

	UPDATE #Tmp SET ThisWeek = NULL WHERE ThisWeek = 0


	--merge data
	INSERT INTO #Tmp(AccidentCategoryID, ProjectID, ProjectCode, ProjectName, NumOfAccidents, IsFooter, IsThisWeek)
		SELECT AccidentCategoryID, ProjectID, ProjectCode, ProjectName, NumOfAccidents, IsFooter, IsThisWeek 
		FROM #Remaining


	--show data
	SELECT a1.AccidentCategoryID, c.AccidentCode, c.AccidentName, ProjectID, ProjectCode, ProjectName, NumOfAccidents, ThisWeek, LastWeek, Total, a1.IsFooter
	FROM #Tmp a1
		INNER JOIN AccidentCategory c ON (c.AccidentCategoryID = a1.AccidentCategoryID)
	WHERE IsThisWeek = 1
	ORDER BY a1.IsFooter, c.SortOrder, c.AccidentName, ProjectName

	DROP TABLE #Remaining
	DROP TABLE #Tmp
END