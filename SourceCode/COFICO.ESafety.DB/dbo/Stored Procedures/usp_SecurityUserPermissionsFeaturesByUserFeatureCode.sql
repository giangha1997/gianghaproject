﻿
CREATE PROCEDURE [dbo].[usp_SecurityUserPermissionsFeaturesByUserFeatureCode]
	@spUser nvarchar(50)	
	, @featureCode varchar(50)
AS
BEGIN
	SET NOCOUNT ON;

	declare @UserName nvarchar(50) = @spUser
	--declare @UserName nvarchar(50) ;
	--declare @spUser1 nvarchar(50);
	--declare @index int;

	--select @spUser1 = REVERSE ( @spUser )  
	--SELECT @index = CHARINDEX('|', @spUser1);  
	--SELECT @UserName = REVERSE(left(@spUser1,@index - 1))

	Select a.FeatureCode, 
		[View]   = cast((case when Sum(CAST(IsNull(a.[View],0) as int))   >= 1 then 1 else 0 end) as bit),
		[Add]    = cast((case when Sum(CAST(IsNull(a.[Add],0) as int))    >= 1 then 1 else 0 end) as bit),
		[Edit]	 = cast((case when Sum(CAST(IsNull(a.[Edit],0) as int))   >= 1 then 1 else 0 end) as bit),
		[Delete] = cast((case when Sum(CAST(IsNull(a.[Delete],0) as int)) >= 1 then 1 else 0 end) as bit),
		[Approval] = cast((case when Sum(CAST(IsNull(a.[Approval],0) as int)) >= 1 then 1 else 0 end) as bit)
	From 
	(
		Select p.[FeatureCode], p.[View], p.[Add], p.[Edit], p.[Delete], p.[Approval]
		From [dbo].[SecurityUserPermissionsFeaturesView] p 
			Inner Join [dbo].[SecurityUsers] s On p.SecurityUserId = s. RowId
		--Where (s.Deleted Is Null Or s.Deleted = 0) And s.SPUser = @spUser And p.FeatureCode = @featureCode
		Where (s.Deleted Is Null Or s.Deleted = 0) And s.UserName = @UserName And p.FeatureCode = @featureCode
		Union All
		Select p.[FeatureCode], p.[View], p.[Add], p.[Edit], p.[Delete], p.[Approval]
		From [dbo].[SecurityGroupPermissionsFeaturesView] p 
			Inner Join [dbo].[SecurityUserGroupView] s On p.SecurityGroupId = s.SecurityGroupId
		--Where s.SPUser = @spUser And p.FeatureCode = @featureCode
		Where s.UserName = @UserName And p.FeatureCode = @featureCode
	) a
	Group By a.FeatureCode
END