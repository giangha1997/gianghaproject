﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace COFICO.ESafety.DAL.DBModel
{
    [Table("Project")]
    public class Project : ProjectBase
    {
        
    }

    public abstract class ProjectBase
    {
        [Key]
        [Column("ProjectID")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [StringLength(50)]
        public string ProjectCode { get; set; }

        [StringLength(255)]
        public string ProjectName { get; set; }

        [StringLength(1000)]
        public string Note { get; set; }

        public int StatusID { get; set; }

        public int? SortOrder { get; set; }

        [JsonIgnore]
        public DateTime Created { get; set; }

        [JsonIgnore]
        [StringLength(50)]
        public string CreatedBy { get; set; }

        [JsonIgnore]
        public DateTime? Modified { get; set; }

        [JsonIgnore]
        [StringLength(50)]
        public string ModifiedBy { get; set; }

        [JsonIgnore]
        public bool Deleted { get; set; }
    }
}
