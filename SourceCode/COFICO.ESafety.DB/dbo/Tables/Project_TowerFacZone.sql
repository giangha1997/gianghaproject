﻿CREATE TABLE [dbo].[Project_TowerFacZone] (
    [RowID]      INT             IDENTITY (1, 1) NOT NULL,
    [ProjectID]  INT             NOT NULL,
    [Code]       NVARCHAR (50)   NULL,
    [Name]       NVARCHAR (255)  NULL,
    [Type]       TINYINT         NULL,
    [ParentID]   INT             NULL,
    [IsParent]   BIT             CONSTRAINT [DF_Project_TowerFacZone_IsParent] DEFAULT ((0)) NOT NULL,
    [Note]       NVARCHAR (1000) NULL,
    [IsActive]   BIT             CONSTRAINT [DF_Project_TowerFacZone_IsActive] DEFAULT ((0)) NOT NULL,
    [SortOrder]  INT             NULL,
    [Created]    DATETIME        NULL,
    [CreatedBy]  NVARCHAR (50)   NULL,
    [Modified]   DATETIME        NULL,
    [ModifiedBy] NVARCHAR (50)   NULL,
    [Deleted]    BIT             CONSTRAINT [DF_Project_TowerFacZone_Deleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Project_TowerFacZone] PRIMARY KEY CLUSTERED ([RowID] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'1: TowerFac; 2: Zone; 3: Level', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Project_TowerFacZone', @level2type = N'COLUMN', @level2name = N'Type';

