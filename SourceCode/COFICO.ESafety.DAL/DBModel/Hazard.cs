﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace COFICO.ESafety.DAL.DBModel
{
    [Table("Hazard")]
    public class Hazard
    {
        [JsonIgnore]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int HazardID { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid GUID { get; set; }

        public byte Type { get; set; }

        public byte Status { get; set; }

        [MaxLength(100)]
        [StringLength(100)]
        public string Approver { get; set; }

        [MaxLength(100)]
        [StringLength(100)]
        public string ApproverLevel { get; set; }

        public int ProjectID { get; set; }

        [MaxLength(100)]
        [StringLength(100)]
        public string Username { get; set; }

        [MaxLength(255)]
        [StringLength(255)]
        public string Fullname { get; set; }

        public DateTime IssuedDate { get; set; }

        public int ContractorID { get; set; }

        public int TeamID { get; set; }

        public int? NumOfEmployees { get; set; }

        public int TowerFacID { get; set; }

        public int ZoneID { get; set; }

        public int LevelID { get; set; }

        [MaxLength(1000)]
        [StringLength(1000)]
        public string SOW { get; set; }

        public int? HazardCategoryID { get; set; }

        [MaxLength(1000)]
        [StringLength(1000)]
        public string HazardDetail { get; set; }

        public int? RiskLevelID { get; set; }

        [MaxLength(1000)]
        [StringLength(1000)]
        public string RiskControl { get; set; }

        public decimal? FinishedPercent { get; set; }

        public int? SafetyStatusID { get; set; }

        public decimal? SafetyStatusPercent { get; set; }

        [MaxLength(1000)]
        [StringLength(1000)]
        public string Note { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }

        [JsonIgnore]
        public DateTime? Created { get; set; }

        [JsonIgnore]
        [MaxLength(50)]
        [StringLength(50)]
        public string CreatedBy { get; set; }

        [JsonIgnore]
        public DateTime? Modified { get; set; }

        [JsonIgnore]
        [MaxLength(50)]
        [StringLength(50)]
        public string ModifiedBy { get; set; }

        [JsonIgnore]
        public bool Deleted { get; set; }
    }

}
