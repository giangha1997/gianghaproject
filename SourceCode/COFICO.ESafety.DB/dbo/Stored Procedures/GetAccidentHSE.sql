﻿-- =============================================
-- Author:		HLinh
-- Create date: 2018/06/07
-- Description:	HSE/Report 01 - Thống kê Sự cố, an toàn trong tuần theo ngày của 1 dự án
-- với dự án có tình trạng = Đang thực hiện & Đang bảo trì
-- =============================================
CREATE PROCEDURE [dbo].[GetAccidentHSE] 
	@ProjectID INT,
	@Month TINYINT,
	@Year SMALLINT,
	@Week TINYINT,
	@PrevWeek TINYINT,
	@ShowFooter BIT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Date SMALLDATETIME = dbo.GetFirstDateOfWeek(@Month, @Year, @Week)
	DECLARE @PreYear SMALLINT = @Year
	DECLARE @PreMonth TINYINT = @Month

	IF @PrevWeek > @Week
		SET @PreMonth = @Month - 1

	IF @PreMonth <= 0
	BEGIN
		SET @PreMonth = 12
		SET @PreYear = @PreYear - 1
	END

	IF (@Date IS NULL) RETURN

	CREATE TABLE #Tmp
	(
		AccidentID INT NOT NULL,
		ProjectID INT NOT NULL,
		ProjectCode NVARCHAR(50),
		ProjectName NVARCHAR(255),
		AccidentCategoryID INT NOT NULL,
		AccidentCode NVARCHAR(50),
		AccidentName NVARCHAR(255),
		[Month] TINYINT NOT NULL,
		[Year] SMALLINT NOT NULL,
		[Week] TINYINT NOT NULL,
		[Date] SMALLDATETIME NULL,
		D2 INT NULL,
		D3 INT NULL,
		D4 INT NULL,
		D5 INT NULL,
		D6 INT NULL,
		D7 INT NULL,
		D8 INT NULL,
		Total DECIMAL(9,2),
		PrevTotal DECIMAL(9,2),
		Total1 DECIMAL(9,2),
		SortOrder INT NULL,
		IsFooter BIT NOT NULL DEFAULT(0),
	)

	SELECT TOP 0 * INTO #Remaining FROM #Tmp

	--thống kê Sự cố, an toàn
	INSERT #Tmp (AccidentID, ProjectID, ProjectCode, ProjectName, AccidentCategoryID, AccidentCode, AccidentName, [Month], [Year], [Week], [Date], D2, D3, D4, D5, D6, D7, D8, Total, SortOrder, IsFooter)
		SELECT AccidentID, ProjectID, ProjectCode, ProjectName, AccidentCategoryID, AccidentCode, AccidentName, [Month], [Year], [Week], [Date], D2, D3, D4, D5, D6, D7, D8, 
			   ISNULL(D2, 0) + ISNULL(D3, 0) + ISNULL(D4, 0) + ISNULL(D5, 0) + ISNULL(D6, 0) + ISNULL(D7, 0) + ISNULL(D8, 0),
			   SortOrder, 0
		FROM AccidentView
		WHERE ProjectID = @ProjectID AND [Month] = @Month AND [Year] = @Year AND [Week] = @Week
	
	--bổ sung những dự án còn thiếu và những danh mục còn thiếu
	INSERT #Remaining (AccidentID, ProjectID, ProjectCode, ProjectName, AccidentCategoryID, AccidentCode, AccidentName, [Month], [Year], [Week], [Date], SortOrder, IsFooter)
		SELECT 0, @ProjectID, ProjectCode, ProjectName, AccidentCategoryID, AccidentCode, AccidentName, @Month, @Year, @Week, @Date, c.SortOrder, c.IsFooter
		FROM AccidentCategory c
			LEFT JOIN Project p ON (p.ProjectID = @ProjectID)
		WHERE c.IsActive = 1 AND c.Deleted = 0 AND (c.IsFooter = 0 OR c.IsFooter = @ShowFooter) 
			  AND p.Deleted = 0 AND (p.StatusID = 1 OR p.StatusID = 2)
			  AND NOT EXISTS (SELECT TOP 1 1 FROM #Tmp t WHERE c.AccidentCategoryID = t.AccidentCategoryID)

	--merge data
	INSERT #Tmp (AccidentID, ProjectID, ProjectCode, ProjectName, AccidentCategoryID, AccidentCode, AccidentName, [Month], [Year], [Week], [Date], D2, D3, D4, D5, D6, D7, D8, SortOrder, IsFooter)
		SELECT AccidentID, ProjectID, ProjectCode, ProjectName, AccidentCategoryID, AccidentCode, AccidentName, [Month], [Year], [Week], [Date], D2, D3, D4, D5, D6, D7, D8, SortOrder, IsFooter
			FROM #Remaining

	--update lũy kế tuần trước
	UPDATE #Tmp SET PrevTotal = (SELECT ISNULL(D2, 0) + ISNULL(D3, 0) + ISNULL(D4, 0) + ISNULL(D5, 0) + ISNULL(D6, 0) + ISNULL(D7, 0) + ISNULL(D8, 0) 
								 FROM AccidentView a
								 WHERE ProjectID = @ProjectID AND [Month] = @PreMonth AND [Year] = @PreYear AND [Week] = @PrevWeek AND a.AccidentCategoryID = #Tmp.AccidentCategoryID)
	
	--update lũy kế cộng dồn
	UPDATE #Tmp SET Total1 = ISNULL(Total, 0) + ISNULL(PrevTotal, 0)

	--
	UPDATE #Tmp SET D2 = NULL WHERE D2 = 0
	UPDATE #Tmp SET D3 = NULL WHERE D3 = 0
	UPDATE #Tmp SET D4 = NULL WHERE D4 = 0
	UPDATE #Tmp SET D5 = NULL WHERE D5 = 0
	UPDATE #Tmp SET D6 = NULL WHERE D6 = 0
	UPDATE #Tmp SET D7 = NULL WHERE D7 = 0
	UPDATE #Tmp SET D8 = NULL WHERE D8 = 0
	UPDATE #Tmp SET Total = NULL WHERE Total = 0
	UPDATE #Tmp SET PrevTotal = NULL WHERE PrevTotal = 0
	UPDATE #Tmp SET Total1 = NULL WHERE Total1 = 0

	--show data
	SELECT * FROM #Tmp ORDER BY IsFooter, SortOrder, AccidentName

	DROP TABLE #Tmp
	DROP TABLE #Remaining
END