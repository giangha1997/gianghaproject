﻿CREATE TABLE [dbo].[SecurityUsers] (
    [RowId]           INT            IDENTITY (1, 1) NOT NULL,
    [SecurityGroupId] INT            NULL,
    [SPUser]          NVARCHAR (100) NOT NULL,
    [UserName]        NVARCHAR (50)  NOT NULL,
    [FullName]        NVARCHAR (255) NULL,
    [Note]            NVARCHAR (255) NULL,
    [AllowInPast]     BIT            CONSTRAINT [DF_SecurityUsers_AllowInPast] DEFAULT ((0)) NOT NULL,
    [AllowedDate]     DATETIME       NULL,
    [Created]         DATETIME       NULL,
    [CreatedBy]       VARCHAR (50)   NULL,
    [Modified]        DATETIME       NULL,
    [ModifiedBy]      VARCHAR (50)   NULL,
    [Deleted]         BIT            NULL,
    CONSTRAINT [PK_SecurityUsers] PRIMARY KEY CLUSTERED ([RowId] ASC)
);

