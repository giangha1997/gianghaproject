﻿using COFICO.ESafety.DAL.DBModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace COFICO.ESafety.DAL.ServiceModels
{
    [Table("HazardView")]
    public class HazardView
    {
        [JsonIgnore]
        [Key]
        public int HazardID { get; set; }

        public Guid GUID { get; set; }

        public byte Type { get; set; }

        public byte Status { get; set; }

        public string StatusName { get; set; }

        public int ProjectID { get; set; }

        public string ProjectCode { get; set; }

        public string ProjectName { get; set; }

        public string Username { get; set; }

        public string Fullname { get; set; }

        public DateTime IssuedDate { get; set; }

        public int ContractorID { get; set; }

        public string ContractorCode { get; set; }

        public string ContractorName { get; set; }

        public int TeamID { get; set; }

        public string TeamName { get; set; }

        public int? NumOfEmployees { get; set; }

        public int TowerFacID { get; set; }

        public string TowerFac { get; set; }

        public int ZoneID { get; set; }

        public string Zone { get; set; }

        public int LevelID { get; set; }

        public string Level { get; set; }

        public string SOW { get; set; }

        public int? HazardCategoryID { get; set; }

        public string HazardName { get; set; }

        public string HazardDetail { get; set; }

        public int? RiskLevelID { get; set; }

        public string RiskLevel { get; set; }

        public string RiskControl { get; set; }

        public decimal? FinishedPercent { get; set; }

        public int? SafetyStatusID { get; set; }

        public string SafetyStatus { get; set; }

        public decimal? SafetyStatusPercent { get; set; }

        public string Approver { get; set; }

        public string Note { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }

        [NotMapped]
        public bool CanEdit { get; set; }

        [NotMapped]
        public bool CanUpdateProcess { get; set; }

        [NotMapped]
        public bool CanFinishProcess { get; set; }

        [NotMapped]
        public bool CanApprove { get; set; }

        [NotMapped]
        public List<ApprovalHistoryView> ApprovalHistories { get; set; }
    }
}
