﻿



CREATE View [dbo].[ProjectView] As

SELECT p.*, s.Name AS StatusName
FROM Project p 
	INNER JOIN ProjectStatus s ON (s.RowID = p.StatusID)
WHERE p.Deleted = 0