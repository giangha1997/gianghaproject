﻿-- =============================================
-- Author:		HLinh
-- Create date: 2018/06/13
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[usp_Project_Exist]
	@ProjectID INT
AS
BEGIN
	SET NOCOUNT ON;

	IF EXISTS(SELECT TOP 1 1 FROM Accident WHERE ProjectID = @ProjectID AND Deleted = 0)
	BEGIN
		SELECT CAST(1 AS BIT)
		RETURN 1
	END

	IF EXISTS(SELECT TOP 1 1 FROM [Image] WHERE ProjectID = @ProjectID AND Deleted = 0)
	BEGIN
		SELECT CAST(1 AS BIT)
		RETURN 1
	END

	--IF EXISTS(SELECT TOP 1 1 FROM Project_Contractor WHERE ProjectID = @ProjectID AND Deleted = 0)
	--BEGIN
	--	SELECT CAST(1 AS BIT)
	--	RETURN 1
	--END

	--IF EXISTS(SELECT TOP 1 1 FROM Project_User WHERE ProjectID = @ProjectID AND Deleted = 0)
	--BEGIN
	--	SELECT CAST(1 AS BIT)
	--	RETURN 1
	--END

	IF EXISTS(SELECT TOP 1 1 FROM SafetyPlan WHERE ProjectID = @ProjectID AND Deleted = 0)
	BEGIN
		SELECT CAST(1 AS BIT)
		RETURN 1
	END

	IF EXISTS(SELECT TOP 1 1 FROM TBM WHERE ProjectID = @ProjectID AND Deleted = 0)
	BEGIN
		SELECT CAST(1 AS BIT)
		RETURN 1
	END

	IF EXISTS(SELECT TOP 1 1 FROM Training WHERE ProjectID = @ProjectID AND Deleted = 0)
	BEGIN
		SELECT CAST(1 AS BIT)
		RETURN 1
	END

	SELECT CAST(0 AS BIT)
END