﻿using System;
using System.Collections.Generic;
using System.Linq;
using COFICO.ESafety.Base;
using COFICO.ESafety.DAL.DBModel;
using COFICO.ESafety.DAL.Services;

namespace COFICO.ESafety.DAL.Services
{
    public class AccidentCategoryService : GenericService<AccidentCategory>
    {
        public List<AccidentCategory> GetAccidents()
        {
            try
            {
                return _table.Where(x => !x.Deleted && !x.IsFooter).OrderBy(x => x.SortOrder).ToList();
            }
            catch (Exception ex)
            {
                Logger.Error("GetAccidents", ex);
                throw ex;
            }
        }

        public List<AccidentCategory> GetAllAccidents()
        {
            try
            {
                return _table.Where(x => !x.Deleted).ToList();
            }
            catch (Exception ex)
            {
                Logger.Error("GetAllAccidents", ex);
                throw ex;
            }
        }

        public int? Insert(AccidentCategory p)
        {
            if (p == null) return null;

            try
            {
                InsertObj(ref p);
                SaveChanges();
                return p.ID;
            }
            catch (Exception ex)
            {
                Logger.Error(p, ex);
                throw ex;
            }
        }

        public int? Update(AccidentCategory p)
        {
            if (p == null) return null;
            if (ExistCode(p)) return -1;// ton tai trong he thong
            //if(ExistCode(p) && p.AccidentCategoryID==0)  return -1;
            try
            {
                AccidentCategory entity = null;
                if ((entity = ParseToAccident(p)) != null)
                {
                    SaveChanges();
                    return entity.ID;
                }
                return null;
            }
            catch (Exception ex)
            {
                Logger.Error(p, ex);
                throw ex;
            }
        }

        public bool ExistCode(AccidentCategory p)
        {
            try
            {
                return _table.Any(s => ((p.ID == 0 && s.AccidentCode == p.AccidentCode)
                                        || (p.ID > 0 && s.AccidentCode == p.AccidentCode && s.ID != p.ID)) &&
                                            !s.Deleted);
            }
            catch (Exception ex)
            {
                Logger.Error(p, ex);
                throw ex;
            }
        }

        public bool Delete(int categoryID)
        {
            try
            {
                ////check existance in table AccidentDetail
                //if (_db.AccidentDetails.Any(s => s.AccidentCategoryID == categoryID && !s.Deleted))
                //    return false;

                if (DeleteMark(categoryID))
                {
                    SaveChanges();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                Logger.Error(categoryID, ex);
                throw ex;
            }
        }

        private AccidentCategory ParseToAccident(AccidentCategory dto)
        {
            if (dto == null) return null;

            AccidentCategory entity = null;
            if (dto.ID == 0)
            {
                InsertObj(ref dto);
                entity = dto;
            }
            else
            {
                entity = _table.SingleOrDefault(x => x.ID == dto.ID && !x.Deleted);
                if (entity == null) return null;

                entity.AccidentCode = dto.AccidentCode;
                entity.AccidentName = dto.AccidentName;
                entity.ParentID = dto.ParentID;
                entity.IsParent = dto.IsParent;
                entity.IsActive = dto.IsActive;
                entity.SortOrder = dto.SortOrder;
                entity.Note = dto.Note;
                UpdateObj(entity);
            }
            return entity;
        }

    }
}
