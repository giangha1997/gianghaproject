﻿using System;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using COFICO.ESafety.DAL.DBModel;
using COFICO.ESafety.DAL.Constants;
using COFICO.ESafety.Base;

namespace COFICO.ESafety.DAL.Services
{
    public class RequestService : IDisposable
    {
        private ESafetyModel _db = new ESafetyModel();

        public void Dispose()
        {
            _db.Dispose();
        }

        public List<T> GetMyRequest<T>(RequestType requestType, DateTime? fromDate, DateTime? toDate, int? projectID, int? contractorID, int? teamID)
        {
            try
            {
                string username = Helper.GetCurrentUsername();

                string strSql = "EXEC " + StoreProcedures.GetMyRequest + " @spUsername, @spRequestType, @spFromDate, @spToDate, @spProjectID, @spContractorID, @spTeamID";
                var prUsername = new SqlParameter("@spUsername", System.Data.SqlDbType.NVarChar, 255)
                {
                    Value = string.IsNullOrWhiteSpace(username) ? (object)DBNull.Value : username
                };
                var prRequestType = new SqlParameter("@spRequestType", (byte)requestType);
                var prFromDate = new SqlParameter("@spFromDate", fromDate ?? (object)DBNull.Value);
                var prToDate = new SqlParameter("@spToDate", toDate ?? (object)DBNull.Value);
                var prProjectID = new SqlParameter("@spProjectID", projectID ?? (object)DBNull.Value);
                var prContractorID = new SqlParameter("@spContractorID", contractorID ?? (object)DBNull.Value);
                var prTeamID = new SqlParameter("@spTeamID", teamID ?? (object)DBNull.Value);
                return _db.Database.SqlQuery<T>(strSql, prUsername, prRequestType, prFromDate, prToDate, prProjectID, prContractorID, prTeamID).ToList();
            }
            catch (Exception ex)
            {
                Logger.Error("GetMyRequest", ex);
                throw ex;
            }
        }

        public int? Submit(int id, byte requestType, string approver, string approverLevel)
        {
            try
            {
                string username = Helper.GetCurrentUsername();

                var prRequestId = new SqlParameter("@requestId", id);
                var prRequestType = new SqlParameter("@requestType", requestType);
                var prApprover = new SqlParameter("@approver", System.Data.SqlDbType.NVarChar, 255)
                {
                    Value = string.IsNullOrWhiteSpace(approver) ? (object)DBNull.Value : approver
                };
                var prApproverLevel = new SqlParameter("@approverLevel", System.Data.SqlDbType.NVarChar, 255)
                {
                    Value = string.IsNullOrWhiteSpace(approverLevel) ? (object)DBNull.Value : approverLevel
                };
                var prUsername = new SqlParameter("@username", System.Data.SqlDbType.NVarChar, 255)
                {
                    Value = string.IsNullOrWhiteSpace(username) ? (object)DBNull.Value : username
                };
                var prResult = new SqlParameter()
                {
                    ParameterName = "@result",
                    SqlDbType = System.Data.SqlDbType.TinyInt,
                    Direction = System.Data.ParameterDirection.Output
                };

                string strSql = "EXEC " + StoreProcedures.SubmitRequest + " @requestId, @requestType, @approver, @approverLevel, @username, @result OUTPUT";
                _db.Database.ExecuteSqlCommand(strSql, prRequestId, prRequestType, prApprover, prApproverLevel, prUsername, prResult);

                return CastUtilities.GetNullableIntValue(prResult.Value);
            }
            catch (Exception ex)
            {
                Logger.Error(id, ex);
                throw ex;
            }
        }

        public int? FinishProcess(int id, byte[] rowVersion, byte requestType, string approver, string approverLevel)
        {
            try
            {
                string username = Helper.GetCurrentUsername();

                var prRequestId = new SqlParameter("@requestId", id);
                var prRequestType = new SqlParameter("@requestType", requestType);
                var prApprover = new SqlParameter("@approver", System.Data.SqlDbType.NVarChar, 255)
                {
                    Value = string.IsNullOrWhiteSpace(approver) ? (object)DBNull.Value : approver
                };
                var prApproverLevel = new SqlParameter("@approverLevel", System.Data.SqlDbType.NVarChar, 255)
                {
                    Value = string.IsNullOrWhiteSpace(approverLevel) ? (object)DBNull.Value : approverLevel
                };
                var prUsername = new SqlParameter("@username", System.Data.SqlDbType.NVarChar, 255)
                {
                    Value = string.IsNullOrWhiteSpace(username) ? (object)DBNull.Value : username
                };
                var prRowVersion = new SqlParameter("@rowVersion", System.Data.SqlDbType.Timestamp, 8)
                {
                    Value = rowVersion ?? (object)DBNull.Value
                };
                var prResult = new SqlParameter()
                {
                    ParameterName = "@result",
                    SqlDbType = System.Data.SqlDbType.TinyInt,
                    Direction = System.Data.ParameterDirection.Output
                };

                string strSql = "EXEC " + StoreProcedures.FinishProcessRequest + " @requestId, @requestType, @approver, @approverLevel, @username, @rowVersion, @result OUTPUT";
                _db.Database.ExecuteSqlCommand(strSql, prRequestId, prRequestType, prApprover, prApproverLevel, prUsername, prRowVersion, prResult);

                return CastUtilities.GetNullableIntValue(prResult.Value);
            }
            catch (Exception ex)
            {
                Logger.Error(id, ex);
                throw ex;
            }
        }

        public int? Approve(int id, byte[] rowVersion, byte requestType, string approver, string approverLevel)
        {
            try
            {
                string username = Helper.GetCurrentUsername();

                var prRequestId = new SqlParameter("@requestId", id);
                var prRequestType = new SqlParameter("@requestType", requestType);
                var prApprover = new SqlParameter("@approver", System.Data.SqlDbType.NVarChar, 255)
                {
                    Value = string.IsNullOrWhiteSpace(approver) ? (object)DBNull.Value : approver
                };
                var prApproverLevel = new SqlParameter("@approverLevel", System.Data.SqlDbType.NVarChar, 255)
                {
                    Value = string.IsNullOrWhiteSpace(approverLevel) ? (object)DBNull.Value : approverLevel
                };
                var prUsername = new SqlParameter("@username", System.Data.SqlDbType.NVarChar, 255)
                {
                    Value = string.IsNullOrWhiteSpace(username) ? (object)DBNull.Value : username
                };
                var prRowVersion = new SqlParameter("@rowVersion", System.Data.SqlDbType.Timestamp, 8)
                {
                    Value = rowVersion ?? (object)DBNull.Value
                };
                var prResult = new SqlParameter()
                {
                    ParameterName = "@result",
                    SqlDbType = System.Data.SqlDbType.TinyInt,
                    Direction = System.Data.ParameterDirection.Output
                };

                string strSql = "EXEC " + StoreProcedures.ApproveRequest + " @requestId, @requestType, @approver, @approverLevel, @username, @rowVersion, @result OUTPUT";
                _db.Database.ExecuteSqlCommand(strSql, prRequestId, prRequestType, prApprover, prApproverLevel, prUsername, prRowVersion, prResult);

                return CastUtilities.GetNullableIntValue(prResult.Value);
            }
            catch (Exception ex)
            {
                Logger.Error(id, ex);
                throw ex;
            }
        }

        public int? Reject(int id, byte[] rowVersion, byte requestType, string approver, string approverLevel)
        {
            try
            {
                string username = Helper.GetCurrentUsername();

                var prRequestId = new SqlParameter("@requestId", id);
                var prRequestType = new SqlParameter("@requestType", requestType);
                var prApprover = new SqlParameter("@approver", System.Data.SqlDbType.NVarChar, 255)
                {
                    Value = string.IsNullOrWhiteSpace(approver) ? (object)DBNull.Value : approver
                };
                var prApproverLevel = new SqlParameter("@approverLevel", System.Data.SqlDbType.NVarChar, 255)
                {
                    Value = string.IsNullOrWhiteSpace(approverLevel) ? (object)DBNull.Value : approverLevel
                };
                var prUsername = new SqlParameter("@username", System.Data.SqlDbType.NVarChar, 255)
                {
                    Value = string.IsNullOrWhiteSpace(username) ? (object)DBNull.Value : username
                };
                var prRowVersion = new SqlParameter("@rowVersion", System.Data.SqlDbType.Timestamp, 8)
                {
                    Value = rowVersion ?? (object)DBNull.Value
                };
                var prResult = new SqlParameter()
                {
                    ParameterName = "@result",
                    SqlDbType = System.Data.SqlDbType.TinyInt,
                    Direction = System.Data.ParameterDirection.Output
                };

                string strSql = "EXEC " + StoreProcedures.RejectRequest + " @requestId, @requestType, @approver, @approverLevel, @username, @rowVersion, @result OUTPUT";
                _db.Database.ExecuteSqlCommand(strSql, prRequestId, prRequestType, prApprover, prApproverLevel, prUsername, prRowVersion, prResult);

                return CastUtilities.GetNullableIntValue(prResult.Value);
            }
            catch (Exception ex)
            {
                Logger.Error(id, ex);
                throw ex;
            }
        }
    }
}
