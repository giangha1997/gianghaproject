﻿CREATE TABLE [dbo].[Hazard] (
    [HazardID]            INT              IDENTITY (1, 1) NOT NULL,
    [GUID]                UNIQUEIDENTIFIER CONSTRAINT [DF_Hazard_GUID] DEFAULT (newid()) ROWGUIDCOL NOT NULL,
    [Type]                TINYINT          CONSTRAINT [DF_Hazard_Type] DEFAULT ((1)) NOT NULL,
    [Status]              TINYINT          CONSTRAINT [DF_Hazard_Status] DEFAULT ((0)) NOT NULL,
    [StatusPrev]          TINYINT          NULL,
    [Approver]            NVARCHAR (100)   NULL,
    [ApproverLevel]       NVARCHAR (100)   NULL,
    [ProjectID]           INT              NOT NULL,
    [Username]            NVARCHAR (100)   NULL,
    [Fullname]            NVARCHAR (255)   NULL,
    [IssuedDate]          DATETIME         NOT NULL,
    [ContractorID]        INT              NOT NULL,
    [TeamID]              INT              NOT NULL,
    [NumOfEmployees]      INT              NULL,
    [TowerFacID]          INT              NOT NULL,
    [ZoneID]              INT              NOT NULL,
    [LevelID]             INT              NOT NULL,
    [SOW]                 NVARCHAR (1000)  NULL,
    [HazardCategoryID]    INT              NULL,
    [HazardDetail]        NVARCHAR (1000)  NULL,
    [RiskLevelID]         INT              NULL,
    [RiskControl]         NVARCHAR (1000)  NULL,
    [FinishedPercent]     DECIMAL (9, 2)   NULL,
    [SafetyStatusID]      INT              NULL,
    [SafetyStatusPercent] DECIMAL (9, 2)   NULL,
    [Note]                NVARCHAR (1000)  NULL,
    [RowVersion]          ROWVERSION       NULL,
    [Created]             DATETIME         NULL,
    [CreatedBy]           NVARCHAR (50)    NULL,
    [Modified]            DATETIME         NULL,
    [ModifiedBy]          NVARCHAR (50)    NULL,
    [Deleted]             BIT              CONSTRAINT [DF_Hazard_Deleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Hazard] PRIMARY KEY CLUSTERED ([HazardID] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'in GeneralCategory', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Hazard', @level2type = N'COLUMN', @level2name = N'SafetyStatusID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'in GeneralCategory', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Hazard', @level2type = N'COLUMN', @level2name = N'RiskLevelID';

