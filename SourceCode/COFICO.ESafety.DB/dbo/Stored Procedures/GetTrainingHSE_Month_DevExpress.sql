﻿-- =============================================
-- Author:		HLinh
-- Create date: 2018/09/12
-- Description:	Báo cáo tháng - Thống kê Huấn luyện an toàn trong tháng của 1 dự án
-- =============================================
CREATE PROCEDURE [dbo].[GetTrainingHSE_Month_DevExpress] 
	@ProjectID INT,
	@Month TINYINT,
	@Year SMALLINT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @PreYear SMALLINT = @Year
	DECLARE @PreMonth TINYINT = @Month - 1

	IF @PreMonth <= 0
	BEGIN
		SET @PreMonth = 12
		SET @PreYear = @PreYear - 1
	END

	CREATE TABLE #Tmp
	(
		TrainingCategoryID INT,
		TrainingCode NVARCHAR(50),
		TrainingName NVARCHAR(255),
		ProjectID INT,
		ProjectCode NVARCHAR(50),
		ProjectName NVARCHAR(255),
		NumOfEmployees DECIMAL(9,2),
		IsThisWeek BIT,
		ThisWeek DECIMAL(9,2),
		LastWeek DECIMAL(9,2),
		Total DECIMAL(9,2)
	)

	SELECT TOP 0 * INTO #Remaining FROM #Tmp
	
	--get ProjectCode, ProjectName
	DECLARE @ProjectCode NVARCHAR(50)
	DECLARE @ProjectName NVARCHAR(255)

	SELECT @ProjectCode = ProjectCode, @ProjectName = ProjectName
	FROM Project
	WHERE ProjectID = @ProjectID

	--thống kê Huấn luyện an toàn
	INSERT #Tmp (TrainingCategoryID, ProjectID, ProjectCode, ProjectName, NumOfEmployees, IsThisWeek)
		SELECT d.TrainingCategoryID, t.ProjectID, @ProjectCode, @ProjectName, SUM(NumOfEmployees) AS NumOfEmployees, CAST(CASE WHEN t.[Month] = @Month THEN 1 ELSE 0 END AS BIT) AS IsThisWeek
		FROM Training t 
			INNER JOIN TrainingDetail d ON (t.TrainingID = d.TrainingID AND t.Deleted = 0 AND d.Deleted = 0)
			INNER JOIN TrainingCategory c ON (c.TrainingCategoryID = d.TrainingCategoryID AND c.Deleted = 0 AND c.IsActive = 1)
		WHERE t.ProjectID = @ProjectID AND (([Month] = @Month AND [Year] = @Year) OR ([Month] = @PreMonth AND [Year] = @PreYear))
		GROUP BY d.TrainingCategoryID, t.ProjectID, CAST(CASE WHEN t.[Month] = @Month THEN 1 ELSE 0 END AS BIT)


	--bổ sung những danh mục còn thiếu
	INSERT INTO #Remaining (TrainingCategoryID, ProjectID, ProjectCode, ProjectName, IsThisWeek)
		SELECT	TrainingCategoryID, @ProjectID, @ProjectCode, @ProjectName, 1
		FROM TrainingCategory c
		WHERE Deleted = 0 AND IsActive = 1
				AND NOT EXISTS (SELECT TOP 1 1 FROM #Tmp t1 WHERE t1.ProjectID = @ProjectID AND t1.TrainingCategoryID = c.TrainingCategoryID AND t1.IsThisWeek = 1)
	

	--merge data
	INSERT INTO #Tmp(TrainingCategoryID, ProjectID, ProjectCode, ProjectName, NumOfEmployees, IsThisWeek)
		SELECT TrainingCategoryID, ProjectID, ProjectCode, ProjectName, NumOfEmployees, IsThisWeek 
		FROM #Remaining

	--update ThisWeek
	UPDATE #Tmp
	SET ThisWeek = (SELECT SUM(ISNULL(NumOfEmployees, 0)) FROM #Tmp t WHERE t.TrainingCategoryID = #Tmp.TrainingCategoryID AND t.IsThisWeek = 1)

	UPDATE #Tmp SET ThisWeek = NULL WHERE ThisWeek = 0

	--update LastWeek
	UPDATE #Tmp
	SET LastWeek = (SELECT SUM(ISNULL(NumOfEmployees, 0)) FROM #Tmp t WHERE t.TrainingCategoryID = #Tmp.TrainingCategoryID AND t.IsThisWeek = 0)

	UPDATE #Tmp SET LastWeek = NULL WHERE LastWeek = 0

	--update Total
	UPDATE #Tmp
	SET Total = ISNULL(ThisWeek, 0) + ISNULL(LastWeek, 0)

	UPDATE #Tmp SET Total = NULL WHERE Total = 0


	--show data
	SELECT a1.TrainingCategoryID, c.TrainingCode, c.TrainingName, ProjectID, ProjectCode, ProjectName, NumOfEmployees, ThisWeek, LastWeek, Total, SortOrder
	FROM #Tmp a1
		INNER JOIN TrainingCategory c ON (c.TrainingCategoryID = a1.TrainingCategoryID)
	WHERE IsThisWeek = 1
	ORDER BY c.SortOrder, c.TrainingName, ProjectName

	DROP TABLE #Remaining
END