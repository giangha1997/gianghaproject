﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace COFICO.ESafety.DAL.DBModel
{
    [Table("TBMCategory")]
    public class TBMCategory
    {
        [Key]
        [Column("TBMCategoryID")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [StringLength(50)]
        public string TBMCode { get; set; }

        [StringLength(255)]
        public string TBMName { get; set; }

        public int? ParentID { get; set; }

        public bool IsParent { get; set; }

        [StringLength(50)]
        public string GroupCode { get; set; }

        public bool IsTotalColumn { get; set; }

        public bool IsActive { get; set; }

        public int? SortOrder { get; set; }

        public DateTime Created { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime? Modified { get; set; }

        [StringLength(50)]
        public string ModifiedBy { get; set; }

        public bool Deleted { get; set; }
    }

}
