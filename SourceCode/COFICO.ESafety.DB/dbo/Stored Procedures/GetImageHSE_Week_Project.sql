﻿-- =============================================
-- Author:		HLinh
-- Create date: 2018/07/06
-- Description:	Báo cáo tuần - HSE công ty
-- =============================================
CREATE PROCEDURE [dbo].[GetImageHSE_Week_Project] 
	@Month TINYINT,
	@Year SMALLINT,
	@Week TINYINT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Date SMALLDATETIME = dbo.GetFirstDateOfWeek(@Month, @Year, @Week)

	IF (@Date IS NULL) RETURN

	CREATE TABLE #Tmp
	(
		ImageID INT NOT NULL,
		ProjectID INT NOT NULL,
		ProjectCode NVARCHAR(50),
		ProjectName NVARCHAR(255),
		[Type] TINYINT,
		ImageCategoryID INT NOT NULL,
		CategoryCode NVARCHAR(50),
		CategoryName NVARCHAR(255),
		[Month] TINYINT,
		[Year] SMALLINT,
		[Week] TINYINT,
		[Date] SMALLDATETIME,
		ImageDetailID INT,
		[Filename] NVARCHAR(255),
		ImageTitle NVARCHAR(255),
		[Description] NVARCHAR(255),
		ShowInReport01 BIT,
		ShowInReport02 BIT,
		ShowInReport03 BIT,
		ShowInReport04 BIT,
		SortOrder INT,
		Id INT,
		Created DATETIME
	)

	INSERT #Tmp (ImageID, ProjectID, ProjectCode, ProjectName, [Type], ImageCategoryID, [Month], [Year], [Week], [Date], ImageDetailID, [Filename], ImageTitle, [Description], ShowInReport01, ShowInReport02, ShowInReport03, ShowInReport04, SortOrder, Id, Created)
		SELECT ISNULL(ImageID, 0) AS ImageID, p.ProjectID, p.ProjectCode, p.ProjectName, CAST(1 AS TINYINT) AS [Type], ISNULL(ImageCategoryID, 0) AS ImageCategoryID, @Month AS [Month], @Year AS [Year], @Week AS [Week], @Date AS [Date], ISNULL(ImageDetailID, 0) AS ImageDetailID, [Filename], ImageTitle, [Description], ISNULL(ShowInReport01, 0) AS ShowInReport01, ISNULL(ShowInReport02, 0) AS ShowInReport02, ISNULL(ShowInReport03, 0) AS ShowInReport03, ISNULL(ShowInReport04, 0) AS ShowInReport04, p.SortOrder, ROW_NUMBER() OVER (ORDER BY ISNULL(ImageID, 0), p.ProjectID), a1.Created
		FROM (
			SELECT i.ImageID, ProjectID, d.ImageCategoryID, @Month AS [Month], @Year AS [Year], @Week AS [Week], @Date AS [Date], ImageDetailID, d.[Filename], d.ImageTitle, d.[Description], d.ShowInReport01, d.ShowInReport02, d.ShowInReport03, d.ShowInReport04, d.Created
			FROM [Image] i
				INNER JOIN ImageDetail d ON (d.ImageID = i.ImageID AND d.Deleted = 0 AND i.Deleted = 0)
				INNER JOIN ImageCategory c ON (c.ImageCategoryID = d.ImageCategoryID AND c.Deleted = 0 AND c.IsActive = 1)
			WHERE [Month] = @Month AND [Year] = @Year AND [Week] = @Week AND ShowInReport02 = 1
		) a1
			RIGHT JOIN Project p ON (p.ProjectID = a1.ProjectID)
		WHERE p.Deleted = 0 AND (p.StatusID = 1 OR p.StatusID = 2)

	--only show 4 images of each project
	SELECT ImageID, ProjectID, ProjectCode, ProjectName, [Type], ImageCategoryID, CategoryCode, CategoryName, [Month], [Year], [Week], [Date], ImageDetailID, [Filename], ImageTitle, [Description], ShowInReport01, ShowInReport02, ShowInReport03, ShowInReport04, SortOrder, Created
	FROM #Tmp t
	WHERE Id IN (SELECT TOP 4 Id FROM #Tmp t1 WHERE t1.ImageID = t.ImageID AND t1.ProjectID = t.ProjectID)
	ORDER BY SortOrder, CategoryName, ProjectCode

	DROP TABLE #Tmp
END