﻿


CREATE View [dbo].[TrainingView] As
SELECT TrainingID, ProjectID, ProjectCode, ProjectName, TrainingCategoryID, TrainingCode, TrainingName, [Month], [Year], [Week], [Date], [2] AS D2, [3] AS D3, [4] AS D4, [5] AS D5, [6] AS D6, [7] AS D7, [8] AS D8, SortOrder
FROM (
	SELECT t.TrainingID, t.ProjectID, p.ProjectCode, p.ProjectName, d.TrainingCategoryID, TrainingCode, TrainingName, t.[Month], t.[Year], t.[Week], t.[Date], d.[DayOfWeek], ISNULL(d.NumOfEmployees, 0) AS NumOfEmployees, c.SortOrder
	FROM Training t 
		INNER JOIN TrainingDetail d ON (t.TrainingID =  d.TrainingID AND t.Deleted = 0 AND d.Deleted = 0)
		INNER JOIN TrainingCategory c ON (c.TrainingCategoryID = d.TrainingCategoryID AND c.Deleted = 0 AND c.IsActive = 1)
		LEFT JOIN Project p ON (t.ProjectID = p.ProjectID)
	WHERE p.Deleted = 0
) c
PIVOT
(
	SUM(NumOfEmployees)
	FOR [DayOfWeek] in ([2],[3],[4],[5],[6],[7],[8])
) p