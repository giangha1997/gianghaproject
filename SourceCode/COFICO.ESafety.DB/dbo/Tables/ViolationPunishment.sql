﻿CREATE TABLE [dbo].[ViolationPunishment] (
    [ViolationPunishmentID] INT             IDENTITY (1, 1) NOT NULL,
    [PunishmentCode]        NVARCHAR (50)   NULL,
    [PunishmentName]        NVARCHAR (1000) NULL,
    [ParentID]              INT             NULL,
    [IsParent]              BIT             CONSTRAINT [DF_ViolationPunishment_IsParent] DEFAULT ((0)) NOT NULL,
    [PenaltyFee]            DECIMAL (19, 4) CONSTRAINT [DF_ViolationPunishment_PenaltyFee] DEFAULT ((0)) NOT NULL,
    [Note]                  NVARCHAR (4000) NULL,
    [IsActive]              BIT             CONSTRAINT [DF_ViolationPunishment_IsActive] DEFAULT ((0)) NOT NULL,
    [SortOrder]             INT             NULL,
    [Created]               DATETIME        NOT NULL,
    [CreatedBy]             NVARCHAR (50)   NULL,
    [Modified]              DATETIME        NULL,
    [ModifiedBy]            NVARCHAR (50)   NULL,
    [Deleted]               BIT             CONSTRAINT [DF_ViolationPunishment_Deleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_ViolationPunishment] PRIMARY KEY CLUSTERED ([ViolationPunishmentID] ASC)
);

