﻿




CREATE View [dbo].[HazardView] As
SELECT h.HazardID, h.[GUID], h.[Type], h.[Status], s.Name AS StatusName,
	   h.ProjectID, p.ProjectCode, p.ProjectName, h.Username, h.Fullname, h.IssuedDate, 
	   h.ContractorID, c.ContractorCode, c.ContractorName,
	   h.TeamID, t.TeamName,
	   h.NumOfEmployees,
	   h.TowerFacID, f.Name AS TowerFac,
	   h.ZoneID, f.Name AS Zone,
	   h.LevelID, f.Name AS [Level],
	   h.SOW,
	   h.HazardCategoryID, hc.HazardName, h.HazardDetail,
	   h.RiskLevelID, g1.GeneralCategoryName AS RiskLevel, h.RiskControl, h.FinishedPercent,
	   h.SafetyStatusID, g2.GeneralCategoryName AS SafetyStatus, h.SafetyStatusPercent,
	   h.Approver, h.Note, h.[RowVersion]
FROM Hazard h
	INNER JOIN RequestStatus s ON s.Id = h.[Status]
	INNER JOIN Project p ON p.ProjectID = h.ProjectID
	INNER JOIN Contractor c ON c.ContractorID = h.ContractorID
	INNER JOIN Team t ON t.TeamID = h.TeamID
	INNER JOIN Project_TowerFacZone f ON f.RowID = h.TowerFacID
	INNER JOIN Project_TowerFacZone z ON z.RowID = h.ZoneID
	INNER JOIN Project_TowerFacZone l ON l.RowID = h.LevelID
	INNER JOIN HazardCategory hc ON hc.HazardCategoryID = h.HazardCategoryID
	INNER JOIN GeneralCategory g1 ON g1.GeneralCategoryID = h.RiskLevelID
	INNER JOIN GeneralCategory g2 ON g2.GeneralCategoryID = h.SafetyStatusID
WHERE h.Deleted = 0