﻿-- =============================================
-- Author:		HLinh
-- Create date: 2018/06/08
-- Description:	HSE/Report 01 - Thống kê Huấn luyện an toàn trong tuần theo ngày của 1 dự án
-- với dự án có tình trạng = Đang thực hiện & Đang bảo trì
-- =============================================
CREATE PROCEDURE [dbo].[GetTrainingHSE] 
	@ProjectID INT,
	@Month TINYINT,
	@Year SMALLINT,
	@Week TINYINT,
	@PrevWeek TINYINT
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @Date SMALLDATETIME = dbo.GetFirstDateOfWeek(@Month, @Year, @Week)
	DECLARE @PreYear SMALLINT = @Year
	DECLARE @PreMonth TINYINT = @Month

	IF @PrevWeek > @Week
		SET @PreMonth = @Month - 1

	IF @PreMonth <= 0
	BEGIN
		SET @PreMonth = 12
		SET @PreYear = @PreYear - 1
	END

	IF (@Date IS NULL) RETURN

	CREATE TABLE #Tmp
	(
		TrainingID INT NOT NULL,
		ProjectID INT NOT NULL,
		ProjectCode NVARCHAR(50),
		ProjectName NVARCHAR(255),
		TrainingCategoryID INT NOT NULL,
		TrainingCode NVARCHAR(50),
		TrainingName NVARCHAR(255),
		[Month] TINYINT NOT NULL,
		[Year] SMALLINT NOT NULL,
		[Week] TINYINT NOT NULL,
		[Date] SMALLDATETIME NULL,
		D2 INT NULL,
		D3 INT NULL,
		D4 INT NULL,
		D5 INT NULL,
		D6 INT NULL,
		D7 INT NULL,
		D8 INT NULL,
		Total INT NULL,
		PrevTotal INT NULL,
		Total1 INT NULL,
		SortOrder INT NULL,
	)

	SELECT TOP 0 * INTO #Remaining FROM #Tmp

	--thống kê Huấn luyện an toàn
	INSERT #Tmp (TrainingID, ProjectID, ProjectCode, ProjectName, TrainingCategoryID, TrainingCode, TrainingName, [Month], [Year], [Week], [Date], D2, D3, D4, D5, D6, D7, D8, Total, SortOrder)
		SELECT TrainingID, ProjectID, ProjectCode, ProjectName, TrainingCategoryID, TrainingCode, TrainingName, [Month], [Year], [Week], [Date], D2, D3, D4, D5, D6, D7, D8, 
			   ISNULL(D2, 0) + ISNULL(D3, 0) + ISNULL(D4, 0) + ISNULL(D5, 0) + ISNULL(D6, 0) + ISNULL(D7, 0) + ISNULL(D8, 0),
			   SortOrder
		FROM TrainingView
		WHERE ProjectID = @ProjectID AND [Month] = @Month AND [Year] = @Year AND [Week] = @Week
	
	--bổ sung những dự án còn thiếu và những danh mục còn thiếu
	INSERT #Remaining (TrainingID, ProjectID, ProjectCode, ProjectName, TrainingCategoryID, TrainingCode, TrainingName, [Month], [Year], [Week], [Date], SortOrder)
		SELECT 0, @ProjectID, ProjectCode, ProjectName, TrainingCategoryID, TrainingCode, TrainingName, @Month, @Year, @Week, @Date, c.SortOrder
		FROM TrainingCategory c
			LEFT JOIN Project p ON (p.ProjectID = @ProjectID)
		WHERE c.IsActive = 1 AND c.Deleted = 0 
			  AND p.Deleted = 0 AND (p.StatusID = 1 OR p.StatusID = 2)
			  AND NOT EXISTS (SELECT TOP 1 1 FROM #Tmp t WHERE c.TrainingCategoryID = t.TrainingCategoryID)

	--merge data
	INSERT #Tmp (TrainingID, ProjectID, ProjectCode, ProjectName, TrainingCategoryID, TrainingCode, TrainingName, [Month], [Year], [Week], [Date], D2, D3, D4, D5, D6, D7, D8, SortOrder)
		SELECT TrainingID, ProjectID, ProjectCode, ProjectName, TrainingCategoryID, TrainingCode, TrainingName, [Month], [Year], [Week], [Date], D2, D3, D4, D5, D6, D7, D8, SortOrder
			FROM #Remaining

	--update lũy kế tuần trước
	UPDATE #Tmp SET PrevTotal = (SELECT ISNULL(D2, 0) + ISNULL(D3, 0) + ISNULL(D4, 0) + ISNULL(D5, 0) + ISNULL(D6, 0) + ISNULL(D7, 0) + ISNULL(D8, 0) 
								 FROM TrainingView a
								 WHERE ProjectID = @ProjectID AND [Month] = @PreMonth AND [Year] = @PreYear AND [Week] = @PrevWeek AND a.TrainingCategoryID = #Tmp.TrainingCategoryID)
	
	--update lũy kế cộng dồn
	UPDATE #Tmp SET Total1 = ISNULL(Total, 0) + ISNULL(PrevTotal, 0)

	--
	UPDATE #Tmp SET D2 = NULL WHERE D2 = 0
	UPDATE #Tmp SET D3 = NULL WHERE D3 = 0
	UPDATE #Tmp SET D4 = NULL WHERE D4 = 0
	UPDATE #Tmp SET D5 = NULL WHERE D5 = 0
	UPDATE #Tmp SET D6 = NULL WHERE D6 = 0
	UPDATE #Tmp SET D7 = NULL WHERE D7 = 0
	UPDATE #Tmp SET D8 = NULL WHERE D8 = 0
	UPDATE #Tmp SET Total = NULL WHERE Total = 0
	UPDATE #Tmp SET PrevTotal = NULL WHERE PrevTotal = 0
	UPDATE #Tmp SET Total1 = NULL WHERE Total1 = 0

	--show data
	SELECT * FROM #Tmp ORDER BY SortOrder, TrainingName

	DROP TABLE #Tmp
	DROP TABLE #Remaining
END