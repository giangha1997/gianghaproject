﻿-- =============================================
-- Author:		HLinh
-- Create date: 06-07-2018
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[Func_ImageHSE]
(
	@ProjectID INT,
	@Month TINYINT,
	@Year SMALLINT,
	@Week TINYINT,
	@Date SMALLDATETIME,
	@Type TINYINT,
	@ImageCategoryID INT,
	@All BIT = 0
)
RETURNS 
@Res TABLE 
(
	ImageID INT,
	ProjectID INT,
	ProjectCode NVARCHAR(50),
	ProjectName NVARCHAR(255),
	[Type] TINYINT,
	ImageCategoryID INT, 
	CategoryCode NVARCHAR(50), 
	CategoryName NVARCHAR(255),
	[Month] TINYINT,
	[Year] SMALLINT,
	[Week] TINYINT,
	[Date] SMALLDATETIME,
	ImageDetailID INT,
	[Filename] NVARCHAR(255), 
	ImageTitle NVARCHAR(255), 
	[Description] NVARCHAR(255), 
	ShowInReport01 BIT, 
	ShowInReport02 BIT, 
	ShowInReport03 BIT,
	ShowInReport04 BIT,
	SortOrder INT,
	Created DATETIME
)
AS
BEGIN
	INSERT @Res
		SELECT ISNULL(ImageID, 0) AS ImageID, @ProjectID AS ProjectID, ProjectCode, ProjectName, @Type, ImageCategoryID, CategoryCode, CategoryName, @Month AS [Month], @Year AS [Year], @Week AS [Week], @Date AS [Date], ISNULL(ImageDetailID, 0) AS ImageDetailID, [Filename], ImageTitle, [Description], ShowInReport01, ShowInReport02, ShowInReport03, ShowInReport04, b1.SortOrder, b1.Created
		FROM (
			SELECT ImageID, @ProjectID AS ProjectID, c.ImageCategoryID, c.CategoryCode, c.CategoryName, @Month AS [Month], @Year AS [Year], @Week AS [Week], @Date AS [Date], ImageDetailID, [Filename], ImageTitle, [Description], CASE WHEN ShowInReport01 IS NULL THEN CAST(0 AS BIT) ELSE ShowInReport01 END AS ShowInReport01, CASE WHEN ShowInReport02 IS NULL THEN CAST(0 AS BIT) ELSE ShowInReport02 END AS ShowInReport02, CASE WHEN ShowInReport03 IS NULL THEN CAST(0 AS BIT) ELSE ShowInReport03 END AS ShowInReport03, CASE WHEN ShowInReport04 IS NULL THEN CAST(0 AS BIT) ELSE ShowInReport04 END AS ShowInReport04, c.SortOrder, a1.Created
			FROM (
				SELECT i.ImageID, @ProjectID AS ProjectID, d.ImageCategoryID, @Month AS [Month], @Year AS [Year], @Week AS [Week], @Date AS [Date], ImageDetailID, d.[Filename], d.ImageTitle, d.[Description], d.ShowInReport01, d.ShowInReport02, d.ShowInReport03, d.ShowInReport04, d.Created
				FROM [Image] i
					INNER JOIN ImageDetail d ON (d.ImageID = i.ImageID AND d.Deleted = 0 AND i.Deleted = 0 AND i.ProjectID = @ProjectID AND (@All = 1 OR (@All = 0 AND d.[Type] = @Type)))
				WHERE i.ProjectID = @ProjectID AND [Month] = @Month AND [Year] = @Year AND [Week] = @Week
			) AS a1
				RIGHT JOIN ImageCategory c ON (c.ImageCategoryID = a1.ImageCategoryID)
			WHERE (ISNULL(@ImageCategoryID, 0) = 0 OR c.ImageCategoryID = @ImageCategoryID) AND c.IsActive = 1 AND c.Deleted = 0
		) AS b1
			LEFT JOIN Project p ON (p.ProjectID = b1.ProjectID)
		WHERE p.Deleted = 0 AND (p.StatusID = 1 OR p.StatusID = 2)
		--ORDER BY b1.SortOrder, b1.CategoryCode
	
	RETURN 
END