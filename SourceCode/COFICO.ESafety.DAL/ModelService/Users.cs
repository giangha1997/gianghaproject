﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COFICO.ESafety.DAL.ServiceModels
{
    public class Users
    {
        public string LoginName { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }

        public string LoginNameFull { get; set; }
    }
}
