﻿-- =============================================
-- Author:		HLinh
-- Create date: 2018/07/20
-- Description:	Report 03 - Thống kê Sự cố, an toàn trong tháng/quý/năm theo dự án
-- @ShowAllProject = 0: chỉ lấy những dự án có tình trạng = Đang thực hiện & Đang bảo trì
-- @ShowAllProject = 1: lấy tất cả những dự án bao gồm tình trạng = Kết thúc
-- =============================================
CREATE PROCEDURE [dbo].[GetAccidentHSE_Month_Project_DevExpress] 
	@FromDate SMALLDATETIME,
	@ToDate SMALLDATETIME,
	@FromPrevDate SMALLDATETIME,
	@ToPrevDate SMALLDATETIME,
	@ShowAllProject BIT
AS
BEGIN
	SET NOCOUNT ON;

	CREATE TABLE #Tmp
	(
		AccidentCategoryID INT,
		AccidentCode NVARCHAR(50),
		AccidentName NVARCHAR(255),
		ProjectID INT,
		ProjectCode NVARCHAR(50),
		ProjectName NVARCHAR(255),
		NumOfAccidents DECIMAL(9,2),
		IsFooter BIT,
		IsThisWeek BIT,
		ThisWeek DECIMAL(9,2),
		LastWeek DECIMAL(9,2),
		Total DECIMAL(9,2)
	)

	SELECT TOP 0 * INTO #Remaining FROM #Tmp

	--thống kê Sự cố, an toàn
	INSERT #Tmp (AccidentCategoryID, ProjectID, ProjectCode, ProjectName, NumOfAccidents, IsFooter, IsThisWeek)
		SELECT d.AccidentCategoryID, t.ProjectID, ProjectCode, ProjectName, SUM(NumOfAccidents) AS NumOfAccidents, 0, CAST(CASE WHEN d.[Date] >= @FromDate AND d.[Date] <= @ToDate THEN 1 ELSE 0 END AS BIT) AS IsThisWeek
		FROM Accident t 
			INNER JOIN AccidentDetail d ON (t.AccidentID =  d.AccidentID AND t.Deleted = 0 AND d.Deleted = 0)
			INNER JOIN Project p ON (t.ProjectID = p.ProjectID AND p.Deleted = 0 AND ((@ShowAllProject = 0 AND (p.StatusID = 1 OR p.StatusID = 2)) OR @ShowAllProject = 1) )
			INNER JOIN AccidentCategory c ON (c.AccidentCategoryID = d.AccidentCategoryID AND c.Deleted = 0 AND c.IsActive = 1)
		WHERE ( d.[Date] BETWEEN @FromDate AND @ToDate OR d.[Date] BETWEEN @FromPrevDate AND @ToPrevDate )
		GROUP BY d.AccidentCategoryID, t.ProjectID, ProjectCode, ProjectName, CAST(CASE WHEN d.[Date] >= @FromDate AND d.[Date] <= @ToDate THEN 1 ELSE 0 END AS BIT)

	--get remainings
	--lấy tất cả các dự án chưa có dữ liệu
	--DECLARE @AccidentCategoryID INT
	--DECLARE @IsFooter BIT

	--DECLARE cursorA CURSOR FOR
	--	SELECT DISTINCT AccidentCategoryID, IsFooter FROM #Tmp
	--	UNION
	--	SELECT AccidentCategoryID, IsFooter FROM AccidentCategory WHERE Deleted = 0 AND IsActive = 1 --AND IsFooter = 0
	--OPEN cursorA
	--FETCH NEXT FROM cursorA INTO @AccidentCategoryID, @IsFooter
	--WHILE @@FETCH_STATUS = 0
	--BEGIN
	--	INSERT INTO #Remaining(AccidentCategoryID, ProjectID, ProjectCode, ProjectName, IsFooter, IsThisWeek)
	--		SELECT	@AccidentCategoryID, ProjectID, ProjectCode, ProjectName, @IsFooter, 1
	--		FROM Project c
	--		WHERE Deleted = 0 AND (StatusID = 1 OR StatusID = 2)
	--			  AND NOT EXISTS (SELECT TOP 1 1 FROM #Tmp t1 WHERE c.ProjectID = t1.ProjectID AND t1.AccidentCategoryID = @AccidentCategoryID AND t1.IsThisWeek = 1)

	--	FETCH NEXT FROM cursorA INTO @AccidentCategoryID, @IsFooter
	--END

	--CLOSE cursorA
	--DEALLOCATE cursorA
	--lấy tất cả các dự án chưa có dữ liệu - end


	--bổ sung những dự án còn thiếu và những danh mục còn thiếu
	DECLARE @ProjectID INT
	DECLARE @ProjectCode NVARCHAR(50)
	DECLARE @ProjectName NVARCHAR(255)

	DECLARE cursorA CURSOR FOR
		SELECT DISTINCT ProjectID, ProjectCode, ProjectName FROM #Tmp
	OPEN cursorA
	FETCH NEXT FROM cursorA INTO @ProjectID, @projectCode, @ProjectName
	WHILE @@FETCH_STATUS = 0
	BEGIN
		INSERT INTO #Remaining (AccidentCategoryID, ProjectID, ProjectCode, ProjectName, IsFooter, IsThisWeek)
			SELECT	AccidentCategoryID, @ProjectID, @ProjectCode, @ProjectName, IsFooter, 1
			FROM AccidentCategory c
			WHERE Deleted = 0 AND IsActive = 1
				  AND NOT EXISTS (SELECT TOP 1 1 FROM #Tmp t1 WHERE t1.ProjectID = @ProjectID AND t1.AccidentCategoryID = c.AccidentCategoryID AND t1.IsThisWeek = 1)

		FETCH NEXT FROM cursorA INTO @ProjectID, @projectCode, @ProjectName
	END
	
	CLOSE cursorA
	DEALLOCATE cursorA
	

	--merge data
	INSERT INTO #Tmp(AccidentCategoryID, ProjectID, ProjectCode, ProjectName, NumOfAccidents, IsFooter, IsThisWeek)
		SELECT AccidentCategoryID, ProjectID, ProjectCode, ProjectName, NumOfAccidents, IsFooter, IsThisWeek 
		FROM #Remaining

	--update ThisWeek
	UPDATE #Tmp
	SET ThisWeek = (SELECT SUM(ISNULL(NumOfAccidents, 0)) FROM #Tmp t WHERE t.AccidentCategoryID = #Tmp.AccidentCategoryID AND t.IsThisWeek = 1)

	UPDATE #Tmp SET ThisWeek = NULL WHERE ThisWeek = 0

	--update LastWeek
	UPDATE #Tmp
	SET LastWeek = (SELECT SUM(ISNULL(NumOfAccidents, 0)) FROM #Tmp t WHERE t.AccidentCategoryID = #Tmp.AccidentCategoryID AND t.IsThisWeek = 0)

	UPDATE #Tmp SET LastWeek = NULL WHERE LastWeek = 0

	--update Total
	UPDATE #Tmp
	SET Total = ISNULL(ThisWeek, 0) + ISNULL(LastWeek, 0)

	UPDATE #Tmp SET Total = NULL WHERE Total = 0


	--show data
	SELECT a1.AccidentCategoryID, c.AccidentCode, c.AccidentName, ProjectID, ProjectCode, ProjectName, NumOfAccidents, ThisWeek, LastWeek, Total, a1.IsFooter
	FROM #Tmp a1
		INNER JOIN AccidentCategory c ON (c.AccidentCategoryID = a1.AccidentCategoryID)
	WHERE IsThisWeek = 1
	ORDER BY a1.IsFooter, c.SortOrder, c.AccidentName, ProjectName

	DROP TABLE #Remaining
END