﻿using System.Collections.Generic;

namespace COFICO.ESafety.DAL.ServiceModels
{
    public class Item
    {
        public int ID { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        public List<Item> Items { get; set; }

        public Item()
        {
            Items = new List<Item>();
        }
    }
}
