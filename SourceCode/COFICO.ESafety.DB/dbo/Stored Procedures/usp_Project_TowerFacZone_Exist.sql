﻿-- =============================================
-- Author:		HLinh
-- Create date: 2019/03/20
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_Project_TowerFacZone_Exist]
	@ProjectID INT,
	@TowerFacZoneID INT
AS
BEGIN
	SET NOCOUNT ON;

	IF EXISTS(SELECT TOP 1 1 FROM Hazard WHERE ProjectID = @ProjectID AND (TowerFacID = @TowerFacZoneID OR ZoneID = @TowerFacZoneID OR LevelID = @TowerFacZoneID) AND Deleted = 0)
	BEGIN
		SELECT CAST(1 AS BIT)
		RETURN 1
	END

	IF EXISTS(SELECT TOP 1 1 FROM ViolationForm WHERE ProjectID = @ProjectID AND (TowerFacID = @TowerFacZoneID OR ZoneID = @TowerFacZoneID OR LevelID = @TowerFacZoneID) AND Deleted = 0)
	BEGIN
		SELECT CAST(1 AS BIT)
		RETURN 1
	END

	SELECT CAST(0 AS BIT)
	RETURN 0
END