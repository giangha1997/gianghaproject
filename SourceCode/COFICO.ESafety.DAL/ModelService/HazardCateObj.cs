﻿using System.Collections.Generic;

namespace COFICO.ESafety.DAL.ServiceModels
{
    public class HazardCateObj
    {
        //public List<object> Projects { get; set; }

        //public List<object> TowerFacs { get; set; }

        public List<object> HazardCategories { get; set; }

        public List<object> RiskLevels { get; set; }

        public List<object> SafetyStatus { get; set; }
    }
}
