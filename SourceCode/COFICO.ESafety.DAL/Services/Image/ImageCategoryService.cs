﻿using System;
using System.Collections.Generic;
using System.Linq;
using COFICO.ESafety.Base;
using COFICO.ESafety.DAL.DBModel;
using COFICO.ESafety.DAL.Services;

namespace COFICO.ESafety.DAL.Services
{
    public class ImageCategoryService : GenericService<ImageCategory>
    {
        public List<ImageCategory> GetImageCategories()
        {
            try
            {
                return _db.ImageCategories.Where(x => !x.Deleted).OrderBy(s => s.SortOrder).ThenBy(s => s.CategoryCode).ToList();
            }
            catch (Exception ex)
            {
                Logger.Error("GetImageCategories", ex);
                return null;
            }
        }

        //public int? Insert(ImageCategory p)
        //{
        //    if (p == null) return null;

        //    try
        //    {
        //        InsertObj(ref p);
        //        SaveChanges();
        //        return p.ID;
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error(p, ex);
        //        throw ex;
        //    }
        //}

        public int? Update(ImageCategory p)
        {
            if (p == null) return null;
            //check existance
            if (ExistCode(p)) return -1;// ton tai trong DB

            try
            {
                ImageCategory entity = null;
                if ((entity = ParseToCategory(p)) != null)
                {
                    SaveChanges();
                    return entity.ID;
                }
                return null;
            }
            catch (Exception ex)
            {
                Logger.Error(p, ex);
                throw ex;
            }
        }

        private bool ExistCode(ImageCategory p)
        {
            try
            {
                return _table.Any(s => ((s.ID == 0 && s.CategoryCode == p.CategoryCode)
                                        || (s.ID > 0 && s.CategoryCode == p.CategoryCode && s.ID != p.ID)) && !s.Deleted);
            }
            catch (Exception ex)
            {
                Logger.Error(p, ex);
                throw ex;
            }
        }

        public bool? Delete(int categoryID)
        {
            try
            {
                ////check existance in table ImageDetail
                //if (_db.ImageDetails.Any(s => s.ImageCategoryID == categoryID && !s.Deleted))
                //    return false;

                if (DeleteMark(categoryID))
                {
                    SaveChanges();
                    return true;
                }
                return null;
            }
            catch (Exception ex)
            {
                Logger.Error(categoryID, ex);
                throw ex;
            }
        }

        private ImageCategory ParseToCategory(ImageCategory dto)
        {
            if (dto == null) return null;

            ImageCategory entity = null;
            if (dto.ID == 0)
            {
                InsertObj(ref dto);
                entity = dto;
            }
            else
            {
                entity = _db.ImageCategories.SingleOrDefault(x => x.ID == dto.ID && !x.Deleted);
                if (entity == null) return null;

                entity.CategoryCode = dto.CategoryCode;
                entity.CategoryName = dto.CategoryName;
                entity.ParentID = dto.ParentID;
                entity.IsParent = dto.IsParent;
                entity.IsActive = dto.IsActive;
                entity.SortOrder = dto.SortOrder;

                UpdateObj(entity);
            }

            return entity;
        }
    }
}
