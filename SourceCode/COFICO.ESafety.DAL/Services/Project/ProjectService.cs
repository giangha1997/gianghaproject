﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using COFICO.ESafety.DAL.Services;
using COFICO.ESafety.DAL.DBModel;
using COFICO.ESafety.DAL.Constants;
using COFICO.ESafety.DAL.ModelService;
using COFICO.ESafety.Base;
using COFICO.ESafety.DAL.ServiceModels;

namespace COFICO.ESafety.DAL.Services
{
    public class ProjectService : GenericService<Project>
    {
        public List<ProjectView> GetProjectByUser()
        {
            try
            {
                string strSql = "EXEC " + StoreProcedures.GetProjectByUser + " @spUser";
                var prUser = new SqlParameter("@spUser", System.Data.SqlDbType.NVarChar, 100)
                {
                    Value = Helper.GetCurrentUsername()
                };
                return _db.Database.SqlQuery<ProjectView>(strSql, prUser).ToList();
            }
            catch (Exception ex)
            {
                Logger.Error("GetProjectByUser", ex);
                throw ex;
            }
        }

        public List<ProjectView> GetProjects()
        {
            try
            {
                return _db.ProjectViews.OrderBy(s => s.SortOrder).ThenBy(s => s.ProjectCode).ToList();
            }
            catch (Exception ex)
            {
                Logger.Error("GetProjects", ex);
                throw ex;
            }
        }

        public ProjectView GetProject(int projectId)
        {
            try
            {
                return _db.ProjectViews.FirstOrDefault(s => s.ID == projectId);
            }
            catch (Exception ex)
            {
                Logger.Error("GetProject " + projectId, ex);
                throw ex;
            }
        }

        public ProjectFullInfo GetProjectFullInfo(int projectId)
        {
            try
            {
                var project = _db.ProjectViews.FirstOrDefault(s => s.ID == projectId);
                if (project == null) return null;

                var res = new ProjectFullInfo
                {
                    ID = project.ID,
                    Code = project.ProjectCode,
                    Name = project.ProjectName
                };

                //get contractors
                List<ProjectContractorView> contractors = null;
                using (var svc = new ProjectContractorService())
                {
                    contractors = svc.GetContractors(projectId);
                }

                if (contractors != null && contractors.Count > 0)
                {
                    List<ProjectContractorTeamView> teams = null;
                    using (var svc = new ProjectContractorTeamService())
                    {
                        teams = svc.GetTeamByProject(projectId);
                    }

                    foreach (var c in contractors)
                    {
                        var item = new Item
                        {
                            ID = c.ContractorID,
                            Code = c.ContractorCode,
                            Name = c.ContractorName
                        };

                        if (teams != null && teams.Count > 0)
                        {
                            var obj = teams.Where(s => s.ProjectID == projectId && s.ContractorID == c.ContractorID)
                                           .OrderBy(s => s.SortOrder).ThenBy(s => s.TeamName)
                                           .Select(s => new Item { ID = s.TeamID, Code = s.TeamCode, Name = s.TeamName })
                                           .ToList();
                            item.Items.AddRange(obj);
                        }

                        res.Contractors.Add(item);
                    }
                }


                //get tower/factories
                List<Project_TowerFacZone> towers = null;
                using (var svc = new ProjectTowerFacZoneService())
                {
                    towers = svc.GetList(projectId);
                }

                if (towers != null && towers.Count > 0)
                {
                    var root = towers.Where(s => s.ParentID == null).OrderBy(s => s.SortOrder).ToList();

                    if (root != null && root.Count > 0)
                    {
                        foreach (var r in root)
                        {
                            var item = new Item { ID = r.ID, Code = r.Code, Name = r.Name };
                            FindChildren(towers, item, r.ID);
                            res.TowerFacs.Add(item);
                        }
                    }
                }

                return res;
            }
            catch (Exception ex)
            {
                Logger.Error("GetProjectFullInfo " + projectId, ex);
                throw ex;
            }
        }

        private void FindChildren(List<Project_TowerFacZone> data, Item item, int id)
        {
            var children = data.Where(s => s.ParentID == id).OrderBy(s => s.SortOrder).ToList();

            if (children != null && children.Count > 0)
            {
                foreach(var c in children)
                {
                    var child = new Item { ID = c.ID, Code = c.Code, Name = c.Name };
                    FindChildren(data, child, c.ID);
                    item.Items.Add(child);
                }
            }
        }

        //public List<ProjectContractorUser> GetProjectContractorUser()
        //{
        //    try
        //    {
        //        var p = _table.Where(s => !s.Deleted).OrderBy(s => s.SortOrder).ThenBy(s => s.ProjectCode).ToList().ConvertAll(
        //                    s => new ProjectContractorUser
        //                    {
        //                        ProjectID = s.ProjectID,
        //                        ProjectCode = s.ProjectCode,
        //                        ProjectName = s.ProjectName,
        //                        Note = s.Note,
        //                        StatusID = s.StatusID,
        //                        SortOrder = s.SortOrder,
        //                        Created = s.Created,
        //                        CreatedBy = s.CreatedBy,
        //                        Modified = s.Modified,
        //                        ModifiedBy = s.ModifiedBy,
        //                        Deleted = s.Deleted
        //                    });

        //        if (p == null) return null;

        //        p.proje

        //        return p;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        private bool ExistCode(int projectID, string projectCode)
        {
            try
            {
                return _table.Any(s => ((projectID == 0 && s.ProjectCode == projectCode) ||
                                            (projectID != 0 && s.ID != projectID && s.ProjectCode == projectCode)) &&
                                        !s.Deleted);
            }
            catch (Exception ex)
            {
                Logger.Error(new { projectID, projectCode }, ex);
                throw ex;
            }
        }

        public int? Update(ProjectView p)
        {
            if (p == null) return null;

            if (ExistCode(p.ID, p.ProjectCode)) return -1;

            try
            {
                Project entity = null;
                if ((entity = ParseToEntity(p)) != null)
                {
                    SaveChanges();
                    int id = entity.ID;

                    //insert Project_User
                    var users = new List<Project_User>();
                    if (p.Users != null && p.Users.Count > 0)
                    {
                        p.Users.ForEach(s => { s.Type = (byte)ProjectUserType.User; s.ObjectID = id; });
                        users.AddRange(p.Users);
                    }
                    if (p.Engineers != null && p.Engineers.Count > 0)
                    {
                        p.Engineers.ForEach(s => { s.Type = (byte)ProjectUserType.Engineer; s.ObjectID = id; });
                        users.AddRange(p.Engineers);
                    }
                    if (p.SiteCommanders != null && p.SiteCommanders.Count > 0)
                    {
                        p.SiteCommanders.ForEach(s => { s.Type = (byte)ProjectUserType.SiteCommander; s.ObjectID = id; });
                        users.AddRange(p.SiteCommanders);
                    }

                    if (users != null && users.Count > 0)
                        using (var service = new ProjectUserService())
                        {
                            service.Update_ProjectUsers(users);
                        }

                    return id;
                }
                return null;
            }
            catch (Exception ex)
            {
                Logger.Error("Update Project: " + p.ID, ex);
                throw ex;
            }
        }

        //Project includes Project_Contractor, Project_User
        //public int? UpdatePCU(ProjectContractorUser p)
        //{
        //    if (p == null) return null;

        //    if (ExistCode(p.ProjectID, p.ProjectCode)) return -1;

        //    try
        //    {
        //        //convert to Project
        //        var proj = new Project
        //        {
        //            ID = p.ProjectID,
        //            ProjectCode = p.ProjectCode,
        //            ProjectName = p.ProjectName,
        //            Note = p.Note,
        //            StatusID = p.StatusID,
        //            SortOrder = p.SortOrder,
        //            Deleted = p.Deleted
        //        };

        //        Project entity = null;
        //        if ((entity = ParseToProject(proj)) != null)
        //        {
        //            SaveChanges();

        //            //insert Project_Contractor
        //            if (p.ProjectContractors != null)
        //                using (var service = new ProjectContractorService())
        //                {
        //                    p.ProjectContractors.ForEach(s => s.ProjectID = entity.ID);

        //                    service.InsertDelete(p.ProjectContractors);
        //                }

        //            //insert Project_User
        //            if (p.ProjectUsers != null)
        //                using (var service = new ProjectUserService())
        //                {
        //                    p.ProjectUsers.ForEach(s => s.ObjectID = entity.ID);

        //                    service.InsertDelete_ProjectUsers(p.ProjectUsers);
        //                }

        //            return entity.ID;
        //        }
        //        return null;
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error(p, ex);
        //        throw ex;
        //    }
        //}

        public bool? Delete(int projectID)
        {
            try
            {
                //check existance
                string strSql = "EXEC " + StoreProcedures.ExistProject + " @spProjectID";
                var prProjectID = new SqlParameter("@spProjectID", projectID);
                if (_db.Database.SqlQuery<bool>(strSql, prProjectID).Single())
                    return false;

                if (DeleteMark(projectID))
                {
                    SaveChanges();

                    //delete in Project_Contractor if exists
                    using (var service = new ProjectContractorService())
                    {
                        service.DeleteByProject(projectID);
                    }

                    //delete in Project_User if exists
                    using (var service = new ProjectUserService())
                    {
                        service.DeleteByProject(projectID);
                    }

                    return true;
                }
                return null;
            }
            catch (Exception ex)
            {
                Logger.Error(projectID, ex);
                throw ex;
            }
        }

        private Project ParseToEntity(ProjectView dtoView)
        {
            if (dtoView == null) return null;

            var dto = ParseToProject(dtoView);

            Project entity = null;
            if (dto.ID == 0)
            {
                InsertObj(ref dto);
                entity = dto;
            }
            else
            {
                entity = _table.SingleOrDefault(x => x.ID == dto.ID && !x.Deleted);
                if (entity == null) return null;

                entity.ProjectCode = dto.ProjectCode;
                entity.ProjectName = dto.ProjectName;
                entity.StatusID = dto.StatusID;
                entity.Note = dto.Note;
                entity.SortOrder = dto.SortOrder;

                UpdateObj(entity);
            }

            return entity;
        }

        private Project ParseToProject(ProjectView dto)
        {
            var p = new Project
            {
                ID = dto.ID,
                ProjectCode = dto.ProjectCode,
                ProjectName = dto.ProjectName,
                StatusID = dto.StatusID,
                Note = dto.Note,
                SortOrder = dto.SortOrder
            };

            return p;
        }
    }
}
