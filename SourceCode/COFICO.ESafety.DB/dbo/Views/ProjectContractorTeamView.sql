﻿




CREATE View [dbo].[ProjectContractorTeamView] As

SELECT pct.RowID, pc.ProjectID, pc.ContractorID, t.TeamID, t.TeamCode, t.TeamName, t.SortOrder
FROM Project_Contractor pc 
	INNER JOIN Project_Contractor_Team pct ON pct.ProjectContractorID = pc.RowID AND pc.Deleted = 0 AND pct.Deleted = 0
	INNER JOIN Team t ON (t.TeamID =  pct.TeamID AND t.Deleted = 0 AND t.IsActive = 1)