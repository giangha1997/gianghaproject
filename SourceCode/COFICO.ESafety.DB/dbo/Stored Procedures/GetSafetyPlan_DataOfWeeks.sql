﻿-- =============================================
-- Author:		HLinh
-- Create date: 2018/07/23
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetSafetyPlan_DataOfWeeks] 
	@ProjectID INT,
	@Month TINYINT,
	@Year SMALLINT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT [Week]
	FROM SafetyPlan
	WHERE ProjectID = @ProjectID AND [Month] = @Month AND [Year] = @Year AND Deleted = 0
		  AND ( ISNULL(RTRIM(LTRIM(PlanForWeek)), '') <> '' OR ISNULL(RTRIM(LTRIM(PlanForNextWeek)), '') <> '')
END