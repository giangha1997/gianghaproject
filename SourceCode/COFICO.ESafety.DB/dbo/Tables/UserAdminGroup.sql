﻿CREATE TABLE [dbo].[UserAdminGroup] (
    [RowID]      INT           IDENTITY (1, 1) NOT NULL,
    [UserName]   NVARCHAR (50) NOT NULL,
    [GroupType]  SMALLINT      CONSTRAINT [DF_UserAdminGroup_GroupType] DEFAULT ((0)) NOT NULL,
    [Created]    DATETIME      NOT NULL,
    [CreatedBy]  NVARCHAR (50) NULL,
    [Modified]   DATETIME      NULL,
    [ModifiedBy] NVARCHAR (50) NULL,
    [Deleted]    BIT           CONSTRAINT [DF_UserAdminGroup_Deleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_UserAdminGroup] PRIMARY KEY CLUSTERED ([RowID] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'1: admin; 2: access all projects', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'UserAdminGroup', @level2type = N'COLUMN', @level2name = N'GroupType';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'UserAdminGroup', @level2type = N'COLUMN', @level2name = N'UserName';

