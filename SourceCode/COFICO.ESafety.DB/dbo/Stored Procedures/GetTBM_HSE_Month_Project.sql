﻿-- =============================================
-- Author:		HLinh
-- Create date: 2018/07/20
-- Description:	Report 03 - Thống kê TBM trong tháng/quý/năm theo dự án
-- @ShowAllProject = 0: chỉ lấy những dự án có tình trạng = Đang thực hiện & Đang bảo trì
-- @ShowAllProject = 1: lấy tất cả những dự án bao gồm tình trạng = Kết thúc
-- =============================================
CREATE PROCEDURE [dbo].[GetTBM_HSE_Month_Project] 
	@FromDate SMALLDATETIME,
	@ToDate SMALLDATETIME,
	@FromPrevDate SMALLDATETIME,
	@ToPrevDate SMALLDATETIME,
	@ShowAllProject BIT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Month TINYINT = MONTH(@FromDate)
	DECLARE @Year SMALLINT = YEAR(@FromDate)

	CREATE TABLE #TBMs
	(
		ProjectID INT,
		ProjectCode NVARCHAR(50),
		ProjectName NVARCHAR(255),
		CatGroupCode NVARCHAR(50),
		DataType VARCHAR(10),
		TBMCategoryID INT,
		TBMName NVARCHAR(255),
		[Month] TINYINT,
		[Year] SMALLINT,
		[Date] SMALLDATETIME,
		[Week] TINYINT,
		Total DECIMAL(9,2),
		PrevTotal DECIMAL(9,2),
		SortOrder_Proj INT,
		SortOrder_TBMCat INT,
		ThisWeek_C1 DECIMAL(9,2), --use in report
		ThisWeek_C2 DECIMAL(9,2),
		ThisWeek_C3 DECIMAL(9,2),
		LastWeek_C1 DECIMAL(9,2),
		LastWeek_C2 DECIMAL(9,2),
		LastWeek_C3 DECIMAL(9,2),
		Total_C1 DECIMAL(9,2),
		Total_C2 DECIMAL(9,2),
		Total_C3 DECIMAL(9,2)
	)

	SELECT TOP 0 * INTO #Remaining FROM #TBMs

	--thống kê TBM
	INSERT INTO #TBMs(ProjectID, TBMCategoryID, [Month], [Year], Total)
		SELECT	t.ProjectID, d.TBMCategoryID, @Month, @Year, SUM(ISNULL(NumOfEmployees, 0))
		FROM TBMDetail d
			INNER JOIN TBM t on (t.TBMID = d.TBMID and t.Deleted = 0 AND d.Deleted = 0)
			INNER JOIN Project p ON (t.ProjectID = p.ProjectID AND p.Deleted = 0 AND ((@ShowAllProject = 0 AND (p.StatusID = 1 OR p.StatusID = 2)) OR @ShowAllProject = 1) )
			INNER JOIN TBMCategory c ON (c.TBMCategoryID = d.TBMCategoryID AND c.Deleted = 0 AND c.IsActive = 1)
		WHERE d.[Date] BETWEEN @FromDate AND @ToDate
		GROUP BY t.ProjectID, d.TBMCategoryID
		
	--get remaining TBM
	--lấy tất cả các dự án chưa có dữ liệu
	--DECLARE @ProjectID INT

	--DECLARE cursorTBM CURSOR FOR
	--	SELECT DISTINCT ProjectID FROM #TBMs
	--	UNION
	--	SELECT ProjectID FROM Project WHERE Deleted = 0 AND (StatusID = 1 OR StatusID = 2)
	--OPEN cursorTBM
	--FETCH NEXT FROM cursorTBM INTO @ProjectID
	--WHILE @@FETCH_STATUS = 0
	--BEGIN
	--	INSERT INTO #Remaining(ProjectID, TBMCategoryID, [Month], [Year])
	--		SELECT	@ProjectID, c.TBMCategoryID, @Month, @Year
	--		FROM TBMCategory c
	--		WHERE c.Deleted = 0 AND c.IsActive = 1 AND c.IsTotalColumn = 0
	--			  AND NOT EXISTS (SELECT TOP 1 1 FROM #TBMs t1 WHERE c.TBMCategoryID = t1.TBMCategoryID AND t1.ProjectID = @ProjectID)

	--	FETCH NEXT FROM cursorTBM INTO @ProjectID
	--END

	--CLOSE cursorTBM
	--DEALLOCATE cursorTBM
	--lấy tất cả các dự án chưa có dữ liệu - end


	--bổ sung những dự án còn thiếu và những danh mục còn thiếu
	DECLARE @ProjectID INT

	DECLARE cursorA CURSOR FOR
		SELECT DISTINCT ProjectID FROM #TBMs
	OPEN cursorA
	FETCH NEXT FROM cursorA INTO @ProjectID
	WHILE @@FETCH_STATUS = 0
	BEGIN
		INSERT INTO #Remaining (ProjectID, TBMCategoryID, [Month], [Year])
			SELECT	@ProjectID, TBMCategoryID, @Month, @Year
			FROM TBMCategory c
			WHERE Deleted = 0 AND IsActive = 1 AND c.IsTotalColumn = 0
				  AND NOT EXISTS (SELECT TOP 1 1 FROM #TBMs t1 WHERE t1.ProjectID = @ProjectID AND t1.TBMCategoryID = c.TBMCategoryID)

		FETCH NEXT FROM cursorA INTO @ProjectID
	END
	
	CLOSE cursorA
	DEALLOCATE cursorA


	--merge data
	INSERT INTO #TBMs(ProjectID, TBMCategoryID, [Month], [Year], Total)
		SELECT	ProjectID, TBMCategoryID, [Month], [Year], Total
		FROM #Remaining
		
	--update lũy kế tuần trước
	UPDATE #TBMs SET PrevTotal = (SELECT SUM(ISNULL(NumOfEmployees, 0))
								  FROM TBMDetail d
									  INNER JOIN TBM t on (t.TBMID = d.TBMID and t.Deleted = 0 AND d.Deleted = 0)
								  WHERE t.ProjectID = #TBMs.ProjectID AND d.TBMCategoryID = #TBMs.TBMCategoryID AND d.[Date] BETWEEN @FromPrevDate AND @ToPrevDate)

	--add footer rows
	DECLARE @MaxSortOrder INT
	SELECT @MaxSortOrder = MAX(SortOrder) FROM Project WHERE Deleted = 0 AND (StatusID = 1 OR StatusID = 2)
	SET @MaxSortOrder = ISNULL(@MaxSortOrder, 0) + 1

	INSERT INTO #TBMs(ProjectID, ProjectCode, ProjectName, TBMCategoryID, [Month], [Year], SortOrder_Proj)
		SELECT -1, N'Tổng cộng', N'Tổng cộng', TBMCategoryID, @Month, @Year, @MaxSortOrder
		FROM TBMCategory
		WHERE Deleted = 0 AND IsActive = 1 AND IsTotalColumn = 0
	
	DELETE FROM #Remaining

	INSERT INTO #Remaining(ProjectID, ProjectCode, ProjectName, CatGroupCode, DataType, TBMCategoryID, TBMName, [Month], [Year], [Date], Total, PrevTotal, SortOrder_Proj, SortOrder_TBMCat)
		SELECT t.ProjectID, CASE WHEN t.ProjectID = -1 THEN t.ProjectCode ELSE p.ProjectCode END, CASE WHEN t.ProjectID = -1 THEN t.ProjectName ELSE p.ProjectName END, GroupCode, c.DataType, t.TBMCategoryID, c.TBMName, [Month], [Year], @FromDate, Total, PrevTotal, CASE WHEN t.ProjectID = -1 THEN t.SortOrder_Proj ELSE p.SortOrder END, c.SortOrder
		FROM #TBMs t
			LEFT JOIN Project p ON (p.ProjectID = t.ProjectID)
			LEFT JOIN TBMCategory c ON (c.TBMCategoryID = t.TBMCategoryID)
	
	--sum(total)
	UPDATE #Remaining 
	SET Total = (SELECT SUM(ISNULL(r.Total, 0)) FROM #Remaining r WHERE r.TBMCategoryID = #Remaining.TBMCategoryID AND r.ProjectID > 0),
		PrevTotal = (SELECT SUM(ISNULL(r.PrevTotal, 0)) FROM #Remaining r WHERE r.TBMCategoryID = #Remaining.TBMCategoryID AND r.ProjectID > 0)
	WHERE ProjectID = -1
	
	--update SET null if is 0
	UPDATE #Remaining SET Total = NULL WHERE Total = 0
	UPDATE #Remaining SET PrevTotal = NULL WHERE PrevTotal = 0
	
	UPDATE #Remaining 
	SET Total = (SELECT TOP 1 ISNULL(Total, 0) FROM #Remaining r WHERE r.ProjectID = #Remaining.ProjectID AND r.TBMCategoryID = 2) / (SELECT TOP 1 ISNULL(Total, 1) FROM #Remaining r WHERE r.ProjectID = #Remaining.ProjectID AND r.TBMCategoryID = 1) * 100,
		PrevTotal = (SELECT TOP 1 ISNULL(PrevTotal, 0) FROM #Remaining r WHERE r.ProjectID = #Remaining.ProjectID AND r.TBMCategoryID = 2) / (SELECT TOP 1 ISNULL(PrevTotal, 1) FROM #Remaining r WHERE r.ProjectID = #Remaining.ProjectID AND r.TBMCategoryID = 1) * 100
	WHERE TBMCategoryID = 3

	--
	UPDATE #Remaining 
	SET Total = (SELECT TOP 1 ISNULL(Total, 0) FROM #Remaining r WHERE r.ProjectID = #Remaining.ProjectID AND r.TBMCategoryID = 5) / (SELECT TOP 1 ISNULL(Total, 1) FROM #Remaining r WHERE r.ProjectID = #Remaining.ProjectID AND r.TBMCategoryID = 4) * 100,
		PrevTotal = (SELECT TOP 1 ISNULL(PrevTotal, 0) FROM #Remaining r WHERE r.ProjectID = #Remaining.ProjectID AND r.TBMCategoryID = 5) / (SELECT TOP 1 ISNULL(PrevTotal, 1) FROM #Remaining r WHERE r.ProjectID = #Remaining.ProjectID AND r.TBMCategoryID = 4) * 100
	WHERE TBMCategoryID = 6

	--update rate if TBMCategoryID = 3 or 6 has total = null or 0
	UPDATE #Remaining SET Total = 0 WHERE TBMCategoryID = 3 AND EXISTS (SELECT TOP 1 1 FROM #Remaining r WHERE r.ProjectID = #Remaining.ProjectID AND r.TBMCategoryID = 1 AND ISNULL(r.Total, 0) = 0)
	UPDATE #Remaining SET PrevTotal = 0 WHERE TBMCategoryID = 3 AND EXISTS (SELECT TOP 1 1 FROM #Remaining r WHERE r.ProjectID = #Remaining.ProjectID AND r.TBMCategoryID = 1 AND ISNULL(r.PrevTotal, 0) = 0)

	UPDATE #Remaining SET Total = 0 WHERE TBMCategoryID = 6 AND EXISTS (SELECT TOP 1 1 FROM #Remaining r WHERE r.ProjectID = #Remaining.ProjectID AND r.TBMCategoryID = 4 AND ISNULL(r.Total, 0) = 0)
	UPDATE #Remaining SET PrevTotal = 0 WHERE TBMCategoryID = 6 AND EXISTS (SELECT TOP 1 1 FROM #Remaining r WHERE r.ProjectID = #Remaining.ProjectID AND r.TBMCategoryID = 4 AND ISNULL(r.PrevTotal, 0) = 0)
	
	--update for report
	UPDATE #Remaining SET ThisWeek_C1 = ( SELECT SUM(ISNULL(Total, 0)) FROM #Remaining t WHERE (t.TBMCategoryID = 1 OR t.TBMCategoryID = 4) AND t.ProjectID = #Remaining.ProjectID)
	UPDATE #Remaining SET ThisWeek_C2 = ( SELECT SUM(ISNULL(Total, 0)) FROM #Remaining t WHERE (t.TBMCategoryID = 2 OR t.TBMCategoryID = 5) AND t.ProjectID = #Remaining.ProjectID)
	UPDATE #Remaining SET ThisWeek_C3 = CASE WHEN ISNULL(ThisWeek_C1, 0) <> 0 THEN ( ISNULL(ThisWeek_C2, 0) / ISNULL(ThisWeek_C1, 0) * 100 ) ELSE 0 END

	UPDATE #Remaining SET LastWeek_C1 = ( SELECT SUM(ISNULL(PrevTotal, 0)) FROM #Remaining t WHERE (t.TBMCategoryID = 1 OR t.TBMCategoryID = 4) AND t.ProjectID = #Remaining.ProjectID)
	UPDATE #Remaining SET LastWeek_C2 = ( SELECT SUM(ISNULL(PrevTotal, 0)) FROM #Remaining t WHERE (t.TBMCategoryID = 2 OR t.TBMCategoryID = 5) AND t.ProjectID = #Remaining.ProjectID)
	UPDATE #Remaining SET LastWeek_C3 = CASE WHEN ISNULL(LastWeek_C1, 0) <> 0 THEN ( ISNULL(LastWeek_C2, 0) / ISNULL(LastWeek_C1, 0) * 100 ) ELSE 0 END

	UPDATE #Remaining SET Total_C1 = ISNULL(ThisWeek_C1, 0) + ISNULL(LastWeek_C1, 0)
	UPDATE #Remaining SET Total_C2 = ISNULL(ThisWeek_C2, 0) + ISNULL(LastWeek_C2, 0)
	UPDATE #Remaining SET Total_C3 = CASE WHEN ISNULL(Total_C1, 0) <> 0 THEN ( ISNULL(Total_C2, 0) / ISNULL(Total_C1, 0) * 100 ) ELSE 0 END
	--update for report - end

	UPDATE #Remaining 
	SET [Week] = 0,
		Total = CASE WHEN ISNULL(Total, 0) = 0 THEN NULL ELSE Total END,
		PrevTotal = CASE WHEN ISNULL(PrevTotal, 0) = 0 THEN NULL ELSE PrevTotal END,
		ThisWeek_C1 = CASE WHEN ISNULL(ThisWeek_C1, 0) = 0 THEN NULL ELSE ThisWeek_C1 END,
		ThisWeek_C2 = CASE WHEN ISNULL(ThisWeek_C2, 0) = 0 THEN NULL ELSE ThisWeek_C2 END,
		ThisWeek_C3 = CASE WHEN ISNULL(ThisWeek_C3, 0) = 0 THEN NULL ELSE ThisWeek_C3 END,
		LastWeek_C1 = CASE WHEN ISNULL(LastWeek_C1, 0) = 0 THEN NULL ELSE LastWeek_C1 END,
		LastWeek_C2 = CASE WHEN ISNULL(LastWeek_C2, 0) = 0 THEN NULL ELSE LastWeek_C2 END,
		LastWeek_C3 = CASE WHEN ISNULL(LastWeek_C3, 0) = 0 THEN NULL ELSE LastWeek_C3 END,
		Total_C1 = CASE WHEN ISNULL(Total_C1, 0) = 0 THEN NULL ELSE Total_C1 END,
		Total_C2 = CASE WHEN ISNULL(Total_C2, 0) = 0 THEN NULL ELSE Total_C2 END,
		Total_C3 = CASE WHEN ISNULL(Total_C3, 0) = 0 THEN NULL ELSE Total_C3 END


	--show data
	SELECT * FROM #Remaining
	ORDER BY SortOrder_Proj, ProjectName, SortOrder_TBMCat, TBMName

	DROP TABLE #TBMs
	DROP TABLE #Remaining
END