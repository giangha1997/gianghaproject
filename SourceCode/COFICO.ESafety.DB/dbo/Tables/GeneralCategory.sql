﻿CREATE TABLE [dbo].[GeneralCategory] (
    [GeneralCategoryID]   INT            IDENTITY (1, 1) NOT NULL,
    [GeneralCategoryCode] NVARCHAR (50)  NULL,
    [GeneralCategoryName] NVARCHAR (255) NULL,
    [ParentID]            INT            NULL,
    [IsParent]            BIT            CONSTRAINT [DF_GeneralCategory_IsParent] DEFAULT ((0)) NOT NULL,
    [Type]                TINYINT        NULL,
    [IsActive]            BIT            CONSTRAINT [DF_GeneralCategory_IsActive] DEFAULT ((0)) NOT NULL,
    [SortOrder]           INT            NULL,
    [Created]             DATETIME       NULL,
    [CreatedBy]           NVARCHAR (50)  NULL,
    [Modified]            DATETIME       NULL,
    [ModifiedBy]          NVARCHAR (50)  NULL,
    [Deleted]             BIT            CONSTRAINT [DF_GeneralCategory_Deleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_GeneralCategory] PRIMARY KEY CLUSTERED ([GeneralCategoryID] ASC)
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'1: risk level; 2: tình trạng an toàn...', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'GeneralCategory', @level2type = N'COLUMN', @level2name = N'Type';

