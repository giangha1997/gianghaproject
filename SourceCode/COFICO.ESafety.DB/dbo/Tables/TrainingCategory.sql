﻿CREATE TABLE [dbo].[TrainingCategory] (
    [TrainingCategoryID] INT            IDENTITY (1, 1) NOT NULL,
    [TrainingCode]       NVARCHAR (50)  NULL,
    [TrainingName]       NVARCHAR (255) NULL,
    [ParentID]           INT            NULL,
    [IsParent]           BIT            CONSTRAINT [DF_TrainingCategory_IsParent] DEFAULT ((0)) NOT NULL,
    [IsActive]           BIT            CONSTRAINT [DF_TrainingCategory_IsActive] DEFAULT ((0)) NOT NULL,
    [SortOrder]          INT            NULL,
    [Created]            DATETIME       NOT NULL,
    [CreatedBy]          NVARCHAR (50)  NULL,
    [Modified]           DATETIME       NULL,
    [ModifiedBy]         NVARCHAR (50)  NULL,
    [Deleted]            BIT            CONSTRAINT [DF_TrainingCategory_Deleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_TrainingCategory] PRIMARY KEY CLUSTERED ([TrainingCategoryID] ASC)
);

